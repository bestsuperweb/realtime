require 'simplecov'
SimpleCov.start 'rails' unless ENV['NO_COVERAGE']

ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/rails'
require 'minitest/autorun'
require 'capybara/rails'
require 'capybara/minitest'
require 'selenium/webdriver'
require 'support/database_cleaner'
require 'support/share_db_connection'

class ActiveSupport::TestCase
  include Warden::Test::Helpers
  include Devise::Test::IntegrationHelpers
  include FactoryBot::Syntax::Methods

  ActiveRecord::Migration.check_pending!

  Minitest::Reporters.use!

  # register_spec_type self do |desc|
  #   desc < ActiveRecord::Base if desc.is_a? Class
  # end

end

class ActionDispatch::IntegrationTest
  include Warden::Test::Helpers
  include Devise::Test::IntegrationHelpers
  include FactoryBot::Syntax::Methods

  # Make the Capybara DSL available in all integration tests
  include Capybara::DSL
  # Make `assert_*` methods behave like Minitest assertions
  include Capybara::Minitest::Assertions

  Minitest::Reporters.use!

  Capybara.javascript_driver = :headless_chrome

  Capybara.register_driver :chrome do |app|
    Capybara::Selenium::Driver.new(app, browser: :chrome)
  end

  Capybara.register_driver(:headless_chrome) do |app|
    options = Selenium::WebDriver::Chrome::Options.new(args: %w[--headless --disable-gpu])    
    # Due to an unknown bug in chromedriver, user agent must be added separately. Don't mess with this.
    user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
    options.add_argument("--user-agent=#{user_agent}")
    options.add_argument("--window-size=1024,768")
    options.add_argument("--proxy=null")

    # increase timeout to prevent Net::Timeout crashes
    client = Selenium::WebDriver::Remote::Http::Default.new
    client.read_timeout = 600

    Capybara::Selenium::Driver.new(
      app,
      browser: :chrome,
      http_client: client,
      options: options
    )
  end

  Capybara.default_driver = :headless_chrome

  # Reset sessions and driver between tests
  # Use super wherever this method is redefined in your individual test classes
  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
end

