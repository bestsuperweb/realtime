# == Schema Information
#
# Table name: merchants
#
#  id              :integer          not null, primary key
#  name            :string           not null
#  public_url      :string           not null
#  alexa_rank      :integer
#  status          :string           default("private"), not null
#  commission      :float            default(100.0), not null
#  ppc_price       :integer          default(0)
#  cpm_price       :integer          default(0)
#  validation_days :integer          default(0)
#  created_at      :datetime
#  updated_at      :datetime
#  icon            :string
#  pending         :boolean          default(FALSE)
#  screenshot      :string

FactoryBot.define do
  factory :merchant do
    name 'name'
    public_url 'url'
  end
end