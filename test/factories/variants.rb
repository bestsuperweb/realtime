# == Schema Information
#
# Table name: variants
#
#  id                  :integer          not null, primary key
#  name                :string           not null
#  price               :integer          default(0), not null
#  product_id          :integer
#  merchant_id         :integer
#  api_id              :string
#  description         :text
#  merchant_url        :string
#  status              :string           default("active"), not null
#  created_at          :datetime
#  updated_at          :datetime
#  product_image       :string
#  pending             :boolean          default(FALSE)
#  color               :string
#  size                :string
#  slug                :string
#  share_count         :integer          default(0)
#  shipping_dimensions :text
#  shipping_category   :text
#  metadata            :text
#

FactoryBot.define do
  factory :variant do
    name 'name'
    price 123
    product
    merchant
    product_image 'image'
    description 'description'
  end
end