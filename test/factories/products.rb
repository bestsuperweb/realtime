# == Schema Information
#
# Table name: products
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime
#  updated_at :datetime
#  pending    :boolean          default(FALSE)
#
FactoryBot.define do
  factory :product do
    name 'name'
  end
end