# == Schema Information
#
# Table name: activities
#
#  id            :integer          not null, primary key
#  activity_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  actable_id    :integer
#  actable_type  :string
#  customer_id   :integer

FactoryBot.define do
  factory :activity do
    association :actable, factory: :basket
    activity_type "connection_request"
  end
end