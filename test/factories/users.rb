FactoryBot.define do
  factory :user do
    sequence :username do |n|
      "user#{n}"
    end

    sequence :email do |n|
      "user#{n}@example.com"
    end

    password "foobar"
    password_confirmation "foobar"
  end

  factory :customer do
    sequence :username do |n|
      "customer#{n}"
    end

    sequence :email do |n|
      "customer#{n}@example.com"
    end

    first_name "firstname"
    last_name "lastname"
    password "12345678"
  end

  factory :giftibly, class: Customer do
    sequence :username do |n|
      "giftibly#{n}"
    end

    sequence :email do |n|
      "giftibly#{n}@example.com"
    end

    first_name "firstname"
    last_name  "lastname"
    password "12345678"
    role 'admin'
  end

  factory :admin, class: Customer do
    sequence :username do |n|
      "admin#{n}"
    end

    sequence :email do |n|
      "admin#{n}@example.com"
    end

    first_name "firstname"
    last_name  "lastname"
    password "12345678"
    role 'admin'
  end
end