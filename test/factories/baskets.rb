# == Schema Information
#
# Table name: baskets
#
#  id          :integer          not null, primary key
#  name        :string           not null
#  customer_id :integer
#  max_gifts   :integer
#  created_at  :datetime
#  updated_at  :datetime
#  basket_type :string           default("lifestyle")
#  description :text
#  banner      :string
#  share_count :integer          default(0)
#  privacy     :string           default("public")
#

FactoryBot.define do
  factory :basket do
    name 'name'
    description 'description'
    customer
  end
end