# == Schema Information
#
# Table name: merchants
#
#  id              :integer          not null, primary key
#  name            :string           not null
#  public_url      :string           not null
#  alexa_rank      :integer
#  status          :string           default("private"), not null
#  commission      :float            default(100.0), not null
#  ppc_price       :integer          default(0)
#  cpm_price       :integer          default(0)
#  validation_days :integer          default(0)
#  created_at      :datetime
#  updated_at      :datetime
#  icon            :string
#  pending         :boolean          default(FALSE)
#  screenshot      :string
#

require 'test_helper'

class MerchantTest < ActiveSupport::TestCase
  setup do
    @removed = Merchant.create(   
      name:            "Ebay",
      public_url:      "www.ebay.com",
      alexa_rank:      2,  
      status:          "removed",
      commission:      80.00,
      ppc_price:       0,  
      cpm_price:       0,  
      validation_days: 0
    )
    @placeholder = Merchant.create(
      name:            "Amazon",
      public_url:      "www.amazon.com",
      alexa_rank:      1,  
      status:          "placeholder",
      commission:      8.5,
      ppc_price:       0,  
      cpm_price:       215,
      validation_days: 0
    )
    @private = Merchant.create(
      name:            "Walmart",
      public_url:      "www.walmart.com",
      alexa_rank:      3,  
      status:          "private",
      commission:      4.0,
      ppc_price:       0,  
      cpm_price:       0,  
      validation_days: 0
    )
    @public = Merchant.create(
      name:            "Etsy",
      public_url:      "www.etsy.com",
      alexa_rank:      4,  
      commission:      3.4,
      status:          "public",
      ppc_price:       0,  
      cpm_price:       0,  
      validation_days: 0
    )
  end

  teardown do

  end

  test "successfully revelas that a merchant is removed" do
    assert @removed.removed? == true
  end

  test "successfully reveals that a merchant is not removed" do
    assert @public.removed? == false
  end

  test "successfully reveals that a merchant is placeholder" do
    assert @placeholder.placeholder? == true
  end

  test "successfully reveals that a merchant is not a placeholder" do
    assert @public.placeholder? == false
  end

  test "successfully reveals that a merchant is public" do
    assert @public.public? == true
  end

  test "successfully reveals that a merchant is not public" do
    assert @private.public? == false
  end

  test "successfully reveals that a merchant is private" do
    assert @private.private? == true
  end

  test "successfully reveals that a merchant is not private" do
    assert @public.private? == false
  end

  test "successfully removes a merchant" do
    assert @public.removed? == false
    @public.remove
    assert @public.removed? == true
  end

  test "successfully makes a merchant a placeholder" do
    assert @public.placeholder? == false
    @public.make_placeholder
    assert @public.placeholder? == true
  end

  test "successfully makes a merchant public" do
    assert @private.public? == false
    @private.make_public
    assert @private.public? == true
  end

  test "successfully makes a merchant private" do
    assert @public.private? == false
    @public.make_private
    assert @public.private? == true
  end

end
