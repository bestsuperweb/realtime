# == Schema Information
#
# Table name: addresses
#
#  id                        :integer          not null, primary key
#  encrypted_address_1       :string           not null
#  encrypted_address_2       :string
#  city                      :string           not null
#  state                     :string           not null
#  postal_code               :string           not null
#  owner_id                  :integer
#  created_at                :datetime
#  updated_at                :datetime
#  address_type              :integer          default(0), not null
#  instructions              :string
#  default                   :boolean          default(FALSE)
#  encrypted_address_1_iv    :string
#  encrypted_address_2_iv    :string
#  encrypted_name            :string
#  encrypted_name_iv         :string
#  encrypted_phone_number    :string
#  encrypted_phone_number_iv :string
#  owner_type                :string           default("Customer")
#  status                    :integer          default(0)
#

require "test_helper"

#TODO
class AddressTest < ActiveSupport::TestCase
end
