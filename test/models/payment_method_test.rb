# == Schema Information
#
# Table name: payment_methods
#
#  id                    :integer          not null, primary key
#  uuid                  :string           not null
#  owner_id              :integer
#  address_id            :integer
#  stripe_id             :string           not null
#  default               :boolean          default(FALSE)
#  encrypted_metadata    :text
#  encrypted_metadata_iv :text
#  payment_type          :string
#  encrypted_name        :string
#  encrypted_name_iv     :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  owner_type            :string           default("Customer")
#  status                :integer          default(0)
#

require "test_helper"

# TODO
class PaymentMethodTest < ActiveSupport::TestCase
end
