# == Schema Information
#
# Table name: baskets
#
#  id          :integer          not null, primary key
#  name        :string           not null
#  customer_id :integer
#  max_gifts   :integer
#  created_at  :datetime
#  updated_at  :datetime
#  basket_type :string           default("lifestyle")
#  description :text
#  banner      :string
#  share_count :integer          default(0)
#  privacy     :string           default("public")
#

require 'test_helper'

# TODO
# class BasketTest < ActiveSupport::TestCase
 
#   setup do
#     @customer = Customer.create(
#       first_name: "John",
#       last_name: "Doe",
#       email: "john@doe.com",
#       password: "passw0rd",
#       username: "johndoe"
#     )

#     @product = Product.create(
#       name: 'jello'
#     )

#     @variant = Variant.create(
#       name: "Cool Product 5000",
#       price: 300,
#       status: "active",
#       product: @product
#     )

#     @basket = Basket.create(
#       name: "test_basket",
#       customer_id: @customer.id
#     )
#   end

#   teardown do
    
#   end

#   test "successfully add a variant to a wishlist" do
#    assert @basket.contains?(@variant) == false
#    @basket.add_to_basket(@variant)
#    assert @basket.contains?(@variant) == true 
#   end 

#   test "unsuccessfully add a variant to the basket from max_gifts limit" do
#     basket = Basket.create(
#       name: "broken basket",
#       max_gifts: 5,
#       customer_id: @customer.id
#     )

#     for i in 1..5 do
#       v = Variant.create(
#         name: "variant #{i}",
#         price: 100,
#         status: "active"
#       )   
#       basket.add_to_basket(v)
#     end

#     assert basket.contains?(@variant) == false
#     assert basket.gifts.length == 5
#     assert basket.add_to_basket(@variant) == false
#     assert basket.gifts.length == 5
#     assert basket.contains?(@variant) == false
#   end 

#   test "unsuccessfully add a variant to the basket because already in it" do
#     @basket.add_to_basket(@variant)
#     assert @basket.contains?(@variant) == true
#     assert @basket.add_to_basket(@variant) == false
#   end 

#   test "successfully informs that a variant is in the basket" do
#     @basket.add_to_basket(@variant)
#     assert @basket.contains?(@variant) == true
#   end 

#   test "successfully informs that a variant is not in the basket" do
#     assert @basket.contains?(@variant) == false
#   end 

#   test "successfully make a wishlist" do
#     assert @basket.max_gifts.nil?
#     @basket.make_wishlist
#     assert @basket.max_gifts.present?
#     assert @basket.max_gifts == 20
#   end

#   test "unsuccessfully make a wishlist from current basket length" do
#     for i in 1..21 do
#       v = Variant.create(
#         name: "variant #{i}",
#         price: 100,
#         status: "active"
#       )
#       @basket.add_to_basket(v)
#     end
#     assert @basket.gifts.length > 20
#     assert @basket.max_gifts.nil?
#     assert @basket.make_wishlist == false
#     assert @basket.max_gifts.nil?
#   end

# end
