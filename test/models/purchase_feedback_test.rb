require "test_helper"

class PurchaseFeedbackTest < ActiveSupport::TestCase
  def purchase_feedback
    @purchase_feedback ||= PurchaseFeedback.new
  end

  def test_valid
    assert purchase_feedback.valid?
  end
end
