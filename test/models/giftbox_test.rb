# == Schema Information
#
# Table name: giftboxes
#
#  id                         :integer          not null, primary key
#  address_id                 :integer
#  payment_method_id          :integer
#  owner_id                   :integer
#  owner_type                 :string
#  cart_id                    :integer
#  is_a_gift                  :boolean          default(FALSE)
#  giftee_id                  :integer
#  giftee_type                :string
#  giftee_shipping_address_id :integer
#  shipping_option            :integer          default(0)
#  status                     :integer          default(0)
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  cart_items_count           :integer          default(0)
#

require "test_helper"

class GiftboxTest < ActiveSupport::TestCase

end
