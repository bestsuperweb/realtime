# == Schema Information
#
# Table name: customers
#
#  id                     :integer          not null, primary key
#  first_name             :string           default("")
#  middle_initial         :string           default("")
#  last_name              :string           default("")
#  location               :string           default("")
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  role                   :string           default("customer"), not null
#  username               :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  website                :string
#  description            :text
#  gender                 :string
#  private                :boolean          default(FALSE)
#  birthday               :date
#  avatar                 :string
#  banner                 :string
#  employer               :string
#  job_start_date         :date
#  auth_token             :string
#  stripe_id              :string
#

require 'test_helper'

# TODO
# class CustomerTest < ActiveSupport::TestCase

#   setup do
#     @customer = Customer.create(
#       first_name: "John",
#       last_name: "Doe",
#       email: "john@doe.com",
#       password: "passw0rd",
#       username: "johndoe"
#     )

#     @admin = Customer.create(
#       first_name: "Admin",
#       middle_initial: "Q",
#       last_name: "User",
#       email: "admin@giftibly.com",
#       password: "passw0rd",
#       role: "admin",
#       username: "giftiblyguy"
#     )

#     @banned = Customer.create(
#       first_name: "Banned",
#       last_name: "User",
#       email: "banned@giftibly.com",
#       password: "passw0rd",
#       role: "banned",
#       username: "bannedguy"
#     )

#     @product = Product.create(
#       name: 'hello'
#     )

#     @variant = Variant.create(
#       name: "Cool Product 5000",
#       price: 300,
#       status: "active",
#       product: @product
#     )   

#     @basket = Basket.create(
#       name: "test_basket",
#       customer_id: @customer.id
#     )

#     @basket.add_to_basket(@variant)
#   end 

#   teardown do
    
#   end 

#   test "successfully informs that customer is an admin" do
#     assert @admin.admin? == true
#   end

#   test "successfully informs that customer is not an admin" do
#     assert @customer.admin? == false
#   end

#   test "successfully informs that customer has customer role" do
#     assert @customer.customer? == true
#   end

#   test "successfully informs that customer doesn't have customer role" do
#     assert @admin.customer? == false
#   end

#   test "successfully informs that customer is banned" do
#     assert @banned.banned? == true
#   end

#   test "successfully informs that customer is not banned" do
#     assert @customer.banned? == false
#   end

#   test "successfully makes a customer banned" do
#     assert @customer.banned? == false
#     @customer.ban
#     assert @customer.banned? == true
#   end

#   test "successfully makes an admin banned" do
#     assert @admin.banned? == false
#     @admin.ban
#     assert @admin.banned? == true
#   end

#   test "successfully makes a customer an admin" do
#     assert @customer.admin? == false
#     @customer.make_admin
#     assert @customer.admin? == true
#   end

#   test "unsuccessfully makes a banned user an admin" do
#     assert @banned.admin? == false
#     assert @banned.make_admin == false
#     assert @banned.admin? == false
#   end

#   test "successfully reveals a name with a middle initial" do
#     assert @admin.middle_initial == "Q"
#     assert @admin.name == "Admin Q User"
#     assert @admin.name != "Admin User"
#   end

#   test "successfully reveals a name without a middle initial" do
#     assert @customer.middle_initial.blank? == true
#     assert @customer.name == "John Doe"
#   end

#   test "successfully creates a copy basket" do
#     assert @admin.baskets.empty? == true
#     @admin.create_a_copy_basket(@basket)
#     assert @admin.baskets.empty? == false
#     assert @admin.baskets.first.name == "test_basket Copy"
#   end

#   test "unsuccessfully creats a copy basket if user already has one" do
#     assert @customer.baskets.empty? == false
#     assert @customer.create_a_copy_basket(@basket) == false
#   end

#   test "successfully follows a new friend" do
#     assert @customer.users_followed.empty? == true
#     assert @customer.follows?(@admin) == false
#     @customer.follow(@admin)
#     assert @customer.follows?(@admin) == true
#   end

#   test "unsuccessfully follows a friend already followed" do
#     @customer.follow(@admin)
#     assert @customer.follows?(@admin) == true
#     assert @customer.follow(@admin) == false
#     assert @customer.follows?(@admin) == true
#   end

#   test "successfully unfollows an old friend" do
#     @customer.follow(@admin)
#     assert @customer.follows?(@admin) == true
#     @customer.unfollow(@admin)
#     assert @customer.follows?(@admin) == false
#   end

#   test "unsuccessfully unfollows someone who is not a friend" do
#     assert @customer.follows?(@admin) == false
#     assert @customer.unfollow(@admin) == false
#   end

#   test "successfully reveals that a given user is followed" do
#     @customer.follow(@admin)
#     assert @customer.follows?(@admin) == true
#   end

#   test "successfully reveals that a given user is not followed" do
#     assert @customer.follows?(@admin) == false
#   end

#   test "successfully reveals that a given user is a follower" do
#     @customer.follow(@admin)
#     assert @admin.is_followed_by?(@customer) == true
#   end

#   test "successfully reveals that a given user is not a follower" do
#     assert @admin.is_followed_by?(@customer) == false
#   end
# end
