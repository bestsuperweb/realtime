require "test_helper"

describe MerchantCategory do
  let(:merchant_category) { MerchantCategory.new }

  it "must be valid" do
    value(merchant_category).must_be :valid?
  end
end
