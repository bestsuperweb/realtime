# == Schema Information
#
# Table name: cart_items
#
#  id                         :integer          not null, primary key
#  cart_id                    :integer
#  merchant_cart_id           :integer
#  gift_id                    :integer
#  variant_id                 :integer
#  quantity                   :integer          default(0)
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  price                      :decimal(, )
#  subtotal                   :decimal(, )
#  giftbox_id                 :integer
#  verified_at                :datetime
#  current_offer_id           :string
#  current_available_quantity :integer          default(0)
#  current_price              :decimal(, )      default(0.0)
#  current_sale_price         :decimal(, )      default(0.0)
#

require 'test_helper'

describe CartItem do

  let(:new_record){ CartItem.create }

  describe "relationships" do

    it "must belong to a cart" do
      assert new_record.errors.added? :cart, :blank
    end

    it "must belong to a variant" do
      assert new_record.errors.added? :variant, :blank
    end

    it "belongs to a merchant_cart" do
      assert new_record.merchant_cart.blank?
    end

    it "belongs to a gift" do
      assert new_record.gift.blank?
    end
  end

end
