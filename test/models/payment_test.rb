# == Schema Information
#
# Table name: payments
#
#  id                :integer          not null, primary key
#  state             :string           default("checkout"), not null
#  created_at        :datetime
#  updated_at        :datetime
#  uuid              :string           not null
#  payment_method_id :integer
#  owner_id          :integer
#  invoice_id        :integer
#  status            :integer          default(0)
#  outcome           :text
#  stripe_id         :string
#  idempotent_key    :string
#  authorized_amount :decimal(, )      default(0.0), not null
#  charged_amount    :decimal(, )      default(0.0), not null
#  owner_type        :string           default("Customer")
#

require "test_helper"

#TODO
class PaymentTest < ActiveSupport::TestCase
end
