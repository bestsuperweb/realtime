# == Schema Information
#
# Table name: gifts
#
#  id                :integer          not null, primary key
#  note              :text
#  variant_id        :integer
#  basket_id         :integer
#  created_at        :datetime
#  updated_at        :datetime
#  quantity_desired  :integer          default(1)
#  quantity_received :integer          default(0)
#  description       :text
#  slug              :string
#  share_count       :integer          default(0)
#  customer_id       :integer
#  rating            :integer          default(0)
#