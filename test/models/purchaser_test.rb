require "test_helper"

class PurchaserTest < ActiveSupport::TestCase
  def purchaser
    @purchaser ||= Purchaser.new
  end

  def test_valid
    assert purchaser.valid?
  end
end
