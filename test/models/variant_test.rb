# == Schema Information
#
# Table name: variants
#
#  id                  :integer          not null, primary key
#  name                :string           not null
#  price               :integer          default(0), not null
#  product_id          :integer
#  merchant_id         :integer
#  api_id              :string
#  description         :text
#  merchant_url        :string
#  status              :string           default("active"), not null
#  created_at          :datetime
#  updated_at          :datetime
#  product_image       :string
#  pending             :boolean          default(FALSE)
#  color               :string
#  size                :string
#  slug                :string
#  share_count         :integer          default(0)
#  shipping_dimensions :text
#  shipping_category   :text
#  metadata            :text
#

require 'test_helper'

class VariantTest < ActiveSupport::TestCase
  setup do
    product = Product.create(
      name: "active product"
    )
    product.tag_list.add("active, variant", parse: true)

    @active = Variant.create(
      name: "active variant",
      price: 100,
      product_id: product.id,
      status: "active"
    )

    @inactive = Variant.create(
      name: "inactive variant",
      price: 100,
      status: "inactive"
    )
  end

  teardown do

  end

  test "successfully reveals if active" do
    assert @active.active? == true
  end

  test "successfully says that inactive is not active" do
    assert @inactive.active? == false
  end

  test "successfully reveals if inactive" do
    assert @inactive.inactive? == true
  end

  test "successfully says that active is not inactive" do
    assert @active.inactive? == false
  end

  test "successfully makes active" do
    assert @inactive.active? == false
    @inactive.make_active
    assert @inactive.active? == true
  end

  test "successfully makes inactive" do
    assert @active.inactive? == false
    @active.make_inactive
    assert @active.inactive? == true
  end

  test "successfully lists all active variants" do
    assert Variant.all_active.include?(@active) == true
    assert Variant.all_active.include?(@inactive) == false
  end

  # TODO
  # test "successfully lists all inactive variants" do
  #   assert Variant.all_inactive.include?(@active) == false
  #   assert Variant.all_inactive.include?(@inactive) == true
  # end
  
end
