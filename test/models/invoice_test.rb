# == Schema Information
#
# Table name: invoices
#
#  id                      :integer          not null, primary key
#  uuid                    :string           not null
#  owner_id                :integer
#  payment_method_id       :integer
#  total_amount            :decimal(, )      default(0.0)
#  status                  :integer          default(0)
#  currency_code           :string
#  currency_symbol         :string
#  idempotent_key          :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  estimated_shipping_fees :decimal(, )      default(0.0), not null
#  shipping_fees           :decimal(, )      default(0.0), not null
#  estimated_taxes         :decimal(, )      default(0.0), not null
#  taxes                   :decimal(, )      default(0.0), not null
#  estimated_subtotal      :decimal(, )      default(0.0), not null
#  subtotal                :decimal(, )      default(0.0), not null
#  service_fee             :decimal(, )      default(0.0)
#  owner_type              :string           default("Customer")
#  giftbox_id              :integer
#

require "test_helper"

#TODO
class InvoiceTest < ActiveSupport::TestCase
end
