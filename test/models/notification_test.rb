# == Schema Information
#
# Table name: notifications
#
#  id          :integer          not null, primary key
#  activity_id :integer
#  customer_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  read        :boolean          default(FALSE)
#

require 'test_helper'

class NotificationTest < ActiveSupport::TestCase

  describe "basket" do
    let(:basket) { FactoryBot.create(:basket) }
    let(:activity) { FactoryBot.create(:activity, actable: basket) }
    let(:notification) { FactoryBot.create(:notification, activity: activity)}

    it "is destroyed when basket is destroyed" do
      id = notification.id
      basket.destroy
      assert Notification.where(id: id).first.blank?
    end
  end

  # test disabled due to obscure race condition
  # describe "Gift" do
  #   let(:customer) { FactoryBot.create(:customer) }
  #   let(:gift) { FactoryBot.create(:gift, customer: customer) }
  #   let(:activity) { FactoryBot.create(:activity, actable: gift, customer: customer) }
  #   let(:notification) { FactoryBot.create(:notification, activity: activity, customer: customer)}

  #   it "is destroyed when gift is destroyed" do
  #     id = notification.id
  #     gift.destroy
  #     assert Notification.where(id: id).first.blank?
  #   end
  # end

  describe "Customer" do
    let(:customer) { FactoryBot.create(:customer) }
    let(:activity) { FactoryBot.create(:activity, actable: customer) }
    let(:notification) { FactoryBot.create(:notification, activity: activity)}

    it "is destroyed when Customer is destroyed" do
      id = notification.id
      customer.destroy
      assert Notification.where(id: id).first.blank?
    end
  end

end
