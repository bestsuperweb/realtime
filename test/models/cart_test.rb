# == Schema Information
#
# Table name: carts
#
#  id                   :integer          not null, primary key
#  owner_id             :integer
#  flagged_as_current   :boolean          default(FALSE)
#  status               :integer          default(0), not null
#  cart_items_count     :integer          default(0)
#  flagged_for_purchase :boolean          default(FALSE)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  address_id           :integer
#  payment_method_id    :integer
#  owner_type           :string           default("Customer")
#
