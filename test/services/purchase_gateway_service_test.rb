require 'test_helper'
require_relative 'purchase_gateway_service/dummy_gifty_bot'

describe "PurchaseGatewayService" do

  before do
    @service = PurchaseGatewayService.build
    @address = Address.new
    @bot = DummyGiftyBot.new
  end

  describe "#call" do
    it "calls component services by name" do
      amazon = Minitest::Mock.new
      test = Minitest::Mock.new

      amazon.expect(:call, @bot, ['string', @address, 'string'])
      service = PurchaseGatewayService.new(amazon, test)
      assert_equal @bot, service.call('amazon', url: 'string', shipping_address: @address, shipping_option: 'string')
    end
  end

end