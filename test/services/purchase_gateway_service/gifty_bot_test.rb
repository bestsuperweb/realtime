require 'test_helper'

describe PurchaseGatewayService::Amazon::AmazonBotSettings do

  before do
    @filepath = "file://#{+ Rails.root.to_s}/test/fixtures/files/purchase_gateway_service"
    @service = PurchaseGatewayService::Amazon
  end

  describe "#initialize" do
    let(:bot) { PurchaseGatewayService::GiftyBot.build }

    it "inherits from Capybara::Session" do
      assert bot.driver.present?
    end

    it "has :success? == false" do
      assert bot.success? == false  
    end

    it "has :errors" do
      assert_kind_of Array, bot.errors
    end

    it ":errors are an Array" do
      assert_equal [], bot.errors
    end

    it "has :index" do
      assert bot.index.zero?
    end

  end

  describe "#page_undefined?" do
    let(:service) { PurchaseGatewayService::Amazon.build }
    let(:bot) {
      service.instance_values['bot'].start(
        service.class::VENDOR,
        "#{@filepath}/amazon/amazon-homepage.html", 
        "Hello, [name]", 
        service.class::SESSION_COOKIES_FILEPATH, 
        service.class::SESSION_COOKIES_LIST,
        service.class::PAGES
      )
    }

    it "checks for undefined titles" do
      bot.visit "#{@filepath}/amazon/amazon-homepage.html"
      assert bot.page_undefined?
      bot.stop
    end
  end

  describe "#start" do
    let(:service) { PurchaseGatewayService::Amazon.build }
    let(:bot) {
      service.instance_values['bot'].start(1, 2, 3, 4, 5, Hash.new)
    }

    it "assigns variables to bot" do
      assert_equal '1', bot.vendor
      assert_equal '4', bot.session_cookies_filepath
      assert_equal '5', bot.session_cookies_list
      assert_kind_of Hash, bot.pages
    end
    
  end

end