require 'test_helper'

describe PurchaseGatewayService::GiftyBot::GiftyBotSettings do

  before do
    @service = PurchaseGatewayService::GiftyBot
  end

  it "sets settings as constants" do
    assert_equal !!@service::ENABLE_LOG_FILES, @service::ENABLE_LOG_FILES
    assert_kind_of String, @service::FILEPATH  
    assert_kind_of Integer, @service::MIN_PAUSE_BETWEEN_PAGES_IN_SECONDS
    assert_kind_of String, @service::REFERRER_SITE_TO_SET_REQUEST_REFERRER
  end
  
end 