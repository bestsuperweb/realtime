class DummyGiftyBot < PurchaseGatewayService::GiftyBot
  attr_accessor :pages, :name

  def initialize(pages: pages, name: name)
    super
    @name = name
    @pages = pages
  end

  def smoke_test_passed?
    true
  end

  def page_undefined?
    false
  end

  def title
    @pages.keys.first
  end

  def log(options={})
    true
  end

  def rest
    true
  end

  def cleanup_cookies
    true
  end

  def stop
    true
  end

end