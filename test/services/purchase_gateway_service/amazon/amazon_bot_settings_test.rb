require 'test_helper'

describe PurchaseGatewayService::Amazon::AmazonBotSettings do

  before do
    @service = PurchaseGatewayService::Amazon
  end

  it "sets settings as constants" do
    assert_equal 'amazon', @service::VENDOR
    assert_kind_of Integer, @service::CAPYBARA_WAIT_IN_SECONDS
    assert_kind_of String, @service::AMZN_CREDIT_CARD_NUMBER
    # assert_kind_of String, @service::AMZN_USERNAME
    # assert_kind_of String, @service::AMZN_PASSWORD
    # assert_kind_of String, @service::GMAIL_USERNAME
    # assert_kind_of String, @service::GMAIL_PASSWORD
    # assert_kind_of String, @service::DEATHBYCAPTCHA_USERNAME
    # assert_kind_of String, @service::DEATHBYCAPTCHA_PASSWORD
    assert_kind_of Hash, @service::PURCHASE_PAGES
    assert_kind_of Hash, @service::UNCOMMON_PAGES
    assert_kind_of Hash, @service::PAGES
    assert_kind_of String, @service::SESSION_COOKIES_FILEPATH
    assert_kind_of Array, @service::SESSION_COOKIES_LIST
    assert_kind_of String, @service::SIGNED_IN_URL
    assert_kind_of String, @service::SIGNED_IN_TEXT
  end

  it "merges PURCHASE_PAGES and UNCOMMON_PAGES into PAGES" do
    assert_equal @service::PAGES, @service::PURCHASE_PAGES.merge(@service::UNCOMMON_PAGES)
  end

  it "assigns amazon purchase pages hash" do
    hash = {
      'Amazon.com: Please Confirm Your Action': 'merge_cart',
      'Amazon.com Shopping Cart': 'proceed_to_checkout',
      'Select a shipping address': 'enter_shipping_address',
      'Select Shipping Options - Amazon.com Checkout': 'choose_shipping_options',
      'Select a Payment Method - Amazon.com Checkout': 'choose_payment_method',
      'Place Your Order - Amazon.com Checkout': 'place_order'
    }
    assert_equal hash, @service::PURCHASE_PAGES
  end

  it "assigns amazon uncommon pages hash" do
    hash = {
      'Amazon Sign In': 'sign_in',
      'Please confirm your identity': 'confirm_identity',
      'Robot Check': 'robot_check',
      'Verify Address - Amazon.com Checkout': 'verify_address',
      'Select a Billing Address - Amazon.com Checkout': 'choose_billing_address',
      'Get UNLIMITED two-day shipping for a year FREE once you join Amazon Prime': 'why_pay_for_shipping',
      'Amazon Password Assistance': 'password_assistance',
      'Amazon Sign In Options': 'sign_in_options'
    }
    assert_equal hash, @service::UNCOMMON_PAGES
  end

end