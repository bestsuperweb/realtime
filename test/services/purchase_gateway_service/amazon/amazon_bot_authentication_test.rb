require 'test_helper'

describe PurchaseGatewayService::Amazon::AmazonBotAuthentication do

  before do
    @filepath = "file://#{+ Rails.root.to_s}/test/fixtures/files/purchase_gateway_service/amazon"
    @service = PurchaseGatewayService::Amazon
  end

  describe "Amazon authentication pages" do
    let(:service) {@service.build}
    let(:bot) {
      service.instance_values['bot'].start(
        service.class::VENDOR,
        service.class::SIGNED_IN_URL,
        service.class::SIGNED_IN_TEXT,
        service.class::SESSION_COOKIES_FILEPATH, 
        service.class::SESSION_COOKIES_LIST,
        service.class::PAGES
      )
    }

    it "#sign_in_normally(bot)" do
      bot.visit "#{@filepath}/sign-in.html"
      assert bot.first("input#ap_email").present?
      assert bot.first("input#ap_password").present?
      assert bot.first("input[name='rememberMe']").present?
      assert bot.first('input#signInSubmit').present?
      bot.stop
    end

    # TODO add deathbycaptcha
    # it "#solve_captcha(bot)" do
    # end

    # TODO Haven't captured a html image yet
    # it "#verify_code_from_email(bot)" do
    #   bot.visit "#{@filepath}/please-confirm-your-identity.html"
    #   verification_code = get_verification_code_from_email
    #   assert bot.first("input[name='code']").present?
    #   assert bot.first("input[type='submit']").present?
    #   bot.stop
    # end

    ## test gmail connection only.
    ## Disabled to prevent spamming gmail servers from tests
    # it "#get_verification_code_from_email" do
    #   gmail = Gmail.connect(ENV['GMAIL_USERNAME'], ENV['GMAIL_PASSWORD'])
    #   assert gmail.logged_in?
    # end

    it "#sign_in_options(bot)" do
      bot.visit "#{@filepath}/sign-in-options.html"
      assert bot.find("input[type='radio'][value='skip']").present?
      assert bot.find("input[type='submit']").present?
      bot.stop
    end

  end
end