require 'test_helper'

describe PurchaseGatewayService::Amazon::AmazonBotPurchase do

  before do
    @filepath = "file://#{+ Rails.root.to_s}/test/fixtures/files/purchase_gateway_service/amazon"
    @service = PurchaseGatewayService::Amazon.build
  end

  describe "Purchase flow" do
    let(:bot) {
      @service.instance_values['bot'].start(
        @service.class::VENDOR,
        @service.class::SIGNED_IN_URL,
        @service.class::SIGNED_IN_TEXT,
        @service.class::SESSION_COOKIES_FILEPATH, 
        @service.class::SESSION_COOKIES_LIST,
        @service.class::PAGES
      )
    }

    it "#merge_cart" do
      # bot.visit "#{@filepath}/merge-cart.html"
      # assert bot.first("input[value='add']").present?
      # bot.stop
    end

    it "#proceed_to_checkout" do
      bot.visit "#{@filepath}/proceed-to-checkout.html"
      assert bot.first("input[name='proceedToCheckout']").present?
      bot.stop
    end

    it "#enter_shipping_address(bot, shipping_address)" do
      bot.visit "#{@filepath}/select-a-shipping-address.html"

      assert bot.first("input#enterAddressFullName").present?
      assert bot.first("input#enterAddressAddressLine1").present?
      assert bot.first("input#enterAddressAddressLine2").present?
      assert bot.first("input#enterAddressCity").present?
      assert bot.first("input#enterAddressStateOrRegion").present?
      assert bot.first("input#enterAddressPostalCode").present?
      assert bot.first("select#enterAddressCountryCode").present?
      assert bot.first("input#enterAddressPhoneNumber").present?
    
      # #billing address is not the same as shipping address
      # bot.find("input[name='isBillingAddress'][value='0']").click

      assert bot.first("input[name='shipToThisAddress']").present?
      bot.stop
    end

    # it "#verify_address(bot)" do
    #   bot.visit "#{@filepath}/verify-address.html"

    #   assert bot.first("input#addr_0").present?
    #   assert bot.first("input[name='useSelectedAddress']").present?
    #   bot.stop
    # end

    it "#choose_shipping_options(bot)" do
      # bot.visit "#{@filepath}/select-shipping-options.html"
      # assert bot.first("input[type='submit']").present?
      # bot.stop
    end

    it "#choose_payment_method(bot)" do
      bot.visit "#{@filepath}/select-payment-method.html"

      assert bot.first("input#pm_0").present?
      # TODO test with cc number
      # #Amazon requires reentry of card number for new shipping addresses    
      # if bot.has_text?("Please re-enter your card number.")
      #   sleep(1)
      #   bot.log(message: 'Reenter Credit Car number.')
      #   bot.find("input#addCreditCardNumber").click.send_keys(AMZN_CREDIT_CARD_NUMBER)
      #   bot.find("input#confirm-card").click
      # end
      assert bot.first("input#continue-top").present?
      bot.stop
    end

    # #TODO test interstitial
    # it "#why_pay_for_shipping(bot)" do
    #   bot.visit "#{@filepath}/why-pay-for-shipping.html"
    #   assert bot.first("a#prime-pip-updp-decline").present?
    #   bot.stop
    # end

    # #TODO test billing address
    # it "#choose_billing_address(bot)" do
    #   bot.visit "#{@filepath}/choose-billing-address.html"
    #   assert bot.first("a.checkout-continue-link").present?
    #   bot.stop
    # end

    it "#place_order(bot)" do
      bot.visit "#{@filepath}/place-order.html"
      assert bot.first("input[name='placeYourOrder1']").present?
      bot.stop
    end
  end

end