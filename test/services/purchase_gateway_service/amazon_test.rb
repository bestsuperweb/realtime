require 'test_helper'
require_relative 'dummy_gifty_bot'

describe PurchaseGatewayService::Amazon do

  before do
    @bot_service_mock = Minitest::Mock.new
    @service = PurchaseGatewayService::Amazon
  end

  describe "required ENV variables" do
    # it "requires ENV variables" do
    #   assert ENV["AMZN_CREDIT_CARD_NUMBER"].present?
    # # end

    # it "requires AMZN_USERNAME" do
    #   assert ENV['AMZN_USERNAME'].present?
    # end

    # it "requires AMZN_PASSWORD" do
    #   assert ENV['AMZN_PASSWORD'].present?
    # end

    # it "requires GMAIL_USERNAME" do
    #   assert ENV['GMAIL_USERNAME'].present?
    # end

    # it "requires GMAIL_PASSWORD" do
    #   assert ENV['GMAIL_PASSWORD'].present?
    # end

    # it "requires DEATHBYCAPTCHA_USERNAME" do
    #   assert ENV['DEATHBYCAPTCHA_USERNAME'].present?
    # end

    # it "requires DEATHBYCAPTCHA_PASSWORD" do
    #   assert ENV['DEATHBYCAPTCHA_PASSWORD'].present?
    # end
  end

  describe "#call" do
    let(:merge_cart_page) {{ 'Amazon.com: Please Confirm Your Action': 'merge_cart' }}

    it "starts giftybot" do
      @service.stub_const :PAGES, merge_cart_page do
        bot = DummyGiftyBot.new(pages: merge_cart_page)
        @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])
        service = @service.new(@bot_service_mock)

        service.stub :merge_cart, bot do
          service.call('url', Hash.new, Hash.new)
          assert_mock @bot_service_mock
        end
      end
    end
  end

  describe "#run_bot" do
    describe "#amazon_bot_purchase" do
      let(:merge_cart_page) {{ 'Amazon.com: Please Confirm Your Action': 'merge_cart' }}
      let(:proceed_to_checkout_page) {{ 'Amazon.com Shopping Cart': 'proceed_to_checkout' }}
      let(:enter_shipping_address_page) {{ 'Select a shipping address': 'enter_shipping_address' }}
      let(:verify_address_page) {{ 'Verify Address - Amazon.com Checkout': 'verify_address' }}
      let(:choose_shipping_options_page) {{ 'Select Shipping Options - Amazon.com Checkout': 'choose_shipping_options' }}
      let(:choose_payment_method_page) {{ 'Select a Payment Method - Amazon.com Checkout': 'choose_payment_method' }}
      let(:choose_billing_address_page) {{ 'Select a Billing Address - Amazon.com Checkout': 'choose_billing_address' }}
      let(:place_order_page) {{ 'Place Your Order - Amazon.com Checkout': 'place_order' }}

      it "calls merge_cart" do
        @service.stub_const :PAGES, merge_cart_page do
          bot = DummyGiftyBot.new(pages: merge_cart_page, name: 'merge_cart')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          merge_cart_mock = Minitest::Mock.new
          merge_cart_mock.expect(:call, bot, [@bot])

          service.stub :merge_cart, merge_cart_mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'merge_cart', result.name
          end
        end
      end

      it "calls proceed_to_checkout" do
        @service.stub_const :PAGES, proceed_to_checkout_page do
          bot = DummyGiftyBot.new(pages: proceed_to_checkout_page, name: 'proceed_to_checkout')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          proceed_to_checkout_mock = Minitest::Mock.new
          proceed_to_checkout_mock.expect(:call, bot, [@bot])

          service.stub :proceed_to_checkout, proceed_to_checkout_mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'proceed_to_checkout', result.name
          end
        end
      end

      it "calls enter_shipping_address" do
        @service.stub_const :PAGES, enter_shipping_address_page do
          bot = DummyGiftyBot.new(pages: enter_shipping_address_page, name: 'enter_shipping_address')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          enter_shipping_address_mock = Minitest::Mock.new
          enter_shipping_address_mock.expect(:call, bot, [@bot])

          service.stub :enter_shipping_address, enter_shipping_address_mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'enter_shipping_address', result.name
          end
        end
      end

      it "calls verify_address" do
        @service.stub_const :PAGES, verify_address_page do
          bot = DummyGiftyBot.new(pages: verify_address_page, name: 'verify_address')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          verify_address_mock = Minitest::Mock.new
          verify_address_mock.expect(:call, bot, [@bot])

          service.stub :verify_address, verify_address_mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'verify_address', result.name
          end
        end
      end

      it "calls choose_shipping_options" do
        @service.stub_const :PAGES, choose_shipping_options_page do
          bot = DummyGiftyBot.new(pages: choose_shipping_options_page, name: 'choose_shipping_options')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          choose_shipping_options_mock = Minitest::Mock.new
          choose_shipping_options_mock.expect(:call, bot, [@bot])

          service.stub :choose_shipping_options, choose_shipping_options_mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'choose_shipping_options', result.name
          end
        end
      end

      it "calls choose_payment_method" do
        @service.stub_const :PAGES, choose_payment_method_page do
          bot = DummyGiftyBot.new(pages: choose_payment_method_page, name: 'choose_payment_method')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          choose_payment_method_mock = Minitest::Mock.new
          choose_payment_method_mock.expect(:call, bot, [@bot])

          service.stub :choose_payment_method, choose_payment_method_mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'choose_payment_method', result.name
          end
        end
      end

      it "calls choose_billing_address" do
        @service.stub_const :PAGES, choose_billing_address_page do
          bot = DummyGiftyBot.new(pages: choose_billing_address_page, name: 'choose_billing_address')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          choose_billing_address_mock = Minitest::Mock.new
          choose_billing_address_mock.expect(:call, bot, [@bot])

          service.stub :choose_billing_address, choose_billing_address_mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'choose_billing_address', result.name
          end
        end
      end

      it "calls place_order" do
        @service.stub_const :PAGES, place_order_page do
          bot = DummyGiftyBot.new(pages: place_order_page, name: 'place_order')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          place_order_mock = Minitest::Mock.new
          place_order_mock.expect(:call, bot, [@bot])

          service.stub :place_order, place_order_mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'place_order', result.name
          end
        end
      end
    end

    describe "#amazon_bot_authentication" do
      let(:sign_in_page) { {'Amazon Sign In': 'sign_in' } }
      let(:confirm_identity_page) { {'Please confirm your identity': 'confirm_identity' } }
      let(:robot_check_page) { {'Robot Check': 'robot_check' } }
      let(:why_pay_for_shipping_page) { {'Get UNLIMITED two-day shipping for a year FREE once you join Amazon Prime': 'why_pay_for_shipping' } }
      let(:password_assistance_page) { {'Amazon Password Assistance': 'password_assistance' } }
      let(:sign_in_options_page) { {'Amazon Sign In Options': 'sign_in_options' } }

      it "calls sign_in" do
        @service.stub_const :PAGES, sign_in_page do
          bot = DummyGiftyBot.new(pages: sign_in_page, name: 'sign_in')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          mock = Minitest::Mock.new
          mock.expect(:call, bot, [@bot])

          service.stub :sign_in, mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'sign_in', result.name
          end
        end
      end

      it "calls confirm_identity" do
        @service.stub_const :PAGES, confirm_identity_page do
          bot = DummyGiftyBot.new(pages: confirm_identity_page, name: 'confirm_identity')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          mock = Minitest::Mock.new
          mock.expect(:call, bot, [@bot])

          service.stub :confirm_identity, mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'confirm_identity', result.name
          end
        end
      end

      it "calls robot_check" do
        @service.stub_const :PAGES, robot_check_page do
          bot = DummyGiftyBot.new(pages: robot_check_page, name: 'robot_check')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          mock = Minitest::Mock.new
          mock.expect(:call, bot, [@bot])

          service.stub :robot_check, mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'robot_check', result.name
          end
        end
      end

      it "calls why_pay_for_shipping" do
        @service.stub_const :PAGES, why_pay_for_shipping_page do
          bot = DummyGiftyBot.new(pages: why_pay_for_shipping_page, name: 'why_pay_for_shipping')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          mock = Minitest::Mock.new
          mock.expect(:call, bot, [@bot])

          service.stub :why_pay_for_shipping, mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'why_pay_for_shipping', result.name
          end
        end
      end

      it "calls password_assistance" do
        @service.stub_const :PAGES, password_assistance_page do
          bot = DummyGiftyBot.new(pages: password_assistance_page, name: 'password_assistance')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          mock = Minitest::Mock.new
          mock.expect(:call, bot, [@bot])

          service.stub :password_assistance, mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'password_assistance', result.name
          end
        end
      end

      it "calls sign_in_options" do
        @service.stub_const :PAGES, sign_in_options_page do
          bot = DummyGiftyBot.new(pages: sign_in_options_page, name: 'sign_in_options')
          @bot_service_mock.expect(:start, bot, [String, String, String, String, Array, Hash])

          service = @service.new(@bot_service_mock)

          mock = Minitest::Mock.new
          mock.expect(:call, bot, [@bot])

          service.stub :sign_in_options, mock do
            result = service.call('url', Hash.new, Hash.new)
            assert_equal 'sign_in_options', result.name
          end
        end
      end
    end

  end

end