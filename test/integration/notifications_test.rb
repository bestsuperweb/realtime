require 'test_helper'

class NotificationsIntegrationTest < ActionDispatch::IntegrationTest
  describe "notification when share item", js: true do
    before(:each) do
      @customer = FactoryBot.create(:customer)
      @product = FactoryBot.create(:product)
      @variant = FactoryBot.create(:variant, product: @product)

      sign_in(@customer)
    end

    it "realtime notification" do
      visit products_feed_path

      assert page.has_no_css?('.notifications-badge')

      @activity = FactoryBot.create(:activity, activity_type: 'share', customer: @customer)
      @notification = Notification.create!(customer: @customer, activity: @activity)

      assert page.has_content?('Giftibly Community Feed')
      assert page.has_css?('.notifications-badge')
      find('.notifications-badge', text: '1')
    end
  end
end
