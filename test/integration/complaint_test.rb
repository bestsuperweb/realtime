require 'test_helper'

class ComplaintIntegrationTest < ActionDispatch::IntegrationTest

	describe "fedback url link", js: true do
    it "link is available" do
      visit feedback_path
      assert page.has_content?('Tell Us')
      fill_in 'complaint[name]',  with: 'Test Name'
      fill_in 'complaint[email]', with: 'testemail@gmail.com'
      click_on 'Choose a Topic'
      find('.dropdown-menu li:last-child').click
      fill_in 'complaint[body]',  with: 'Test body'
      find('.checkbox label').click
      click_on 'Send'
      assert page.has_content?('Thank you')
    end
  end

end
