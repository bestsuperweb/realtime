require 'test_helper'

class GiftUrlIntegrationTest < ActionDispatch::IntegrationTest

  describe "GiftUrl Integration Test", js: true do

    describe "when not signed in" do
      describe "wish url link" do
        it "is not available" do
          visit root_path
          assert !page.has_selector?('li.add_wish_icon')
        end
      end
    end

    describe "when signed in" do
      let(:customer) { FactoryBot.create(:customer)}

      before(:each) do
        sign_in(customer)
      end

      describe "wish url link" do
        it "is available on trending feed" do
          visit most_active_feed_path
          assert page.has_selector?('li.add_wish_icon')
        end

        it "is available on products feed" do
          visit products_feed_path
          assert page.has_selector?('li.add_wish_icon')
        end

        it "has a link to open a modal" do
          visit products_feed_path
          assert page.has_selector?('i.ion-plus-circled')
        end
      end

      describe "modal" do
        before(:each) do
          visit products_feed_path
        end

        it "is hidden" do
          assert page.has_selector?('input#gift_url', visible: false)
        end

        it "is shown when navbar link is clicked" do
          find('li.add_wish_icon a').click
          assert page.find('div#gift_url').visible?
        end

      end
    end
  end

end 