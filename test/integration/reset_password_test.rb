require 'test_helper'

class ResetPasswordIntegrationTest < ActionDispatch::IntegrationTest

  describe "Reset Password Integration Test", js: true do
    
    let(:customer) { FactoryBot.create(:customer) }

    describe "forgot password page" do

      before(:each) do
        ActionMailer::Base.deliveries = []
        visit new_customer_password_path
      end

      it "has a title" do
        assert page.has_content?('FORGOT YOUR PASSWORD')
      end

      describe "reset password email" do
        before(:each) do
          fill_in 'customer[email]', with: customer.email
          click_on 'Reset My Password'
        end

        it "sends an email" do
          assert_equal ActionMailer::Base.deliveries.count, 1
        end

        it "sends an email with subject" do
          email = ActionMailer::Base.deliveries.first
          assert_equal email.subject, "Reset password instructions"
        end

        it "sends an email to the customer's email" do 
          email = ActionMailer::Base.deliveries.first
          assert_equal email.to.first, customer.email
        end

        it "sends an email from info@giftibly.com" do
          email = ActionMailer::Base.deliveries.first
          assert_equal email.from.first, "info@giftibly.com"
        end
      end

      describe "reset password" do
        before(:each) do
          fill_in 'customer[email]', with: customer.email
          click_on 'Reset My Password'
        end

        it "creates a success flash message" do
          # assert page.has_content?('You will receive')
          assert page.body.include?('You will receive an email')
        end

        it "redirects to login path" do 
          page.assert_current_path(new_customer_session_path)
        end

      end
    end

    describe "new password page" do
      let(:token) {customer.send_reset_password_instructions}

      before(:each) do
        visit "password/edit?reset_password_token=#{token}"
      end

      it "has a title" do
        assert page.has_content?('UPDATE YOUR PASSWORD')
      end

      describe "create a new password" do

        before(:each) do
          fill_in 'customer[password]', with: 'abcd1234'
          fill_in 'customer[password_confirmation]', with: 'abcd1234'
          click_on 'Reset My Password'
        end

        it "creates a flash message" do
          # assert page.has_content? 'password has been changed'
          assert page.body.include?('password has been changed')
        end

        it "redirects to login path" do
          page.assert_current_path(new_customer_session_path)
        end

        it "does NOT sign in the user" do
          assert page.has_content?('Login')
        end
        
      end
    end

  end

end
