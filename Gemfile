source 'https://rubygems.org'
ruby '2.5.1'

gem 'rails', '5.2.0'
gem 'bootsnap', require: false
gem 'pg', '~> 0.21'
gem 'sass-rails', '~> 5.0'
gem 'bootstrap-sass'
gem 'uglifier', '~> 3.2.0'
gem 'coffee-rails', '~> 4.2.2'
gem 'thor', '0.19.1'
gem "font-ionicons-rails"
gem "paperclip", "~> 4.1" #todo remove
gem 'dropzonejs-rails'
gem 'momentjs-rails', '>= 2.9.0'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17.47'
gem 'geocomplete_rails'
gem 'bootstrap-switch-rails'
gem 'vacuum'
gem 'domainatrix'
gem 'recaptcha', require: 'recaptcha/rails'
gem 'mailgun-ruby'

gem 'jquery-rails'
gem 'turbolinks'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'acts-as-taggable-on', '~> 5.0.0'
gem 'social-share-button' #todo remove
gem 'gemoji'
gem 'obscenity'
gem 'whenever'
gem 'browser', '~> 1.1'
gem 'clipboard-rails'

gem 'remotipart', '~> 1.2'

gem 'gibbon', git: 'https://github.com/amro/gibbon.git'

gem 'devise'
gem 'pundit'
gem 'httparty'
gem 'rest-client'
gem 'city-state' #todo remove
gem 'aasm'
gem 'carrierwave', '~> 1.0'
gem 'fog'
gem 'mini_magick'
gem 'dotenv-rails'

gem 'omniauth-oauth2', '~> 1.5.0'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'omniauth-linkedin-oauth2'
gem 'omniauth-twitter'
gem 'koala'

gem 'sunspot_rails', :github => 'sunspot/sunspot'
gem 'sunspot_solr', '2.2.8' # optional pre-packaged Solr distribution for use in development

gem 'will_paginate'
gem 'friendly_id', '~> 5.1.0' # Note: You MUST use 5.0.0 or greater for Rails 4.0+

gem 'sitemap_generator'
gem 'redis', '~> 4.0', '>= 4.0.1'

#Purchase gateway dependencies
gem 'nokogiri'
gem 'capybara'
gem 'gmail'
gem 'deathbycaptcha', '~> 5.0.0'
gem 'minitest-rails'
gem 'rake'

# Payment gateway dependencies
gem 'stripe'
gem 'stripe-ruby-mock', '~> 2.5.0', :require => 'stripe_mock'
gem 'attr_encrypted'
gem 'simple_form'
gem 'shoryuken'
gem 'aws-sdk-sqs'

#admin dependencies
gem 'active_link_to'
gem 'chartkick'
gem 'groupdate'

#server
gem 'private_pub', git: 'https://github.com/ryleto/private_pub.git'
gem 'puma'
gem 'rack-cors'

# Performance tests
gem "rbtrace"

group :staging do
  gem 'pry-byebug'
  gem 'pry-rails'
end

#Purchase gateway dependencies
group :test do
  gem 'launchy'
  gem 'minitest-stub-const'
  gem 'minitest-reporters'
  gem 'simplecov', require: false
  gem "factory_bot_rails", "~> 4.0"
  gem 'm', '~> 1.5.0'
  gem 'rails-controller-testing'
  gem 'faker'
  gem 'minitest-spec-rails'
  gem "selenium-webdriver"
  gem 'chromedriver-helper'
  gem 'database_cleaner'
end

group :development do
  gem 'web-console', '~> 3.6.1'
  gem 'spring'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'meta_request'
  gem 'rails-erd'

  gem 'capistrano',         require: false
  gem 'capistrano-rvm',     require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano3-puma',   github: "seuros/capistrano-puma", require: false
  gem 'capistrano-shoryuken', github: 'joekhoobyar/capistrano-shoryuken', require: false
  gem 'rvm1-capistrano3',   require: false

  gem 'guard'
  gem 'guard-minitest'
  gem 'annotate'
end

group :development, :test do
  gem "teaspoon-jasmine"
  gem 'byebug'
  gem 'pry-byebug'
  gem 'pry-rails'
end


