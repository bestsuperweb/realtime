class AddDescriptionToShippingOption < ActiveRecord::Migration[4.2]
  def change
    add_column :shipping_options, :default, :boolean, default: false, index: true
    add_column :shipping_options, :description, :text
  end
end
