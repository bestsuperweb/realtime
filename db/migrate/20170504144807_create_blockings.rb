class CreateBlockings < ActiveRecord::Migration[4.2]
  def change
    create_table :blockings do |t|
      t.integer :customer_1_id
      t.integer :customer_2_id

      t.timestamps null: false
    end

    add_foreign_key :blockings, :customers, column: :customer_1_id
    add_foreign_key :blockings, :customers, column: :customer_2_id
  end
end
