class CreateLikes < ActiveRecord::Migration[4.2]
  def change
    create_table :likes do |t|
      t.belongs_to :product, index: true
      t.belongs_to :variant, index: true
      t.belongs_to :customer, index: true
      t.timestamps null: false
    end
  end
end
