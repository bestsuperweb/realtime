class DropContentFromActivitiesAndNotifications < ActiveRecord::Migration[4.2]
  def up
    remove_column :notifications, :content
    remove_column :activities, :content
  end

  def down
    add_column :activities, :content, :string
    add_column :notifications, :content, :string
  end
end
