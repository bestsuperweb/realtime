class ChangeMessagingTable < ActiveRecord::Migration[4.2]
  def change
    add_column :messages,:body, :text
    add_reference :messages, :conversation, index: true
    add_reference :messages,:customer, index: true

    remove_column :messages, :text

  end
end
