class AddUidToShippingOrder < ActiveRecord::Migration[4.2]
  def change
    add_column :shipping_orders, :uuid, :string, null: false
    add_index :shipping_orders, :uuid
  end
end
