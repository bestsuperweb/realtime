class CreateMerchantCarts < ActiveRecord::Migration[4.2]
  def change
    create_table :merchant_carts do |t|
      t.references :cart, index: true, foreign_key: true
      t.references :merchant, index: true, foreign_key: true
      t.integer :status, default: 0, null: false, index: true

      t.timestamps null: false
    end
  end
end
