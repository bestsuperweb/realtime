class AddDefaultToSettings < ActiveRecord::Migration[4.2]
  def change
    add_column :settings, :default_value, :string
  end
end
