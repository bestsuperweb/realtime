class CreateGuests < ActiveRecord::Migration[4.2]
  def change
    create_table :guests do |t|
      t.string :email, null: false, default: ""

      t.timestamps null: false
    end
  end
end
