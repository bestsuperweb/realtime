class AddGroupToSettings < ActiveRecord::Migration[4.2]
  def change
    add_column :settings, :group, :string
  end
end
