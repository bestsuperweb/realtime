class AddPendingColorAndSize < ActiveRecord::Migration[4.2]
  def change
    add_column :merchants, :pending, :boolean, default: false
    add_column :products, :pending, :boolean, default: false
    add_column :variants, :pending, :boolean, default: false
    add_column :variants, :color, :string
    add_column :variants, :size, :string
  end
end
