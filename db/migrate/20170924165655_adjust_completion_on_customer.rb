class AdjustCompletionOnCustomer < ActiveRecord::Migration[4.2]
  def change
    remove_column :customers, :completion
    add_column :customers, :walkthrough_active, :boolean, default: true
  end
end
