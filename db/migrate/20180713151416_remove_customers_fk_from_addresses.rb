class RemoveCustomersFkFromAddresses < ActiveRecord::Migration[5.2]
  def change
    if foreign_key_exists?(:addresses, :customers)
      remove_foreign_key :addresses, :customers
    end
  end
end
