class AddDefaultAndTypeToAddress < ActiveRecord::Migration[4.2]
  def change
    add_column :addresses, :default, :boolean, default: false, index: true
    change_column :addresses, :address_type, 'integer USING (CASE address_type WHEN \'shipping\' THEN \'0\'::integer ELSE \'1\'::integer END)', default: 0, null: false

    add_index :addresses, :customer_id
    add_index :addresses, :address_type
  end
end
