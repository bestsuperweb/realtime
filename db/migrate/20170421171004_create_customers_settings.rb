class CreateCustomersSettings < ActiveRecord::Migration[4.2]
  def change
    create_table :customers_settings do |t|
      t.integer :customer_id
      t.integer :setting_id
      t.string :selected_option

      t.timestamps null: false
    end

    add_foreign_key :customers_settings, :customers
    add_foreign_key :customers_settings, :settings
  end
end
