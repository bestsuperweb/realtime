class RemoveCustomersFkFromCarts < ActiveRecord::Migration[5.2]
  def change
    if foreign_key_exists?(:carts, :customers)
      remove_foreign_key :carts, :customers
    end
  end
end
