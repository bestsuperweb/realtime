class RemoveShippings < ActiveRecord::Migration[4.2]
  def change
    drop_table :shippings
  end
end
