class AddCartItemsCountToGiftbox < ActiveRecord::Migration[4.2]
  def change
    add_column :giftboxes, :cart_items_count, :integer, default: 0
    add_index :giftboxes, :cart_items_count
  end
end
