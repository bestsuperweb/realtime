class CreateDeals < ActiveRecord::Migration[4.2]
  def change
    create_table :deals do |t|
      t.string :name
      t.string :ad_image
      t.datetime :start_time
      t.datetime :end_time
      t.string :alt_text
      t.string :size, default: "small"
      t.string :google_analytics_code

      t.timestamps null: false
    end
  end
end
