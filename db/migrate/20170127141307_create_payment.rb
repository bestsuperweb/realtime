class CreatePayment < ActiveRecord::Migration[4.2]
  def change
    create_table :payments do |t|
      t.integer :total, null: false, default: 0
      t.string  :state, null: false, default: "checkout"
      t.integer :order_id, null: false
      t.integer :address_id, null: false

      t.timestamps
    end

    add_foreign_key :payments, :orders
  end
end
