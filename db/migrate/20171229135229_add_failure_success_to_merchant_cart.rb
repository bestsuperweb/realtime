class AddFailureSuccessToMerchantCart < ActiveRecord::Migration[4.2]
  def change
    add_column :merchant_carts, :purchase_gateway_failure_date, :datetime
    add_index :merchant_carts, :purchase_gateway_failure_date
    add_column :merchant_carts, :purchase_gateway_success_date, :datetime
    add_index :merchant_carts, :purchase_gateway_success_date
  end
end
