class AlterCustomerGroupLocationAddBanner < ActiveRecord::Migration[4.2]
  def change
    remove_column :customers, :state
    rename_column :customers, :city, :location
    add_column :customers, :banner, :string
  end
end
