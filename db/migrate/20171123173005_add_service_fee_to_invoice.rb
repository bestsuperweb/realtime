class AddServiceFeeToInvoice < ActiveRecord::Migration[4.2]
  def change
    add_column :invoices, :service_fee, :decimal, default: 0.0, index: true
  end
end
