class AddQuantityManagementToGift < ActiveRecord::Migration[4.2]
  def change
    add_column :gifts, :quantity_desired, :integer, default: 1
    add_column :gifts, :quantity_received, :integer, default: 0
  end
end
