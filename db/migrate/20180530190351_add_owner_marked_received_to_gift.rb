class AddOwnerMarkedReceivedToGift < ActiveRecord::Migration[4.2]
  def change
    add_column :gifts, :owner_marked_received, :boolean, default: false
  end
end
