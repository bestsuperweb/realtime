class CreateShippingOption < ActiveRecord::Migration[4.2]
  def change
    create_table :shipping_options do |t|
      t.string :postal_type, null: false
      t.integer :merchant_id, null: false

      t.timestamps
    end

    add_foreign_key :shipping_options, :merchants
  end
end
