class CreateCarts < ActiveRecord::Migration[4.2]
  def change
    create_table :carts do |t|
      t.references :customer, index: true, foreign_key: true
      t.boolean :flagged_as_current, default: false
      t.integer :status, default: 0, null: false
      t.integer :cart_items_count, default: 0
      t.boolean :flagged_for_purchase, default: false

      t.timestamps null: false
    end
    add_index :carts, :flagged_as_current
    add_index :carts, :status
    add_index :carts, :flagged_for_purchase
  end
end
