class AddCompletionToCustomer < ActiveRecord::Migration[4.2]
  def change
  	    add_column :customers, :completion, :integer, default: 10
  end
end
