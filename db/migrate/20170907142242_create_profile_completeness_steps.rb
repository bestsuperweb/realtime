class CreateProfileCompletenessSteps < ActiveRecord::Migration[4.2]
  def change
    create_table :profile_steps do |t|
      t.string :name
      t.integer :percentage_amount
      t.integer :step_number

      t.timestamps null: false
    end

    create_table :customers_profile_steps do |t|
      t.datetime :completed_at
      t.integer :customer_id
      t.integer :profile_step_id
    end

    add_index :customers_profile_steps, :customer_id
    add_index :customers_profile_steps, :profile_step_id
  end
end
