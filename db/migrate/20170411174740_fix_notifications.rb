class FixNotifications < ActiveRecord::Migration[4.2]
  def change
    remove_column :notifications, :text
    remove_column :notifications, :boolean

    add_column :notifications, :content, :text
    add_column :notifications, :read, :boolean, default: false
  end
end
