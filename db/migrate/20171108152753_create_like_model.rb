class CreateLikeModel < ActiveRecord::Migration[4.2]
  def change
    drop_table :likes

    create_table :likes do |t|
      t.string :likable_type
      t.integer :likable_id
      t.integer :customer_id

      t.timestamps null: false
    end

    add_foreign_key :likes, :customers
    add_index :likes, [:likable_type, :likable_id]
  end
end
