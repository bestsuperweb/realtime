class AddCustomerIdToGift < ActiveRecord::Migration[4.2]
  def change
    add_column :gifts, :customer_id, :integer

    add_foreign_key :gifts, :customers
  end
end
