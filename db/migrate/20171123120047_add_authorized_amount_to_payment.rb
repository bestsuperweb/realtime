class AddAuthorizedAmountToPayment < ActiveRecord::Migration[4.2]
  def change
    add_column :payments, :authorized_amount, :decimal, default: 0.0, null: false
  end
end
