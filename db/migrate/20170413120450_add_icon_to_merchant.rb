class AddIconToMerchant < ActiveRecord::Migration[4.2]
  def change
    add_column :merchants, :icon, :string
  end
end
