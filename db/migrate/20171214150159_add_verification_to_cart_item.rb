class AddVerificationToCartItem < ActiveRecord::Migration[4.2]
  def change
    add_column :cart_items, :verified_at, :datetime
    add_column :cart_items, :current_offer_id, :string
    add_column :cart_items, :current_available_quantity, :integer, default: 0
    add_column :cart_items, :current_price, :decimal, default: 0.0
    add_column :cart_items, :current_sale_price, :decimal, default: 0.0

    add_index :cart_items, :verified_at
    add_index :cart_items, :current_offer_id
    add_index :cart_items, :current_available_quantity
    add_index :cart_items, :current_price
    add_index :cart_items, :current_sale_price

  end
end
