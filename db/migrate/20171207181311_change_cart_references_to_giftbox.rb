class ChangeCartReferencesToGiftbox < ActiveRecord::Migration[4.2]
  def change
    remove_column :carts, :shipping_option, :integer

    remove_reference :invoices, :cart, index: true, foreign_key: true
    add_reference :invoices, :giftbox, index: true, foreign_key: true
    
    remove_reference :merchant_carts, :cart, index: true, foreign_key: true
    add_reference :merchant_carts, :giftbox, index: true, foreign_key: true

    add_reference :cart_items, :giftbox, index: true, foreign_key: true
  end
end
