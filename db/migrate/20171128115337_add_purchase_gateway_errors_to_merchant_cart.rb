class AddPurchaseGatewayErrorsToMerchantCart < ActiveRecord::Migration[4.2]
  def change
    add_column :merchant_carts, :purchase_gateway_errors, :text
  end
end
