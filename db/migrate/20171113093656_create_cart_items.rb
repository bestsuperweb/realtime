class CreateCartItems < ActiveRecord::Migration[4.2]
  def change
    create_table :cart_items do |t|
      t.references :cart, index: true, foreign_key: true
      t.references :merchant_cart, index: true, foreign_key: true
      t.references :gift, index: true, foreign_key: true
      t.references :variant, index: true, foreign_key: true

      t.integer :quantity, default: 0

      t.timestamps null: false
    end
  end
end
