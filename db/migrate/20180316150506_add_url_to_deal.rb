class AddUrlToDeal < ActiveRecord::Migration[4.2]
  def change
    add_column :deals, :url, :string
  end
end
