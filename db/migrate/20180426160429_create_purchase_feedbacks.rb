class CreatePurchaseFeedbacks < ActiveRecord::Migration[4.2]
  def change
    create_table :purchase_feedbacks do |t|
      t.integer :actable_id
      t.string :actable_type
      t.integer :customer_id
      t.integer :quantity
      t.boolean :purchased
      t.boolean :surprise
      t.string :confirmation_number
      t.string :feedback

      t.timestamps null: false
    end
  end
end
