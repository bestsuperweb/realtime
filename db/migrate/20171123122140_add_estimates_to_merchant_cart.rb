class AddEstimatesToMerchantCart < ActiveRecord::Migration[4.2]
  def change
    add_column :merchant_carts, :estimated_shipping_fee, :decimal, default: 0.0, null: false
    add_column :merchant_carts, :estimated_taxes, :decimal, default: 0.0, null: false
    add_column :merchant_carts, :estimated_subtotal, :decimal, default: 0.0, null: false
    add_column :merchant_carts, :actual_shipping_fee, :decimal, default: 0.0, null: false
    add_column :merchant_carts, :actual_taxes, :decimal, default: 0.0, null: false
    add_column :merchant_carts, :actual_subtotal, :decimal, default: 0.0, null: false
    add_column :merchant_carts, :actual_total, :decimal, default: 0.0, null: false
    add_column :merchant_carts, :cart_items_count, :integer
  end
end
