class ChangeCustomerToPolymorphicOwner < ActiveRecord::Migration[4.2]
  def change
    # remove_foreign_key :carts, :customer
    rename_column :carts, :customer_id, :owner_id
    add_column :carts, :owner_type, :string, after: :id, default: 'Customer'

    # remove_foreign_key :payment_methods, :customer
    rename_column :payment_methods, :customer_id, :owner_id
    add_column :payment_methods, :owner_type, :string, after: :id, default: 'Customer'

    # remove_foreign_key :payments, :customer
    rename_column :payments, :customer_id, :owner_id
    add_column :payments, :owner_type, :string, after: :id, default: 'Customer'

    # remove_foreign_key :invoices, :customer
    rename_column :invoices, :customer_id, :owner_id
    add_column :invoices, :owner_type, :string, after: :id, default: 'Customer'

    # remove_foreign_key :shipping_orders, :customer
    rename_column :shipping_orders, :customer_id, :owner_id
    add_column :shipping_orders, :owner_type, :string, after: :id, default: 'Customer'

    # remove_foreign_key :addresses, :customer
    rename_column :addresses, :customer_id, :owner_id
    add_column :addresses, :owner_type, :string, after: :id, default: 'Customer'

  end
end
