class CreateSettings < ActiveRecord::Migration[4.2]
  def change
    create_table :settings do |t|
      t.string :name
      t.text :description
      t.string :options, array: true, default: []
      t.string :family

      t.timestamps null: false
    end
  end
end
