class CreateCharities < ActiveRecord::Migration[4.2]
  def change
    create_table :charities do |t|
      t.string :name
      t.string :url
      t.boolean :cash_charity
      t.integer :basket_id

      t.timestamps null: false
    end

    add_foreign_key :charities, :baskets
  end
end
