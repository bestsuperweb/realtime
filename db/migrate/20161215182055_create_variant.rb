class CreateVariant < ActiveRecord::Migration[4.2]
  def change
    create_table :variants do |t|
      t.string  :name, null: false
      t.integer :price, default: 0, null: false
      t.integer :product_id
      t.integer :merchant_id
      t.integer :api_id
      t.text    :description
      t.string  :merchant_url
      t.string  :status, null: false, default: "active"

      t.timestamps
    end

    add_foreign_key :variants, :products
  end
end
