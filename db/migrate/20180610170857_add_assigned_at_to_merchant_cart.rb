class AddAssignedAtToMerchantCart < ActiveRecord::Migration[4.2]
  def change
    add_column :merchant_carts, :assigned_at, :datetime
    add_column :merchant_carts, :completed_at, :datetime
    add_reference :merchant_carts, :completable, polymorphic: true, index: true
  end
end
