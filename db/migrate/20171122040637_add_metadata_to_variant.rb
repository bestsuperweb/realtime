class AddMetadataToVariant < ActiveRecord::Migration[4.2]
  def change
    add_column :variants, :shipping_dimensions, :text
    add_column :variants, :shipping_category, :text
    add_column :variants, :metadata, :text
  end
end
