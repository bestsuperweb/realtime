class RemoveNullConstraints < ActiveRecord::Migration[4.2]
  def change
    change_column_null :addresses, :customer_id, true
  end
end
