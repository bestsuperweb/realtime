class AddColumnsToComplaint < ActiveRecord::Migration[4.2]
  def change
    add_column :complaints, :resolved_at, :datetime
    add_column :complaints, :admin_id, :integer
    rename_column :complaints, :topic, :error_type
  end
end
