class AddSlugToGift < ActiveRecord::Migration[4.2]
  def change
    add_column :gifts, :slug, :string, unique: true
  end
end
