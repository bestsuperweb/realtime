class AddFromfbToCustomer < ActiveRecord::Migration[4.2]
  def change
    add_column :customers, :fromfb, :boolean, default: false
  end
end
