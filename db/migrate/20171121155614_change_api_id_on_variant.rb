class ChangeApiIdOnVariant < ActiveRecord::Migration[4.2]
  def up
    change_column :variants, :api_id, :string
  end

  def down
    change_column :variants, :api_id, 'integer USING CAST(api_id AS integer)'
  end
end
