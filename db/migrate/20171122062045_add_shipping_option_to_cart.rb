class AddShippingOptionToCart < ActiveRecord::Migration[4.2]
  def change
    add_reference :carts, :shipping_option, index: true, foreign_key: true
  end
end
