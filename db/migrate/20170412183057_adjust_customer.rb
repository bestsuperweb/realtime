class AdjustCustomer < ActiveRecord::Migration[4.2]
  def change
    add_column :customers, :website, :string
    add_column :customers, :description, :text
    add_column :customers, :gender, :string
    add_column :customers, :private, :boolean, default: false
    add_column :customers, :birthday, :date
  end
end
