class GiftOtd < ActiveRecord::Migration[4.2]
  def change
    create_table :gift_otds do |t|
      t.belongs_to :variant
      t.string :title, null: false
      t.text :description, null: false
      t.date :occurs_at, null: false

      t.timestamps
    end
  end

end
