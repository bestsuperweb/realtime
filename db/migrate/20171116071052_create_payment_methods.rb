class CreatePaymentMethods < ActiveRecord::Migration[4.2]
  def change
    create_table :payment_methods do |t|
      t.string :uuid, null: false
      t.references :customer, index: true, foreign_key: true
      t.references :address, index: true, foreign_key: true
      t.string :stripe_id, null: false
      t.boolean :default, default: false
      t.text :encrypted_metadata
      t.text :encrypted_metadata_iv

      t.string :payment_type
      t.string :encrypted_name
      t.string :encrypted_name_iv

      t.timestamps null: false
    end
    add_index :payment_methods, :uuid
    add_index :payment_methods, :default
  end
end
