class AddShippingOrderToMerchantCart < ActiveRecord::Migration[4.2]
  def change
    add_reference :merchant_carts, :shipping_order, index: true, foreign_key: true
  end
end
