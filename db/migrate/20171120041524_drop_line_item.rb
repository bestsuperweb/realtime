class DropLineItem < ActiveRecord::Migration[4.2]
  def change
    drop_table :line_items
  end
end
