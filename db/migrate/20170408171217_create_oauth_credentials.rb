class CreateOauthCredentials < ActiveRecord::Migration[4.2]
  def change
    remove_column :customers, :provider
    remove_column :customers, :uid

    create_table :oauth_credentials do |t|
      t.string :provider
      t.string :uid  
      t.integer :customer_id

      t.timestamps null: false
    end

    add_foreign_key :oauth_credentials, :customers
  end
end
