class AddPhoneNumberToAddress < ActiveRecord::Migration[4.2]
  def change
    add_column :addresses, :encrypted_phone_number, :string
    add_column :addresses, :encrypted_phone_number_iv, :string
  end
end
