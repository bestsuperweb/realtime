class AddAddressAndPaymentMethodToCart < ActiveRecord::Migration[4.2]
  def change
    add_reference :carts, :address, index: true, foreign_key: true
    add_reference :carts, :payment_method, index: true, foreign_key: true
  end
end
