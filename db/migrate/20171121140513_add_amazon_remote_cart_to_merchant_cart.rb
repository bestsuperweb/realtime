class AddAmazonRemoteCartToMerchantCart < ActiveRecord::Migration[4.2]
  def change
    add_column :merchant_carts, :amazon_remote_cart_id, :text
    add_index :merchant_carts, :amazon_remote_cart_id
    add_column :merchant_carts, :amazon_remote_cart_hmac, :text
  end
end
