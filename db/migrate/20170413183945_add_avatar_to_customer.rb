class AddAvatarToCustomer < ActiveRecord::Migration[4.2]
  def change
    add_column :customers, :avatar, :string
  end
end
