class CreateNotifications < ActiveRecord::Migration[4.2]
  def change
    create_table :notifications do |t|
      t.string :text
      t.string :boolean
      t.integer :activity_id
      t.integer :customer_id

      t.timestamps null: false
    end

    add_foreign_key :notifications, :activities
    add_foreign_key :notifications, :customers
  end
end
