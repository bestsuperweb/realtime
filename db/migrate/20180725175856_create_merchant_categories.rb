class CreateMerchantCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :merchant_categories do |t|
      t.string :name
      t.string :merchants, array: true, default: [] 
      t.string :screenshot

      t.timestamps
    end
  end
end
