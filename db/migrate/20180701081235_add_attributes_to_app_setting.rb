class AddAttributesToAppSetting < ActiveRecord::Migration[4.2]
  def change
    remove_column :app_settings, :estimator_padding_rate, :decimal
    remove_column :app_settings, :invoice_service_fee_rate, :decimal
    remove_column :app_settings, :estimator_sales_tax_rate, :decimal
    remove_column :app_settings, :estimator_service_fee_rate, :decimal

    add_column :app_settings, :shipping_flat_rate, :decimal, default: 7.00, null: false
    add_column :app_settings, :shipping_rate_multiplier, :decimal, default: 1.1, null: false
    add_column :app_settings, :sales_tax_rate, :decimal, default: 7.00, null: false
    add_column :app_settings, :service_fee_rate, :decimal, default: 4.00, null: false
    add_column :app_settings, :stripe_processing_rate, :decimal, default: 2.9, null: false
    add_column :app_settings, :stripe_per_transaction_fee, :decimal, default: 0.3, null: false
    add_column :app_settings, :authorization_multiplier, :decimal, default: 2.0, null: false

  end
end
