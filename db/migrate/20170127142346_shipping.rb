class Shipping < ActiveRecord::Migration[4.2]
  def change
    create_table :shippings do |t|
      t.string  :state, null: false, default: "pending"
      t.integer :total, null: false, default: 0
      t.string  :tracking_number
      t.integer :order_id, null: false
      t.integer :shipping_option_id, null: false
      t.integer :address_id, null: false

      t.timestamps
    end

    add_foreign_key :shippings, :orders
  end
end
