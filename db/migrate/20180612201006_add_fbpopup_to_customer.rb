class AddFbpopupToCustomer < ActiveRecord::Migration[4.2]
  def change
    add_column :customers, :fbpopup, :boolean
  end
end
