class AddSlugToMerchantCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :merchant_categories, :slug, :string, unique: true
  end
end
