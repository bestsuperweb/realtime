class AddPermalinkToTags < ActiveRecord::Migration[4.2]
  def change
    add_column :tags, :permalink, :string
  end
end
