class AddMerchantUrlIndexToVariants < ActiveRecord::Migration[4.2]
  def change
    add_index :variants, :merchant_url
  end
end