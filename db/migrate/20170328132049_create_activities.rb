class CreateActivities < ActiveRecord::Migration[4.2]
  def change
    create_table :activities do |t|
      t.string :activity_type
      t.string :link_path
      t.string :content
      t.integer :customer_id

      t.timestamps null: false
    end

    add_foreign_key :activities, :customers
  end
end
