class AddCaptionToGift < ActiveRecord::Migration[4.2]
  def change
    add_column :gifts, :caption, :string
  end
end
