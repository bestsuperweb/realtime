class Event < ActiveRecord::Migration[4.2]
  def change
    create_table :events do |t|
      t.belongs_to :customer, index: true
      t.string :name, null: false
      t.text :description
      t.datetime :occurs_at, null: false
      t.string :state, null: false, default: "draft"

      t.timestamps
    end
  end
end
