class AddQuoteToInvoice < ActiveRecord::Migration[4.2]
  def change
    add_column :invoices, :estimated_shipping_fees, :decimal, default: 0.0, null: false
    add_column :invoices, :shipping_fees, :decimal, default: 0.0, null: false
    add_column :invoices, :estimated_taxes, :decimal, default: 0.0, null: false
    add_column :invoices, :taxes, :decimal, default: 0.0, null: false
    add_column :invoices, :estimated_subtotal, :decimal, default: 0.0, null: false
    add_column :invoices, :subtotal, :decimal, default: 0.0, null: false
  end
end
