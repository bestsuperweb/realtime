class AddChargedAmountToPayment < ActiveRecord::Migration[4.2]
  def change
    add_column :payments, :charged_amount, :decimal, default: 0.0, null: false
    add_index :payments, :charged_amount
  end
end
