class AddDataToApiReport < ActiveRecord::Migration[4.2]
  def change
    add_column :api_reports, :data, :text
  end
end
