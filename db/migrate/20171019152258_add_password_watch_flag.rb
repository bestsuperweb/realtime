class AddPasswordWatchFlag < ActiveRecord::Migration[4.2]
  def change
    add_column :customers, :password_set_flag, :boolean, default: true
  end
end
