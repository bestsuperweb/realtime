class AddRatingToGift < ActiveRecord::Migration[4.2]
  def change
    add_column :gifts, :rating, :integer, default: 0
  end
end
