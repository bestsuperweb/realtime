class AddSlugToVariant < ActiveRecord::Migration[4.2]
  def change
    add_column :variants, :slug, :string, unique: true
  end
end
