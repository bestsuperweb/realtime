class AddCustomerIdToActivity < ActiveRecord::Migration[4.2]
  def change
    add_column :activities, :customer_id, :integer

    add_foreign_key :activities, :customers
  end
end
