class AddBannerToBasket < ActiveRecord::Migration[4.2]
  def change
    add_column :baskets, :banner, :string
  end
end
