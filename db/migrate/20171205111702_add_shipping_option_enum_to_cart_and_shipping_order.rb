class AddShippingOptionEnumToCartAndShippingOrder < ActiveRecord::Migration[4.2]
  def change
    remove_reference :carts, :shipping_option, index: true, foreign_key: true

    add_column :carts, :shipping_option, :integer, default: 0, null: false
    add_index :carts, :shipping_option

  end
end
