class AlterCustomers < ActiveRecord::Migration[4.2]
  def change
    change_column :customers, :state, :string, null: true
    change_column :customers, :city, :string, null: true
  end
end
