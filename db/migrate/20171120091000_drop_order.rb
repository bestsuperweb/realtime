class DropOrder < ActiveRecord::Migration[4.2]
  def change
    drop_table :orders
  end
end
