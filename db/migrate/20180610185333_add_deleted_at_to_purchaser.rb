class AddDeletedAtToPurchaser < ActiveRecord::Migration[4.2]
  def change
    add_column :purchasers, :deleted_at, :datetime
  end
end
