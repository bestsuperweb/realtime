class ChangeVariantApiIdToString < ActiveRecord::Migration[4.2]
  def change
    change_column :variants, :api_id, :string
  end
end
