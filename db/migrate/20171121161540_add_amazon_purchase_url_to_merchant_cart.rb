class AddAmazonPurchaseUrlToMerchantCart < ActiveRecord::Migration[4.2]
  def change
    add_column :merchant_carts, :amazon_remote_cart_purchase_url, :text
  end
end
