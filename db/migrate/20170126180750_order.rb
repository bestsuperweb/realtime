class Order < ActiveRecord::Migration[4.2]
  def change
    create_table :orders do |t|
      t.string  :state, null: false, default: "cart"
      t.integer :subtotal, null: false, default: 0
      t.integer :tax_total, null: false, default: 0
      t.integer :total, null: false, default: 0
      t.integer :customer_id, null: false

      t.timestamps
    end

    add_foreign_key :orders, :customers
  end
end
