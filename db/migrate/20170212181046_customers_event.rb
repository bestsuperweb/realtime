class CustomersEvent < ActiveRecord::Migration[4.2]
  def change
    create_table :customers_events, :id => false do |t|
      t.integer :customer_id
      t.integer :event_id
    end

    add_index :customers_events, [:customer_id, :event_id]
  end
end
