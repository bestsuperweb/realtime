class AddIndexToBaskets < ActiveRecord::Migration[4.2]
  def change
    add_index :baskets, [:customer_id, :name, :created_at]
  end
end
