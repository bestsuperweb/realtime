class CreateAddress < ActiveRecord::Migration[4.2]
  def change
    create_table :addresses do |t|
      t.string  :address_1, null: false
      t.string  :address_2
      t.string  :city, null: false
      t.string  :state, null: false
      t.string  :postal_code, null: false
      t.integer :customer_id, null: false

      t.timestamps
    end

    add_foreign_key :addresses, :customers
    add_foreign_key :payments, :addresses
  end
end
