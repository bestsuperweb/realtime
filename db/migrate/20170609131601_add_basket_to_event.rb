class AddBasketToEvent < ActiveRecord::Migration[4.2]
  def change
    add_column :events, :basket_id, :integer
    add_foreign_key :events, :baskets
  end
end
