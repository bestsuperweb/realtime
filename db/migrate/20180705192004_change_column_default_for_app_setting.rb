class ChangeColumnDefaultForAppSetting < ActiveRecord::Migration[4.2]
  def change
    change_column_default(:app_settings, :shipping_rate_multiplier, 110.0)
    change_column_default(:app_settings, :authorization_multiplier, 200.0)
  end
end