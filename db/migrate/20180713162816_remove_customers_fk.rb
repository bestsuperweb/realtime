class RemoveCustomersFk < ActiveRecord::Migration[5.2]
  def change
    if foreign_key_exists?(:payments, :customers)
      remove_foreign_key :payments, :customers
    end

    if foreign_key_exists?(:payment_methods, :customers)
      remove_foreign_key :payment_methods, :customers
    end

    if foreign_key_exists?(:shipping_orders, :customers)
      remove_foreign_key :shipping_orders, :customers
    end

    if foreign_key_exists?(:invoices, :customers)
      remove_foreign_key :invoices, :customers
    end
  end
end
