class CreateComments < ActiveRecord::Migration[4.2]
  def change
    create_table :comments do |t|
      t.string :content
      t.integer :customer_id
      t.references :commentable, polymorphic: true, index: true

      t.timestamps null: false
    end

    add_foreign_key :comments, :customers
  end
end
