class AddPriceAndSubtotalToCartItem < ActiveRecord::Migration[4.2]
  def change
    add_column :cart_items, :price, :decimal
    add_column :cart_items, :subtotal, :decimal
    add_index :cart_items, :subtotal
  end
end
