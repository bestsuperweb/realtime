class RemoveNameValidationFromCustomer < ActiveRecord::Migration[4.2]
  def change
    change_column :customers, :first_name, :string, null: true
    change_column :customers, :last_name, :string, null: true
  end
end
