class LineItem < ActiveRecord::Migration[4.2]
  def change
    create_table :line_items do |t|
      t.integer :price, null: false, default: 0
      t.integer :variant_id, null: false
      t.integer :order_id, null:false

      t.timestamps
    end

    add_foreign_key :line_items, :variants
    add_foreign_key :line_items, :orders
  end
end
