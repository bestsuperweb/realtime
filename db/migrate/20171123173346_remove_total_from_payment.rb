class RemoveTotalFromPayment < ActiveRecord::Migration[4.2]
  def change
    remove_column :payments, :total
    remove_column :payments, :amount
  end
end
