class AlterVariantDescription < ActiveRecord::Migration[4.2]
  def change
    change_column :variants, :description, :text
    add_column :gifts, :description, :text
  end
end
