class AddDescriptionToBasket < ActiveRecord::Migration[4.2]
  def change
    add_column :baskets, :description, :text
  end
end
