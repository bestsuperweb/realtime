class AddJobHistoryToCustomer < ActiveRecord::Migration[4.2]
  def change
    add_column :customers, :employer, :string
    add_column :customers, :job_start_date, :date
  end
end
