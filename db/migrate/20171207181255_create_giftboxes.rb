class CreateGiftboxes < ActiveRecord::Migration[4.2]
  def change
    create_table :giftboxes do |t|
      t.references :address, index: true, foreign_key: true
      t.references :payment_method, index: true, foreign_key: true
      t.references :owner, polymorphic: true, index: true
      t.references :cart, index: true, foreign_key: true

      t.boolean :is_a_gift, default: false, index: true
      t.references :giftee, polymorphic: true, index: true
      t.references :giftee_shipping_address, index: true

      t.integer :shipping_option, default: 0
      t.integer :status, default: 0, index: true

      t.timestamps null: false
    end
    add_index :giftboxes, :shipping_option
  end
end
