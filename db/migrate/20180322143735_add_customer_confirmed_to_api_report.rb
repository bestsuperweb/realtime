class AddCustomerConfirmedToApiReport < ActiveRecord::Migration[4.2]
  def change
    add_column :api_reports, :customer_confirmed, :boolean, default: false
    add_index :api_reports, :customer_confirmed
  end
end
