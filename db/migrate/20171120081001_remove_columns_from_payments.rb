class RemoveColumnsFromPayments < ActiveRecord::Migration[4.2]
  def change
    remove_column :payments, :order_id
    remove_column :payments, :address_id
  end
end
