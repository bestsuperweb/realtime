class CreateMerchant < ActiveRecord::Migration[4.2]
  def change
    create_table :merchants do |t|
      t.string :name, null: false
      t.string :public_url, null: false
      t.integer :alexa_rank
      t.string :status, null: false, default: "private"
      t.float :commission, null: false, default: 100.0
      t.integer :ppc_price, default: 0
      t.integer :cpm_price, default: 0
      t.integer :validation_days, default: 0

      t.timestamps
    end

    add_foreign_key :variants, :merchants
  end
end
