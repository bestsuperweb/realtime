class AddPrivacyToBaskets < ActiveRecord::Migration[4.2]
  def change
    add_column :baskets, :privacy, :string, default: "friends"
  end
end
