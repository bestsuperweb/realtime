class AddIndexQuantityToCartItem < ActiveRecord::Migration[4.2]
  def change
    add_index :cart_items, :quantity
  end
end
