class AddResolvedToApiReport < ActiveRecord::Migration[4.2]
  def change
    add_column :api_reports, :resolved_at, :datetime
  end
end
