class ChangeCustomersEvents < ActiveRecord::Migration[4.2]
  def up
    drop_table :customers_events

    create_table :customers_events do |t|
      t.integer :customer_id
      t.integer :event_id
      t.string :status, default: "invited"

      t.timestamps null: false
    end

    add_index :customers_events, [:customer_id, :event_id]
  end

  def down
    drop_table :customers_events

    create_table :customers_events, :id => false do |t| 
      t.integer :customer_id
      t.integer :event_id
    end 

    add_index :customers_events, [:customer_id, :event_id]
  end
end
