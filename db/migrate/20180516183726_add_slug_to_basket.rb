class AddSlugToBasket < ActiveRecord::Migration[4.2]
  def change
    add_column :baskets, :slug, :string, unique: true
  end
end
