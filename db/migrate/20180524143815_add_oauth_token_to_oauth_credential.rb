class AddOauthTokenToOauthCredential < ActiveRecord::Migration[4.2]
  def change
    add_column :oauth_credentials, :oauth_token, :string
  end
end
