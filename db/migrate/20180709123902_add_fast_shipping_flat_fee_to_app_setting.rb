class AddFastShippingFlatFeeToAppSetting < ActiveRecord::Migration[4.2]
  def change
    add_column :app_settings, :fast_shipping_flat_rate, :decimal, default: 30.00, null: false
  end
end
