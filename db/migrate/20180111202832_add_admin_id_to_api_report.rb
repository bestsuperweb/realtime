class AddAdminIdToApiReport < ActiveRecord::Migration[4.2]
  def change
    add_column :api_reports, :admin_id, :integer

    add_foreign_key :api_reports, :admins
  end

end
