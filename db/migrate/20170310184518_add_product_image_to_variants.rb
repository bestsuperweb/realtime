class AddProductImageToVariants < ActiveRecord::Migration[4.2]
  def change
    add_column :variants, :product_image, :string
  end
end
