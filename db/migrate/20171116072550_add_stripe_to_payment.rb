class AddStripeToPayment < ActiveRecord::Migration[4.2]
  def change
    add_column :payments, :uuid, :string, null: false
    add_reference :payments, :payment_method, index:true, foreign_key: true
    add_reference :payments, :customer, index:true, foreign_key: true
    add_reference :payments, :invoice, index:true, foreign_key: true
    add_column :payments, :status, :integer, default: 0
    add_column :payments, :amount, :decimal, default: 0.0
    add_column :payments, :outcome, :text
    add_column :payments, :stripe_id, :string
    add_column :payments, :idempotent_key, :string

    add_index :payments, :uuid
    add_index :payments, :amount
    add_index :payments, :status
  end
end
