class AddInstructionToAddresses < ActiveRecord::Migration[4.2]
  def change
    add_column :addresses, :instructions, :string
  end
end
