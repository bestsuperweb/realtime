class CreateAppSetting < ActiveRecord::Migration[4.2]
  def change
    create_table :app_settings do |t|
      t.decimal :estimator_sales_tax_rate, default: 0.045
      t.decimal :estimator_service_fee_rate, default: 0.04
      t.decimal :estimator_padding_rate, default: 2.0
      t.decimal :invoice_service_fee_rate, default: 0.04
    end
  end
end
