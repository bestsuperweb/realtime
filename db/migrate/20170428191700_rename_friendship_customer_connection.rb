class RenameFriendshipCustomerConnection < ActiveRecord::Migration[4.2]
  def change
    rename_table :friendships, :followings
  end
end
