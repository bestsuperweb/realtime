class DropShippingOption < ActiveRecord::Migration[4.2]
  def change
    drop_table :shipping_options
  end
end
