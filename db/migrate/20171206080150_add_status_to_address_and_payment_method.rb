class AddStatusToAddressAndPaymentMethod < ActiveRecord::Migration[4.2]
  def change
    add_column :addresses, :status, :integer, default: 0, index: true    
    add_column :payment_methods, :status, :integer, default: 0, index: true
  end
end
