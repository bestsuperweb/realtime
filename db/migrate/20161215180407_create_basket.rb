class CreateBasket < ActiveRecord::Migration[4.2]
  def change
    create_table :baskets do |t|
      t.string :name, null: false
      t.integer :customer_id
      t.integer :max_gifts

      t.timestamps
    end

  end
end
