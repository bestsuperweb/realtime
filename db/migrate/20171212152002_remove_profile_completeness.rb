class RemoveProfileCompleteness < ActiveRecord::Migration[4.2]
  def up
    remove_index :customers_profile_steps, :profile_step_id
    remove_index :customers_profile_steps, :customer_id

    drop_table :customers_profile_steps
    drop_table :profile_steps
    
    remove_column :customers, :walkthrough_active
    remove_column :customers, :password_set_flag
  end

  def down
    add_column :customers, :password_set_flag, :boolean, default: false
    add_column :customers, :walkthrough_active, :boolean, default: true

    create_table :profile_steps do |t| 
      t.string :name
      t.integer :percentage_amount
      t.integer :step_number

      t.timestamps null: false
    end 

    create_table :customers_profile_steps do |t| 
      t.datetime :completed_at
      t.integer :customer_id
      t.integer :profile_step_id
    end 

    add_index :customers_profile_steps, :customer_id
    add_index :customers_profile_steps, :profile_step_id
  end
end
