class AddMessagesToMessage < ActiveRecord::Migration[4.2]
  def change
    add_column :messages, :prev_message_id, :integer
    add_column :messages, :next_message_id, :integer

  end
end
