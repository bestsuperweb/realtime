class AddStripeIdToCustomer < ActiveRecord::Migration[4.2]
  def change
    add_column :customers, :stripe_id, :string
  end
end
