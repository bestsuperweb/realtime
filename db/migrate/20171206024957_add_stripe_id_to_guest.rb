class AddStripeIdToGuest < ActiveRecord::Migration[4.2]
  def change
    add_column :guests, :stripe_id, :string
    add_index :guests, :stripe_id
    add_index :guests, :email
  end
end
