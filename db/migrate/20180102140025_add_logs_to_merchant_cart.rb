class AddLogsToMerchantCart < ActiveRecord::Migration[4.2]
  def change
    add_column :merchant_carts, :logs, :text
  end
end
