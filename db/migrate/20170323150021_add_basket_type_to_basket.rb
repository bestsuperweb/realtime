class AddBasketTypeToBasket < ActiveRecord::Migration[4.2]
  def change
    add_column :baskets, :basket_type, :string, default: "lifestyle"
  end
end
