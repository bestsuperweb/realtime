class ChangeDefaultForBasketPrivacy < ActiveRecord::Migration[4.2]
  def up
    change_column :baskets, :privacy, :string, default: "public"
  end

  def down
    change_column :baskets, :privacy, :string, default: "friends"
  end
end
