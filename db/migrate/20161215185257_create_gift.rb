class CreateGift < ActiveRecord::Migration[4.2]
  def change
    create_table :gifts do |t|
      t.text :note
      t.integer :variant_id
      t.integer :basket_id

      t.timestamps
    end

    add_foreign_key :gifts, :variants
    add_foreign_key :gifts, :baskets
  end
end
