class AddShareCount < ActiveRecord::Migration[4.2]
  def change
    add_column :baskets, :share_count, :integer, default: 0
    add_column :variants, :share_count, :integer, default: 0
    add_column :gifts, :share_count, :integer, default: 0
  end
end
