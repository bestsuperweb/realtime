class AddAssignableToMerchantCart < ActiveRecord::Migration[4.2]
  def change
    add_reference :merchant_carts, :assignable, polymorphic: true, index: true
  end
end
