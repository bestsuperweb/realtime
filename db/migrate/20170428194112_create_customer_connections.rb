class CreateCustomerConnections < ActiveRecord::Migration[4.2]
  def change
    create_table :customer_connections do |t|
      t.integer :customer_1_id
      t.integer :customer_2_id
      t.string  :state, default: "pending"

      t.timestamps null: false
    end 

    add_foreign_key :customer_connections, :customers, column: :customer_1_id
    add_foreign_key :customer_connections, :customers, column: :customer_2_id

  end
end
