class AddManualToMerchantCart < ActiveRecord::Migration[4.2]
  def change
    add_column :merchant_carts, :manual, :boolean, default: false, null: false
    add_index :merchant_carts, :manual
  end
end
