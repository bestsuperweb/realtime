class AlterActivities < ActiveRecord::Migration[4.2]
  def change
    remove_column :activities, :link_path
    remove_column :activities, :customer_id
    add_column :activities, :actable_id, :integer
    add_column :activities, :actable_type, :string

    add_index :activities, [:actable_type, :actable_id]
  end
end
