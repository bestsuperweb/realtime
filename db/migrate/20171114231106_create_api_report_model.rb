class CreateApiReportModel < ActiveRecord::Migration[4.2]
  def change
    create_table :api_reports do |t|
      t.string  :error_type
      t.integer :customer_id
      t.string  :url
      t.string  :note

      t.timestamps
    end

    add_foreign_key :api_reports, :customers

    create_table :complaints do |t|
      t.string :name
      t.string :topic
      t.text   :body
      t.string :email

      t.timestamps
    end

  end
end
