class CreateInvoices < ActiveRecord::Migration[4.2]
  def change
    create_table :invoices do |t|
      t.string :uuid, null: false
      t.references :customer, index: true, foreign_key: true
      t.references :cart, index: true, foreign_key: true
      t.references :payment_method, index: true, foreign_key: true
      t.decimal :total_amount, default: 0.0
      t.integer :status, default: 0
      t.string :currency_code
      t.string :currency_symbol
      t.string :idempotent_key

      t.timestamps null: false
    end
    add_index :invoices, :uuid
    add_index :invoices, :total_amount
    add_index :invoices, :status
  end
end
