class RenameColumnsInAddress < ActiveRecord::Migration[4.2]
  def change
    rename_column :addresses, :address_1, :encrypted_address_1
    rename_column :addresses, :address_2, :encrypted_address_2

    add_column :addresses, :encrypted_address_1_iv, :string
    add_column :addresses, :encrypted_address_2_iv, :string

    add_column :addresses, :encrypted_name, :string
    add_column :addresses, :encrypted_name_iv, :string
  end
end
