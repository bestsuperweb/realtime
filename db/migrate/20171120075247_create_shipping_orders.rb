class CreateShippingOrders < ActiveRecord::Migration[4.2]
  def change
    create_table :shipping_orders do |t|
      t.references :customer, index: true, foreign_key: true
      t.references :invoice, index: true, foreign_key: true
      t.integer :status, default: 0
      t.references :address, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :shipping_orders, :status
  end
end
