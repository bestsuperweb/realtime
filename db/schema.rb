# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_01_194928) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", id: :serial, force: :cascade do |t|
    t.string "activity_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "actable_id"
    t.string "actable_type"
    t.integer "customer_id"
    t.index ["actable_type", "actable_id"], name: "index_activities_on_actable_type_and_actable_id"
  end

  create_table "addresses", id: :serial, force: :cascade do |t|
    t.string "encrypted_address_1", null: false
    t.string "encrypted_address_2"
    t.string "city", null: false
    t.string "state", null: false
    t.string "postal_code", null: false
    t.integer "owner_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "address_type", default: 0, null: false
    t.string "instructions"
    t.boolean "default", default: false
    t.string "encrypted_address_1_iv"
    t.string "encrypted_address_2_iv"
    t.string "encrypted_name"
    t.string "encrypted_name_iv"
    t.string "encrypted_phone_number"
    t.string "encrypted_phone_number_iv"
    t.string "owner_type", default: "Customer"
    t.integer "status", default: 0
    t.index ["address_type"], name: "index_addresses_on_address_type"
    t.index ["owner_id"], name: "index_addresses_on_owner_id"
  end

  create_table "admins", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "name"
    t.string "token_id"
    t.string "encrypted_mobile_number"
    t.string "encrypted_mobile_number_iv"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
    t.index ["token_id"], name: "index_admins_on_token_id"
  end

  create_table "api_reports", id: :serial, force: :cascade do |t|
    t.string "error_type"
    t.integer "customer_id"
    t.string "url"
    t.string "note"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "resolved_at"
    t.integer "admin_id"
    t.boolean "customer_confirmed", default: false
    t.text "data"
    t.index ["customer_confirmed"], name: "index_api_reports_on_customer_confirmed"
  end

  create_table "app_settings", id: :serial, force: :cascade do |t|
    t.decimal "shipping_flat_rate", default: "7.0", null: false
    t.decimal "shipping_rate_multiplier", default: "110.0", null: false
    t.decimal "sales_tax_rate", default: "7.0", null: false
    t.decimal "service_fee_rate", default: "4.0", null: false
    t.decimal "stripe_processing_rate", default: "2.9", null: false
    t.decimal "stripe_per_transaction_fee", default: "0.3", null: false
    t.decimal "authorization_multiplier", default: "200.0", null: false
    t.decimal "fast_shipping_flat_rate", default: "30.0", null: false
  end

  create_table "baskets", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.integer "customer_id"
    t.integer "max_gifts"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "basket_type", default: "lifestyle"
    t.text "description"
    t.string "banner"
    t.integer "share_count", default: 0
    t.string "privacy", default: "public"
    t.string "slug"
    t.index ["customer_id", "name", "created_at"], name: "index_baskets_on_customer_id_and_name_and_created_at"
  end

  create_table "blockings", id: :serial, force: :cascade do |t|
    t.integer "customer_1_id"
    t.integer "customer_2_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cart_items", id: :serial, force: :cascade do |t|
    t.integer "cart_id"
    t.integer "merchant_cart_id"
    t.integer "gift_id"
    t.integer "variant_id"
    t.integer "quantity", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "price"
    t.decimal "subtotal"
    t.integer "giftbox_id"
    t.datetime "verified_at"
    t.string "current_offer_id"
    t.integer "current_available_quantity", default: 0
    t.decimal "current_price", default: "0.0"
    t.decimal "current_sale_price", default: "0.0"
    t.index ["cart_id"], name: "index_cart_items_on_cart_id"
    t.index ["current_available_quantity"], name: "index_cart_items_on_current_available_quantity"
    t.index ["current_offer_id"], name: "index_cart_items_on_current_offer_id"
    t.index ["current_price"], name: "index_cart_items_on_current_price"
    t.index ["current_sale_price"], name: "index_cart_items_on_current_sale_price"
    t.index ["gift_id"], name: "index_cart_items_on_gift_id"
    t.index ["giftbox_id"], name: "index_cart_items_on_giftbox_id"
    t.index ["merchant_cart_id"], name: "index_cart_items_on_merchant_cart_id"
    t.index ["quantity"], name: "index_cart_items_on_quantity"
    t.index ["subtotal"], name: "index_cart_items_on_subtotal"
    t.index ["variant_id"], name: "index_cart_items_on_variant_id"
    t.index ["verified_at"], name: "index_cart_items_on_verified_at"
  end

  create_table "carts", id: :serial, force: :cascade do |t|
    t.integer "owner_id"
    t.boolean "flagged_as_current", default: false
    t.integer "status", default: 0, null: false
    t.integer "cart_items_count", default: 0
    t.boolean "flagged_for_purchase", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "address_id"
    t.integer "payment_method_id"
    t.string "owner_type", default: "Customer"
    t.index ["address_id"], name: "index_carts_on_address_id"
    t.index ["flagged_as_current"], name: "index_carts_on_flagged_as_current"
    t.index ["flagged_for_purchase"], name: "index_carts_on_flagged_for_purchase"
    t.index ["owner_id"], name: "index_carts_on_owner_id"
    t.index ["payment_method_id"], name: "index_carts_on_payment_method_id"
    t.index ["status"], name: "index_carts_on_status"
  end

  create_table "charities", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "url"
    t.boolean "cash_charity"
    t.integer "basket_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", id: :serial, force: :cascade do |t|
    t.string "content"
    t.integer "customer_id"
    t.string "commentable_type"
    t.integer "commentable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id"
  end

  create_table "complaints", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "error_type"
    t.text "body"
    t.string "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "resolved_at"
    t.integer "admin_id"
  end

  create_table "conversations", id: :serial, force: :cascade do |t|
    t.integer "sender_id"
    t.integer "recipient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["recipient_id"], name: "index_conversations_on_recipient_id"
    t.index ["sender_id"], name: "index_conversations_on_sender_id"
  end

  create_table "customer_connections", id: :serial, force: :cascade do |t|
    t.integer "customer_1_id"
    t.integer "customer_2_id"
    t.string "state", default: "pending"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers", id: :serial, force: :cascade do |t|
    t.string "first_name", default: ""
    t.string "middle_initial", default: ""
    t.string "last_name", default: ""
    t.string "location", default: ""
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "role", default: "customer", null: false
    t.string "username", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "website"
    t.text "description"
    t.string "gender"
    t.boolean "private", default: false
    t.date "birthday"
    t.string "avatar"
    t.string "banner"
    t.string "employer"
    t.date "job_start_date"
    t.string "auth_token"
    t.string "stripe_id"
    t.boolean "fbpopup"
    t.boolean "fromfb", default: false
    t.index ["email"], name: "index_customers_on_email", unique: true
    t.index ["reset_password_token"], name: "index_customers_on_reset_password_token", unique: true
  end

  create_table "customers_events", id: :serial, force: :cascade do |t|
    t.integer "customer_id"
    t.integer "event_id"
    t.string "status", default: "invited"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id", "event_id"], name: "index_customers_events_on_customer_id_and_event_id"
  end

  create_table "customers_settings", id: :serial, force: :cascade do |t|
    t.integer "customer_id"
    t.integer "setting_id"
    t.string "selected_option"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deals", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "ad_image"
    t.datetime "start_time"
    t.datetime "end_time"
    t.string "alt_text"
    t.string "size", default: "small"
    t.string "google_analytics_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "url"
  end

  create_table "events", id: :serial, force: :cascade do |t|
    t.integer "customer_id"
    t.string "name", null: false
    t.text "description"
    t.datetime "occurs_at", null: false
    t.string "state", default: "draft", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "location"
    t.string "event_image"
    t.integer "basket_id"
    t.index ["customer_id"], name: "index_events_on_customer_id"
  end

  create_table "followings", id: :serial, force: :cascade do |t|
    t.integer "customer_1_id"
    t.integer "customer_2_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "friendly_id_slugs", id: :serial, force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "gift_otds", id: :serial, force: :cascade do |t|
    t.integer "variant_id"
    t.string "title", null: false
    t.text "description", null: false
    t.date "occurs_at", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "giftboxes", id: :serial, force: :cascade do |t|
    t.integer "address_id"
    t.integer "payment_method_id"
    t.string "owner_type"
    t.integer "owner_id"
    t.integer "cart_id"
    t.boolean "is_a_gift", default: false
    t.string "giftee_type"
    t.integer "giftee_id"
    t.integer "giftee_shipping_address_id"
    t.integer "shipping_option", default: 0
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cart_items_count", default: 0
    t.index ["address_id"], name: "index_giftboxes_on_address_id"
    t.index ["cart_id"], name: "index_giftboxes_on_cart_id"
    t.index ["cart_items_count"], name: "index_giftboxes_on_cart_items_count"
    t.index ["giftee_shipping_address_id"], name: "index_giftboxes_on_giftee_shipping_address_id"
    t.index ["giftee_type", "giftee_id"], name: "index_giftboxes_on_giftee_type_and_giftee_id"
    t.index ["is_a_gift"], name: "index_giftboxes_on_is_a_gift"
    t.index ["owner_type", "owner_id"], name: "index_giftboxes_on_owner_type_and_owner_id"
    t.index ["payment_method_id"], name: "index_giftboxes_on_payment_method_id"
    t.index ["shipping_option"], name: "index_giftboxes_on_shipping_option"
    t.index ["status"], name: "index_giftboxes_on_status"
  end

  create_table "gifts", id: :serial, force: :cascade do |t|
    t.text "note"
    t.integer "variant_id"
    t.integer "basket_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "quantity_desired", default: 1
    t.integer "quantity_received", default: 0
    t.text "description"
    t.string "slug"
    t.integer "share_count", default: 0
    t.integer "customer_id"
    t.integer "rating", default: 0
    t.string "caption"
    t.boolean "owner_marked_received", default: false
  end

  create_table "guests", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "stripe_id"
    t.string "name", default: "Guest", null: false
    t.index ["email"], name: "index_guests_on_email"
    t.index ["stripe_id"], name: "index_guests_on_stripe_id"
  end

  create_table "invoices", id: :serial, force: :cascade do |t|
    t.string "uuid", null: false
    t.integer "owner_id"
    t.integer "payment_method_id"
    t.decimal "total_amount", default: "0.0"
    t.integer "status", default: 0
    t.string "currency_code"
    t.string "currency_symbol"
    t.string "idempotent_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "estimated_shipping_fees", default: "0.0", null: false
    t.decimal "shipping_fees", default: "0.0", null: false
    t.decimal "estimated_taxes", default: "0.0", null: false
    t.decimal "taxes", default: "0.0", null: false
    t.decimal "estimated_subtotal", default: "0.0", null: false
    t.decimal "subtotal", default: "0.0", null: false
    t.decimal "service_fee", default: "0.0"
    t.string "owner_type", default: "Customer"
    t.integer "giftbox_id"
    t.index ["giftbox_id"], name: "index_invoices_on_giftbox_id"
    t.index ["owner_id"], name: "index_invoices_on_owner_id"
    t.index ["payment_method_id"], name: "index_invoices_on_payment_method_id"
    t.index ["status"], name: "index_invoices_on_status"
    t.index ["total_amount"], name: "index_invoices_on_total_amount"
    t.index ["uuid"], name: "index_invoices_on_uuid"
  end

  create_table "likes", id: :serial, force: :cascade do |t|
    t.string "likable_type"
    t.integer "likable_id"
    t.integer "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["likable_type", "likable_id"], name: "index_likes_on_likable_type_and_likable_id"
  end

  create_table "media", id: :serial, force: :cascade do |t|
    t.string "file_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "merchant_carts", id: :serial, force: :cascade do |t|
    t.integer "merchant_id"
    t.integer "status", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "shipping_order_id"
    t.text "amazon_remote_cart_id"
    t.text "amazon_remote_cart_hmac"
    t.text "amazon_remote_cart_purchase_url"
    t.decimal "estimated_shipping_fee", default: "0.0", null: false
    t.decimal "estimated_taxes", default: "0.0", null: false
    t.decimal "estimated_subtotal", default: "0.0", null: false
    t.decimal "actual_shipping_fee", default: "0.0", null: false
    t.decimal "actual_taxes", default: "0.0", null: false
    t.decimal "actual_subtotal", default: "0.0", null: false
    t.decimal "actual_total", default: "0.0", null: false
    t.integer "cart_items_count"
    t.boolean "manual", default: false, null: false
    t.text "purchase_gateway_errors"
    t.integer "giftbox_id"
    t.datetime "purchase_gateway_failure_date"
    t.datetime "purchase_gateway_success_date"
    t.text "logs"
    t.string "assignable_type"
    t.integer "assignable_id"
    t.datetime "assigned_at"
    t.datetime "completed_at"
    t.string "completable_type"
    t.integer "completable_id"
    t.index ["amazon_remote_cart_id"], name: "index_merchant_carts_on_amazon_remote_cart_id"
    t.index ["assignable_type", "assignable_id"], name: "index_merchant_carts_on_assignable_type_and_assignable_id"
    t.index ["completable_type", "completable_id"], name: "index_merchant_carts_on_completable_type_and_completable_id"
    t.index ["giftbox_id"], name: "index_merchant_carts_on_giftbox_id"
    t.index ["manual"], name: "index_merchant_carts_on_manual"
    t.index ["merchant_id"], name: "index_merchant_carts_on_merchant_id"
    t.index ["purchase_gateway_failure_date"], name: "index_merchant_carts_on_purchase_gateway_failure_date"
    t.index ["purchase_gateway_success_date"], name: "index_merchant_carts_on_purchase_gateway_success_date"
    t.index ["shipping_order_id"], name: "index_merchant_carts_on_shipping_order_id"
    t.index ["status"], name: "index_merchant_carts_on_status"
  end

  create_table "merchant_categories", force: :cascade do |t|
    t.string "name"
    t.string "merchants", default: [], array: true
    t.string "screenshot"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
  end

  create_table "merchants", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.string "public_url", null: false
    t.integer "alexa_rank"
    t.string "status", default: "private", null: false
    t.float "commission", default: 100.0, null: false
    t.integer "ppc_price", default: 0
    t.integer "cpm_price", default: 0
    t.integer "validation_days", default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "icon"
    t.boolean "pending", default: false
    t.string "screenshot"
  end

  create_table "messages", id: :serial, force: :cascade do |t|
    t.string "media"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "body"
    t.integer "conversation_id"
    t.integer "customer_id"
    t.boolean "read", default: false
    t.integer "prev_message_id"
    t.integer "next_message_id"
    t.index ["conversation_id"], name: "index_messages_on_conversation_id"
    t.index ["customer_id"], name: "index_messages_on_customer_id"
  end

  create_table "notifications", id: :serial, force: :cascade do |t|
    t.integer "activity_id"
    t.integer "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "read", default: false
  end

  create_table "oauth_credentials", id: :serial, force: :cascade do |t|
    t.string "provider"
    t.string "uid"
    t.integer "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "oauth_token"
  end

  create_table "payment_methods", id: :serial, force: :cascade do |t|
    t.string "uuid", null: false
    t.integer "owner_id"
    t.integer "address_id"
    t.string "stripe_id", null: false
    t.boolean "default", default: false
    t.text "encrypted_metadata"
    t.text "encrypted_metadata_iv"
    t.string "payment_type"
    t.string "encrypted_name"
    t.string "encrypted_name_iv"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "owner_type", default: "Customer"
    t.integer "status", default: 0
    t.index ["address_id"], name: "index_payment_methods_on_address_id"
    t.index ["default"], name: "index_payment_methods_on_default"
    t.index ["owner_id"], name: "index_payment_methods_on_owner_id"
    t.index ["uuid"], name: "index_payment_methods_on_uuid"
  end

  create_table "payments", id: :serial, force: :cascade do |t|
    t.string "state", default: "checkout", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "uuid", null: false
    t.integer "payment_method_id"
    t.integer "owner_id"
    t.integer "invoice_id"
    t.integer "status", default: 0
    t.text "outcome"
    t.string "stripe_id"
    t.string "idempotent_key"
    t.decimal "authorized_amount", default: "0.0", null: false
    t.decimal "charged_amount", default: "0.0", null: false
    t.string "owner_type", default: "Customer"
    t.index ["charged_amount"], name: "index_payments_on_charged_amount"
    t.index ["invoice_id"], name: "index_payments_on_invoice_id"
    t.index ["owner_id"], name: "index_payments_on_owner_id"
    t.index ["payment_method_id"], name: "index_payments_on_payment_method_id"
    t.index ["status"], name: "index_payments_on_status"
    t.index ["uuid"], name: "index_payments_on_uuid"
  end

  create_table "products", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "pending", default: false
  end

  create_table "purchase_feedbacks", id: :serial, force: :cascade do |t|
    t.integer "actable_id"
    t.string "actable_type"
    t.integer "customer_id"
    t.integer "quantity"
    t.boolean "purchased"
    t.boolean "surprise"
    t.string "confirmation_number"
    t.string "feedback"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "purchasers", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "name"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["email"], name: "index_purchasers_on_email", unique: true
    t.index ["reset_password_token"], name: "index_purchasers_on_reset_password_token", unique: true
  end

  create_table "settings", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "options", default: [], array: true
    t.string "family"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "default_value"
    t.string "group"
  end

  create_table "shipping_orders", id: :serial, force: :cascade do |t|
    t.integer "owner_id"
    t.integer "invoice_id"
    t.integer "status", default: 0
    t.integer "address_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "uuid", null: false
    t.string "owner_type", default: "Customer"
    t.index ["address_id"], name: "index_shipping_orders_on_address_id"
    t.index ["invoice_id"], name: "index_shipping_orders_on_invoice_id"
    t.index ["owner_id"], name: "index_shipping_orders_on_owner_id"
    t.index ["status"], name: "index_shipping_orders_on_status"
    t.index ["uuid"], name: "index_shipping_orders_on_uuid"
  end

  create_table "taggings", id: :serial, force: :cascade do |t|
    t.integer "tag_id"
    t.string "taggable_type"
    t.integer "taggable_id"
    t.string "tagger_type"
    t.integer "tagger_id"
    t.string "context", limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
  end

  create_table "tags", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "taggings_count", default: 0
    t.string "permalink"
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "variants", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.integer "price", default: 0, null: false
    t.integer "product_id"
    t.integer "merchant_id"
    t.string "api_id"
    t.text "description"
    t.string "merchant_url"
    t.string "status", default: "active", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "product_image"
    t.boolean "pending", default: false
    t.string "color"
    t.string "size"
    t.string "slug"
    t.integer "share_count", default: 0
    t.text "shipping_dimensions"
    t.text "shipping_category"
    t.text "metadata"
    t.index ["merchant_url"], name: "index_variants_on_merchant_url"
  end

  add_foreign_key "activities", "customers"
  add_foreign_key "api_reports", "admins"
  add_foreign_key "api_reports", "customers"
  add_foreign_key "baskets", "customers"
  add_foreign_key "blockings", "customers", column: "customer_1_id"
  add_foreign_key "blockings", "customers", column: "customer_2_id"
  add_foreign_key "cart_items", "carts"
  add_foreign_key "cart_items", "giftboxes"
  add_foreign_key "cart_items", "gifts"
  add_foreign_key "cart_items", "merchant_carts"
  add_foreign_key "cart_items", "variants"
  add_foreign_key "carts", "addresses"
  add_foreign_key "carts", "payment_methods"
  add_foreign_key "charities", "baskets"
  add_foreign_key "comments", "customers"
  add_foreign_key "customer_connections", "customers", column: "customer_1_id"
  add_foreign_key "customer_connections", "customers", column: "customer_2_id"
  add_foreign_key "customers_settings", "customers"
  add_foreign_key "customers_settings", "settings"
  add_foreign_key "events", "baskets"
  add_foreign_key "followings", "customers", column: "customer_1_id"
  add_foreign_key "followings", "customers", column: "customer_2_id"
  add_foreign_key "giftboxes", "addresses"
  add_foreign_key "giftboxes", "carts"
  add_foreign_key "giftboxes", "payment_methods"
  add_foreign_key "gifts", "baskets"
  add_foreign_key "gifts", "customers"
  add_foreign_key "gifts", "variants"
  add_foreign_key "invoices", "giftboxes"
  add_foreign_key "invoices", "payment_methods"
  add_foreign_key "likes", "customers"
  add_foreign_key "merchant_carts", "giftboxes"
  add_foreign_key "merchant_carts", "merchants"
  add_foreign_key "merchant_carts", "shipping_orders"
  add_foreign_key "notifications", "activities"
  add_foreign_key "notifications", "customers"
  add_foreign_key "oauth_credentials", "customers"
  add_foreign_key "payment_methods", "addresses"
  add_foreign_key "payments", "invoices"
  add_foreign_key "payments", "payment_methods"
  add_foreign_key "shipping_orders", "addresses"
  add_foreign_key "shipping_orders", "invoices"
  add_foreign_key "variants", "merchants"
  add_foreign_key "variants", "products"
end
