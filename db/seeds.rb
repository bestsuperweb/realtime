# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

unless Rails.env.test?

  Setting.destroy_all
  Gift.destroy_all
  # Event.destroy_all #misnamed migration causing db:seed error.
  Basket.destroy_all
  Variant.destroy_all
  Product.destroy_all
  Cart.destroy_all
  Merchant.destroy_all
  Following.destroy_all
  CustomerConnection.destroy_all
  Customer.destroy_all
  ActsAsTaggableOn::Tag.destroy_all

  p "Database cleared"

  # TODO - Removed for beta launch. Add back in at some point
  #  [ "old_connection", "Someone you know joins giftibly" ],

  notification_ray = [ [ "new_connection_request", "Someone connects with you", "Connections" ],
                       [ "share_profile", "Someone shares your profile", "Sharing" ],
                       [ "receive_message", "Someone messages you", "Comments and Messages" ],
                       [ "gift_comment", "Someone comments on your wish", "Comments and Messages" ],
                       [ "basket_comment", "Someone comments on a wish list of yours", "Comments and Messages" ],
                       [ "public_events", "Public events near you", "Events" ],
                       [ "purchase_gift", "Someone buys one of your wishes for you", "Gifts for Me" ],
                       [ "like_gift", "Someone likes one of your wishes", "Likes" ],
                       [ "like_basket", "Someone likes one of your wish lists", "Likes" ],
                       [ "like_comment", "Someone likes a comment of yours", "Likes" ],
                       [ "accept_connection_request", "Someone accepts your connection request", "Connections" ],
                       [ "event_invite", "Someone invites you to an event", "Events" ],
                       [ "event_attendee_status_change", "Someone changes their status of attending your events", "Events" ],
                       [ "share_gift", "Someone shares one of your wishes", "Sharing" ],
                       [ "share_basket", "Someone shares one of your wish lists", "Sharing" ]
                     ]   

  notification_ray.each do |notification|
    Setting.create(
          name:          notification[0],
          description:   notification[1],
          options:       [ "true", "false" ],
          family:        "notification",
          group:         notification[2],
          default_value: "true"
    )   
  end 

  p "Settings seeded!"

  Merchant.create([
    { 
      name:            "Ebay",
      public_url:      "www.ebay.com",
      alexa_rank:      2,
      status:          "private",
      commission:      80.00,
      ppc_price:       0,
      cpm_price:       0,
      validation_days: 0
    }, {
      name:            "Amazon",
      public_url:      "www.amazon.com",
      alexa_rank:      1,
      status:          "private",
      commission:      8.5,
      ppc_price:       0,
      cpm_price:       215,
      validation_days: 0
    }, {
      name:            "Walmart",
      public_url:      "www.walmart.com",
      alexa_rank:      3,
      status:          "private",
      commission:      4.0,
      ppc_price:       0,
      cpm_price:       0,
      validation_days: 0
    }, {
      name:            "Etsy",
      public_url:      "www.etsy.com",
      alexa_rank:      4,
      commission:      3.4,
      ppc_price:       0,
      cpm_price:       0,
      validation_days: 0
    }, {
      name:            "Best Buy",
      public_url:      "www.bestbuy.com",
      alexa_rank:      5,
      commission:      1.0,
      ppc_price:       0,
      cpm_price:       0,
      validation_days: 0
    }, {
      name:            "Target",
      public_url:      "www.target.com",
      alexa_rank:      6,
      commission:      8.0,
      ppc_price:       0,
      cpm_price:       0,
      validation_days: 0
    }, {
      name:            "Home Depot",
      public_url:      "www.homedepot.com",
      alexa_rank:      9,
      commission:      3.0,
      ppc_price:       0,
      cpm_price:       0,
      validation_days: 0
    }, {
      name:            "Groupon",
      public_url:      "www.groupon.com",
      alexa_rank:      10,
      commission:      10.0,
      ppc_price:       0,
      cpm_price:       0,
      validation_days: 0
    }, {
      name:            "Bodybuilding.com",
      public_url:      "www.bodybuilding.com",
      alexa_rank:      21,
      commission:      5.0,
      ppc_price:       45,
      cpm_price:       225,
      validation_days: 0
    }, {
      name:            "Yoox",
      public_url:      "www.yoox.com",
      alexa_rank:      41,
      commission:      11.0,
      ppc_price:       0,
      cpm_price:       0,
      validation_days: 45
    }
  ])

  p "Merchants seeded!"

  admin = Admin.where(
    email: ENV["ADMIN_EMAIL"],
  ).first_or_initialize(
    password: ENV["ADMIN_PASSWORD"],
    name: "giftibly",
  )
  admin.save

  customer = Customer.where(
    first_name: "Giftibly",
    last_name: "Inc.",
    location: "Orlando, FL, United States",
    email: ENV["ADMIN_EMAIL"],
    username: "giftibly",
    description: "Giftibly is the the world's Social Gift Collection. We take the stress & worry out of giving by making sure you find the perfect gift, every time, guaranteed.",
    birthday: Date.new(1982, 9, 24),
    website: "www.giftibly.com",
    role: "admin"
  ).first_or_initialize(
    password: ENV["ADMIN_PASSWORD"],
  )
  customer.save

  purchaser = Purchaser.where(
    email: ENV["ADMIN_EMAIL"], 
    name: "giftibly"
  ).first_or_initialize(
    password: ENV["ADMIN_PASSWORD"],
  )
  purchaser.save

  p "Customer seeded!"

  tag_ray = ["zelda", "zeta tau alpha", "zodiac jewelry", "zippers", "zipper", "zippo", "zodiac", "zero waste", "zine", "zombie", "yarn",
             "zeta phi beta", "yarn bowl", "yoga", "yeezy", "yeti cup", "yuri on ice", "yoga mat", "yoga pants", "xray", "xray markers", 
             "xxxtenacion", "xacto", "xray lead markers", "x files", "xfiles", "xenomorph", "xray tech", "x-wing", "xbox", "unicorn", 
             "unicorn costume", "unicorn headband", "unicorn party", "unicorn slime", "unicorn invitation", "unicorn birthday", "wall art", 
             "ugly christmas sweater", "farmhouse decor", "flower girl dress", "fishbowl", "fursuit", "fabric", "fire opal", "wreath",
             "wooden serving", "wedding gift", "wedding dress", "wedding", "wedding cake topper", "wedding invitation", "wall decor", 
             "wedding signs", "wooden signs", "vintage", "vinyl decal", "vintage clothing", "vase", "vegan", "velvet", "vans", "voltron", 
             "vintage engagement ring", "vintage rings", "throw blanket", "taco", "tapestry", "t-shirt", "tshirt", "turquoise", "rug", 
             "throw pillows", "tote bag", "teacher shirts", "stickers", "sapphire", "supreme", "soap", "star wars", "star trek", "scarf", 
             "squishy", "rustic decor", "rings", "rick and morty", "resume template", "rotary cutter", "return address stamp", "pins", 
             "rose gold engagement ring", "reborn babies", "robe", "quilts", "quilting rulers", "quilting rulers and templates", "mug",
             "quilts for sale", "quilt patterns", "quotes", "quote prints", "quiet book", "patches", "personalized", "personalized gift", 
             "painting", "planner", "pillows", "purse", "opal", "opal negative", "outlander", "overwatch", "office decor", "owl", "jeep", 
             "ornaments", "necklace", "nurse", "opal engagment ring", "name necklace", "neon sign", "nose ring", "nursery decor", "baby", 
             "necklaces for women", "notebook", "newborn girl coming home outfit", "mens", "Monogram", "mens ring", "mens wedding band", 
             "mens gift", "mother of the bride gift", "magnolia wreath", "mickey ears", "leather", "laptop stickers", "lanyard", "locket",
             "leather tote", "lingerie", "lamp", "leather bag", "license plate frame", "louis vuitton", "leather bracelet", "key chain",
             "knit accessories", "keychain", "kitchen aid mixer decals", "knife", "kimono", "kitchen decor", "key fob", "kitchen canisters", 
             "kappa delta", "iphone 7 plus case", "iphone 7 case", "initial necklace", "iphone 8 plus case", "iphone case", "iron on patch", 
             "iphone 6 case", "invitation", "invitations", "iphone 6s case", "infinity", "jewelry", "journal", "jewelry box", "christmas",
             "jewelry handmade", "jacket", "jewelry organizer", "jeep decal", "japanese", "headbands", "headband", "headboard", "destiny",
             "headbands for women", "helix earring", "hedgehog", "herb grinder", "gift", "heart necklace", "hello kitty", "dog collar",
             "healing crystals and stones", "hermes", "gift for men", "game of thrones", "dog", "gift for women", "groomsmen gift", "black",
             "gucci", "gender reveal", "gifts for mom", "gold ring", "gift for her", "dress", "disney shirts", "disney", "baby girl", "red",
             "disney family shirts", "disney ears", "women", "decals", "coffee mug", "candles", "cake topper", "costume", "coasters", "car",
             "bridesmaid gift", "baby boy", "bridesmaid robes", "bath bomb", "bracelet", "bridesmaid proposal", "big little shirts", "blue",
             "art prints", "anniversary gifts for men", "art", "anniversary gifts", "apple watch band", "apron", "address stamp", "orange",
             "anniversary gift for boyfriend", "new", "white", "yellow", "green", "purple", "book", "electronic", "fashion", "art", "craft", 
             "arts and crafts", "luggage", "watch", "kitchen", "shoe", "motors", "car motors", "automotive motors", "truck motors", "rv hat",
             "motorcycle motors", "boatt motors", "aviation motors", "truck", "vehicle motors", "automotive", "automotive parts", "toy car",
             "automotive supplies", "automotive part & accessories", "automotive performance", "automotive electronics", "automotive salvage",
             "automotive atv parts", "automotive apparel", "automotive merchandise", "automotive jacket", "automotive hat", "vintage hat",
             "automotive performance & racing", "car parts", "automotive services", "automotive wholesale", "automotive paint", "rv paint",
             "car part & accessories", "car motor", "car apparel", "car electronics", "car merchandise", "car electronics", "car performance", 
             "car paint", "car performance & racing", "car salvage", "car services", "car wholesale", "car paint", "truck", "truck parts",
             "truck supplies", "truck part & accessories", "trucker hat", "truck apparel", "truck merchandise", "truck paint", "scooter",
             "truck electronics", "truck performance", "truck performance & racing", "truck salvage", "truck services", "truck wholesale", 
             "truck motor", "watercraft", "watercraft motors", "watercraft parts", "watercraft merchandise", "watercraft supplies", "rv",
             "watercraft part & accessories", "watercraft apparel", "watercraft jacket", "motorcycle", "watercraft hat", "vintage motors",
             "watercraft electronics", "watercraft performance", "watercraft performance & racing", "watercraft salvage", "vintage parts",
             "watercraft services", "watercraft wholesale", "watercraft paint", "vintage supplies", "vintage part & accessories", "boat",
             "vintage apparel", "vintage merchandise", "vintage jacket", "vintage electronics", "vintage performance", "scooter parts",
             "vintage performance & racing", "vintage salvage", "vintage services", "vintage wholesale", "vintage paint", "scooter motors",
             "scooter supplies", "scooter part & accessories", "scooter apparel", "scooter merchandise", "scooter jacket", "scooter hat", 
             "scooter electronics", "scooter performance", "scooter performance & racing", "scooter salvage", "scooter services", "trailer",
             "scooter wholesale", "scooter paint", "motorcycle", "motorcycle parts", "motorcycle accessories", "motorcycle supplies", "atv",
             "motorcycle performance", "motorcycle merchandise", "motorcycle jacket", "motorcycle apparel", "motorcycle electronics", 
             "motorcycle performance & racing", "motorcycle salvage", "motorcycle services", "motorcycle wholesale", "motorcycle paint", 
             "rv motors", "rv parts", "rv supplies", "rv part & accessories", "camper", "rv apparel", "rv merchandise", "rv jacket", 
             "rv electronics", "rv performance", "rv performance & racing", "rv salvage", "rv services", "rv wholesale", "rv paint", 
             "boat motors", "boat parts", "boat supplies", "atv jacket", "boat accessories", "boating apparel", "life jacket", "vehicle",
             "boat electronics", "boat performance", "boat salvage", "boat services", "boat wholesale", "boat paint", "trailer motors", 
             "trailer supplies", "trailer part & accessories", "trailer parts", "trailer apparel", "trailer merchandise", "trailer jacket", 
             "trailer hat", "trailer electronics", "trailer performance", "trailer performance & racing", "trailer salvage", "aviation", 
             "trailer services", "trailer wholesale", "trailer paint", "aviation motors", "aviation parts", "aviation supplies", "beauty",
             "aviation part & accessories", "aviation atv parts", "aviation apparel", "aviation merchandise", "aviation jacket", "health",
             "aviation hat", "aviation paint", "aviation electronics", "aviation performance & racing", "aviation performance", "blush",
             "aviation wholesale", "vehicle paint", "aviation paint", "camper performance & racing", "camper part & accessories", "hair",
             "camper performance", "camper services", "camper electronics", "camper merchandise", "camper wholesale", "camper parts", 
             "camper apparel", "camper salvage", "camper paint", "camper jacket", "camper hat", "vehicle performance & racing", "atv hat",
             "vehicle performance", "vehicle services", "vehicle motors", "vehicle parts", "vehicle supplies", "vehicle accessories", 
             "vehicle apparel", "vehicle merchandise", "vehicle jacket", "vehicle hat", "vehicle electronics", "vehicle salvage", "skin",
             "vehicle wholesale", "atv parts & accessories", "atv performance & racing", "atv motors", "atv parts", "atv supplies", 
             "atv part & accessories", "atv merchandise", "atv electronics", "atv performance", "atv salvage", "atv wholesale", "atv paint", 
             "makeup", "natural makeup", "makeup brush", "homeopathic makeup", "foundation", "lip stick", "eye shadow", "concealer", "bronzer",
             "eye liner", "mascara", "natural makeup", "lip gloss", "makeup spa", "makeup protection", "makeup care", "makeup health",
             "salon quality makeup", "makeup art", "makeup accessories", "eye lashes", "beauty spa", "beauty protection", "beauty art",
             "beauty salon quality", "beauty health", "beauty management", "beauty accessories", "beauty natural", "beauty homeopathic",
             "face scrub", "face cream", "eye cream", "clarifier", "purifying", "brightening", "peel-off", "skin protection",
             "skin spa", "skin care", "skin salon quality", "skin art", "skin health", "skin management", "skin accessories", "tan oil",
             "skin natural", "skin homeopathic", "lotion", "sun screen", "essential oil", "nail", "nail spa", "nail care", "nail art",
             "nail protection", "nail salon quality", "nail health", "nail management", "nail accessories", "nail natural",
             "nail homeopathic", "vitamin e", "beta carating", "hair spa", "hair care", "salon quality", "hair art", "hair management",
             "hair health", "shampoo", "conditioner", "keratin", "straightener", "organic", "organic spa", "organic protection", "spa",
             "organic protection", "organic care", "organic salon quality", "organic art", "organic health", "organic management",
             "organic accessories", "organic natural", "organic homeopathic", "tattoos", "temporary tattoos", "henna tattoos",
             "artistic tattoos", "stick-on tattoos", "colorful tattoos", "super hero tattoos", "pirate tattoos", "skull tattoos",
             "halloween tattoos", "accessories", "fashion accessories", "womens accessories", "mens accessories", "sports accessories",
             "clothing", "fall clothing", "winter clothing", "womens clothing", "mens clothing", "fashion clothing", "vintage clothing",
             "kids clothing", "suit", "fashion suit", "vintage suit", "womens suit", "kids suit", "mens suit", "sports suit", "pants",
             "jeans", "denim", "womens pants", "kids pants", "mens pants", "sports bottoms", "vintage pants", "apparel", "fashion apparel",
             "womens apparel", "kids apparel", "mens apparel", "sports apparel", "fashion t-shirt", "vintage t-shirt", "womens t-shirt",
             "kids t-shirt", "mens t-shirt", "sports t-shirt", "shorts", "fashion shorts", "vintage shorts", "womens shorts", "shoes",
             "kids shorts", "mens shorts", "sports shorts", "vest", "fashion vest", "vintage vest", "womens vest", "kids vest", "blazer",
             "mens vest", "sports vest", "waist coat", "sweater", "fashion sweater", "vintage sweater", "womens sweater", "kids sweater",
             "mens sweater", "sports sweater", "sports hoodie", "mens jeans", "womens jeans", "stretch denim", "fashion jeans", "coat",
             "vintage jeans", "kids jeans", "tennis shoes", "casual shoes", "womens shoes", "heels", "kids shoes", "mens shoes", "watches",
             "sports shoes", "vintage shoes", "fashion watches", "vintage watches", "womens watches", "kids watches", "mens watches",
             "sports watches", "fashion denim", "vintage denim", "womens denim", "kids denim", "mens denim", "stretch denim", "underwear",
             "fashion coat", "vintage coat", "womens coat", "mens coat", "sports coat", "kids coat", "fashion underwear", "swimwear",
             "fashion underwear", "vintage underwear", "womens underwear", "kids underwear", "mens underwear", "sports underwear",
             "fashion swimwear", "vintage swimwear", "womens swimwear", "kids swimwear", "mens swimwear", "sports swimwear", "audio",
             "electronics", "personal electronics", "marine electronics", "home electronics", "entertainment electronics", "video",
             "cell phone electronics", "phone electronics", "smart phone electronics", "gaming electronics", "desktop electronics",
             "computer electronics", "vr electronics", "console electronics", "network electronics", "business electronics", "cable",
             "printer electronics", "copier electronics", "80s electronics", "90s electronics", "car cable", "vehicle cable", "parts",
             "cell phone cable", "phone cable", "smart phone cable", "gaming cable", "desktop cable", "computer cable", "gps cable",
             "laptop cable", "vr cable", "console cable", "network cable", "business cable", "printer cable", "scanner cable", "90s cable",
             "copier cable", "vintage cable", "80s cable", "marine cable", "home cable", "entertainment cable", "personal cable",
             "car audio", "vehicle audio", "personal audio", "vintage audio", "marine audio", "home audio", "entertainment audio",
             "cell phone audio", "phone audio", "smart phone audio", "gaming audio", "desktop audio", "computer audio", "vr audio",
             "laptop audio", "console audio", "network audio", "business audio", "adaptor", "cell phone adaptor", "phone adaptor",
             "smart phone adaptor", "gaming adaptor", "desktop adaptor", "computer adaptor", "laptop adaptor", "wireless adaptor",
             "vr adaptor", "console adaptor", "network adaptor", "printer adaptor", "scanner adaptor", "copier adaptor", "90s adaptor",
             "vintage adaptor", "80s adaptor", "gps adaptor", "marine adaptor", "car video", "vehicle video", "entertainment video",
             "personal video", "vintage video", "marine video", "home video", "cell phone video", "phone video", "smart phone video",
             "gaming video", "desktop video", "computer video", "laptop video", "vr video", "console video", "smart phone accessories",
             "network video", "business video", "printer video", "scanner video", "copier video", "90s video", "80s video", "charger",
             "car charger", "vehicle charger", "90s charger", "80s charger", "gps charger", "cell phone charger", "smart phone charger",
             "phone charger", "gaming charger", "desktop charger", "computer charger", "laptop charger", "vr charger", "console charger",
             "network charger", "business charger", "printer charger", "scanner charger", "copier charger", "wireless charger",
             "accessories", "car accessories", "vehicle accessories", "personal accessories", "vintage accessories", "marine accessories",
             "home accessories", "entertainment accessories", "cell phone accessories", "phone accessories", "gaming accessories",
             "desktop accessories", "computer accessories", "laptop accessories", "vr accessories", "console accessories", "computer",
             "network accessories", "business accessories", "printer accessories", "scanner accessories", "copier accessories",
             "80s accessories", "90s accessories", "gps accessories", "head set", "car head set", "gaming head set", "wireless head set",
             "smart phone head set", "desktop head set", "computer head set", "laptop head set", "vr head set", "vehicle head set",
             "entertainment head set", "cell phone head set", "network head set", "business head set", "personal head set", "components",
             "vintage head set", "80s head set", "90s head set", "gps head set", "marine head set", "home head set", "laptop batteries",
             "batteries", "car batteries", "vehicle batteries", "personal batteries", "vintage batteries", "marine batteries",
             "home batteries", "entertainment batteries", "cell phone batteries", "phone batteries", "smart phone batteries", "remote",
             "gaming batteries", "desktop batteries", "computer batteries", "laptop batteries", "vr batteries", "console batteries",
             "network batteries", "business batteries", "printer batteries", "scanner batteries", "copier batteries", "80s batteries",
             "90s batteries", "gps batteries", "laptop computer", "desktop computer", "personal computer", "vintage computer",
             "80s computer", "90s computer", "gps computer", "marine computer", "home computer", "entertainment computer", "vr parts",
             "gaming computer", "car computer", "vr computer", "console computer", "network computer", "business computer", "gps parts",
             "car remote", "vehicle remote", "personal remote", "vintage remote", "marine remote", "home remote", "entertainment remote",
             "cell phone remote", "phone remote", "smart phone remote", "gaming remote", "desktop remote", "computer remote",
             "laptop remote", "vr remote", "console remote", "network remote", "business remote", "printer remote", "scanner remote",
             "copier remote", "80s remote", "90s remote", "gps remote", "cell phone components", "smart phone components", "vr components",
             "laptop components", "printer components", "scanner components", "copier components", "80s components", "console components",
             "90s components", "gps components", "marine components", "car components", "vehicle components", "phone components",
             "gaming components", "desktop components", "computer components", "vintage components", "network components", "computer parts",
             "business components", "wireless components", "speakers", "car speakers", "vehicle speakers", "personal speakers", "media",
             "vintage speakers", "marine speakers", "home speakers", "entertainment speakers", "cell phone speakers", "phone speakers", 
             "smart phone speakers", "gaming speakers", "desktop speakers", "computer speakers", "laptop speakers", "console speakers", 
             "vr speakers", "network speakers", "wireless speakers", "80s speakers", "90s speakers", "gps speakers", "cell phone parts",
             "phone parts", "smart phone parts", "gaming parts", "desktop parts", "laptop parts", "console parts", "network parts",
             "business parts", "printer parts", "scanner parts", "copier parts", "wireless parts", "surveillance", "console surveillance",
             "car surveillance", "vehicle surveillance", "personal surveillance", "vintage surveillance", "marine surveillance",
             "home surveillance", "entertainment surveillance", "cell phone surveillance", "phone surveillance", "smart phone surveillance",
             "gaming surveillance", "desktop surveillance", "computer surveillance", "laptop surveillance", "vr surveillance", "car media",
             "network surveillance", "business surveillance", "software", "smart phone software", "gaming software", "desktop software",
             "computer software", "laptop software", "vr software", "console software", "network software", "business software", "80s media",
             "printer software", "scanner software", "copier software", "wireless software", "entertainment software", "personal software",
             "vintage software", "90s software", "80s software", "gps software", "marine software", "car software", "vehicle software",
             "radar detector", "laser detector", "vehicle media", "home media", "entertainment media", "cell phone media", "vintage media",
             "90s media", "gaming media", "desktop media", "computer media", "laptop media", "vr media", "console media", "business media",
             "decorative", "decorative art", "decorative furniture", "decorative textiles", "decorative photos", "decorative posters",
             "decorative painting", "decorative coins", "decorative advertising", "decorative comics", "decorative soda", "decorative print",
             "decorative doll", "decorative bear", "decorative book", "decorative supplies", "decorative pottery", "decorative glass",
             "decorative stamps", "decorative shirt", "sci-fi", "sci-fi art", "sci-fi furniture", "sci-fi textiles", "sci-fi photos",
             "sci-fi painting", "sci-fi posters", "sci-fi painting", "sci-fi coins", "sci-fi advertising", "sci-fi comics", "sci-fi soda",
             "sci-fi print", "sci-fi doll", "sci-fi bear", "sci-fi book", "sci-fi supplies", "sci-fi pottery", "sci-fi glass", "antique",
             "sci-fi stamps", "sci-fi shirt", "decorative memorabilia", "sci-fi memorabilia", "decorative souvenirs", "sci-fi souvenirs",
             "collectible", "collectible art", "collectible furniture", "collectible textiles", "collectible photos", "collectaile doll",
             "collectible posters", "collectible painting", "collectible coins", "collectible advertising", "collectible memorabilia",
             "collectible comics", "collectible beverage", "decorative beverage", "sci-fi beverage", "collectible soda", "antique art",
             "collectible print", "collectible doll", "collectible bear", "collectible book", "collectible supplies", "antique furniture",
             "collectible memorabilia", "collectible pottery", "collectible glass", "collectible stamps", "collectible souvenirs",
             "collectible shirt", "antique textiles", "antique photos", "antique posters", "antique painting", "antique coins", "asian",
             "antique advertising", "antique comics", "antique beverage", "antique soda", "antique print", "antique doll", "vintage art",
             "antique bear", "antique book", "antique supplies", "antique memorabilia", "antique pottery", "antique glass", "asian art",
             "antique stamps", "antique souvenirs", "antique shirt", "science fiction", "science fiction art", "science fiction doll",
             "science fiction furniture", "science fiction textiles", "science fiction photos", "science fiction posters", "fantasy",
             "science fiction painting", "science fiction coins", "science fiction advertising", "science fiction comics", "medieval",
             "science fiction soda", "science fiction beverage", "science fiction print", "science fiction souvenirs", "medieval art",
             "science fiction doll", "science fiction bear", "science fiction book", "science fiction supplies", "super hero textiles",
             "science fiction memorabilia", "science fiction pottery", "science fiction glass", "science fiction stamps", "fantasy art",
             "science fiction souvenirs", "science fiction shirt", "vintage furniture", "vintage textiles", "vintage photos", "comic",
             "vintage posters", "vintage painting", "vintage coins", "vintage advertising", "vintage comics", "vintage beverage",
             "vintage soda", "vintage print", "vintage doll", "vintage bear", "vintage book", "vintage supplies", "vintage memorabilia",
             "vintage pottery", "vintage glass", "vintage stamps", "vintage souvenirs", "vintage shirt", "super hero", "super hero art",
             "super hero furniture", "super hero textiles", "super hero photos", "super hero posters", "super hero painting", "primative",
             "super hero coins", "super hero advertising", "super hero comics", "super hero beverage", "super hero soda", "primative art",
             "super hero print", "super hero doll", "super hero bear", "super hero book", "super hero supplies", "super hero souvenirs",
             "super hero memorabilia", "super hero pottery", "super hero glass", "super hero stamps", "super hero shirt", "golden age",
             "asian furniture", "asian textiles", "asian photos", "asian posters", "asian painting", "asian coins", "asian advertising",
             "asian comics", "asian beverage", "asian soda", "asian print", "asian doll", "asian bear", "asian book", "asian supplies",
             "asian memorabilia", "asian pottery", "asian glass", "asian stamps", "asian souvenirs", "asian shirt", "fantasy furniture",
             "fantasy textiles", "fantasy photos", "fantasy posters", "fantasy painting", "fantasy coins", "fantasy advertising",
             "fantasy comics", "fantasy soda", "fantasy beverage", "fantasy print", "fantasy doll", "fantasy bear", "fantasy souvenir",
             "fantasy book", "fantasy supplies", "fantasy memorabilia", "fantasy pottery", "fantasy glass", "fantasy stamps", "silver age",
             "fantasy shirt", "medieval furniture", "medieval textiles", "medieval photos", "medieval photos", "medieval memorabilia",
             "medieval painting", "medieval painting", "medieval coins", "medieval advertising", "medieval comics", "medieval beverage",
             "medieval soda", "medieval print", "medieval doll", "medieval bear", "medieval book", "medieval supplies", "medieval glass",
             "medieval stamps", "medieval souvenirs", "medieval shirt", "golden age art", "golden age furntiure", "golden age textiles",
             "golden age photos", "golden age posters", "golden age painting", "golden age coins", "golden age advertising", "comic art",
             "golden age comics", "golden age soda", "golden age beverage", "golden age print", "golden age print", "golden age print",
             "golden age doll", "golden age bear", "golden age book", "golden age supplies", "golden age memorabilia", "silver age art",
             "golden age pottery", "golden age pottery", "golden age glass", "golden age souvenirs", "golden age shirt", "primative doll",
             "primative furniture", "primative textiles", "primative photos", "primative painting", "primative coins", "primative bear",
             "primative advertising", "primative comics", "primative beverage", "primative soda", "primative print", "primative book",
             "primative supplies", "primative memorabilia", "primative pottery", "primative glass", "primative stamps", "primative shirt",
             "primative souvenirs", "silver age furniture", "silver age textiles", "silver age photos", "silver age painting", "comic",
             "silver age comics", "silver age beverage", "silver age soda", "silver age print", "silver age doll", "silver age souvenir",
             "silver age bear", "silver age book", "silver age supplies", "silver age memorabilia", "silver age pottery", "comic art",
             "silver age glass", "silver age stamps", "silver age shirt", "comics", "comic furniture", "comic textiles", "custom art",
             "comic photos", "comic posters", "comic painting", "comic coins", "comic advertising", "comic soda", "comic beverage",
             "comic print", "comic doll", "comic bear", "comic book", "comic supplies", "comic memorabilia", "comic pottery", "custom",
             "comic glass", "comic stamps", "comic souvenirs", "comic shirt", "pop", "pop art", "pop furniture", "pop memorabilia",
             "pop textiles", "pop photos", "pop posters", "pop painting", "pop coins", "pop advertising", "pop comics", "pop soda",
             "pop beverage", "pop print", "pop doll", "pop bear", "pop book", "pop supplies", "pop pottery", "pop glass", "pop shirt",
             "pop souvenir", "pop stamp", "mint condition", "mint condition art", "mint condition furniture", "mint condition print",
             "mint condition textiles", "mint condition photos", "mint condition posters", "mint condition painting", "custom shirt",
             "mint condition coins", "mint condition advertising", "mint condition comics", "mint condition beverage", "custom coins",
             "mint condition soda", "mint condition print", "mint condition doll", "mint condition bear", "mint condition souvenirs",
             "mint condition book", "mint condition supplies", "mint condition memorabilia", "mint condition pottery", "custom bear",
             "mint condition glass", "mint condition shirt", "custom furniture", "custom textiles", "custom photos", "custom painting",
             "custom posters", "custom advertising", "custom comics", "custom soda", "custom beverage", "custom print", "custom glass",
             "custom doll", "custom bear", "custom book", "custom supplies", "custom memorabilia", "custom pottery", "custom souvenirs",
             "consignment", "consignment art", "consignment furniture", "consignment textiles", "consignment photos", "consignment doll",
             "consignment posters", "consignment painting", "consignment coins", "consignment advertising", "consignment memorabilia",
             "consignment comics", "consignment beverage", "consignment soda", "consignment print", "consignment souvenir", "baby gear",
             "consignment bear", "consignment book", "consignment supplies", "consignment pottery", "consignment stamps", "baby diapers",
             "consignment glass", "baby", "baby safety", "baby bath", "baby car seats", "baby keepsakes", "baby nursery bedding", 
             "keepsakes", "baby nursery decor", "baby nursery furniture", "baby nursing", "baby potty training", "baby pregnancy pillow",
             "baby strollers", "baby toys", "bath", "bath accessory", "bath caddy", "bath storage", "bath product", "bath shelves",
             "bath cabinet", "bath mat", "bath bar", "shower curtain", "shower curtain hooks", "tissue box", "toilet paper", "soap",
             "tooth brush", "towel", "washcloth", "shampoo", "conditioner", "bedding", "pillows", "bed pillows", "blanket", "crafts",
             "comforter", "duvet", "matress", "matress pad", "matress topper", "quilt", "sheet", "pillowcase", "throw pillow", "beads",
             "kitchen & dining", "kitchen", "dining", "bakeware", "appliance", "bar stool", "cake", "cookware", "dishes", "flatware",
             "glassware", "kitchen storage", "storage", "silverware", "baking utensils", "pots", "pans", "microwave", "stove", "oven",
             "wooden utensils", "silocone utensils", "crafts", "art supplies", "jewelry making", "fabric", "sewing", "glass crafts",
             "mosaics", "craft supplies", "scrapbooking", "yarn", "needlework", "furniture", "bar stool", "bedroom set", "bed", "chair",
             "bench", "armchair", "couch", "arm chair", "cabinet", "cupboard", "office furniture", "desk", "entertainment unit", "sofa",
             "tv stand", "ottoman", "footstool", "table", "dresser", "storage unit", "furniture protector", "furniture cleaner", "tools",
             "home decor", "candle", "candle holder", "clock", "decorative pillow", "frame", "floral", "lamp", "fan", "mirror", "vase",
             "shelf", "homekeeping", "cleaner", "cleaning product", "organic cleaner", "carpet shampoo", "towels", "cloths", "rags",
             "laundry", "steamers", "vacuum", "mop", "broom", "dust pan", "trash can", "major appliances", "appliances", "cooktop",
             "dishwasher", "dryer", "freezer", "microwave oven", "range hoods", "refrigerator", "washing machine", "water filter",
             "yard & garden", "yard", "garden", "bqq grill", "fire pit", "flowers", "tree", "plant", "succulent", "perinnial",
             "patio furniture", "decking", "outdoor lighting", "lawn mower", "pressure washer", "pool", "grass seed", "weed killer",
             "bug killer", "tool set", "multi-tool", "screwdriver", "hammer", "air compressor", "flashlight", "hand tool", "ladder",
             "measuring", "tape measure", "power tool", "tool box", "tool", "tool belt", "boxing", "boxing gloves", "boxing gear",
             "boxing helmet", "boxing pants", "boxing t-shirt", "boxing shirt", "boxing guards", "boxing shoes", "boxing cleats",
             "boxing jersey", "boxing equipment", "boxing safety", "boxing goggles", "cardio", "cardio gear", "cardio shoes", "hunting",
             "cardio top", "cardio bottom", "cardio pants", "cardio video", "cardio set", "cardio equipment", "cardio goggles", "mma",
             "hunting gear", "hunting shirt", "hunting pants", "hunting equipment", "hunting safety", "hunting training", "hunting set",
             "hunting goggles", "swimming", "swimming gear", "swimming shoes", "swimming top", "swimming bottom", "swimming pants",
             "swimming video", "swimming set", "swimming equipment", "swimming goggles", "mma gloves", "mma gear", "mma helmet",
             "mma pants", "mma t-shirt", "mma shirt", "mma guards", "mma shoes", "mma cleats", "mma jersey", "mma equipment", "combat",
             "mma safety", "mma goggles", "fitness", "fitness gear", "fitness shoes", "fitness top", "fitness bottom", "fitness pants",
             "fitness video", "fitness set", "fitness equipment", "fitness goggles", "fishing", "fishing gear", "fishing equipment",
             "fishing shirt", "fishing pants", "fishing safety", "fishing training", "fishing set", "fishing goggles", "martia arts",
             "martial arts gloves", "martial arts gear", "martial arts helment", "martial art pants", "martial arts t-shirt", "gym",
             "martial arts shirt", "martial arts guards", "martial arts shoes", "martial arts cleats", "martial arts jersey", "rugby",
             "martial arts equipment", "martial arts safety", "martial arts goggles", "running", "running gear", "running shoes", 
             "running top", "running bottom", "running pants", "running video", "running set", "running equipment", "running goggles",
             "hockey", "hockey gear", "hockey shirt", "hockey pants", "hockey equipment", "hockey safety", "hockey training", "cricket",
             "hockey set", "hockey goggles", "combat gloves", "combat gear", "combat helmet", "combat pants", "combat t-shirt", "tennis", 
             "combat shirt", "combat guards", "combat shoes", "combat cleats", "combat jersey", "combat equipment", "combat safety",
             "combat goggles", "vintage gear", "vintage shoes", "vintage top", "vintage bottom", "vintage pants", "vintage equipment",
             "vintage video", "vintage set", "vintage goggles", "cricket gear", "cricket shirt", "cricket pants", "cricket equipment",
             "cricket safety", "cricket training", "cricket set", "cricket goggles", "protective", "protective gloves", "gym gear",
             "protective gear", "protective helmet", "protective pants", "protective t-shirt", "protective shirt", "protective guards",
             "protective shoes", "protective cleats", "protective jersey", "protective equipment", "protective safety", "gym shoes",
             "protective goggles", "gym top", "gym bottom", "gym pants", "gym video", "gym set", "gym equipment", "gym goggles",
             "rugby gear", "rugby shirt", "rugby pants", "rugby equipment", "rugby safety", "rugby training", "rugby set", "novelty",
             "rugby goggles", "training", "training gloves", "training gear", "training helmet", "training pants", "training t-shirt",
             "training shirt", "training guards", "training shoes", "training cleats", "training jersey", "training equipment",
             "training safety", "training goggles", "tennis gear", "tennis shoes", "tennis top", "tennis bottom", "tennis equipment",
             "tennis pants", "tennis video", "tennis set", "tennis goggles", "fencing", "fencing gear", "fencing shirt", "outdoor",
             "fencing pants", "fencing equipment", "fencing safety", "fencing training", "fencing set", "fencing goggles", "indoor",
             "cycling", "cycling gloves", "cycling gear", "cycling helmet", "cycling pants", "cycling t-shirt", "cycling shirt",
             "cycling guards", "cycling shoes", "cycling cleats", "cycling jersey", "cycling equipment", "cycling safety", "bicycle",
             "cycling goggles", "outdoor gear", "outdoor shoes", "outdoor top", "outdoor bottom", "outdoor pants", "outdoor video",
             "outdoor set", "outdoor equipment", "outdoor goggles", "novelty gear", "novelty shirt", "novelty equipment", "bowling",
             "novelty safety", "novelty training", "novelty set", "novelty goggles", "bicycle gloves", "bicycle gear", "kayaking",
             "bicycle helmet", "bicycle pants", "bicycle t-shirt", "bicycle shirt", "bicycle guards", "bicycle shoes", "gymnastic",
             "bicycle cleats", "bicycle jersey", "bicycle equipment", "bicycle safety", "bicycle goggles", "hiking equipment",
             "hiking", "hiking gear", "hiking shoes", "hiking top", "hiking bottom", "hiking pants", "hiking video", "hiking set",
             "hiking goggles", "bowling", "bowling gear", "bowling shirt", "bowling pants", "bowling equipment", "bowling safety",
             "bowling training", "bowling set", "bowling goggles", "baseball", "baseball gloves", "baseball gear", "baseball helmet",
             "baseball pants", "baseball t-shirt", "baseball shirt", "baseball guards", "baseball shoes", "baseball cleats", "yoga",
             "baseball jersey", "baseball equipment", "baseball safety", "baseball goggles", "indoor gear", "indoor shoes", "golf",
             "indoor top", "indoor bottom", "indoor pants", "indoor video", "indoor set", "indoor equipment", "indoor googles",
             "ping pong", "ping pong gear", "ping pong shirt", "ping pong pants", "ping pong equipment", "ping pong training",
             "ping pong safety", "ping pong set", "ping pong goggles", "tactical", "tactical gloves", "tactical gear", "archery",
             "tactical helmet", "tactical pants", "tactical t-shirt", "tactical shirt", "tactical guards", "tactical shoes",
             "tactical cleats", "tactical jersey", "tactical safety", "tactical goggles", "gymnastic gear", "gymnastic equipment",
             "gymnastic shoes", "gymnastic top", "gymnastic bottom", "gymnastic pants", "gymnastic video", "gymnastic set", "yoga set",
             "gymnastic goggles", "lacrosse", "lacrosse gear", "lacrosse shirt", "lacrosse pants", "lacrosse equipment", "lacrosse set",
             "lacrosse safety", "lacrosse training", "lacrosse goggles", "football", "football gloves", "football gear", "basketball",
             "football helmet", "football pants", "football t-shirt", "football shirt", "football guards", "football equipment",
             "football shoes", "football cleats", "football jersey", "football safety", "football goggles", "archery equipment",
             "archery gear", "archery shoes", "archery top", "archery bottom", "archery pants", "archery video", "golf equipment",
             "archery set", "archery goggles", "kayaking gear", "kayaking shirt", "kayaking pants", "kayaking equipment", "golf set",
             "kayaking safety", "kayaking training", "kayaking set", "kayaking goggles", "basketball gear", "basketball equipment",
             "basketball shoes", "basketball top", "basketball bottom", "basketball pants", "basketball video", "basketball jersey",
             "basketball set", "basektball goggles", "yoga gear", "yoga shoes", "yoga top", "yoga bottom", "yoga pants", "yoga video",
             "yoga equipment", "yoga goggles", "golf gear", "golf shirt", "golf pants", "golf safety", "golf training", "golf set",
             "golf goggles", "sunglasses", "sunglasses men", "sunglasses shape", "sunglasses fit", "sunglasses wooden", "phone cases",
             "wood fram sunglasses", "aviator sunglasses", "wayfair sunglasses", "phone cases for apple", "apple phone cases", "led",
             "phone cases men", "mens phone cases", "womens phone cases", "phone cases women", "phone cases liquid", "men fashion",
             "phone cases waterproof", "waterproof phone case", "waterproof phone bag", "waterproof backpack", "men fashion accessories",  
             "men watches", "men sweater", "men style", "gluten free", "gluten free dessert", "gluten free recipe", "led skin treatment",
             "led strip", "led lighting", "led lamp", "coconut oil", "3d printer", "phone power bank", "power bank", "phone charger",
             "drone", "drones", "drones best", "drones business", "drones with camera", "personal drone", "mini drone", "e-liquid",
             "remote control drone", "electric cigarettes", "e-cigs", "vape", "vapors", "coffee", "coffee scrub", "organic coffee",
             "virtual reality", "v-r", "vr viewer", "vr machine", "bluetooth", "bluetooth speakers", "bluetooth earbuds", "charcoal",
             "bluetooth adaptor", "charcoal face mask", "charcoal face mask peel", "charcoal face cleaner", "charcoal toothpaste",
             "charcoal bags", "fitness tracker", "fitness nutrition", "fitness notebook", "fitness watch", "smart watch",
             "wooden watch", "healthy", "healthy snacks", "healthy food", "healthy drinks", "healthy smoothie", "matcha", "detox tea",
             "matcha powder", "matcha green tea", "detox", "fidget spinner", "fidget cube", "gadget", "leggings", "leggings and boots",
             "leggings outfit", "fall leggings", "women leggings", "winter leggings", "gym leggings", "sports leggings"]


  tag_ray.each do |tag|
    ActsAsTaggableOn::Tag.create( name: tag, taggings_count: 0 )
  end

  p "Tags seeded!"

      # Grab merchant file
      merchant_text = File.open("merchant.txt").read
      merchant_text.gsub!(/\r\n?/, "\n")
      merchant_text.each_line do |line|
        m = JSON.parse(line)
        merchant = Merchant.find_by( name: m["name"] )

        if merchant.nil?
          merchant = Merchant.new
          merchant.name = m["name"]
          merchant.public_url = m["url"]

          merchant.save!

          p "Added #{merchant.name} merchant to our system"
        else
          p "Already had #{merchant.name} in our system"
        end 
      end 

      # Grab product file
      product_text = File.open("product.txt").read
      product_text.gsub!(/\r\n?/, "\n")
      # create a product based off of name of each row 
      product_text.each_line do |line|
        p = JSON.parse(line)
        product = Product.new
        product.name = p["name"]

        product.save!

        product.tag_list.add( p["tag_list"].downcase, parse: true )

        product.save!

        p "Added #{product.name} product to system"
      end 

      # Grab variant file
      variant_text = File.open("variant.txt").read
      variant_text.gsub!(/\r\n?/, "\n")
      # create a variant based off of each json object.
      variant_text.each_line do |line|
        v = JSON.parse( line )
        product = Product.find_by( name: v["name"] )
        merchant = Merchant.find_by( public_url: v["merchant"] )

        variant = Variant.new
        variant.name = v["name"]
        variant.description = v["description"]
        variant.price = v["price"]
        variant.color = v["color"]
        variant.size = v["size"]
        variant.merchant_url = v["merchant_url"]
        variant.api_id = v["api_id"]
        variant.product = product
        variant.merchant = merchant

        variant.remote_product_image_url = v["image"] if v["image"].present?

        variant.save! if variant.valid?

        p "Added #{variant.name} variant to system"
      end
      # connect to existing product based off of name
      # create image based off of staging copy
end
