# GIFTIBLY README

1. [Development](#setting-up-development)
2. [How to deploy](#how-to-deploy)
3. [Server setup](#server-setup)
    - [pre deployment checklist](#pre-deployment-checklist)
    - [permissions](#permissions)
    - [chromedriver](#install-chromedriver)
    - [sunspot & solr](#sunspot-and-solr)
    - [deployment notes](#deployment-notes)
4. [Accessing the console in staging and production](#accessing-the-console)
5. [Versioning & workflow](#versioning-and-workflow)
    - [New features](#new-features-and-development)
    - [Hotfixes](#hotfixes-and-patches)
    - [Bug reports and Issue Handling](#bug-reports-and-issue-handling)
6. [Testing](#testing)
7. [TODO](#todo)

### Notes
[How to use js with turbolinks](https://github.com/Giftibly/giftibly/wiki/How-to-use-JS-with-Rails-and-turbolinks)

# Setting up development

1. Email interception https://mailcatcher.me/
```
  $ gem install mailcatcher
  $ mailcatcher
  # in browser open http://localhost:1080/
```

2. Start solr
```
  $ bundle exec rake sunspot:solr:start
  $ bundle exec rake sunspot:solr:reindex
```

3. Backing up production/staging/development database
```
  # 1. backup development
  $ RAILS_ENV=development bundle exec rake db:backup

  # 2. dump production db to local and restore
  $ RAILS_ENV=production bundle exec rake db:backup 
  --- enter RDS password
  $ RAILS_ENV=production bundle exec rake db:restore
  
  # 3. Once debugging is done, you can restore development db
  $ RAILS_ENV=development bundle exec rake db:restore
```

4. Create App settings
```
  $ rake db:app_settings
```

# Deployment workflow

## How to deploy
**IMPORTANT: if deploying for the first time, go through the [pre deployment checklist](#pre-deployment-checklist) in the server setup section first**.

1. Push all local commits to the right branch
  ```
    $ git add --all 
    $ git commit -m "message"
    $ git push origin {branch}
  ```

2. Run the following on your local to deploy
  ```
    $ cap staging deploy  # for staging
    $ cap hotfix deploy   # for hotfix
    $ cap production deploy # for production

    (will be prompted to enter 'staging!', 'production!' to confirm)
  ```

## Server Setup 
(Only needs to be performed once on each new instance)

### Pre deployment checklist
1. Ensure that your local repository has the following dotenv files with the right set of env variables in the root.
  ```
    - .env.development
    - .env.staging
    - .env.production
  ```

2. Ensure that you have the right AWS credentials in `giftibly.pem`
  ```
    $ ~/.ssh/giftibly.pem
  ```

3. Ensure that all local dependencies are installed and up to date
  ```
    $ bundle install
  ```

### Permissions

1. SSH into server 
  ```
    $ ssh -i ~/.ssh/giftibly.pem ubuntu@ec2-54-196-89-160.compute-1.amazonaws.com #staging
    $ ssh -i ~/.ssh/giftibly.pem ubuntu@ec2-34-239-35-173.compute-1.amazonaws.com #production
  ```

2. Change permissions to allow the installation of RVM, ruby, and bundler:
  ```
    remote$ sudo chown -R ubuntu:ubuntu /home
  ```
3. Remove old installation of RVM and Ruby # if rvm was previously installed
  ```
    remote$ sudo apt-get --purge remove ruby-rvm 
    remote$ sudo rm -rf /usr/share/ruby-rvm /etc/rvmrc /etc/profile.d/rvm.sh
    remote$ env | grep rvm    #=> should have no output  
  ```
4. Install RVM
  ```
    \curl -L https://get.rvm.io |    bash -s stable --ruby --autolibs=enable --auto-dotfile
  ```

  Note: You may run into odd issues with the prepackaged bundler when deploying:
  ```
    LoadError: cannot load such file -- bundler/dep_proxy
  ```

  If so, uninstall and reinstall bundler on the server:
  ```
    remote $ gem uninstall -i /home/ubuntu/.rvm/gems/ruby-2.5.1@global bundler
    remote $ gem install bundler
  ```

### Install Chromedriver
1. [Install chromedriver](https://github.com/Giftibly/giftibly/wiki/Headless-chrome-on-AWS-Ubuntu-EC2)

### Setting up nginx

1. Enter nginx.conf
  ```
    remote$ sudo nano /etc/nginx/nginx.conf
  ```

2. Uncomment gzip settings, edit configs, and save
  ```
    gzip on;
    gzip_disable "msie6";

    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_min_length 256;
    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype image/svg+xml image/x-icon;
  ```

3. Restart Nginx and test
  ```
    remote$ sudo service nginx restart
    remote$ curl -H "Accept-Encoding: gzip" -I http://localhost/test.css
  ```
  
### Sunspot and Solr
For production and staging environments, a standalone instance of Solr has to be installed on the server. Only needs to be done once. For now, solr installation and deployment is not automated until we are certain that the workflow is stable.
references:
- https://lucene.apache.org/solr/guide/6_6/taking-solr-to-production.html#TakingSolrtoProduction-Planningyourdirectorystructure

1. Ensure that the app's config/sunspot.yml has the correct path and port
  ```
    production:
      solr:
        hostname: localhost
        port: 8983
        log_level: WARNING
        path: /solr/production
        solr_home: solr
        # read_timeout: 2
        # open_timeout: 0.5

    staging:
      solr:
        hostname: localhost
        port: 8983
        log_level: WARNING
        path: /solr/production # yes, we're using production as the subfolder for convenience.
        solr_home: solr
        # read_timeout: 2
        # open_timeout: 0.5
  ```

2. Install dependencies. Last supported version of Solr for Sunspot is < 7.
  ```
    remote$ sudo apt-add-repository -y ppa:webupd8team/java
    remote$ sudo apt-get update
    remote$ sudo echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
    remote$ sudo apt-get install -y oracle-java7-installer
    remote$ sudo wget http://www-us.apache.org/dist/lucene/solr/6.6.3/solr-6.6.3.tgz
    remote$ sudo tar xzf solr-6.6.3.tgz solr-6.6.3/bin/install_solr_service.sh --strip-components=2
    remote$ sudo bash ./install_solr_service.sh solr-6.6.3.tgz -f
  ```

3. Install default configs
  ```
    remote$ sudo su - solr -c "/opt/solr/bin/solr create -c production -n data_driven_schema_configs"
  ```

4. A folder with config files will be created in `remote$ /var/solr/data/production/conf`. Copy solr configs from app folder to solr standalone configs
  ```
    remote$ sudo cp www/giftibly/current/config/solr/configsets/sunspot/conf/schema.xml /var/solr/data/production/conf/schema.xml
    remote$ sudo cp www/giftibly/current/config/solr/configsets/sunspot/conf/solrconfig.xml /var/solr/data/production/conf/solrconfig.xml
  ```

5. Restart Solr and check status
  ```
    remote$ sudo service solr restart
    remote$ sudo service solr status
  ```

6. Manually reindex solr on a fresh install
  ```
    remote$ cd www/giftibly/current
    remote$ RAILS_ENV=staging bundle exec rake sunspot:solr:reindex     # for staging
    remote$ RAILS_ENV=production bundle exec rake sunspot:solr:reindex  # for production
  ```

### Deployment Notes
- if instances become unresponsive or you receive the message "connection terminated by peer", reboot the instance via the command line or the AWS web ui.

- if facing issues with the prepackaged sunspot on your local, do the following:
  ```
    $ bundle exec rake sunspot:solr:stop
    $ rm -rvf solr
    $ bundle exec rake sunspot:solr:start 
    $ bundle exec rake sunspot:solr:reindex
    
    if all else fails, reboot.
  ```

# Accessing the Console
1. SSH into the server then run the following:
  ```
    $cd www/giftibly/current

    $ RAILS_ENV=staging bundle exec rails c     # for staging
    $ RAILS_ENV=production bundle exec rails c  # for production
  ``` 
# Accessing the Logs
1. SSH into the server then run the following:
  ```
    $cd www/giftibly/current/log

    $ tail -f staging.log     # for staging
    $ tail -f production.log  # for production
  ``` 

# Versioning and workflow
```                  
`master`
=> `cap production deploy`
=========================================
  ||                              ||
  \/                              \/
`development`                     `hotfix`
=> `cap staging deploy`             => `cap hotfix deploy`
```

1. All code will be developed and pushed to individual branches.
2. Branch names must be descriptive.
3. Hotfix & patch branches must be prefixed with `hotfix-{current-master-version}` (i.e `hotfix-1.0.0-create-gift`)

### New features and development
```
                                3.PR
`master`                       ======>    'hotfix'   
=> `cap production deploy`
=========================================
  /\   
  || 2.PR                           
  ||                             1.PR
`development`                  <======    'new-feature-branch'
=> `cap staging deploy`                 
```
1. Do not submit a PR to `development` until your new code has been tested.
  ```
    $ git pull origin development
    $ rake
    $ git push origin {branch}
  ``` 
    - Submit a PR and CI will automatically test again. 
    - Once approved by CI, your branch can be merged into `development`.
    - Delete branch
2. To deploy `development` branch to staging:
  ```
    $ cap staging deploy
  ```
3. At a regular tempo (once or twice monthly), the `development` branch will be elevated to `master`
    - On Github `master`, tag the merge as a new update release (i.e `1.0.0` to `1.1.0`)
    - On Github `master`, open `app/config/initializers/version.rb` and update to new version number
4. To deploy to production
  ```
    $ cap production deploy
  ```
5. If the new release proves to be unstable, the deployment can be rolled back to the latest stable release (i.e `1.0.0`)
6. Once the new release has been deployed, pull the `master` branch to the `hotfix` branch for quick hotfixes and patches.

### Hotfixes and patches
**IMPORTANT: To avoid pushing up immature or unstable new features to production, do NOT mix or merge `hotfix` and `development` branches**
```
                            3.PR
'master'                  ======>    'development'
=> 'cap production deploy'
=========================================
  /\                              
  || 2.PR 
  ||                        1.PR
'hotfix'                  <======    'hotfix-1.0.0-fix-something'
=> 'cap hotfix deploy'                 
```

1. For hotfixes and patches, submit a PR to the `hotfix` branch, **not** the `development` branch. 
```
  $ git pull origin hotfix
  $ rake
  $ git push origin {branch}
```

  - Submit a PR to `hotfix`.
  - Once approved by CI, your branch can be merged into `hotfix`.
  - Delete branch

2. To `hotfix` to staging
```
  $ cap hotfix deploy
```

3. Once approved by CI and code review, the `hotfix` branch can be merged with `master` **at any time**.
  - On Github `master`, tag the merge as a new patch release (i.e `1.0.1`)
  - On Github `master`, open `app/config/initializers/version.rb` and update to new version number

4. Deploy to production with `$ cap production deploy`

5. If the new release proves to be unstable, the deployment can be rolled back to the latest stable release (i.e `1.0.1` to `1.0.0`)

6. Once deployed, pull the `master` branch to the `development` branch and fix conflicts.

# Bug reports and Issue handling
1. For all bugs that cannot be solved by yourself, please open a github issue tagged as `bug` with the following details:
    -  A stacktrace, and/or server logs and/or browser console.log.
    -  A description of what you expected to happen, and what actually happened.
    -  If front-end related, a screenshot or screencap video.

2. Please alert the relevant dev in slack with a link to the github issue.

3. Once quashed, the relevant dev will link to the relevant commit in the github issue and close the bug report.

# Testing
Procedure for submitting a PR
  ```
    # 1. Run all tests before submitting a PR
    $ rake

    # 2. Push all git commits to origin and submit a PR
```

### Setup
1. Setup guard for macosx
https://github.com/guard/guard/wiki/Add-Readline-support-to-Ruby-on-Mac-OS-X

### Test suite
- Javascript (We use teaspoon-jasmine for purely functional javascript tests)
- Serverside/Rails/Ruby (We use minitest)
- Integration/Acceptance (We use capybara to test entire features and systems)
- FactoryGirl

### Test Coverage Report
```
  $ open coverage/index.html
```

### CI
This project uses [Circle CI](https://circleci.com/gh/Giftibly)

### What to test
Unnecessary tests (though nice to have):
  - views (unless testing for mobile vs desktop)
  - controller tests
  - javascript functions (unless mission critical)
  - tests that only test complexity (method calls etc.)

Good things to test:
  - Integration (the most important, as they test what the end-user sees)
  - Model tests and data integrity (also important, as they ensure that the data is sound)
  - Services (only outcomes)
  - Performance and load testing
  - Edge cases (what if data is nil, is a string instead of an integer etc.)

1. As a rule of thumb, test outcomes not specifics.
2. When doing integration tests, try to test 3 states:
  - what happens when there's no data (empty state)
  - what happens when there's an error in the data (error state)
  - what happens when the outcome is as planned (desired state)
3. More tests == better, but don't get too bogged down in the specifics.
4. TDD is good, but focus on data integrity and integration tests.
5. Use factories

### Running tests
1. start solr in test environment
  ```
    $ RAILS_ENV=test bundle exec rake sunspot:solr:start
  ```
2. To run all tests
  ```
    $ rake
  ```
3. To run a single test
  ```
    $ m test/my_spec.rb:25
  ```
4. To run teaspoon tests, you can either 
  ```
    // run in the browser by 
    $ rails s
    //then open localhost:3000/teaspoon in the browser
  ```
  Or
  ```
    // run in the terminal
    $ teaspoon
  ```
5. To run guard and have your tests autorun each time you change a test or file:
  ```
    $ bundle exec guard
  ```

# TODO
### Deployment
- [x] Automate deployment
- [x] Integrate standalone Solr into staging
- [x] Integrate standalone Solr into production
- [ ] Auto migrate Solr index
- [x] Application versioning to ease deployment and rollbacks
- [ ] Blue green deployments
- [ ] Move dotenv files to s3. Enable sync with remote s3 bucket upon deployment.
- [ ] Document deployment rollback protocol
- [ ] Automate Solr deployment

### Testing and continuous deployment
- [x] Test coverage report
- [x] Circle CI

### Bugfixes
- [ ] Auto linking of production error reports to github issues.
