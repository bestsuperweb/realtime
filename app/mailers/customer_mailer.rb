class CustomerMailer < ApplicationMailer
  include Rails.application.routes.url_helpers

  def new_customer( customer )
    @customer = customer
    mail( to: "Alfred McNair <alfred@giftibly.com>", subject: "New Customer - #{@customer.name}" )
  end 

  def new_complaint( complaint )
  	@complaint = complaint
  	mail( from: @complaint.email, to: "info@giftibly.com", subject: "New Feedback by - #{@complaint.name}" )
  end

  def order_pending(invoice, giftbox)
    @shipping_order = invoice.shipping_order
    @invoice = invoice
    @giftbox = giftbox
    mail( from: "info@giftibly.com", 
          to: @shipping_order.owner.email, 
          bcc: "admin@giftibly.com",
          subject: "#{@invoice.owner.name} Your Order Has Been Received" )
  end

  def successful_order(shipping_order)
    @shipping_order = shipping_order
    @invoice = @shipping_order.invoice
    mail( from: "info@giftibly.com", 
          to: @shipping_order.owner.email, 
          subject: "#{@invoice.owner.name} Your Order Is Successful" )
  end

end
