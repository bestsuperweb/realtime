class ApplicationMailer < ActionMailer::Base
  default from: "Giftibly Inc. <info@giftibly.com>"
  layout 'mailer'
end
