class AdminController < ApplicationController
  before_action :authenticate_admin!, except: :ga_reports_resolve

  def index
    @complaints = Complaint.all
    @reports = ApiReport.all
  end

  def ga_reports_resolve
    if params[:reports].present?

      params[:reports].each do |report_id|
        report = ApiReport.find report_id

        if report.present?
          report.resolved_at = Time.now
          report.admin = current_admin
          report.save
        end 
      end 

      response = true
      flash[:success] = "Issue Resolved"
    else
      response = false
    end 

    respond_to do |format|
      format.json { render json: {
        response: response
      } } 
    end 
  end 

  def complaints_resolve
    if params[:reports].present?

      params[:reports].each do |report_id|
        report = Complaint.find report_id

        if report.present?
          report.resolved_at = Time.now
          report.admin = current_admin
          report.save
        end 
      end 

      response = true
      flash[:success] = "Issue Resolved"
    else
      response = false
    end 

    respond_to do |format|
      format.json { render json: {
        response: response
      } } 
    end 
  end 

  protected

end
