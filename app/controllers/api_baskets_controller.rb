class ApiBasketsController < BaseApiController

  def index
    if session[:customer_id].present?

      customer = Customer.find session[:customer_id]

      baskets = []

      customer.baskets.where.not(name: "Liked Products" ).order('created_at desc').each do |basket|
        baskets << { id: basket.id, name: basket.name } 
      end

      render json: {header: {customer_name: customer.name}, data: baskets}
    else
      render json: {error: 'Bad Token bro'}, status: 401
    end
  end


  #################################################################################
  #
  # create
  #
  # attributes
  #   - customer token
  #   - name
  #   - description
  #
  # Grabs the customer off of the token, and assigns to the basket. Assigns the
  # name passed as an attribute. 
  #
  #################################################################################

  def create
    if session[:customer_id].present?
      
      basket = Basket.new
      basket.name = params[:name].blank? ? "New Basket" : params[:name]
      basket.customer_id = session[:customer_id]
      basket.description = params[:description] if params[:description].present?

      if basket.valid?
        basket.save!
        render json: {response: "Successfully created #{basket.name}"}
      else
        render json: {response: "An error has occurred."}
      end
    else
      render json: {error: 'Bad Token'}, status: 401
    end
  end
end
