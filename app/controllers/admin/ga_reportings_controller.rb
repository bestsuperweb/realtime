class Admin::GaReportingsController < AdminController
  layout 'admin'

  def home
    @resolved = { 
                  "Reported Issue":   ApiReport.where( "resolved_at IS NOT NULL AND error_type = ?", "reported_issue" ).order( 'resolved_at desc' ),
                  "Unknown Merchant": ApiReport.where( "resolved_at IS NOT NULL AND error_type = ?", "unknown_merchant" ).order( 'resolved_at desc' ),
                  "Not Product Page": ApiReport.where( "resolved_at IS NOT NULL AND error_type = ?", "not_product_page" ).order( 'resolved_at desc' ),
                  "Validation":       ApiReport.where( "resolved_at IS NOT NULL AND error_type = ?", "validation" ).order( 'resolved_at desc' )
                }

    @unresolved = {
                  "Reported Issue":   ApiReport.where( resolved_at: nil, error_type: "reported_issue" ),
                  "Unknown Merchant": ApiReport.where( resolved_at: nil, error_type: "unknown_merchant" ),
                  "Not Product Page": ApiReport.where( resolved_at: nil, error_type: "not_product_page" ),
                  "Validation":       ApiReport.where( resolved_at: nil, error_type: "validation" ) 
                }
  end

end
