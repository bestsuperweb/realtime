class Admin::PurchasersController < AdminController
  before_action :set_purchaser, only: [:show, :update, :destroy, :reset_password]

  def index
    @purchasers = Purchaser.all.paginate(:page => params[:page])
  end

  def show
  end

  def new
    @purchaser = Purchaser.new
  end

  def create
    @purchaser = Purchaser.where(email: purchaser_params[:email]).first_or_initialize
    if @purchaser.new_record?
      @purchaser.password = purchaser_params[:password]
    end
    if @purchaser.save
      flash[:success] = "Purchaser saved"
      redirect_to admin_purchaser_path(@purchaser)
    else
      flash[:error] = @purchaser.errors.full_messages
      render :new
    end
  end

  def update
    if @purchaser.update(purchaser_params)
      flash[:success] = "Purchaser updated"
      redirect_to admin_purchaser_path(@purchaser)
    else
      flash[:error] = @purchaser.errors.full_messages
      render :edit
    end
  end

  def destroy
    if @purchaser.soft_delete
      flash[:success] = "Purchaser deactivated"
      redirect_to admin_purchasers_path
    else
      render :show
    end
  end

  def reset_password
    @purchaser.send_reset_password_instructions
    flash[:success] = "Password reset mail sent to #{@purchaser.email}"
    redirect_to admin_purchaser_path(@purchaser)
  end

  private

  def set_purchaser
    @purchaser ||= Purchaser.where(id: params[:id]).first
  end

  def purchaser_params
    params.require(:purchaser).permit(:email, :password, :name)
  end

end