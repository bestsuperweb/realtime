class Admin::FeedbacksController < AdminController
  layout 'admin'

  def complaints

    @resolved = {
    "Gift Assistant":   Complaint.where( "resolved_at IS NOT NULL AND error_type = ?", "Gift Assistant" ).order( 'resolved_at desc' ),
    "Login / Sign Up":  Complaint.where( "resolved_at IS NOT NULL AND error_type = ?", "Login / Sign Up" ).order( 'resolved_at desc' ),
    "Basket Help":      Complaint.where( "resolved_at IS NOT NULL AND error_type = ?", "Basket Help" ).order( 'resolved_at desc' ),
    "Life Event":       Complaint.where( "resolved_at IS NOT NULL AND error_type = ?", "Life Event" ).order( 'resolved_at desc' ),
    "Careers":          Complaint.where( "resolved_at IS NOT NULL AND error_type = ?", "Careers" ).order( 'resolved_at desc' ),
    "Advertisement":    Complaint.where( "resolved_at IS NOT NULL AND error_type = ?", "Advertisement" ).order( 'resolved_at desc' ),
    "Affiliate":   		Complaint.where( "resolved_at IS NOT NULL AND error_type = ?", "Affiliate" ).order( 'resolved_at desc' ),
    "Help":   			Complaint.where( "resolved_at IS NOT NULL AND error_type = ?", "Help" ).order( 'resolved_at desc' ),
    "Legal":   			Complaint.where( "resolved_at IS NOT NULL AND error_type = ?", "Legal" ).order( 'resolved_at desc' ),
    "Other":   			Complaint.where( "resolved_at IS NOT NULL AND error_type = ?", "Other" ).order( 'resolved_at desc' )
	}

	@unresolved = {
	"Gift Assistant": 	Complaint.where( resolved_at: nil, error_type: "Gift Assistant" ),
	"Login / Sign Up": 	Complaint.where( resolved_at: nil, error_type: "Login / Sign Up" ),
	"Basket Help": 		Complaint.where( resolved_at: nil, error_type: "Basket Help" ),
	"Life Event": 		Complaint.where( resolved_at: nil, error_type: "Life Event" ),
	"Careers": 			Complaint.where( resolved_at: nil, error_type: "Careers" ),
	"Advertisement": 	Complaint.where( resolved_at: nil, error_type: "Advertisement" ),
	"Affiliate": 		Complaint.where( resolved_at: nil, error_type: "Affiliate" ),
	"Help": 			Complaint.where( resolved_at: nil, error_type: "Help" ),
	"Legal": 			Complaint.where( resolved_at: nil, error_type: "Legal" ),
	"Other": 			Complaint.where( resolved_at: nil, error_type: "Other" )
	}

  end

  def purchases

  	@purchased_reports = PurchaseFeedback.where( purchased: true ).order( 'created_at desc' )

  	@failed_purchased_reports = PurchaseFeedback.where( purchased: false ).order( 'created_at desc' )
  	
  end

  def mailinglist

    @customers = Customer.where.not(username: ['giftibly', 'Giftibly'])

    respond_to do |format|
      format.html
      format.csv { send_data @customers.to_csv }
    end
    
  end

end

