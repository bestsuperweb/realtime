class Admin::DealsController < AdminController

  layout 'admin'

  def home
    @deals = Deal.order( name: 'asc' )
  end

  def view
    @deal = Deal.find params[:id]
  end

  def edit
    deal = Deal.find params[:id]

    deal.name                  = params[:deal][:name]

    year                       = params[:deal]["start_time(1i)"].to_i
    month                      = params[:deal]["start_time(2i)"].to_i
    day                        = params[:deal]["start_time(3i)"].to_i
    hour                       = params[:deal]["start_time(4i)"].to_i
    min                        = params[:deal]["start_time(5i)"].to_i
    deal.start_time            = DateTime.new( year, month, day, hour, min )

    year                       = params[:deal]["end_time(1i)"].to_i
    month                      = params[:deal]["end_time(2i)"].to_i
    day                        = params[:deal]["end_time(3i)"].to_i
    hour                       = params[:deal]["end_time(4i)"].to_i
    min                        = params[:deal]["end_time(5i)"].to_i
    deal.end_time              = DateTime.new( year, month, day, hour, min )

    deal.url                   = params[:deal][:url]
    deal.alt_text              = params[:deal][:alt_text]
    deal.size                  = params[:deal][:size]
    deal.google_analytics_code = params[:deal][:google_analytics_code]

    deal.ad_image              = params[:deal][:ad_image] if params[:deal][:ad_image].present?

    if deal.valid?
      deal.save
      flash[:notice] = "Successfully updated Deal"
      redirect_to home_admin_deals_path
    else
      flash[:alert] = "Something went wrong"
      redirect_to view_admin_deals_path( deal.id )
    end

  end 

  def new
    @deal = Deal.new
  end

  def create
    deal = Deal.new

    deal.name                  = params[:deal][:name]

    year                       = params[:deal]["start_time(1i)"].to_i
    month                      = params[:deal]["start_time(2i)"].to_i
    day                        = params[:deal]["start_time(3i)"].to_i
    hour                       = params[:deal]["start_time(4i)"].to_i
    min                        = params[:deal]["start_time(5i)"].to_i
    deal.start_time            = DateTime.new( year, month, day, hour, min )

    year                       = params[:deal]["end_time(1i)"].to_i
    month                      = params[:deal]["end_time(2i)"].to_i
    day                        = params[:deal]["end_time(3i)"].to_i
    hour                       = params[:deal]["end_time(4i)"].to_i
    min                        = params[:deal]["end_time(5i)"].to_i
    deal.end_time              = DateTime.new( year, month, day, hour, min )

    deal.url                   = params[:deal][:url]
    deal.alt_text              = params[:deal][:alt_text]
    deal.size                  = params[:deal][:size]
    deal.google_analytics_code = params[:deal][:google_analytics_code]

    deal.ad_image              = params[:deal][:ad_image] if params[:deal][:ad_image].present?

    if deal.valid?
      deal.save
      flash[:notice] = "Successfully created Deal"
      redirect_to home_admin_deals_path
    else
      flash[:alert] = "Something went wrong"
      redirect_to view_admin_deals_path( deal.id )
    end

  end

end
