class Admin::ShippingOrdersController < AdminController
  before_action :set_shipping_order, only: [:show, :edit, :update, :mark_as_complete]

  def index
    if params[:status]
      @shipping_orders = ShippingOrder.includes(:merchant_carts).where(status: params[:status]).order(created_at: :desc).paginate(:page => params[:page])
    else
      @shipping_orders = ShippingOrder.includes(:merchant_carts).order(created_at: :desc).paginate(:page => params[:page])
    end
  end

  def show
    @merchant_carts = @shipping_order.merchant_carts
    @owner = @shipping_order.owner
    @invoice = @shipping_order.invoice
    @shipping_address = @shipping_order.shipping_address
    @payment = @invoice.payments.first
  end

  def edit
  end

  def update
    if @shipping_order.update(shipping_order_params)
      flash[:success] = "Shipping order updated"
      redirect_to admin_shipping_order_path(@shipping_order)
    else
      flash[:error] = @shipping_order.errors.full_messages
      render :edit
    end
  end

  def mark_as_complete
    if @shipping_order.ready_to_complete? && @shipping_order.complete!
      CustomerMailer.successful_order(@shipping_order).deliver_now
      flash[:success] = "Shipping Order completed"
      redirect_to admin_shipping_orders_path
    else
      flash[:warning] = "Please complete all merchant carts"
      redirect_to admin_shipping_order_path(@shipping_order)
    end
  end

  private

  def set_shipping_order
    @shipping_order = ShippingOrder.includes(:owner, :invoice, :shipping_address, merchant_carts: [:merchant, :cart_items]).where(id: params[:id]).first
  end

  def shipping_order_params
    params.require(:shipping_order).permit(:status)
  end

end