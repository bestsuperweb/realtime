class Admin::BrandManagersController < AdminController

  layout 'admin'

  def home
    @brands = Merchant.order_by_name
  end

  def view
    @brand = Merchant.find params[:id]
  end

  def edit
    brand = Merchant.find params[:id]

    merchant = params[:merchant]

    brand.public_url      = merchant[:public_url]
    brand.alexa_rank      = merchant[:alexa_rank]
    brand.status          = merchant[:status]
    brand.commission      = merchant[:commission]
    brand.ppc_price       = merchant[:ppc_price]
    brand.cpm_price       = merchant[:cpm_price]
    brand.validation_days = merchant[:validation_days]
    brand.pending         = merchant[:pending]
    brand.icon            = merchant[:icon] if merchant[:icon].present?
    brand.screenshot      = merchant[:screenshot] if merchant[:screenshot].present?
    brand.save
    redirect_to view_admin_brand_manager_path( brand.id )

  end

end
