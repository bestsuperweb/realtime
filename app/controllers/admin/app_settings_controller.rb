class Admin::AppSettingsController < AdminController
  layout 'admin'
  before_action :set_app_settings

  def index
  end

  def edit
  end

  def update
    if @app_settings.update(app_settings_params)
      redirect_to admin_app_settings_path, notice: 'App settings updated'
    else
      render :new, error: @app_settings.errors.full_messages
    end
  end

  private

  def set_app_settings
    @app_settings = AppSetting.first
  end

  def app_settings_params
    params.require(:app_setting).permit(
      :shipping_flat_rate,
      :fast_shipping_flat_rate,
      :shipping_rate_multiplier,
      :sales_tax_rate,
      :service_fee_rate,
      :stripe_processing_rate,
      :stripe_per_transaction_fee,
      :authorization_multiplier
    )
  end

end