class Admin::DashboardsController < AdminController
  layout 'admin'

  def home
    @most_tagged = ActsAsTaggableOn::Tag.most_used
    @tags = ActsAsTaggableOn::Tag.all.order(taggings_count: :desc).paginate(:page => params[:page])
  end

end