# frozen_string_literal: true
class Admin::MerchantCartsController < AdminController
  before_action :set_merchant_cart
  before_action :set_shipping_order, only: [:show]

  def show
    @owner = @shipping_order.owner
    @invoice = @shipping_order.invoice
    @shipping_address = @shipping_order.shipping_address
    @payment = @invoice.payments.first
  end

  def update
    @merchant_cart.assign_attributes(merchant_cart_params)
    @merchant_cart.completed_by(current_admin)
    if @merchant_cart.save
      flash[:success] = 'Cart Completed'
      redirect_to admin_shipping_order_path(@merchant_cart.shipping_order_id)
    else
      flash[:warning] = @merchant_cart.errors.full_messages
      redirect_to :back
    end
  end

  def start
    @merchant_cart.assign_to(current_admin)
    if @merchant_cart.save
      flash['success'] = 'Cart ready to run'
      redirect_to admin_merchant_cart_path(@merchant_cart)
    else
      flash['error'] = 'Error preparing card'
      redirect_to admin_shipping_order_path(@merchant_cart.shipping_order_id)
    end
  end

  private

  def set_shipping_order
    @shipping_order = @merchant_cart.shipping_order
  end

  def set_merchant_cart
    @merchant_cart = MerchantCart.includes(shipping_order: :shipping_address, cart_items: :variant).where(id: params[:id]).first
  end

  def merchant_cart_params
    params.require(:merchant_cart).permit(:actual_taxes, :actual_subtotal, :actual_shipping_fee)
  end

end