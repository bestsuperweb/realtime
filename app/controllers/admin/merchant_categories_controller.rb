class Admin::MerchantCategoriesController < AdminController
  layout 'admin'
  before_action :all_merchants

  def categories
  	@categories = MerchantCategory.all.order(name: 'ASC')
  end

  def category
  	@category = MerchantCategory.find(params[:id])
  	checked_merchants = @category.category_merchants  	
  	unchecked_merchants = @all_merchants - checked_merchants
  	@checked_merchants = checked_merchants.order(name: 'ASC')
  	@unchecked_merchants = unchecked_merchants.group_by{|c| c.name.first.downcase}.sort_by{|k, v| k}
  end

  def new
  	@all_merchants = @all_merchants.group_by{|c| c.name.first.downcase}.sort_by{|k, v| k}
  end

  def add
  	merchant_list = params[:merchant_category][:merchants]
  	category = MerchantCategory.new 
  	category.name = params[:merchant_category][:name].downcase
  	category.merchants = merchant_list
  	category.screenshot = params[:merchant_category][:screenshot]

  	merchant_list.each do |id|
  		merchant = Merchant.find(id)
  		merchant.category_list.add( params[:merchant_category][:name] )
  		merchant.save
  	end

    if category.valid?
       category.save!
       flash[:success] = "You Created a New Category"
    else
       flash[:error] = "Somthing went wrong please try again"
    end

    respond_to do |format|
      format.html { redirect_to request.referer }
    end

  end

  def edit
  	new_merchants = params[:merchant_category][:merchants].present? ? params[:merchant_category][:merchants] : []
  	old_merchants = params[:merchant_category][:old_merchants].present? ? params[:merchant_category][:old_merchants] : []
  	merchant_list = new_merchants + old_merchants
  	category = MerchantCategory.find(params[:id])
  	category.name = params[:merchant_category][:name].downcase if params[:merchant_category][:name].present?
  	category.merchants = merchant_list if !merchant_list.empty?
  	category.screenshot = params[:merchant_category][:screenshot] if params[:merchant_category][:screenshot].present?

  	if !merchant_list.empty?
	  	@all_merchants.each do |merchant|
	  		if merchant.category_list.include?( params[:merchant_category][:name].downcase )
	  			merchant.category_list.delete( params[:merchant_category][:name] )
	  		end
	  		merchant.save
	  	end
	  	category.category_merchants.each do |merchant|
	  		merchant.category_list.add( params[:merchant_category][:name].downcase )	
	  		merchant.save
	  	end
  	end

  	if category.valid?
       category.save!
       flash[:success] = "Category Successfully Edited"
    else
       flash[:error] = "Somthing went wrong please try again"
    end

    respond_to do |format|
      format.html { redirect_to request.referer }
    end
  	
  end


  def delete
  	category = MerchantCategory.find(params[:id])

  	@all_merchants.each do |merchant|
  		if merchant.category_list.include?( category.name.downcase )
  			merchant.category_list.delete( category.name.downcase )
  			merchant.save
  		end
  	end

  	if category.valid?
       category.destroy
       flash[:success] = "Category Successfully Deleted"
    else
       flash[:error] = "Somthing went wrong please try again"
    end

  	respond_to do |format|
      format.html { redirect_to request.referer }
    end

  end

  private

  def all_merchants
  	@all_merchants = Merchant.all.order(name: 'ASC')
  end

end