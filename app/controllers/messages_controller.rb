class MessagesController < ApplicationController
  before_action :authenticate_customer!

  def create
    @conversation = Conversation.find(params[:conversation_id])
    prev_message = @conversation.messages.last

    @message = Message.new
    @message.body = params[:message][:body] if params[:message].present?
    @message.customer_id = current_customer.id
    @message.conversation = @conversation

    if params[:media].present?
      @message.media = params[:media]
      @message.body = params[:body] if params[:body].present?
    elsif params[:message][:media].present?
      @message.media = params[:message][:media]
    end

    @message.prev_message_id = prev_message.id if prev_message.present?

    @message.save!

    if prev_message.present?
      prev_message.next_message_id = @message.id
      prev_message.save
    end

    respond_to do |format|
      format.js
    end

  end

  def read
    #add conversation id if null do nothing

    messages = Message.joins(:conversation)
                      .where(read: false)
                      .where.not(customer_id: current_customer.id)
                      .where(conversation_id: params[:conversation_id] )
                      .where('conversations.recipient_id = ? or conversations.sender_id = ?', current_customer.id, current_customer.id)

    messages.each do |message|
      m = Message.find(message.id)
      m.read = true
      m.save!
    end

    render json: messages.count
  end

  def not_read
    # TODO - figure out what count and last_message are meant to do

    unread_messages = current_customer.number_unread_messages

    render json: {count: unread_messages, unread_messages: unread_messages, last_message: unread_messages}
  end

end
