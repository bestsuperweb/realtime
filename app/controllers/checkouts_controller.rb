class CheckoutsController < ApplicationController
  before_action :set_giftbox
  before_action :redirect_to_root, only: [:create, :show, :guest_email, :confirm, :amazon_prime]

  before_action :set_giftee
  before_action :set_merchant_cart_for_amazon_prime, only: [:amazon_prime]
  
  before_action :set_guest, only: [:confirm, :guest_email], unless: :customer_signed_in?
  before_action :redirect_to_guest_email, only: [:confirm], unless: :customer_signed_in?
  before_action :set_payment_method, only: [:confirm]
  before_action :set_shipping_address, only: [:confirm]
  before_action :set_cart_items, only: [:confirm]
  before_action :set_payment_methods, only: [:confirm]
  before_action :set_customer_shipping_addresses, only: [:confirm]
  before_action :set_current_shipping_address, only: [:confirm]
  before_action :set_giftee_shipping_address, only: [:confirm]
  before_action :set_shipping_options, only: [:confirm]

  before_action :set_shipping_order, only: [:order_placed]

  def create
    @invoice = @giftbox.checkout!
    if @invoice.authorized?
      CustomerMailer.order_pending(@invoice, @giftbox).deliver_now
      flash[:success] = "Checkout successful!"
      redirect_to order_placed_checkout_path(giftbox_id: @giftbox.id)
    else
      flash[:error] = @invoice.errors.full_messages[0]
      redirect_to confirm_checkout_path(giftbox_id: @giftbox.id)
    end
  end

  def show
    @title = "#{ @giftee.name.present? ? @giftee.name : 'Guest'} Cart"
    #@title = @giftee.name + " Cart"
  end

  def guest_email
  end

  def confirm
    @title = "#{ @giftee.name.present? ? @giftee.name : 'Guest'} Checkout"
    #@title = @giftee.name + " Checkout"
  end

  def amazon_prime
    @merchant_cart.prepare!
    redirect_to @merchant_cart.amazon_remote_cart_purchase_url
  end

  def order_placed
    @title = "Order Placed"
    @dataLayer_arr = dataLayer_arr( @giftbox )
  end

  private

  def redirect_to_root
    unless @giftbox && !@giftbox.checked_out?
      flash[:success] = @giftbox && @giftbox.checked_out? ? 'Cart already checkout out' : "Your cart is empty."
      redirect_to root_path
    end
  end

  def redirect_to_guest_email
    return true if privacy_not_compromised?
    
    redirect_to guest_email_checkout_path(giftbox_id: @giftbox.id)
  end

  def privacy_not_compromised?
    @giftbox.owner && 
    @giftbox.owner.email.present? &&
    @giftbox.shipping_address.blank? && 
    @giftbox.payment_method.blank?
  end

  def set_guest
    @guest = @current_cart.owner
  end

  def set_giftbox
    @giftbox = Giftbox.where(id: params[:giftbox_id]).first
  end

  def set_giftee
    @giftee = @giftbox.giftee
  end

  def set_shipping_options
    @shipping_options = Giftbox.shipping_options
  end

  def set_cart_items
    @cart_items = CartItem.includes(:variant).where(giftbox_id: @giftbox.id)
  end

  def set_payment_method
    @payment_method = PaymentMethod.new
  end

  def set_shipping_address
    @shipping_address = Address.new
  end

  def set_customer_shipping_addresses
    customer_shipping_addresses =  if customer_signed_in?
                                      current_customer.addresses.shipping.where(default: true).to_a << @giftbox.shipping_address
                                    else
                                      []
                                    end
    customer_shipping_addresses << @giftbox.shipping_address if @giftbox.giftee == current_customer
    @customer_shipping_addresses = customer_shipping_addresses.compact
  end 

  def set_current_shipping_address
    @current_shipping_address = @giftbox.shipping_address
  end

  def set_giftee_shipping_address
    @giftee_shipping_address = @giftee.default_shipping_address
  end

  def set_payment_methods
    @payment_methods =  if customer_signed_in?
                          current_customer.payment_methods
                        else
                          []
                        end
  end

  def set_merchant_cart_for_amazon_prime
    @merchant_cart = @giftbox.merchant_carts.where(id: params[:merchant_cart_id]).first
  end

  def set_shipping_order
    @invoice = @giftbox.invoice
    @shipping_order = @invoice.shipping_order
  end

  def dataLayer_arr( giftbox )
    return_arr = []
    giftbox.cart_items.each do |item|
      obj = Hash.new
      obj['name'] = item.variant.name
      obj['id'] = item.variant.id
      obj['price'] = item.current_price.to_f
      obj['brand'] = item.merchant_cart.merchant.name
      obj['quantity'] = item.quantity
      obj['variant'] = item.variant.color.present? ? "#{item.variant.color}" : "none"
      return_arr << obj
    end 
    return return_arr.to_json
  end

end
