class DiscoverController < ApplicationController

  before_action :available_deals

  def discover_categories
    c = categories.sort!{ | x, y | x[:name].downcase <=> y[:name].downcase }.uniq
    @categories = c.paginate(page: 1, per_page: 30) 
    @title = "Discover Categories"
    @meta_description = 'Building a gift registry? These companies have products that can be easily added to your wish lists in a few simple clicks.'
  end

  def discover_wish_lists
    b = wish_lists
    @baskets = b.paginate(page: 1, per_page: 30) 
    @title = "Discover Wish Lists"
    @meta_description = 'Take a look at some of our member\'s personal registries to get ideas for how you can set up your very own wish list on Giftibly.'
  end

  def discover_search_categories
    b = search_categories
    @brands = b.paginate(page: 1, per_page: 30) 
    @title = "#{params[:category].titleize} Category"
    @meta_description = "Discover #{params[:category].titleize} items. Add them to your personal registry, share them with friends, only on Giftibly."
    @category = MerchantCategory.friendly.find params[:category]
  end

  def discover_deals
    d = deals
    @deals = d.paginate(page: 1, per_page: 30) 
    @title = "Discover Deals"
    @meta_description = 'Occasionally we will post coupon offers for great savings on wonderful gifts. You can find them here!'
  end

#  def discover_brands
#    @title = "Discover Brands"
#    brands = Merchant.where.not( screenshot: nil).order( created_at: 'desc' )
#
#    if params[:q].present?
#      @brands = @brands.paginate(page: params[:page], per_page: 30) 
#    else
#      @brands = brands.paginate(page: params[:page], per_page: 30) 
#    end 
#  end

  def sort
    sort_then_paginate
  end

  def paginate
    sort_then_paginate
  end

  private

  def available_deals
    if !Deal.active_small.empty? || !Deal.active_large.empty?
      @deals = true
    end
  end

  def wish_lists
      baskets = Basket.where( privacy: "public", basket_type: "lifestyle" ).collect { |b| b if b.gifts.count > 7 }.compact
      return baskets 
  end

  def categories
    MerchantCategory.all.collect{|t| t}.flatten.uniq
  end

  def search_categories
    c = ActsAsTaggableOn::Tag.where( permalink: params[:category] ).first
    return c.present? ? ActsAsTaggableOn::Tagging.where( context: "categories", tag_id: c.id ).collect { |t| t.taggable }.flatten : []
  end

  def deals
      deals = []

      small_deals = Deal.active_small if !Deal.active_small.empty?
      large_deals = Deal.active_large if !Deal.active_large.empty?

      if small_deals.present?
         small_deals.each do |s|
          deals << s
         end
      end

      if large_deals.present?
         large_deals.each do |l|
          deals << l
         end
      end

      return deals
  end


  def char(discover_type, char)
    
    discover_sort = []
    
    if discover_type == 'wish_lists'

      b = wish_lists
      b.each do |basket|
        if basket.name.downcase.include?(char.downcase)
          discover_sort << basket
        end  
      end 

    elsif discover_type == 'categories'

      c = categories
      c.each do |category|
        if category[:name].downcase.include?(char.downcase)
          discover_sort << category
        end  
      end 

    elsif discover_type == 'brands' || discover_type == 'category_search'

      b = search_categories
      b.each do |brand|
        if brand.name.downcase.include?(char.downcase)
          discover_sort << brand
        end  
      end 
      
    elsif discover_type == 'deals'

      d = deals
      d.each do |deal|
        if deal.name.downcase.include?(char.downcase)
          discover_sort << deal
        end  
      end 

    end

    return discover_sort

  end

  def sort_then_paginate
    
    #WISHLISTS
    if params[:discover_type] == 'wish_lists'
      
      if params[:sort] == 'a-z'
        b = wish_lists.sort!{ | x, y | x.name.downcase <=> y.name.downcase }.uniq
      elsif params[:sort] == 'z-a'
        b = wish_lists.sort!{ | x, y | y.name.downcase <=> x.name.downcase }.uniq
      elsif params[:char].present?
        b = char( params[:discover_type], params[:char] )
      else
        b = wish_lists.shuffle
      end

      @baskets = b.paginate(page: params[:page], per_page: 30)

    #CATEGORIES
    elsif params[:discover_type] == 'categories'

      if params[:sort] == 'a-z' #A - Z
        c = categories.sort!{ | x, y | x[:name].downcase <=> y[:name].downcase }.uniq
      elsif params[:sort] == 'z-a' #Z - A
        c = categories.sort!{ | x, y | y[:name].downcase <=> x[:name].downcase }.uniq
      elsif params[:char].present?
        c = char( params[:discover_type], params[:char] )
      else #Random
        c = categories.shuffle
      end

      @categories = c.paginate(page: params[:page], per_page: 30) 
          
    #DEALS
    elsif params[:discover_type] == 'brands' || params[:discover_type] == 'category_search'

      @category = params[:category]

      if params[:sort] == 'a-z' #A - Z
        b = search_categories.sort!{ | x, y | x.name.downcase <=> y.name.downcase }.uniq
      elsif params[:sort] == 'z-a' #Z - A
        b = search_categories.sort!{ | x, y | y.name.downcase <=> x.name.downcase }.uniq
      elsif params[:char].present?
        b = char( params[:discover_type] , params[:char])
      else #Random
        b = search_categories.shuffle
      end

      @brands = b.paginate(page: params[:page], per_page: 30) 
    
    #DEALS
    elsif params[:discover_type] == 'deals'

      if params[:sort] == 'a-z' #A - Z
        d = deals.sort!{ | x, y | x.name.downcase <=> y.name.downcase }.uniq
      elsif params[:sort] == 'z-a' #Z - A
        d = deals.sort!{ | x, y | y.name.downcase <=> x.name.downcase }.uniq
      elsif params[:char].present?
        d = char( params[:discover_type], params[:char])
      else #Random
        d = deals.shuffle
      end

      @deals = d.paginate(page: params[:page], per_page: 30)

    end

    respond_to do |format|
      format.js 
    end

  end
        
end

