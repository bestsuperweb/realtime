class CommentsController < ApplicationController

  before_action :authenticate_customer!

  def create
    if params[:content].present? && params[:commentable_id].present? && params[:commentable_type].present?

      @comment = Comment.new
      @comment.customer         = current_customer
      @comment.content          = params[:content]
      @comment.commentable_type = params[:commentable_type]
      @comment.commentable_id   = params[:commentable_id]

      if @comment.valid?
        @comment.save!
        respond_to do |format|
          format.js
          format.json { render json: {
            response: true
          } }
        end
      else
        respond_to do |format|
          format.json { render json: {
            response: false
          } }
        end
      end ## ends  if comment.valid
    else
      respond_to do |format|
        format.json { render json: {
          response: false
        } }
      end
    end ## ends if params[:content]

  end

  def delete
      comment = Comment.find params[:id]
      if comment.blank?
        respond_to do |format|
          format.json { render json: {
            response: false
          } }
        end
      else
        comment.destroy
        respond_to do |format|
          format.json { render json: {
            response: true
          } }
        end
      end

  end

end
