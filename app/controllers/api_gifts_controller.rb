class ApiGiftsController < BaseApiController

  ##################################################################################
  #
  # add_tags
  #
  # attributes
  #   - gift_id *
  #   - tags *
  #
  ##################################################################################

  def add_tags
    if session[:customer_id].present?
      customer = Customer.find session[:customer_id]

      error_msg = []
      error_msg << "Gift ID" if params[:gift_id].nil?
      error_msg << "Tags" if params[:tags].nil?

      if error_msg.present?
        render json: {response: "Missing attributes: #{error_msg.join(', ')}"}
      else

        gift = Gift.find params[:gift_id]

        if gift.present?
          tag_ray = params[:tags].split(", ")
          tag_ray.each do |tag|
            if !( Obscenity.profane?( tag ) || gift.tag_list.include?( tag ) )
              gift.tag_list.add( tag.downcase )
              gift.save

              if customer.admin?
                gift.variant.product.tag_list.add( tag.downcase )
                gift.variant.product.save
              end

            end
          end 

          render json: {response: "Successfully added tags to #{gift.variant.name}"}
        else
          render json: {response: "Gift not found"}, status: 401
        end

      end
    end

  end


end
