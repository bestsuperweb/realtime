class Purchasers::BaseController < ApplicationController
  layout 'purchasers'
  before_action :authenticate_purchaser!

end
