class LikesController < ApplicationController

  before_action :authenticate_customer!

  def toggle
    begin
      if params[:likable_type].present? && params[:likable_id].present?
        likable = ( Object.const_get params[:likable_type] ).find params[:likable_id]

        if current_customer.likes? likable
          current_customer.unlike likable
          like_action = false
        else
          current_customer.like likable
          like_action = true

          a               = Activity.new
          a.activity_type = "like"
          a.customer      = current_customer
          a.actable       = likable
          a.save
        end

        if ["Variant", "Gift"].include? params[:likable_type]
          basket = Basket.find_by( name: "Liked Products", customer_id: current_customer.id )

          if params[:likable_type] == "Gift"
            if like_action
              basket.add_to_basket likable.variant

              if ( current_customer != likable.basket.customer ) && ( likable.basket.customer.setting_selected? ( "like_gift" ) ) 
                n          = Notification.new
                n.activity = a
                n.customer = likable.basket.customer

                n.save
              end
            else
              basket.remove_from_basket likable.variant
            end
          elsif params[:likable_type] == "Variant"
            if like_action
              basket.add_to_basket likable
            else
              basket.remove_from_basket likable
            end
          end
        elsif ( !["Merchant"].include? params[:likable_type] ) && like_action && ( current_customer != likable.customer )
          notify = false
          if ( params[:likable_type] == "Basket" )
            notify = true if likable.customer.setting_selected?( "like_basket" )
          elsif ( params[:likable_type] == "Comment" )
            notify = true if likable.customer.setting_selected?( "like_comment" )
          end

          if notify
            n          = Notification.new
            n.activity = a
            n.customer = likable.customer

            n.save
          end
        end

        likable.reload

        respond_to do |format|
          format.json { render json: {
            response: true, like_count: likable.likes.count
          } }
        end
      else
        respond_to do |format|
          format.json { render json: {
            response: false, message: "wrong params"
          } } 
        end 
      end
    rescue ActiveRecord::RecordNotFound => e
      respond_to do |format|
        format.json { render json: {
          response: false, message: "major error"
        } } 
      end 
    end
  end

end
