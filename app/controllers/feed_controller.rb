class FeedController < ApplicationController

  def following
    session[:last_feed] = "following"
    @multitool_feed = true
    @view_walkthrough = true
    @title = 'Following Feed'
    if current_customer.present?

      products = []
      current_customer.users_followed.map( &:baskets ).flatten.each do |basket|
        products << basket.gifts if basket.visible_to?( current_customer )
      end 

      products = products.flatten.sort_by(&:created_at).reverse

      if params[:q].present?
        @products = @products.paginate(page: params[:page], per_page: 30) 
      else
        @products = products.paginate(page: params[:page], per_page: 30) 
      end 

      @baskets = current_customer.present? ? current_customer.baskets : []
      # @events = Event.current_month_events
      # @charity = Charity.current_month_events
      # @birthdays = Customer.current_month_birthdays

      # this instance variable is for the popular tab 
      # @popular = Variant.paginate(page: params[:page],per_page:20).order('name ASC').reverse_order
    else
      @products = []
    end

  end 
  @multitool_feed = true
  def most_active
    session[:last_feed] = "most-active"
    @multitool_feed = true
    @view_walkthrough = true
    @title = 'Most Active Feed'
    @intro_active = current_customer.present? && session[:intro_active].present? && ( session[:intro_active]["feed"] == true ) if current_customer.present?
    if current_customer.present?
      products = Basket.where( [ "privacy = :privacy and customer_id != :customer_id", { privacy: "public", customer_id: current_customer.id } ] ).collect { |b| b.gifts }.flatten.sort_by(&:created_at).reverse
    else
      products = Basket.where( privacy: "public" ).collect { |b| b.gifts }.flatten.sort_by(&:created_at).reverse
    end
      
    products = products.flatten.sort_by(&:created_at).reverse

    if params[:q].present?
      @products = @products.paginate(page: params[:page], per_page: 30) 
    else
      @products = products.paginate(page: params[:page], per_page: 30) 
    end 
  end

  def products
    session[:last_feed] = "products"
    @multitool_feed = true
    @view_walkthrough = true
    @title = 'Products Feed'
    products = Variant.all.order(Arel.sql('random()'))
    @intro_active = current_customer.present? && session[:intro_active].present? && ( session[:intro_active]["feed"] == true ) if current_customer.present?

    if params[:q].present?
      @products = @products.paginate(page: params[:page], per_page: 30) 
    else
      @products = products.paginate(page: params[:page], per_page: 30) 
    end 
  end
  
  def friend_search
    search = Customer.solr_search do
      fulltext params[:friend_q] # Full text search
    end

    @customer_results = []

    # an array from the search results.
    results = search.results.map do |customer|
      # Each element will be a hash containing only the full name of customer.
      # name method - customer.rb line 163
      # binding.pry
      if ['customer', 'admin'].include? customer.role
        custo = { name: customer.name,
          avatar: customer.avatar.profile_square.url,
          username: customer.username
        }
        @customer_results.push( custo )
      end
    end

    render json: {customers: @customer_results}
  end

  def search
    query = params[:q].downcase
    # Get the search terms from the q parameter and do a search
    # We search through both searchable models - Customer & Variant
    search = Sunspot.search [Customer, Variant, Merchant] do
      fulltext query
    end

    # empty arrays for individual results
    @customer_result = []
    @product_result = []
    @merchant_result = []
    @products = []

    # if both have the first 3 matching letters, load both customer and variants
    # an array from the search results.
    results = search.results.map do |result|

      # Each element will be a hash containing only the full name of customer.

      # if results is an instance of Customer
      if result.instance_of? Customer
        # pass results into custo variable

        if ['customer', 'admin'].include? result.role
          custo = {
            name:     result.name,
            avatar:   result.avatar.profile_square.url,
            username: result.username,
            id:       result.id
          }
          @customer_result.push(custo)
        end
        # push custo variable to empty customer_result array

      # if results is an instance of Variant
      elsif result.instance_of? Variant

        # regex
        regex = Regexp.new query
        # if query duplicate. only return one
        # final_tag
        # select an individual value that match the regex

        final_tag_list = result.product.tag_list.select do |t|
          regex.match t
        end

        # empty products array
        prods = []

        final_tag_list.each do |t|
          @product_result.push({
            name:     t,
            id:       result.product.id,
            tag_list: t
          })
        end

        #secondary search to search through description and name of variants
        #by query
        description_search =  Variant.search do
                                fulltext query do
                                  fields(:description, :name)
                                end
                              end

        #push results to products array
        @products.push(description_search.results)
        @products.push(Product.tagged_with(query).map{|p| p.variants})

        #TODO check if duplicates and remove them

        @product_result.uniq! { |p| p[:name] }
        # render the json results for each.

      elsif result.instance_of? Merchant

        merch = {
          id: result.id,
          name: result.name,
          url: result.public_url
        }
        #merchant results autocomplete
        @merchant_result.push(merch)

        #load variants by merchant id search
        variant_merch_search = Variant.search do
                                 #scope query
                                 #Variants with merchant ids = results.id
                                 with(:merchant_id, result.id)
                               end
        #push merchant variatns to products
        @products.push(variant_merch_search.results)
      end
    end

    @products.flatten!
    @products.uniq!

    respond_to do |format|
      format.html {
        products
        render :products
      }
      format.js
      format.json { render json: {
        products: @product_result, customers: @customer_result, merchants: @merchant_result
      } }
    end

  end

end
