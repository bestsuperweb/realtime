class SiteController < ApplicationController
  layout "site_pages", except: [ :welcome, :search_customer ]

  def landing
    @transparent_nav = true
    @landing_page = true
    if current_customer.present?
      if session[:last_feed] == "following"
        redirect_to following_feed_path
      elsif session[:last_feed] == "products"
        redirect_to products_feed_path
      else
        redirect_to most_active_feed_path
      end
    elsif session[:browsing].present?
      redirect_to products_feed_path
    else

      if Rails.env.production?
        @baskets = Basket.includes(gifts: :variant).where(customer_id: 2, privacy: 'public').limit(12).reverse
      else
        @baskets = Basket.includes(gifts: :variant).where(customer_id: 1, privacy: 'public').limit(12).reverse
      end
    end

  end

  def unlimited_gifts
    @title = "Unlimited Gifts"
    @meta_description = '1. Sign up and join your friends on Giftibly.com. 2. Install our Gift Assistant for Google Chrome, Firefox, or Opera. 3. While at your favorite online store, click the gift icon to start wishing!'
    @intro_active = current_customer.present? && session[:intro_active].present? && ( session[:intro_active]["gifts"] == true )
  end

  def complaint

    complaint = Complaint.new
    complaint.name = params[:complaint][:name]
    complaint.email = params[:complaint][:email]
    complaint.error_type = params[:complaint][:error_type]
    complaint.body = params[:complaint][:body]
    
    if complaint.valid?
       complaint.save!
       CustomerMailer.new_complaint( complaint ).deliver_now
       flash[:success] = "Thank you your message has been received"
    else
       flash[:error] = "Somthing went wrong please try again"
    end

    respond_to do |format|
      format.html { redirect_to request.referer }
    end

  end

  def browsing
    session[:browsing] = Time.now

    respond_to do |format|
      format.json { render json: { response: true } }
      format.html
    end
  end

  def welcome
    @no_nav = true
  end

  def customers_search
    @clear_nav = true
    @title = "Search for Friends"
    @meta_description = 'Have a friend or family member on Giftibly? Type their first name and select the member you are searching for to see their Giftibly profile.'
  end

  def faq
    @title = "FAQ"
    @meta_description = 'Have Questions? We\'ve started a little list of Frequently Asked Questions. Hopefully it will help provide some clarity on your topic. If not, please contact us.'
    @clear_nav = true
  end

  def join_our_team
    @title = "Join Our Team"
    @clear_nav = true
    @meta_description = 'Soon Giftibly Inc will be looking for talented team members with a passion for growth and learning! Scroll down to see why working with Giftibly will be great!'
  end
    
  def privacy_policy
    @title = "Privacy Policy"
    @meta_description = 'Protecting your private information is our priority. This Statement of Privacy applies to the www.Giftibly.com and Giftibly Inc. and governs data collection and usage. For the purposes of this Privacy Policy...'
  end
    
  def community
    @title = "Community"
    @meta_description = 'Giftibly is a place where people who care about each other come to show how much they care by sharing, suggesting and buying fantastic gifts for each other. For our platform to work, we have to show respect to each other and to the merchants...'
  end
    
  def feedback   
    @title = "Feedback"
    @meta_description = 'Thank you for the gift of your feedback. We will do our very best to review and respond to every submission, and we have been using feedback just like yours to improve the Giftibly experience since Day One. We promise to continue that effort.'
  end

  def terms
    @title = "Terms and Conditions"
    @meta_description = 'Our site is made up of various web pages operated by Giftibly Inc. Giftibly.com is offered to you conditioned on your acceptance without modification of the terms, conditions, and notices contained herein (the "Terms"). By accessing or using our platform, you agree...'
  end
    
  def sitemaps
    redirect_to "https://#{ENV['S3_BUCKET']}.s3.amazonaws.com/sitemaps/sitemap.xml.gz"
  end

end
