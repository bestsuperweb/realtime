class GiftController < ApplicationController

  before_action :authenticate_customer!, only: [ :destroy, :share, :add_tags ]

  def view
    begin
      @gift = Gift.friendly.find params[:id]
      @multitool_gift = true
      @clear_nav = true

      @variant = @gift.variant
      @basket = @gift.basket
      @title = @variant.name + ' (@' + @basket.customer.username + ')'
      @meta_description = @basket.customer.name + ' (@' + @basket.customer.username + '). Wish List: ' + @basket.name + '. Wish Name: ' + @variant.name + '. Price: $' + @variant.pretty_price.to_s + (@variant.size.present? ? '. Size: ' + @variant.size : '') + (@variant.color.present? ? '. Color: ' + @variant.color : '') + '.'

      @baskets_arr = []
      @connection_arr = []

      if current_customer.present?

        @baskets = current_customer.baskets
        @product_discussion = true

        # search baskets autocomplete
        if current_customer.baskets.present?
          current_customer.baskets.each do |basket|
            @baskets_arr << basket.name
          end
        end

        # search connections autocomplete
        if current_customer.connections.present?
          # connections
          current_customer.connections.each do |connection|
            @connection_arr << connection.name
          end
        end
        
      else
        @baskets = []
        @product_discussion = false
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:error] = "Sorry this gift no longer exists"
      redirect_to root_path
    end
  end

  def rate
    @gift = Gift.friendly.find( params[:id] )

    if @gift.customer_id == current_customer.id

      ratings = params[:score].to_i || 0
      if @gift.update_attributes(rating: ratings)
        respond_to do |format|
          format.json {render json:{
            response: 'success'
          }}
        end
      else
        respond_to do |format|
          format.json {render json:{
            response: 'error'
          }}
        end
      end
    end

  end

  def destroy
    gift = Gift.find_by_id(params['gift_id'])

    if gift.present?
      gift.destroy

      respond_to do |format|
        format.html { redirect_to current_basket_view_path }
        format.json { render json: {
          response: "hooray"
        } }
      end
    else
      respond_to do |format|
        format.html { redirect_to root_path }
        format.json { render json: {
          response: "Oh no!"
        } }
      end
    end
  end

  def recieved
    gift = Gift.find params[:id]
    if gift.blank?
      respond_to do |format|
        format.json { render json: { response: false } }
      end
    else
      gift.quantity_received = gift.quantity_desired
      gift.owner_marked_received = true
      gift.save!
      respond_to do |format|
        format.json { render json: { response: true } }
      end
    end
  end

  def quantity_change
    gift = Gift.find params[:id]
    if gift.blank?
      respond_to do |format|
        format.json { render json: { response: false } }
      end
    else
      
      if params[:quantity_type] == 'received'
        gift.quantity_received = params[:quantity]
        gift.save!
        respond_to do |format|
          format.json { render json: { response: true, quantity_received: gift.quantity_received} }
        end
      else
        gift.quantity_desired = params[:quantity]
        gift.save!
        respond_to do |format|
          format.json { render json: { response: true } }
        end
      end

    end
  end

  def add_tags
    gift = Gift.find( params[:id] )
    tag_return = []
 
   if gift.blank?
      respond_to do |format|
        format.json { render json: { response: false } }
      end
    else
      tag_ray = params[:tags].split(", ")
      tag_ray.each do |tag|
        if !( Obscenity.profane?( tag ) || gift.tag_list.include?( tag ) )
          gift.tag_list.add( tag.downcase )
          gift.save
          tag_return << tag.downcase
        end
      end
      respond_to do |format|
        format.json { render json: { response: true, tags: tag_return } }
      end
    end
  end

  def caption
    gift = Gift.find( params[:id] )
    if gift.present?
      gift.caption = params[:gift][:caption]
      gift.save!
      flash[:success] = "Caption Added"
    else
      flash[:error] = "Something went wrong please try again."
    end
    redirect_to request.referrer
  end

  def confirm
    gift = Gift.find( params[:gift_id] ) if  params[:gift_id].present?
    reason = params[:reason] if  params[:reason].present?
    variant = Variant.find( params[:variant_id] ) if  params[:variant_id].present?
    suprise = params[:suprize_gift] if params[:suprize_gift].present?

    if gift.blank? && variant.blank? && reason.blank?
      flash[:error] = "Sorry somthing went wrong"
      redirect_to request.referrer
    else
      
      if gift
        respond_to do |format|

          data                          = {}
          data['actable_id']            = gift.id
          data['actable_type']          = 'gift'
          data['customer_id']           = gift.customer.id
          
          if reason
            
            format.js { render js: "new toastr.success('Thanks for the feedback');
            $('#leave_modal').modal('hide'); " }

            data['quantity']            = 0
            data['purchased']           = false
            data['surprise']            = false
            data['confirmation_number'] = nil
            data['feedback']            = params[:reason]


          elsif suprise != 'on'
            
            if current_customer.present?         
              gift.purchase_responses( current_customer )
            else
              gift.purchase_responses( "Guest" )
            end

            format.js { render js: "new toastr.success('#{gift.customer.name} will be notified of the gift.');
            $('#leave_modal_tab_two').hide();  
            $('#leave_modal_tab_four').collapse('show'); " }

            data['quantity']            = gift.quantity_received
            data['purchased']           = true
            data['surprise']            = false
            data['confirmation_number'] = params[:order_confirmation_number] if params[:order_confirmation_number].present?
            data['feedback']            = nil

          else

            format.js { render js: "new toastr.success('This will be a suprise gift for #{gift.customer.name}');
            $('#leave_modal_tab_two').hide();  
            $('#leave_modal_tab_four').collapse('show'); " }

            data['quantity']            = gift.quantity_received
            data['purchased']           = true
            data['surprise']            = true
            data['confirmation_number'] = params[:order_confirmation_number] if params[:order_confirmation_number].present?
            data['feedback']            = nil

          end
          #ADD FEEBACK
          gift.customer.purchase_feedback(data)
        end
        
      elsif variant
        respond_to do |format|
          
          data                          = {}
          data['actable_id']            = variant.id
          data['actable_type']          = 'variant'
          data['customer_id']           = current_customer.id

          if reason 

            format.js { render js: "new toastr.success('Thanks for the feedback');
            $('#leave_modal').modal('hide'); " }

            data['purchased']           = false
            data['quantity']            = 0
            data['surprise']            = false
            data['confirmation_number'] = nil
            data['feedback']            = params[:reason]

          else

            format.js { render js: "new toastr.info('Thanks for purchasing this product');
            $('#leave_modal_tab_two').hide();  
            $('#leave_modal_tab_four').collapse('show'); " }

            data['purchased']           = true
            data['quantity']            = 1
            data['surprise']            = suprise.present? ? true : false
            data['confirmation_number'] = params[:order_confirmation_number] if params[:order_confirmation_number].present?
            data['feedback']            = nil

          end
          #ADD FEEBACK
          current_customer.purchase_feedback(data)
        end
      end
      
    end

  end

end
