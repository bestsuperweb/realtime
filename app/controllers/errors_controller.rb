class ErrorsController < ApplicationController

  def show
    err_status = params[:status]
    if err_status == '301'
      redirect_to root_path
    elsif err_status == '404'
      @no_nav = true
      @status = { code: err_status, message: 'Page Not Found', title: 'Looks like you got lost. How can we help?' }
    elsif err_status == '500'
      @no_nav = true
      @status = { code: err_status, message: 'Server Error', title: 'Oops! Something went wrong on our end. <br> <a class="btn btn-lg" href="/feedback">Please Report the Issue</a>'.html_safe }
    end
  end

end
