class BaseApiController < ApplicationController
  protect_from_forgery with: :null_session
  before_action :authenticate_user_from_token
  
  skip_before_action :set_current_cart
  skip_before_action :configure_permitted_parameters
  skip_before_action :browsing_check
  skip_before_action :load_convos 

  private

  def authenticate_user_from_token
    unless authenticate_with_http_token { |token, options| 
        session[:customer_id] = Customer.find_by(auth_token: token).id 
      }
      render json: {error: 'Bad Token'}, status: 401
    end
  end

  def set_api_customer
    @current_customer = current_customer
    if @current_customer.blank? && request.headers['Authorization'].present?
      unless authenticate_with_http_token { |token, options| 
        @current_customer = Customer.find_by(auth_token: token)
      }
      render json: {error: 'Bad Token'}, status: 401
      end
    end
  end

end
