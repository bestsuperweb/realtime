class ApiErrorsController < BaseApiController
  protect_from_forgery with: :null_session
  before_action :get_report, only: [:confirm]

  #################################################################################
  #
  # submit
  #
  # attributes
  #   - customer token
  #   - url
  #   - error_type
  #   - note
  #
  # In the case the Gift Assistant errors, an ApiError will be created and
  # submitted to the system.
  #
  #################################################################################

  def submit
    if session[:customer_id].present?
     
      report             = ApiReport.new
      report.error_type  = params[:error_type]
      report.customer_id = session[:customer_id]
      report.url         = params[:url]
      report.note        = params[:note]

      if report.valid?
        report.save!

        render json: {response: "Successfully submitted issue"}
      else
        render json: {response: "An error has occurred."}
      end
    else
      render json: {error: 'Bad Token'}, status: 401
    end
  end

  def confirm
    if @api_report.present?
      @api_report.customer_confirmed! 
      render json: {response: @api_report.to_json}, status: 200
    else
      render json: {error: 'Not found'}, status: 401
    end
  end

  private

  def get_report
    @api_report = ApiReport.where(id: api_report_params[:id]).first
  end

  def api_report_params
    params.require(:api_report).permit(:id)
  end

end
