class Customers::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  # You should configure your model like this:
  # devise :omniauthable, omniauth_providers: [:twitter]

  def facebook
    oauthorize "Facebook"
  end

  def google_oauth2
    oauthorize "Google+"
  end

  def linkedin
    oauthorize "LinkedIn"
  end

  def twitter
    oauthorize "Twitter"
  end

private

  def oauthorize(kind)
    session[:oauth_return] = current_customer.present?
    @customer = Customer.from_omniauth(kind, request.env["omniauth.auth"], current_customer)
    @customer.update(:fromfb => true) unless session[:oauth_return]
    if @customer.present?
      @customer.reactivate if @customer.deactivated?
      if @customer.sign_in_count < 2
        session[:intro_active] = { feed: true, settings: true, profile: true, gifts: true }
      else
        session[:intro_active] = { feed: false, settings: false, profile: false, gifts: false }
      end
      if (kind == "Facebook") && @customer.persisted?
        set_flash_message( :success, :success, kind: "Facebook" ) if is_navigational_format?
        sign_in_and_redirect @customer, event: :authentication
      elsif @customer
        flash[:success] = I18n.t "devise.omniauth_callbacks.success", kind: kind
        session["devise.#{kind.downcase}_data"] = request.env["omniauth.auth"]
        sign_in_and_redirect @customer
      end
    else
      flash[:error] = "Something went wrong. Please try again."
      redirect_to new_customer_session_path
    end
  end

  # More info at:
  # https://github.com/plataformatec/devise#omniauth

  # GET|POST /resource/auth/twitter
  # def passthru
  #   super
  # end

  # GET|POST /users/auth/twitter/callback
  # def failure
  #   super
  # end

  # protected

  # The path used when OmniAuth fails
  # def after_omniauth_failure_path_for(scope)
  #   super(scope)
  # end

end
