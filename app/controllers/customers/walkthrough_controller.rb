class Customers::WalkthroughController < ApplicationController

  def pause
    if [ 'feed', 'profile', 'settings', 'gifts' ].include? params[:page]

      session[:intro_active][params[:page]] = false

      respond_to do |format|
        format.json { render json: { response: true } }
      end
    else
      respond_to do |format|
        format.json { render json: { response: false } }
      end
    end
  end

end
