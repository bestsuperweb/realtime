class Customers::ViewsController < ApplicationController

  before_action :authenticate_customer!, only: :deactivate

  def show
    @multitool_profile = true
    @view_walkthrough = true
    @customer = Customer.find_by_username( params[:username] )
    @clear_nav = true

    if @customer.blank? || @customer.deactivated? || ( current_customer.present? && !( @customer.not_blocked? current_customer ) )
      flash[:error] = "Sorry this page is forbidden!"
      redirect_to root_path
    else
      @title = @customer.name + ' (@' + @customer.username + ')'
      @meta_description = 'See what ' + @customer.name + ' (@' + @customer.username + ') has wished for on Giftibly. ' + @customer.name + ': ' + @customer.followers.count.to_s + ' followers, ' + @customer.users_followed.count.to_s + ' following, ' + @customer.baskets.count.to_s + ' Wish Lists. Join Giftibly to connect with ' + @customer.name + ' and others you may know.'
        
      @intro_active = ( current_customer.present? ) && ( session[:intro_active].present? ) && ( session[:intro_active]["profile"] == true ) && ( current_customer == @customer )

      if current_customer.present? && current_customer == @customer
        #WISH LIST 
        @all_wishlist   = current_customer.baskets.includes(gifts: :variant).to_a.flatten.sort_by(&:created_at).reverse
        @wishlist       = @all_wishlist.paginate(page: 1, per_page: 12)
        @wishlist_arr   = easyAutocomplete(@all_wishlist, 'connection', 1)
        @all_wishes     = current_customer.gifts
      elsif current_customer.present? && current_customer.connected_to?(@customer)
        #WISH LIST 
        @all_wishlist   = @customer.baskets.where( privacy: ['public', 'friends'] ).includes(gifts: :variant).to_a.flatten.sort_by(&:created_at).reverse
        @wishlist       = @all_wishlist.paginate(page: 1, per_page: 12)
        @wishlist_arr   = easyAutocomplete(@all_wishlist, 'connection', 1) 
        @all_wishes     = @customer.baskets.where( privacy: ['public', 'friends'] ).collect { |b| b.gifts }.flatten
      else
        #WISH LIST 
        @all_wishlist   = @customer.baskets.where( privacy: "public" ).to_a.flatten.sort_by(&:created_at).reverse
        @wishlist       = @all_wishlist.paginate(page: 1, per_page: 12) 
        @wishlist_arr   = easyAutocomplete(@all_wishlist, 'connection', 1) 
        @all_wishes     = @customer.baskets.where( privacy: 'public').collect { |b| b.gifts }.flatten
      end

    end

  end

  def wishlist
    
    #WISH LIST 
    @customer = Customer.find_by_username( params[:username] ) if present? || current_customer

    if @customer == current_customer
      @all_wishlist = current_customer.baskets.to_a.flatten.sort_by(&:created_at).reverse
    elsif current_customer.present? && current_customer.connected_to?(@customer)
      @all_wishlist = @customer.baskets.where( privacy: ['public', 'friends'] ).to_a.flatten.sort_by(&:created_at).reverse
    else
      @all_wishlist = @customer.baskets.where( privacy: "public" ).to_a.flatten.sort_by(&:created_at).reverse
    end

    if params[:page].present?
      @wishlist = @all_wishlist.paginate(page: params[:page], per_page: 12) 
      #WISH LIST AUTOCOMPLETE
      @wishlist_arr = easyAutocomplete(@all_wishlist, 'connection', params[:page])
    end 

    respond_to do |format|
      format.js
    end
    
  end

  def wishes
    #WISHES
    @customer = Customer.find_by_username( params[:username] ) if present? || current_customer

    if @customer == current_customer
      @all_wishes =  @customer.baskets.where( privacy: "public" ).collect { |b| b.gifts }.flatten.sort_by(&:created_at).reverse
    elsif current_customer.present? && current_customer.connected_to?(@customer)
      @all_wishes = @customer.baskets.where( privacy: ['public', 'friends'] ).collect { |b| b.gifts }.flatten.sort_by(&:created_at).reverse
    else
      @all_wishes = @customer.baskets.where( privacy: "public" ).collect { |b| b.gifts }.flatten.sort_by(&:created_at).reverse
    end

    if params[:page].present?
      @wishes = @all_wishes.paginate(page: params[:page], per_page: 25) 
      #WISHES AUTOCOMPLETE
      @wishes_arr = easyAutocomplete(@all_wishes, 'wishes', params[:page])
    end 

    respond_to do |format|
      format.js 
    end
    
  end


  def connection
    @customer = Customer.find_by_username( params[:username] ) if present? || current_customer
    @connection_type = params[:connection_type]

    #FRIENDS
    if @connection_type == 'friends'
    
      if @customer == current_customer
        @all_connections = current_customer.connections.flatten
      else
        @all_connections = @customer.connections.flatten
      end

      if params[:page].present?
        @connections = @all_connections.paginate(page: params[:page], per_page: 12)      
        #FRIENDS AUTOCOMPLETE
        @connection_arr = easyAutocomplete(@all_connections, 'connection', params[:page])
      end 

    #FOLLOWING
    elsif @connection_type == 'following'

      if @customer == current_customer
        @all_connections = current_customer.users_followed.flatten
      else
        @all_connections = @customer.users_followed.flatten
      end

      if params[:page].present?
        @connections = @all_connections.paginate(page: params[:page], per_page: 12) 
        #FOLLOWING AUTOCOMPLETE
        @connection_arr = easyAutocomplete(@all_connections, 'connection', params[:page])
      end 

    #FOLLOWERS
    elsif @connection_type == 'followers'

      if @customer == current_customer
        @all_connections = current_customer.followers.flatten
      else
        @all_connections = @customer.followers.flatten
      end

      if params[:page].present?
        @connections = @all_connections.paginate(page: params[:page], per_page: 12) 
        #FOLLOWERS AUTOCOMPLETE
        @connection_arr = easyAutocomplete(@all_connections, 'connection', params[:page])
      end 

    #PENDING
    elsif @connection_type == 'pending'

      if @customer == current_customer
        @all_connections = current_customer.connection_requests.flatten
      else
        @all_connections = @customer.connection_requests.flatten
      end

      if params[:page].present?
        @connections = @all_connections.paginate(page: params[:page], per_page: 12) 
        #PENDING AUTOCOMPLETE
        @connection_arr = easyAutocomplete(@all_connections, 'connection', params[:page])
      end 

    #BLOCKED
    elsif @connection_type == 'blocked'

      if @customer == current_customer
        @all_connections = current_customer.all_blocked.flatten
      else
        @all_connections = @customer.all_blocked.flatten
      end

      if params[:page].present?
        @connections = @all_connections.paginate(page: params[:page], per_page: 12) 
        #BLOCKED AUTOCOMPLETE
        @connection_arr = easyAutocomplete(@all_connections, 'connection', params[:page])
      end 
      
    end

    respond_to do |format|
      format.js 
    end

  end

  def deactivate
    current_customer.deactivate
    flash[:success] = "Your account was deleted. To sign in, make a new account."

    current_customer.destroy
    sign_out_and_redirect( current_customer )
  end

  def fb_popup_session
    if (params['show'])
      session[:fbpopup] = 'yes'
    else
      session[:fbpopup] = 'no'
    end
    render json: { response: session[:fbpopup] }
  end

  def fb_popup_db
    current_customer.update(:fbpopup => params[:show])
    render json: {response: "add fbpopup #{params[:show]}"}
  end

  def fromfb
    current_customer.update(:fromfb => params[:show])
    render json: {response: "add fromfb #{params[:show]}"}
  end

end
