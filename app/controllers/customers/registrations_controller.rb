class Customers::RegistrationsController < Devise::RegistrationsController
# before_action :configure_sign_up_params, only: [:create]
# before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  def create
    if Customer.find_by( email: params[:customer][:email] ).present?
      flash[:error] = "That email already exists."
      redirect_to root_path
    else
      c = Customer.new
      c.first_name = params[:name].split(" ").first
      c.last_name = params[:name].split(" ").last
      c.email = params[:customer][:email]
      c.password = params[:customer][:password]
      c.generate_username

      c.save!

      flash[:notice] = "Welcome to Giftibly! Successfully create a new account!"
      # KEEP in case we still need
      #super
      session[:intro_active] = { feed: true, settings: true, profile: true, gifts: true }
      sign_in_and_redirect( c, scope: :customer)
    end
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    super
  end

  # DELETE /resource
  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  def cancel
    super
  end

  # protected

  def validate str
    chars = ('a'..'z').to_a + ('A'..'Z').to_a + ('0'..'9').to_a
    str.chars.detect {|ch| !chars.include?(ch)}.nil?
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
