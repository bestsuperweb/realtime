class Customers::ProfileController < ApplicationController

  before_action :authenticate_customer!
    

  def view
    @multitool_settings = true
    @view_walkthrough = true
    @current_address = current_customer.shipping_address.nil? ? Address.new : current_customer.shipping_address
    @clear_nav = true
    @title = 'Settings'
    @birthdays = []
    current_customer.connections.each do |customer|
      @birthdays << customer if customer.birthday.present? && customer.birthday.month == DateTime.now.month
    end
    @intro_active = session[:intro_active].present? && ( session[:intro_active]["settings"] == true )

    # Might be something I shove into the customer model
    current_month = DateTime.now
    current_key = current_month.strftime("%B %Y")
    @events_collection = {}
    @events_collection[current_key] = []

    @connection_settings = Setting.where( group: "Connections" )
    @sharing_settings = Setting.where( group: "Sharing" )
    @comments_settings = Setting.where( group: "Comments and Messages" )
    @events_settings = Setting.where( group: "Events" )
    @gifts_settings = Setting.where( group: "Gifts for Me" )
    @likes_settings = Setting.where( group: "Likes" )

    if current_customer.upcoming_events.present?
      current_customer.upcoming_events.each do |event|
        if ( current_month.month == event.occurs_at.month ) && ( current_month.year == event.occurs_at.year )
          @events_collection[current_key] << event 
        else
          current_month = event.occurs_at
          current_key = current_month.strftime("%B %Y")

          @events_collection[current_key] = [ event ]
        end
      end
    end
  end

  def profile_update
    redirect_to root_path if !request.patch?

    reason = []

    reason << "First name required" if params['customer']['first_name'].blank?
    reason << "Last name required" if params['customer']['last_name'].blank?
    reason << "Username required" if params['customer']['username'].blank?

    customer_check = Customer.find_by_username(params['customer']['username'])

    reason << "Need a unique username" if (customer_check != current_customer) && customer_check.present?

    if reason.empty?

      current_customer.first_name     = params['customer']['first_name']
      current_customer.last_name      = params['customer']['last_name']
      current_customer.username       = params['customer']['username']
      current_customer.description    = params['customer']['description']
      current_customer.location       = params['customer']['location']
      current_customer.birthday       = DateTime.strptime( params['customer']['birthday'], "%m/%d/%Y") if params['customer']['birthday'].present?
      current_customer.employer       = params['customer']['employer']
      current_customer.job_start_date = DateTime.strptime( params['customer']['job_start_date'], "%m/%d/%Y") if params['customer']['job_start_date'].present?
      current_customer.website        = params['customer']['website']

      if current_customer.valid?
        current_customer.save!
        flash[:success] = "Successfully updated your profile"
        redirect_to request.referer

      else
        flash[:error] = "Something went wrong. Please try again."
        redirect_to request.referer
      end
    else
      flash[:alert] = "Cannot validate at this time: " + reason.join(", ")
      redirect_to request.referer
    end
  end

  def profile_verify
    if current_customer.valid_password?( params['password_varify'] )
      respond_to do |format|
        format.js
      end
    else
      respond_to do |format|
        format.js { render :js => " toastr.error('Incorrect password please try again') "}
      end
    end
  end

  def profile_update_email
    current_customer.email = params['customer']['email']
    if current_customer.valid?
      current_customer.save!
       flash[:success] = "Email updated"
    else
       flash[:error] = "Sorry this email is taken"
    end
      redirect_to profile_view_path( anchor: 'info')
  end

  def basic_update
    redirect_to root_path if !request.patch?

    reason = []

    # TODO: I don't think the password is saving correctly or has correct validation. Fix later
    reason << "Old password incorrect" if ( params['old_password'].present? && params['new_password'].present? && current_customer.valid_password?(params['old_password']) )

    # Exclusive or - will only return true if only one of the passwords is blank
    reason << "Both password fields need an entry" if params['old_password'].blank? ^ params['new_password'].blank?

    if reason.empty?

      current_customer.first_name = params['customer']['first_name'] if params['customer']['first_name'].present?
      current_customer.last_name = params['customer']['last_name'] if params['customer']['last_name'].present?
      current_customer.username = params['customer']['username'] if params['customer']['username'].present?

      current_customer.email = params['customer']['email'] if params['customer']['email'].present?
      current_customer.gender = params['gender_other'].present? ? params['gender_other'] : params['customer']['gender']
      current_customer.private = params['customer']['private']

      current_customer.password = params['new_password'] if params['new_password'].present?

      if current_customer.valid?
        current_customer.save!


        flash[:success] = "Successfully updated your profile"
      else
        flash[:error] = "Something went wrong. Please try again."
      end
    else
      flash[:alert] = "Cannot validate at this time: " + reason.join(", ")
    end
    redirect_to profile_view_path( anchor: 'info')
  end

  def shipping_update
    redirect_to root_path if !request.patch?

    reason = []

    reason << "First address line required" if params['address']['address_1'].blank?
    reason << "City required" if params['address']['city'].blank?
    reason << "State required" if params['address']['state'].blank?
    reason << "Zip Code required" if params['address']['postal_code'].blank?

    if reason.empty?
      address = current_customer.shipping_address.nil? ? Address.new : current_customer.shipping_address

      address.address_1 = params['address']['address_1']
      address.address_2 = params['address']['address_2']
      address.instructions = params['address']['instructions']
      address.city = params['address']['city']
      address.state = params['address']['state']
      address.postal_code = params['address']['postal_code']
      address.owner = current_customer
      address.address_type = "shipping"

      if address.valid?
        address.save!

        flash[:success] = "Successfully updated your shipping address"
      else
        flash[:error] = "Something went wrong. Please try again."
      end
    else
      flash[:error] = "Cannot validate at this time: " + reason.join(", ")
    end
    redirect_to profile_view_path( anchor: 'shipping')
  end

  def notification_update
    Setting.where( family: "notification").each do |setting|
      cs = CustomersSetting.where( customer_id: current_customer.id, setting_id: setting.id ).first
      cs.selected_option = params[setting.name]
      cs.save
    end

    flash[:success] = "Successfully updated settings"
    redirect_to profile_view_path
  end

  def follow_customer
    customer = Customer.find params[:id]
    if customer.present?

      current_customer.follow( customer )

      flash[:success] = "Successfully followed #{customer.name}" unless params[:notify]

      respond_to do |format|
        format.html { redirect_to root_path }
        format.json { render json: {
          response: true,
          flash: "Successfully followed #{customer.first_name}"
        } }
      end
    else
      respond_to do |format|
        format.json { render json: {
          response: false
        } }
      end
    end
  end

  def request_connection
    customer = Customer.find params[:id]
    if customer.present?

      current_customer.request_connection( customer )
        
      flash[:success] = "Connection request sent to #{customer.name}" unless params[:notify]
      respond_to do |format|
        format.json { render json: {
          response: true,
          flash: "Connection request sent to #{customer.first_name}",
          format: format
        } }
      end
    else
      respond_to do |format|
        format.json { render json: {
          response: false
        } }
      end
    end
  end

  def approve_connection
    customer = Customer.find_by_id( params[:id] )

    if customer.present?
      if session[:connection_lock].nil?
        # This prevents multiple connections to be approved
        session[:connection_lock] = true
        current_customer.accept_connection( customer )
        session[:connection_lock] = nil

        respond_to do |format|
          format.html { redirect_to root_path }
          format.json { render json: {
            response: true,
            flash: "Accepted #{customer.first_name}'s connection request"
          } }
        end
      else
        respond_to do |format|
          format.html { redirect_to root_path }
          format.json { render json: {
            response: false
          } }
        end
      end
    else
      respond_to do |format|
        format.json { render json: {
          response: false
        } }
      end
    end
  end

  def reject_connection
    customer = Customer.find_by_id( params[:id] )
    if customer.present?

      current_customer.reject_connection( customer )

      respond_to do |format|
        format.html { redirect_to root_path }
        format.json { render json: {
          response: true,
          flash: "Rejected #{customer.first_name}'s connection request"
        } }
      end
    else
      respond_to do |format|
        format.json { render json: {
          response: false
        } }
      end
    end
  end

  def unfollow_customer
    customer = Customer.find_by_id( params[:id] )
    if customer.present?

      current_customer.unfollow( customer )

      flash[:success] = "Successfully unfollowed #{customer.name}" unless params[:notify]

      respond_to do |format|
        format.html { redirect_to root_path }
        format.json { render json: {
          response: true,
          flash: "Successfully unfollowed #{customer.first_name}"
        } }
      end
    else
      respond_to do |format|
        format.json { render json: {
          response: false
        } }
      end
    end
  end

  def block_customer
    customer = Customer.find_by_id( params[:id] )
    if customer.present?

      current_customer.block( customer )

      respond_to do |format|
        format.html { redirect_to root_path }
        format.json { render json: {
          response: true,
          flash: "Successfully blocked #{customer.first_name}"
        } }
      end
    else
      respond_to do |format|
        format.json { render json: {
          response: false
        } }
      end
    end
  end

  def unblock_customer
    customer = Customer.find params[:id]
    if customer.present?
      current_customer.unblock customer

      respond_to do |format|
        format.json { render json: {
          response: true
        } }
      end
    else
      respond_to do |format|
        format.json { render json: {
          response: false
        } }
      end
    end
  end

  def remove_customer
    customer = Customer.find_by_id( params[:id] )
    if customer.present?

      current_customer.unfriend( customer )

      respond_to do |format|
        format.json { render json: {
          response: true,
          flash: "Successfully removed #{customer.first_name}"
        } }
      end
    else
      respond_to do |format|
        format.json { render json: {
          response: false,
          flash: "Something went wrong. Please try again."
        } }
      end
    end
  end

  def banner_upload
    if params[:file].present?

      current_customer.banner = params[:file]
      current_customer.save
      response = true
      flash[:success] = "Successfully loaded new banner!"
    else
      response = false
      flash[:error] = "Something went wrong! Please try again!"
    end

    respond_to do |format|
      format.html { redirect_to request.referer }
      format.json { render json: {
        response: response
        } }
      end
  end

  def avatar_upload
    if params[:file].present?
      current_customer.avatar = params[:file]
      current_customer.save
      response = true
      flash[:success] = "Successfully loaded new avatar!"
    else
      response = false
      flash[:error] = "Something went wrong! Please try again!"
    end

    respond_to do |format|
      format.html { redirect_to request.referer }
      format.json { render json: {
        response: response,
        file: current_customer.avatar.url
      } }
    end
  end

  def banner_default
    if params[:banner_path].present?

      f = File.open("#{Rails.public_path}#{params[:banner_path]}")
      current_customer.banner = f
      current_customer.save!
      f.close
      response = true
      flash[:success] = "Successfully loaded new banner!"
    else
      response = false
      flash[:error] = "Something went wrong! Please try again!"
    end

    respond_to do |format|
      format.html { redirect_to request.referer }
    end
  end

  def avatar_default
    if params[:banner_path].present?
      f = File.open("#{Rails.public_path}#{params[:banner_path]}")
      current_customer.avatar = f
      current_customer.save!
      f.close
      response = true
      flash[:success] = "Successfully loaded new avatar!"
    else
      response = false
      flash[:error] = "Something went wrong! Please try again!"
    end

    respond_to do |format|
      format.html { redirect_to request.referer }
    end
  end

  def notifications
    @notifications_page = true
    @title = 'Notifications'
    @connection_requests = []
    CustomerConnection.where( customer_2_id: current_customer.id, state: "pending" ).each do |request|
      c = Customer.find request.customer_1_id
      @connection_requests << { created_at: request.created_at.strftime("%B %e, %Y"), connection: c }
    end
  end

  def modal_share

    @sharable = Customer.find_by_id(params[:id])
    @share_register = "#{request.host}#{new_customer_registration_path}"
    @share_url = "#{request.host}#{user_path(@sharable.username)}"
    @image_url = "#{@sharable.banner.url}"
    @name = @sharable.name
      
    if @sharable.blank?
      respond_to do |format|
        format.js { render js: "new toastr.error('Something went wrong. Please try again.')" }
      end
    else
      respond_to do |format|
        format.js
      end
    end
  end

  def paginate_notifications
    if params[:page].present?
       @notifications = current_customer.paginate_notifications(params[:page])
    end
    respond_to do |format|
      format.js
    end
  end

end
