class Customers::OmniauthController < ApplicationController

  def destroy
    connection = params[:connection]
    redirect_to root_path unless connection.in? %w[ twitter facebook linkedin google_oauth2 ]
    # current_customer.update(fromfb: false) if connection == 'facebook'
    creds = current_customer.oauth_credentials.where( provider: connection.capitalize ).first
    creds.destroy if creds.present?
    respond_to do |format|
      flash[:success] = "Successfully disconnected from #{params[:for_message]}"
      format.html { redirect_to profile_view_path(anchor: 'social') }
      format.json { render json: {
        response: true
      } } 
    end 
  end

end
