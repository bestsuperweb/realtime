class Api::V1::CartItemsController < BaseApiController
  skip_before_action :authenticate_user_from_token
  before_action :set_current_cart, only: [:index, :create]
  before_action -> { set_current_cart(create_guest:true) if @current_cart.blank? && !customer_signed_in?}, only: [:create]
  before_action :set_cart_item_by_variant, only: [:create]
  before_action :set_cart_item, only: [:update, :destroy]

  def index
    @cart_items = @current_cart.cart_items
    render json: @cart_items
  end

  # POST (add to cart)
  def create
    if @cart_item.add_to_cart!
      render json: @cart_item, status: :ok
    else
      render json: {
        errors: @cart_item.errors.full_messages
      },
      status: 400
    end
  end

  # PUT (remove item from cart one at a time)
  def update
    success = if cart_item_params[:quantity]
                @cart_item.adjust_quantity!(cart_item_params[:quantity])
              else
                @cart_item.remove_from_cart!
              end
              
    if success
      render json: @cart_item, status: 200
    else
      render json: {
        errors: @cart_item.errors.full_messages 
      },
      status: 400
    end
  end

  # DELETE (remove specific item from cart)
  def destroy
    if @cart_item.blank? || @cart_item.destroy
      render json: {}, status: 200
    else
      render json: {}, status: 400
    end
  end

  private

  def set_cart_item_by_variant
    return set_cart_item_by_id if cart_item_params[:id].present?
    @cart_item =  if cart_item_params[:gift_id].present?
                    CartItem.joins(:giftbox).where(
                      giftboxes: { status: 0}, 
                      cart: @current_cart,
                      gift_id: cart_item_params[:gift_id]
                    ).first_or_create(
                      variant_id: cart_item_params[:variant_id]
                    )
                  else
                    CartItem.joins(:giftbox).where(
                      giftboxes: { status: 0}, 
                      cart: @current_cart,
                      variant_id: cart_item_params[:variant_id]
                    ).first_or_create
                  end
  end

  def set_cart_item_by_id
    @cart_item = CartItem.where(id: cart_item_params[:id]).first
  end

  def set_cart_item
    @cart_item = CartItem.where(id: params[:id]).first
  end

  def cart_item_params
    params.require(:cart_item).permit(:id, :variant_id, :gift_id, :quantity)
  end

end