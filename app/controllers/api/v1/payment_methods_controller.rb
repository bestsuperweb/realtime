class Api::V1::PaymentMethodsController < ApplicationController
  include CurrentGiftbox

  before_action :authenticate_customer!, only: [:index]
  before_action :initialize_payment_method, only: [:create]

  def index
    @payment_methods = current_customer.payment_methods
    render 'index.json.jbuilder', status: 200
  end

  def create
    if @payment_method.save
      select_payment_method if params[:select_payment_method]
      render 'create.json.jbuilder', status: 200
    else
      render json: {errors: @payment_method.errors.full_messages}, status: 400
    end
  end

  private

  def initialize_payment_method
    @payment_method =   if customer_signed_in?
                          payment_method = current_customer.payment_methods.new(payment_method_params)
                        else
                          payment_method = PaymentMethod.new(payment_method_params)
                          payment_method.owner = @giftbox.owner
                          payment_method
                        end
  end

  def existing_payment_method?
    @giftbox.payment_method.present?
  end

  def select_payment_method
    @giftbox.payment_method = @payment_method
    @giftbox.save
  end

  def payment_method_params
    params.require(:payment_method).permit(
      :name,
      :stripe_id, 
      :payment_type,
      :address_id,
      metadata: [:exp_month,
                 :exp_year,
                 :address_zip_check,
                 :brand,
                 :card_automatically_updated,
                 :country,
                 :cvc_check,
                 :funding,
                 :last4,
                 :three_d_secure,
                 :address_line1_check,
                 :tokenization_method,
                 :dynamic_last4
               ]
    )
  end

end