class Api::V1::CheckoutsController < ApplicationController
  before_action :set_giftbox
  before_action :set_cart_items, only: [:review], if: proc {params[:show_cart_items].present?}
  before_action :set_shipping_option, only: [:select_shipping_option]
  before_action :set_payment_method, only: [:select_payment_method]
  before_action :set_shipping_address, only: [:select_shipping_address]

  def create
    @invoice = @giftbox.checkout!
    if @invoice.authorized?
      render 'create.json.jbuilder', status: 200
    else
      render json: @invoice.errors.full_messages
    end
  end

  def select_shipping_option
    @giftbox.shipping_option = params[:shipping_option]
    if @giftbox.save
      render 'select_shipping_option.json.jbuilder', status: 200
    else
      render @giftbox.errors.full_messages, status: 400
    end
  end

  def select_shipping_address
    if @address.present?
      @giftbox.shipping_address = @address
      if @giftbox.save
        render 'select_shipping_address.json.jbuilder', status: 200
      else
        render json: @giftbox.errors.full_messages, status: 400
      end
    else
      render json: {}, status: 404
    end
  end

  def select_payment_method
    if @payment_method.present?
      @giftbox.payment_method = @payment_method
      if @giftbox.save
        render 'select_payment_method.json.jbuilder', status: 200
      else
        render json: @giftbox.errors.full_messages, status: 400
      end
    else
      render json: {}, status: 404
    end
  end

  def review
    @shipping_address = @giftbox.shipping_address
    @payment_method = @giftbox.payment_method
    render 'review.json.jbuilder', status: 200
  end

  private

  def set_giftbox
    @giftbox = Giftbox.includes(:payment_method, :shipping_address).where(id: params[:giftbox_id]).first
  end

  def set_shipping_option
    @shipping_option = Cart.shipping_options
  end

  def set_cart_items
    @cart_items = CartItem.includes(:variant).where(cart_id: @cart.id)
  end

  def set_payment_method
    @payment_method = PaymentMethod.where(uuid: params[:payment_method_id]).first
  end

  def set_shipping_address
    @address = Address.where(id: params[:shipping_address_id]).first
  end

end