class Api::V1::ApiReportsController < BaseApiController
  skip_before_action :authenticate_user_from_token
  before_action :set_api_customer
  before_action :get_report, only: [:confirm]

  def create
    @report = ApiReport.new(api_report_params)
    @report.customer = @current_customer
    @report.url = api_report_params[:data][:url]
    @report.customer_confirmed = true
    if @report.save
      render json: {response: @api_report.as_json}, status: 200
    else
      render json: {}, status: 400
    end
  end

  def confirm
    if @api_report.present?
      @api_report.customer_confirmed! 
      render json: {response: @api_report.as_json}, status: 200
    else
      render json: {error: 'Not found'}, status: 401
    end
  end

  private

  def get_report
    @api_report = ApiReport.where(id: params[:api_report][:id]).first
  end

  def api_report_params
    data_keys = params.require(:api_report).fetch(:data, {}).keys
    params.require(:api_report).permit(
      :error_type,
      :url,
      :note,
      data: data_keys
    )
  end

end