class Api::V1::GiftUrlsController < BaseApiController
  skip_before_action :authenticate_user_from_token
  before_action :set_api_customer
  before_action :initialize_scrape_api_service, only: [:show, :html]
  before_action :initialize_create_gift_service, only: [:create]
  
  def baskets
    @baskets = current_customer.try(:baskets)
    render json: @baskets
  end

  # GET with simple url
  def show
    @data = @scrape_api_service.call(params[:url], customer: @current_customer)
    if @data.present?
      render json: {data: @data.data.as_json, status: @data.status, error_report: @data.error_report.try(:as_json) }, status: @data.status
    else
      render json: {status: @data.status}, status: @data.status
    end
  end

  def create
    @gifts = @create_gift_service.call(product_params, params[:basket_ids], current_customer)
    if @gifts
      render json: @gifts.as_json, status: 200
    else
      render json: {}, status: 500
    end
  end

  # POST with url and html
  # for the GA, don't throw server errors
  def html
    @data = @scrape_api_service.call(params[:url], customer: @current_customer, html: params[:html])
    render json: {data: @data.try(:data).try(:as_json), status: @data.try(:status), error_report: @data.try(:error_report).try(:as_json) }
  end

  private

  def initialize_scrape_api_service
    @scrape_api_service = ScrapeApiService.build
  end

  def initialize_create_gift_service  
    @create_gift_service = CreateGiftFromUrlService.build
  end

  def product_params
    params.require(:product).permit(
      :item_name,
      :url,
      :merchant_name,
      :item_size,
      :price,
      :image,
      :description,
      :color,
      :api_id
    )
  end
end