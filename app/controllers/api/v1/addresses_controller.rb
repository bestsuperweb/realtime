class Api::V1::AddressesController < ApplicationController
  before_action :initialize_address, only: [:create]

  def create
    if @address.save
      render 'create.json.jbuilder', status: 200
    else
      render json: {errors: @address.errors.full_messages}, status: 400
    end
  end

  private

  def initialize_address
    @address =  address = Address.new(address_params)
                address.owner = @current_cart.owner
                address.save_as_default = address_params[:save_as_default] == "true" ? true : false
                address.force_check_for_save_as_default = address_params[:force_check_for_save_as_default] == "true" ? true : false
                address
  end

  def existing_shipping_address?
    params[:address][:address_type].casecmp("shipping").zero? && @current_cart.shipping_address.present?
  end

  def address_params
    params.require(:address).permit(
      :name,
      :address_1,
      :address_2,
      :city,
      :state,
      :postal_code,
      :address_type,
      :phone_number,
      :save_as_default,
      :force_check_for_save_as_default
      )
  end

end