class Api::V1::BasketsController < ApplicationController
  protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token
  before_action :authenticate_customer!

  def create
    @basket = current_customer.baskets.where(
                name: basket_params[:name]
              ).first_or_initialize
    if @basket.save
      render json: @basket.as_json
    else
      render json: {errors: @basket.errors.full_messages}, status: 500
    end
  end

  private

  def basket_params
    params.require(:basket).permit(
      :name
    )
  end

end
