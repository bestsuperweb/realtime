class Api::V1::CartsController < ApplicationController
  before_action :dataLayer_arr
  before_action :set_items_count

  #GET 
  def items_count
    render json: @count
  end

  #GET
  def items
    render json: @dataLayer_arr
  end

  private

  def set_items_count
    @count = @current_cart.pending_items_count
  end

  def dataLayer_arr
  	giftboxes = @current_cart.giftboxes.includes(:gifts).pending.includes(:gifts)
  	return_arr = []
  	giftboxes.each do |giftbox|
  		giftbox.cart_items.each do |item| 
  			obj = Hash.new
  			obj['name'] = item.variant.name
  			obj['id'] = item.variant.id
  			obj['price'] = item.current_price.to_f
  			obj['brand'] = item.merchant_cart.merchant.name
  			obj['quantity'] = item.quantity
  			obj['variant'] = item.variant.color.present? ? "#{item.variant.color}" : "none"
  			return_arr << obj
  		end
  	end
  	@dataLayer_arr = return_arr.to_json
  end

end