class CustomSessionsController < Devise::SessionsController
  before_action :before_login, only: :create
  after_action :after_login, only: :create

  def before_login
  end

  def after_login
    current_customer.reactivate if current_customer.deactivated?
    if current_customer.sign_in_count > 1
      session[:intro_active] = { feed: false, settings: false, profile: false, gifts: false }
    end
  end

end
