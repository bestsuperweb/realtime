class ConversationsController < ApplicationController
  before_action :authenticate_customer!

  layout false

  def create
    if Conversation.between( params[:sender_id], params[:recipient_id] ).present?
      @conversation = Conversation.between( params[:sender_id], params[:recipient_id] ).first
    else
      @conversation = Conversation.create!( conversation_params )
    end
    render json: { conversation_id: @conversation.id }
  end

  def show
    @conversation = Conversation.find params[:id]
    @reciever = interlocutor( @conversation )
    messages = @conversation.messages.order( 'created_at asc' )

    message_count = messages.count

    total_pages = message_count / 40

    total_pages = total_pages + 1 if ( message_count % 40 != 0 )

    if params[:page]
      page = params[:page].to_i
      difference_from_end = total_pages - page
      last_element = ( message_count ) - ( difference_from_end * 40 )
      first_element = last_element - 39
      first_element = 0 if first_element < 0

      @messages = messages[first_element..last_element]
      @current_page = params[:page]
    else
      @messages = messages.last(39)
      @current_page = total_pages
    end

    @message = Message.new
  end


private

  def conversation_params
    params.permit(:sender_id, :recipient_id)
  end

  def interlocutor(conversation)
    current_customer == conversation.recipient ? conversation.sender : conversation.recipient
  end

end
