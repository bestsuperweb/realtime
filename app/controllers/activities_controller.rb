class ActivitiesController < ApplicationController

  before_action :authenticate_customer!

  def share
    if params[:actable_type].present? && params[:actable_id].present? && params[:customers].present?

      # The person who is having something shared with them
      a1               = Activity.new
      a1.activity_type = "share"
      a1.actable_type  = params[:actable_type]
      a1.actable_id    = params[:actable_id]
      a1.customer_id   = current_customer.id

      # The owner of the thing being shared
      a2               = Activity.new
      a2.activity_type = "shared_thing"
      a2.actable_type  = params[:actable_type]
      a2.actable_id    = params[:actable_id]
      a2.customer_id   = current_customer.id

      if a1.valid? && a2.valid?
        a1.save!
        a2.save!

        # Create notification
        customers_ray = params[:customers].split(",")

        customers_ray.each do |customer_id|
          c = Customer.find customer_id.to_i

         if c.present?
            n             = Notification.new
            n.customer_id = customer_id
            n.activity_id = a1.id

            n.save!
          end
        end

        notify_owner = false
        if ( a1.actable_type == "Customer" )
          owner = a1.actable
          notify_owner = true if owner.setting_selected?( "share_profile" ) && !( customers_ray.include? "#{owner.id}" ) && ( owner != current_customer )
        elsif ( a1.actable_type == "Gift" )
          owner = a1.actable.basket.customer
          notify_owner = true if owner.setting_selected?( "share_gift" )  && !( customers_ray.include? "#{owner.id}" ) && ( owner != current_customer )
        elsif ( a1.actable_type == "Basket" )
          owner = a1.actable.customer
          notify_owner = true if owner.setting_selected?( "share_basket" )  && !( customers_ray.include? "#{owner.id}" ) && ( owner != current_customer )
        elsif ( a1.actable_type == "Variant" )
          notify_owner = false
        end 

        if notify_owner
          n             = Notification.new
          n.customer_id = owner.id 
          n.activity_id = a2.id

          n.save!
        end 

        unless a1.actable_type == "Customer"
          a1.actable.share_count += customers_ray.count
          a1.actable.save
        end

        respond_to do |format|
          format.json { render json: {
            response: true, actable_type: a1.actable_type
          } }
        end
      else
        respond_to do |format|
          format.json { render json: {
            response: false, message: "Unable to save activity"
          } }
        end
      end
    else
      respond_to do |format|
        format.json { render json: {
          response: false, message: "Missing params"
        } }
      end
    end
  end

end
