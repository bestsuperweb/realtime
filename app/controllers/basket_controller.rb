class BasketController < ApplicationController

  before_action :authenticate_customer!, only: [ :index, :add, :create, :destroy, :remove, :update, :banner_upload, :banner_default, :share, :clone, :modal_builder, :modal_destroy ]

  def index
    @baskets = current_customer.baskets
  end

  def add
    basket = Basket.find_by_id( params[:id] )
    variant = Variant.find_by_id( params[:variant_id] )

    if basket.blank? || variant.blank? || variant.inactive? 
      respond_to do |format|
        format.js { render :js => "new toastr.error('Something went wrong. Please try again.')" }
      end
    elsif basket.contains?(variant)
      respond_to do |format|
        format.js { render :js => "new toastr.error('Sorry, this wish list already contains this item.')" }
      end
    else

      quantity = params['quantity'].present? ? params['quantity'] : 1
      basket.add_to_basket( variant, quantity )
      respond_to do |format|

        #flash link to basket gift tab
        if basket.lifestyle?
          format.js { render :js => "new toastr.success('<a href=/#{basket.customer.username}/registry/#{basket.basket_slug_name}#gifts>Wish added to #{basket.name}.</a>')" }
        else
          format.js { render :js => "new toastr.success('<a href=/#{basket.customer.username}/#{basket.basket_type}/#{basket.basket_slug_name}#gifts>Wish added to #{basket.name}.</a>')" }
        end

      end
    end
  end

  def view
    begin
      @basket = Basket.friendly.find params[:id]
      @clear_nav = true
      @lifestyle = true
      @multitool_basket = true
      @title = @basket.name + ' (@' + @basket.customer.username + ')'
      @meta_description = 'Wish List owner: ' + @basket.customer.name + '. ' + @basket.name + ' - Personal Registry.' + (@basket.description.present? ? ' ' + @basket.description : '')
      if @basket.present?
        #GIFTS EASYAUTOCOMPLETE
        @gifts_arr = easyAutocomplete(@basket.gifts.order(created_at: :desc), 'wishes', 1)
        #DATA LAYER
        @dataLayer_arr = dataLayer_arr ( @basket.gifts.order(created_at: :desc) ) 
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:error] = "Sorry this wishlist does not exist!"
      redirect_to root_path
    end
  end

  def create
    basket = Basket.new
    basket.name = params['basket']['name'].strip
    basket.description = params['basket']['description']
    basket.customer = current_customer
    basket.privacy = params['basket']['privacy']

    #condition if basket name already taken 
    if basket.customer.basket_name_taken? ( basket.name )
      valid_flag = false
      basket_name_exist = true
    else
      valid_flag = true
    end

    if params['basket_type'] == 'lifestyle'

      if params['file'].present?
        basket.banner = params['file']
      elsif params['default_banner'].present?
        f = File.open("#{Rails.public_path}#{params[:default_banner]}")
        basket.banner = f
        f.close
      end

    elsif params['basket_type'] == 'event'

      basket.basket_type = "event"
      parsed_date = DateTime.now

      if params['date'].present?
          parsed_date = DateTime.strptime(params['date'], "%m/%d/%Y %H:%M %p")
      end

      event = Event.new
      event.customer = basket.customer
      event.basket = basket
      event.location = params['location'] if params['location'].present?
      event.occurs_at = parsed_date
      event.name = basket.name
      event.description = basket.description

      if params['file'].present?
        basket.banner = params['file']
      elsif params['default_banner'].present?
        f = File.open("#{Rails.public_path}#{params[:default_banner]}")
        basket.banner = f
        f.close
      end

      if basket.valid? && event.valid?
        #event.save!
      else
        valid_flag = false
      end

    elsif params['basket_type'] == 'charity'
      basket.basket_type = "charity"
      charity = Charity.new
      charity.url = params['url'] if params['url'].present?
      charity.basket = basket
      charity.cash_charity = params[:cash].present?

      if params['file'].present?
        charity.basket.banner = params['file']
      elsif params['default_banner'].present?
        f = File.open("#{Rails.public_path}#{params[:default_banner]}")
        charity.basket.banner = f
        f.close
      end

      if basket.valid? && charity.valid?
        charity.save!
      else
        valid_flag = false
      end

    # ================ TO DO FOR CASH BASKETS ================

    # elsif params['basket_type'] == 'cash'

    #   basket.basket_type = "cash"
    #   cash = Cash.new
    #   cash.basket = basket

    #   if params['file'].present?
    #     cash.basket.banner = params['file']
    #   elsif params['default_banner'].present?
    #     f = File.open("#{Rails.public_path}#{params[:default_banner]}")
    #     cash.basket.banner = f
    #     f.close
    #   end

    #   if basket.valid? && cash.valid?
    #     cash.save!
    #   else
    #     valid_flag = false
    #   end

    # ================ TO DO FOR CASH BASKETS ================

    end

    if valid_flag

      basket.save!

      if ( params['basket_type'] == "event" ) && ( params["invites"].present? )
        invites = JSON.parse( params["invites"] )

        invites.each do |customer_id|
          basket.event.invite_customer( customer_id ) 
        end
      end

      if params['basket']['variant_id'].present?
        variant = Variant.find_by_id(params['basket']['variant_id'])

        quantity = params['basket']['quantity'].present? ? params['basket']['quantity'] : 1

        if variant.present? && variant.valid?
          basket.add_to_basket( variant, quantity )
        end
      end

      #success notice with links 
      if basket.lifestyle?
        flash[:success] = %Q[ <a href=/#{basket.customer.username}/registry/#{basket.basket_slug_name}> Successfully created #{basket.name} wish list! </a> ]
      else
        flash[:success] = %Q[ <a href=/#{basket.customer.username}/#{basket.basket_type}/#{basket.basket_slug_name}> Successfully created #{basket.name} wish list! </a> ]
      end
      redirect_to request.referrer

    #notice error if basket name exist 
    elsif basket_name_exist.present?
      flash[:error] = "Sorry basket name taken try again"
      redirect_to request.referrer
    #Default error notice 
    else
      flash[:error] = "Something went wrong. Please try again."
      redirect_to request.referrer
    end
  end

  def destroy
    basket = Basket.find_by_id(params[:id])
    if basket.blank?
      respond_to do |format|
        flash[:error] = "Something went wrong. Please try again."
        format.html { redirect_to user_path(current_customer.username) }
      end
    else
      basket.nuke
      respond_to do |format|
        format.json { render json: { response: true, basket_id: basket.id } }
      end
    end
  end

  def remove
      basket =  Basket.find_by_id( params[:basket_id] )
      variant = Variant.find_by_id( params[:id] )

      if basket.blank?
        respond_to do |format|
        format.json { render json: { response: false } }
        end
      else
        basket.remove_from_basket( variant )
        respond_to do |format|
        basket_type = basket.lifestyle? ? "registry" : basket.basket_type
        format.json { render json: { response: true, basket: {url: "/#{basket.customer.username}/#{basket_type}/#{basket.basket_slug_name}" } } }
        end
      end
  end

  def update
      basket = Basket.find_by_id(params['id'])
      if basket.nil?
        flash[:error] = "Somthing went wrong. Please try again."
        redirect_to basket_view_path(basket.customer.username,basket.basket_slug_name)
      else
        
        basket.name = params['basket']['name'].capitalize unless basket.name == "Liked Products"
        basket.description = params['basket']['description']
        basket.privacy = params['basket']['privacy']
        
        if basket.valid?
          basket.save!
          flash[:success] = "Basket Info Saved"
          redirect_to basket_view_path(basket.customer.username, basket.basket_slug_name)
        else
          flash[:error] = "Something went wrong. Please try again."
          redirect_to basket_view_path(basket.customer.username, basket.basket_slug_name)
        end
        
      end
  end

  def popup_share

      if params[:type] == 'profile'
        @sharable = Customer.find_by_id(params[:id])
        @link_url = "#{user_path(@sharable.username)}"
        @share_url = "#{request.host}#{user_path(@sharable.username)}"
        @image_url = "#{@sharable.banner.url}"
        @verbiage = "Profile"
        @share_type = "Customer"
        @name = @sharable.name
      elsif params[:type] == 'basket'
        @sharable = Basket.find_by_id(params[:id])
        @link_url = "#{basket_view_path( @sharable.customer.username, @sharable.basket_slug_name )}"
        @share_url = "#{request.host}#{basket_view_path( @sharable.customer.username, @sharable.basket_slug_name )}"
        @image_url = "#{@sharable.banner.url}"
        @verbiage = "Wish List"
        @share_type = "Basket"
        @name = @sharable.name
      elsif params[:type] == 'gift'
        @sharable = Gift.find_by_id(params[:id])
        @link_url = "#{gift_view_path( @sharable.gift_slug_name )}"
        @share_url = "#{request.host}#{gift_view_path( @sharable.gift_slug_name )}"
        @image_url = "#{@sharable.variant.product_image.url}"
        @verbiage = "Wish"
        @share_type = "Gift"
        @name = @sharable.variant.name
      elsif params[:type] == 'variant'
        @sharable = Variant.find_by_id(params[:id])
        @link_url = "#{variant_path(@sharable.id)}"
        @share_url = "#{request.host}#{variant_path(@sharable.id)}"
        @image_url = "#{@sharable.product_image.url}"
        @verbiage = "Product"
        @share_type = "Variant"
        @name = @sharable.name
      end

      #search connections autocomplete
      if current_customer.present?
        @connection_arr = []
        if current_customer.connections
          current_customer.connections.each do |connection|
            @connection_arr << connection.name
          end
        end
      end

      if @sharable.blank?
        respond_to do |format|
          format.js { render js: "new toastr.error('Something went wrong. Please try again.')" }
        end
      else
        respond_to do |format|
          format.js
        end
      end
  end

  def banner_upload
    if params[:file].present?
      basket = Basket.find_by_id(params['id'])
      if basket.present?
        basket.banner = params[:file]
        if basket.valid?
          basket.save!
          response = true
          flash[:success] = "Successfully loaded new banner!"
        else
          response = false
          flash[:error] = "Something went wrong. Please try again."
        end
      else
        response = false
        flash[:error] = "Something went wrong. Please try again."
      end
    else
      response = false
      flash[:error] = "Something went wrong. Please try again."
    end
    respond_to do |format|
      format.html { redirect_to request.referrer }
      format.json { render json: {
        response: response
        } }
    end
  end

  def banner_default
    if params[:banner_path].present?
      basket = Basket.find_by_id(params[:id])

      if basket.present?
        f = File.open("#{Rails.public_path}#{params[:banner_path]}")
        basket.banner = f
        if basket.valid?
          basket.save!
          response = true
          flash[:success] = "Successfully loaded new banner!"
        else
          response = false
        end
      else
        response = false
      end
    else
      response = false
    end
    flash[:error] = "Something went wrong. Please try again." if response == false
    redirect_to request.referrer 
    f.close
  end

  def clone
    customer = Customer.find_by_id(params[:id])
    basket = Basket.find_by_id(params[:basket_id])
    if customer.blank? || basket.blank?
      respond_to do |format|
        format.js { render :js => "new toastr.error('Something went wrong. Please try again.')" }
      end
    else
      customer.create_a_copy_basket( basket )
      respond_to do |format|
        format.js { render :js => "new toastr.success('Wish list copied.')" }
      end
    end
  end

  def modal_builder
    @variant = Variant.find_by_id( params[:id] )
    if @variant.nil?
      respond_to do |format|
        format.js { render :js => "new toastr.error('Something went wrong. Please try again.')" }
      end
    else
      respond_to do |format|
        format.js
      end
    end
  end

  def modal_destroy
    @basket = Basket.find_by_id(params[:id])

    if @basket.blank?
      respond_to do |format|
        flash[:error] = "Something went wrong. Please try again."
        format.html { redirect_to user_path(current_customer.username) }
      end
    else
      respond_to do |format|
        format.js
      end
    end
  end


  def paginate_basket
    @basket = Basket.find_by_id(params[:id])

    if params[:page].present?
      gifts = @basket.gifts.order(created_at: :desc)
      @gifts = gifts.paginate(page: params[:page], per_page: 25) 
      #PENDING AUTOCOMPLETE
      @gifts_arr = easyAutocomplete(gifts, 'wishes', params[:page])
    end 

    respond_to do |format|
      format.js 
    end
  end

  private

  def dataLayer_arr( gifts )
    return_arr = []
    gifts.each do |gift|
      obj = Hash.new
      obj['name'] = gift.variant.name
      obj['id'] = gift.variant.id
      obj['price'] = gift.variant.pretty_price
      obj['brand'] = gift.variant.merchant.name
      obj['variant'] = gift.variant.color.present? ? "#{gift.variant.color}" : "none"
      obj['position'] = gift.variant.id
      return_arr << obj
    end 
    return return_arr.to_json
  end

end
