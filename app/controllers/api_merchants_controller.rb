class ApiMerchantsController < BaseApiController

  #################################################################################
  #
  # check
  #
  # attributes
  #   - customer token
  #   - url
  #
  # Checks to see if a merchant exists in our system
  #
  #################################################################################

  def check
    if session[:customer_id].present?
      
      valid_flag = true

      if params[:url].present?
        merchant = Merchant.find_by_url(params[:url])

        unless merchant.present?
          merchant = Merchant.new
          merchant.name = params[:url]
          merchant.url = params[:url]
          merchant.pending = true

          if merchant.valid?
            merchant.save!
          else
            valid_flag = false
          end
        end
        render json: {response: valid_flag}
      else
        render json: {error: 'No URL detected'}, status: 401
      end
    else
      render json: {error: 'Bad Token'}, status: 401
    end
  end

end
