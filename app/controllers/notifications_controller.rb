class NotificationsController < ApplicationController

  before_action :authenticate_customer!

  def mark_as_read
    notification = Notification.find params[:id]

    if notification.present?
      notification.read = true

      if notification.valid?
        notification.save!

        respond_to do |format|
          format.json { render json: {
            response: true
          } }
        end
      else
        respond_to do |format|
          format.json { render json: {
            response: false
          } }
        end
      end
    else
      respond_to do |format|
        format.json { render json: {
          response: false
        } }
      end
    end
  end

end
