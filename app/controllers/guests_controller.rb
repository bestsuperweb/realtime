class GuestsController < ApplicationController
  before_action :find_or_initialize_guest
  before_action :set_giftbox

  def update
    if @guest.save
      @current_cart.assign_guest_owner!(@guest)
      redirect_to confirm_checkout_path(giftbox_id: @giftbox.id)
    else
      redirect_to confirm_checkout_path(giftbox_id: @giftbox.id)
    end
  end

  private

  def set_giftbox
    @giftbox = Giftbox.where(id: guest_params[:giftbox_id]).first
  end

  def find_or_initialize_guest
    if @current_cart && @current_cart.owner.class == Guest
      @guest = @current_cart.owner
      #@guest.assign_attributes(email: guest_params[:email])
      @guest.assign_attributes(email: guest_params[:email], name: guest_params[:name])
    else
      @guest = Guest.where(email: guest_params[:email]).first_or_initialize
    end
  end

  def guest_params
    params.require(:guest).permit(:giftbox_id, :email, :name)
  end

end