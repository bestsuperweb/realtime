class AjaxPartials::ShippingAddressesFormController < ApplicationController
  include CurrentGiftbox
  
  before_action :set_giftee
  before_action :set_customer_shipping_addresses
  before_action :set_giftee_shipping_address
  before_action :set_current_shipping_address, only: [:index]
  before_action :set_new_shipping_address, only: [:new]

  def index
    render partial: "checkouts/components/accordion_shipping_location/select_shipping_address_form", 
                    locals: {
                      giftbox: @giftbox,
                      giftee: @giftee,
                      giftee_shipping_address: @giftee_shipping_address,
                      customer_shipping_addresses: @customer_shipping_addresses
                    } 
  end

  # loads a new form
  def new
    render partial: "checkouts/components/accordion_shipping_location/new_shipping_address_form", 
                    locals: {
                      giftbox: @giftbox,
                      giftee_shipping_address: @giftee_shipping_address,
                      customer_shipping_addresses: @customer_shipping_addresses
                    } 
  end

  private

  def set_current_shipping_address
    @shipping_address = if customer_signed_in?
                          @giftbox.shipping_address || Address.new
                        else
                          Address.new
                        end
  end 

  def set_new_shipping_address
    @shipping_address = Address.new
  end

  def set_customer_shipping_addresses
    customer_shipping_addresses =  if customer_signed_in?
                                      current_customer.addresses.shipping.where(default: true)
                                    else
                                      []
                                    end
    customer_shipping_addresses << @giftbox.shipping_address if @giftbox.giftee == current_customer
    @customer_shipping_addresses = customer_shipping_addresses.compact
  end 

  def set_giftee
    @giftee = @giftbox.giftee
  end

  def set_giftee_shipping_address
    @giftee_shipping_address = @giftee.default_shipping_address
  end

end