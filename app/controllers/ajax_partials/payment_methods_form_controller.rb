class AjaxPartials::PaymentMethodsFormController < ApplicationController
  include CurrentGiftbox

  before_action :set_payment_methods
  before_action :set_payment_method

  def index
    render partial: "checkouts/components/accordion_payment/select_payment_method_form", 
                    locals: {
                      payment_methods: @payment_methods,
                      giftbox: @giftbox
                    } 
  end

  # loads a new form
  def new
    render partial: "checkouts/components/accordion_payment/new_payment_method_form", locals: {payment_methods: @payment_methods} 
  end

  private

  def set_payment_method
    @payment_method =   if customer_signed_in?
                          @giftbox.payment_method || PaymentMethod.new
                        else
                          PaymentMethod.new
                        end
  end

  def set_payment_methods
    @payment_methods =  if customer_signed_in?
                          current_customer.payment_methods
                        else
                          [] << @giftbox.payment_method
                        end
  end

end