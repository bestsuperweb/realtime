class AjaxPartials::CheckoutsController < ApplicationController
  before_action :set_giftbox
  before_action :set_merchant_carts, only: [:cart_tab]
  before_action :set_shipping_option, only: [:review_shipping_option]
  before_action :set_payment_method, only: :review_payment_method
  before_action :set_shipping_address, only: :review_shipping_address
  before_action :set_giftee, only: :review_shipping_address

  def empty_cart
    render partial: "checkouts/components/empty_cart" 
  end

  def cart_tab
    # disable cart item verification
    # VerifyCartItemService.build.call(@giftbox)
    render partial: "checkouts/components/cart_tab"
  end

  def sidebar
    render partial: "checkouts/components/sidebar", locals: {giftbox: @giftbox, giftee: @giftbox.try(:giftee) }
  end

  def review_shipping_option
    render partial: "checkouts/components/accordion_review/review_shipping_option", 
                    locals: {
                      shipping_option: @shipping_option
                    } 
  end

  def review_payment_method
    render partial: "checkouts/components/accordion_review/review_payment_method", 
                    locals: {
                      payment_method: @payment_method
                    } 
  end

  # loads a new form
  def review_shipping_address
    render partial: "checkouts/components/accordion_review/review_shipping_address", locals: {
      shipping_address: @shipping_address} 
  end

  private

  def set_giftbox
    @giftbox = Giftbox.where(id: params[:giftbox_id]).first
  end

  def set_merchant_carts
    @merchant_carts = MerchantCart.includes(:merchant, cart_items: [:cart, :variant]).where(giftbox_id: @giftbox.id)
  end

  def set_shipping_option
    @shipping_option = @giftbox.shipping_option
  end

  def set_payment_method
    @payment_method = @giftbox.payment_method
  end

  def set_shipping_address
    @shipping_address = @giftbox.shipping_address
  end

  def set_giftee
    @giftee = @giftbox.giftee
  end

end