class AjaxPartials::GiftboxesController < ApplicationController

  def giftboxes_modal
    if @current_cart
      @giftboxes = @current_cart.giftboxes.includes(:giftee).pending.order(is_a_gift: :asc)
    else
      @giftboxes = []
    end
    render partial: "shared/modals/modal_giftbox", locals: {giftboxes: @giftboxes}
  end

end
