class CharityController < ApplicationController

  before_action :authenticate_customer!, only: [:update, :banner_upload, :banner_default]

  def view
    
    begin
      @multitool_basket = true
      @basket = Basket.find_by_slug( params[:id] )
      @clear_nav = true
      @title = @basket.name + ' (@' + @basket.customer.username + ')'
      @meta_description = 'Wish List owner: ' + @basket.customer.name + '. ' + @basket.name + ' - Charity List.' + (@basket.description.present? ? ' ' + @basket.description : '')

      #GIFTS EASYAUTOCOMPLETE
      @gifts_arr = easyAutocomplete(@basket.gifts.order(created_at: :desc), 'wishes', 1) if @basket.present?
      #DATA LAYER
      @dataLayer_arr = dataLayer_arr ( @basket.gifts.order(created_at: :desc) ) if @basket.present?

    rescue ActiveRecord::RecordNotFound => e
      flash[:error] = "Sorry this charity does not exist!"
      redirect_to root_path
    end
    
  end

  def update
    basket = Basket.find(params['id'])
    basket.name = params['basket']['name']
    basket.description = params['basket']['description']
    basket.privacy = params['basket']['privacy']
    charity = Charity.find_by_id( basket.charity.id )
    charity.url = params['url']

    if basket.valid? && charity.valid?
      basket.save!
      charity.save!
      respond_to do |format|
        flash[:success] = "#{basket.name} updated!"
        format.html { redirect_to charity_view_path( basket.customer.username, basket.basket_slug_name ) }
      end
    else
      respond_to do |format|        
        flash[:error] = "Something went wrong Please try again!!"
        format.html { redirect_to charity_view_path( basket.customer.username, basket.basket_slug_name ) }
      end
    end
  end

  def banner_upload
    if params[:file].present?
      basket = Basket.find_by_id(params['id'])
      if basket.present?
        basket.banner =  params[:file]
        if basket.valid?
          basket.save!
          response = true
          flash[:success] = "Successfully loaded new banner!"
        else
          response = false
        end
      else
        response = false
      end
    else
      response = false
    end

    flash[:error] = "Something went wrong! Please try again!" if response == false
    respond_to do |format|
      format.html { redirect_to request.referrer }
      format.json { render json: {
        response: response
      } }
    end
  end

  def banner_default
    if params[:banner_path].present?
      basket = Basket.find_by_id(params['id'])
      if basket.present?
        f = File.open("#{Rails.public_path}#{params[:banner_path]}")
        basket.banner = f 
        if basket.valid?
          basket.save!
          response = true
          flash[:success] = "Successfully loaded new banner!"
        else
          response = false
          flash[:error] = "Something went wrong try again!" if response == false
          redirect_to request.referrer
        end
      else
        response = false
      end
    else
      response = false
    end
    redirect_to request.referrer
    f.close
  end

  private

  def dataLayer_arr( gifts )
    return_arr = []
    gifts.each do |gift|
      obj = Hash.new
      obj['name'] = gift.variant.name
      obj['id'] = gift.variant.id
      obj['price'] = gift.variant.pretty_price
      obj['brand'] = gift.variant.merchant.name
      obj['variant'] = gift.variant.color.present? ? "#{gift.variant.color}" : "none"
      obj['position'] = gift.variant.id
      return_arr << obj
    end 
    return return_arr.to_json
  end

end
