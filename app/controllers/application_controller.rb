class ApplicationController < ActionController::Base
  include CurrentCart

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery prepend: true, with: :exception

  include ActionController::HttpAuthentication::Basic::ControllerMethods
  include ActionController::HttpAuthentication::Token::ControllerMethods

  before_action :set_cache_headers
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :browsing_check
  before_action :load_convos

  def load_convos
    if current_customer.present?
      @conversations = Conversation.involving(current_customer)
      @messages_count = current_customer.number_unread_messages
    end
  end

  def browsing_check
    if session[:browsing].present? && ( session[:browsing] >= (Time.now - 1.day) )
      @browsing = true
    else
      session[:browsing] = nil
      @browsing = false
    end
  end

  def after_sign_in_path_for(resource)
    if resource.class == Admin
      admin_root_path
    else
      if ( defined? session[:oauth_return] ) && ( session[:oauth_return] == true )
        "/profile#social"
      else
        request.env['omniauth.orgin'] || root_path
      end
    end
  end

  def token
    authenticate_with_http_basic do |email, password|
      user = Customer.find_by(email: email)

      if user && user.valid_password?( password )
        render json: { token: user.auth_token }
      else
        render json: { error: 'Incorrect credentials' }, status: 401
      end
    end
  end

  protected

  def configure_permitted_parameters
    if resource_class == Admin
      devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :password])
    else
      devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :first_name, :last_name, :middle_initial, :location, :email, :remember_me])
      devise_parameter_sanitizer.permit(:account_update, keys: [:username, :first_name, :last_name, :middle_initial, :location, :email, :remember_me])
    end
  end 

  def http_access
    authenticate_or_request_with_http_basic do |username, password|
      username == "GiftiblyAdmin" && password == "theperfectgift"
    end
  end

  def after_sign_in_path_for(resource)
    if resource.class == Admin
      admin_root_path
    elsif resource.class == Purchaser
      purchasers_root_path
    else
      if resource.sign_in_count == 1
        welcome_path
      else
        root_path
      end
    end
  end

  private

  def set_cache_headers
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Mon, 01 Jan 1990 00:00:00 GMT"
  end

  def set_api_customer
    @customer = current_customer
    if @customer.blank? && request.headers['Authorization'].present?
      unless authenticate_with_http_token { |token, options| 
        @customer = Customer.find_by(auth_token: token).id 
      }
      render json: {error: 'Bad Token'}, status: 401
      end
    end
  end

  def easyAutocomplete(data, type, page)

    if data.empty?
      autocomplete_arr = []
    else
      autocomplete_arr = []
      if type == 'connection'
        data.each do |connection|
          autocomplete_arr << connection.name
        end
        page = (page.to_i * 12)
        autocomplete_arr = autocomplete_arr[0,page]
      else
        data.each do |gift|
          basket = gift.basket
          if ( current_customer.present? && basket.visible_to?( current_customer ) ) || ( basket.privacy == "public" )
            autocomplete_arr << gift.variant.trunc_name(60)
          end
        end
        page = (page.to_i * 25)
        autocomplete_arr = autocomplete_arr[0,page]
      end
    end

    return autocomplete_arr

  end

end
