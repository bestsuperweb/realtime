# frozen_string_literal: true
module CurrentCart
  extend ActiveSupport::Concern

  included do
    before_action :set_current_cart
  end

  # if a user is not logged in and a cookie does not exist, current_cart is nil 
  # use options[:create_guest] to force the creation of a guest user and cart
  def set_current_cart(options={})
    @current_cart = if customer_signed_in?
                      get_customer_current_cart
                    elsif options[:create_guest]
                      owner = Guest.create
                      owner.carts.create
                    else
                      get_cart_from_cookie
                    end
    clear_cart_cookie
    set_cart_cookie(@current_cart) if @current_cart && @current_cart.owner_is_a_guest?
  end

  private

  def get_cart_from_cookie(options={})
    id = get_cart_cookie
    return nil if id.blank?

    cart = Cart.where(id: id).first
    cart
  end

  def get_customer_current_cart
    current_cart = current_customer.current_cart || Cart.create(owner_id: current_customer.id, owner_type: 'Customer')
    cookie_id = get_cart_cookie
    if cookie_id.present?
      cookie_cart = Cart.where(id: cookie_id).first
      merge(current_cart, cookie_cart) if cookie_cart.id != current_cart.id
    end
    current_cart
  end

  def merge(cart, cookie_cart)
    cart_items =  CartItem.joins(:giftbox).where(
                    giftboxes: { status: 0}, 
                    cart: cookie_cart
                  )
    if cart_items.any?
      cart_items.each do |e| 
        e.cart = cart 
        e.assign_to_giftbox
        e.assign_to_merchant_cart
        e.save
      end
    end

    cookie_cart.destroy if !cookie_cart.checked_out?
  end

  def clear_cart_cookie
    cookies.delete :current_cart
  end

  def set_cart_cookie(cart)
    cookies.permanent.signed[:current_cart] = cart.id
  end

  def get_cart_cookie
    cookies.signed[:current_cart]
  end

end