# frozen_string_literal: true
module CurrentGiftbox
  extend ActiveSupport::Concern

  included do
    before_action :set_giftbox
  end

  private

  def set_giftbox
    @giftbox = Giftbox.where(id: params[:giftbox_id]).first
  end
end