class EventsController < ApplicationController

  before_action :authenticate_customer!, except: :view

  def view
    begin
      @clear_nav = true
      @events_basket = true
      @multitool_basket = true
      @basket = Basket.find_by_slug( params[:id] )
      @title = @basket.name + ' (@' + @basket.customer.username + ')'
      @meta_description = 'Wish List owner: ' + @basket.customer.name + '. ' + @basket.name + ' - Public Event.' + (@basket.description.present? ? ' ' + @basket.description : '')
      if @basket.present? && ( ( @basket.privacy == "public" ) || current_customer.present? )

        #GIFTS EASYAUTOCOMPLETE
        @gifts_arr = easyAutocomplete(@basket.gifts.order(created_at: :desc),'wishes', 1)
        #DATA LAYER
        @dataLayer_arr = dataLayer_arr ( @basket.gifts.order(created_at: :desc) ) 

        if @basket.event.attendees.include? current_customer
          ce = CustomersEvent.find_by( customer_id: current_customer, event_id: @basket.event.id )

          if ce.status == "attending"
            @event_status = "Going"
          elsif ce.status == "maybe"
            @event_status = "Interested"
          elsif ce.status == "not_attending"
            @event_status = "Not Going"
          else
            @event_status = "Are you Going?"
          end
        else
          @event_status = "Are you Going?"
        end

        if @basket.customer == current_customer
          @invite_more = current_customer.connections - @basket.event.attendees
        end
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Sorry this event does not exist!"
      redirect_to root_path
    end

  end

  def remove
    basket = Basket.find_by_id(params[:basket_id])
    event = Event.find_by_id( basket.event.id )
    variant = Variant.find_by_id( params[:id] )

    if event.nil? || basket.nil? || variant.nil?
      respond_to do |format|
        format.json { render json: { response: false } }
      end
    else
      event.remove_from_event( variant )
      respond_to do |format|        
        format.json { render json: { response: true } }
      end
    end
  end
    
  def update
    event = Event.find params[:id]
    basket = event.nil? ? nil : event.basket

    if basket.present? && event.present?
      basket.name           = params['event']['name']
      basket.description    = params['event']['description']
      basket.privacy        = params['privacy']

      event.name            = params['event']['name']
      event.description     = params['event']['description']
      event.occurs_at       = DateTime.strptime(params['event']['occurs_at'], "%m/%d/%Y %H:%M %p")
      event.location        = params['event']['location']

      if event.valid? && basket.valid?
        event.save!
        basket.save!
        flash[:success] = "#{event.name} basket updated!"
        redirect_to event_view_path( basket.customer.username, basket.basket_slug_name )
      else
        redirect_to event_view_path( basket.customer.username, basket.basket_slug_name )
        flash[:error] = "Something went wrong. Please try again."
      end
    else
      redirect_to root_path
    end
  end

  def banner_upload
    if params[:file].present?

      event = Event.find params[:id]

      if event.present?
        event.basket.banner = params[:file]

        if event.valid?
          event.basket.save!

          response = true
          flash[:success] = "Successfully loaded new banner!"
        else
          response = false
        end
      else
        response = false
      end
    else
      response = false
    end

    flash[:error] = "Something went wrong. Please try again." if response == false
    respond_to do |format|
      format.html { redirect_to request.referrer }
      format.json { render json: {
        response: response
      } }
    end
  end

  def banner_default
    if params[:banner_path].present?
      basket = Basket.find params[:id]

      if basket.present?
        event = basket.event
        f = File.open("#{Rails.public_path}#{params[:banner_path]}")
        basket.banner = f
        if event.valid? && basket.banner.present?
          event.save!
          basket.save!
          response = true
          flash[:success] = "Successfully loaded new banner!"
          redirect_to request.referrer
        else
          response = false
          redirect_to request.referrer
        end
      else
        response = false
        redirect_to root_path
      end
    else
      response = false
      redirect_to root_path
    end 

    flash[:error] = "Something went wrong try again!" if response == false
    f.close
  end

  def confirm_invitation
    if params[:event_id].present?
      event = Event.find params[:event_id]
      if event.present?
        
        event.confirm_invitation( current_customer.id )

        invite_count = event.confirmed_count
        response = true
      else
        response = false
      end
    else
      response = false
    end

    respond_to do |format|
      format.json { render json: {
        response: response, invite_count: invite_count
      } }
    end
  end

  def reject_invitation
    if params[:event_id].present?
      event = Event.find params[:event_id]

      if event.present?
        event.reject_invitation ( current_customer.id )
        invite_count = event.confirmed_count        
        response = true
      else
        response = false
      end
    else
      response = false
    end

    respond_to do |format|
      format.json { render json: {
        response: response, invite_count: invite_count
      } }
    end
  end

  def maybe_invitation
    if params[:event_id].present?
      event = Event.find params[:event_id]

      if event.present?
        event.maybe_invitation ( current_customer.id ) 
        invite_count = event.confirmed_count     
        response = true
      else
        response = false
      end
    else
      response = false
    end

    respond_to do |format|
      format.json { render json: {
        response: response, invite_count: invite_count
      } }
    end
  end

  def invite_customer
    begin
      event = Event.find params[:id]
      event.invite_customer( params[:customer_id] ) 
      invite_count = event.pending_attendees.count
      customer = Customer.find(params[:customer_id])

      if event.valid? && customer.present?
        response = true
      else
        customer = nil
        response = false
      end
      
      respond_to do |format|
        format.json { render json: {
          response: response, customer: customer, invite_count: invite_count
        } }
      end

    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Sorry this event does not exist!"
      redirect_to root_path
    end
    
  end

  private

  def dataLayer_arr( gifts )
    return_arr = []
    gifts.each do |gift|
      obj = Hash.new
      obj['name'] = gift.variant.name
      obj['id'] = gift.variant.id
      obj['price'] = gift.variant.pretty_price
      obj['brand'] = gift.variant.merchant.name
      obj['variant'] = gift.variant.color.present? ? "#{gift.variant.color}" : "none"
      obj['position'] = gift.variant.id
      return_arr << obj
    end 
    return return_arr.to_json
  end

end
