class ProductsController < ApplicationController

  before_action :authenticate_customer!, only: :add_tags

  def show
    begin
      @variant = Variant.friendly.find params[:id]
      @baskets_arr = []
      @connection_arr = []
      @title = @variant.name
      @meta_description = 'Name: ' + @variant.name + '. Price: $' + @variant.pretty_price.to_s + '. More Items from ' + @variant.merchant.name + 'on Giftibly...'

      if current_customer.present?
        # search baskets autocomplete
        if current_customer.baskets.present?
          current_customer.baskets.each do |basket|
            @baskets_arr << basket.name
          end
        end
        # search connections autocomplete
        if current_customer.connections.present?
          # connections
          current_customer.connections.each do |connection|
            @connection_arr << connection.name
          end
        end
      end
    rescue ActiveRecord::RecordNotFound => e
      redirect_to '/404'
    end
  end

  def modal_builder

    @variant = Variant.find params[:id]

    if @variant.present?

      if params[:gift_id].present?
        @gift = Gift.find_by_id( params[:gift_id] )
      end 

      @connection_arr = []
      @baskets_arr = []
        
      @action_flag = params[:action_flag]

      if current_customer.present?
        @baskets = current_customer.baskets

        # search connections autocomplete
        if current_customer.connections.present?
          current_customer.connections.each do |connection|
            @connection_arr << connection.name
          end
        end

        # search baskets autocomplete
        if current_customer.baskets.present?
          current_customer.baskets.each do |basket|
            @baskets_arr << basket.name
          end
        end
      else
        @baskets = []
      end

      respond_to do |format|
        format.js
      end

    else
      respond_to do |format|
        format.js { render js: "new toastr.error( 'Something went wrong. Please try again.' )" }
      end
    end
  end

  def modal_leave
    @variant = Variant.find_by_id( params[:id] ) if params['type'] == 'variant'
    @gift = Gift.find_by_id( params[:id] ) if params['type'] == 'gift'
    @purchased = params[:purchased] if params['purchased'].present?

    if @variant.blank? && @gift.blank?
      respond_to do |format|
        format.js { render :js => "new toastr.error('Something went wrong. Please try again.')" }
      end
    else
      respond_to do |format|
        format.js 
      end
    end
  end

  def add_tags
    variant = Variant.find params[:id]
    tag_return = []
 
   if variant.blank?
      respond_to do |format|
        format.json { render json: { response: false } } 
      end 
    else
      tag_ray = params[:tags].split(", ")
      tag_ray.each do |tag|
        if !( Obscenity.profane?( tag ) || variant.product.tag_list.include?( tag ) ) 
          variant.product.tag_list.add( tag.downcase )
          variant.product.save
          tag_return << tag.downcase   
        end 
      end 
      respond_to do |format|
        format.json { render json: { response: true, tags: tag_return } } 
      end 
    end 
  end 

end
