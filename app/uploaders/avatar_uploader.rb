class AvatarUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  #storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  def default_url
    # For Rails 3.1+ asset pipeline compatibility:
    # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  
    "/images/fallback/user_avatar_default.png"
  end

  # Process files as they are uploaded:
  # process scale: [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  version :profile_square do
    process resize_to_fill: [50, 50]
    process convert: 'png'
    process :round_square
    process quality: 100
  end

  version :profile do
    process resize_to_fill: [175, 175]
    process convert: 'png'
    process :circle_trim
    process quality: 100
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_whitelist
    %w(jpg jpeg png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

  def quality(percentage)
    manipulate! do |img|
      img.quality(percentage)
      img = yield(img) if block_given?
      img
    end
  end

  def circle_trim
    manipulate! do |img|
      path = img.path
      new_tmp_path = File.join(Rails.root, 'public', cache_dir, "/round_#{File.basename(path)}")

      width, height = img[:dimensions]

      radius_point = ((width > height) ? [width / 2, height] : [width, height / 2]).join(',')

      imagemagick_command = ['convert',
                         "-size #{ width }x#{ height }", 
                         'xc:transparent',
                         "-fill #{ path }", 
                         "-draw 'circle #{ width / 2 },#{ height / 2 } #{ radius_point }'",
                         "+repage #{new_tmp_path}"].join(' ')

      system(imagemagick_command)
      MiniMagick::Image.open(new_tmp_path)
    end 
  end 

  def round_square
    manipulate! do |img|
      path = img.path
      new_tmp_path = File.join(Rails.root, 'public', cache_dir, "/round_#{File.basename(path)}")

      width, height = img[:dimensions]

      imagemagick_command = ['convert',
                         "-size #{ width }x#{ height }", 
                         'xc:transparent',
                         "-fill #{ path }", 
                          "-draw 'roundrectangle 0,0 #{ width },#{ height } 6,6'",
                         "+repage #{new_tmp_path}"].join(' ')

      system(imagemagick_command)
      MiniMagick::Image.open(new_tmp_path)
    end 
  end 

end
