class ProductImageUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  #storage :file
  #storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  def default_url
    # For Rails 3.1+ asset pipeline compatibility:
    # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
 
    "/images/fallback/product_image_default.png"
  end

  process resize_to_limit: [1920, 1920]

  version :large do
    process :optimize_large
  end

  version :tile do
    process :optimize_tile
  end

  version :medium, from_version: :large do
    process resize_to_limit: [480, nil]
  end

  version :small, from_version: :medium do
    process resize_to_limit: [240, nil]
  end

  def extension_white_list
    %w(jpg jpeg gif png bmp tif tiff)
  end

  def optimize_large(options={})
    manipulate! do |img|
      img.combine_options do |c|
        c.background '#FFFFFF'
        c.alpha 'remove'
        c.alpha 'off'
      end
      img.strip
      img.format("jpg") do |c|
        c.resize      ">960x" #only shrink larger than 800 width
        c.gravity    "center"
        c.quality "80"
        c.depth "8"
        c.interlace "plane"
        c.fuzz        "3%"
        c.colorspace  "sRGB"
      end
      img
    end
  end

  def optimize_tile(options={})
    manipulate! do |img|
      img.combine_options do |c|
        c.background '#FFFFFF'
        c.alpha 'remove'
        c.alpha 'off'
      end
      img.strip
      img.format("jpg") do |c|
        c.resize      ">300x" #only shrink larger than 300 width
        c.gravity    "center"
        c.quality "80"
        c.depth "8"
        c.interlace "plane"
        c.fuzz        "3%"
        c.colorspace  "sRGB"
        c.extent      ">300x" #fill up to 300px width
      end
      img
    end
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # ensure images are saved as jpg
  def filename
    if original_filename
      new_filename = original_filename.rpartition('.').first << '.jpg'

      # remove 'large_large_' and 'basket_image_' from reuploaded filenames
      return new_filename.gsub(/large_large_/, '').gsub(/basket_image_/, '')
    end
  end

end