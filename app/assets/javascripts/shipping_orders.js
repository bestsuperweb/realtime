$(document).on('turbolinks:load', function () {
  
  if ((/admin/.test(window.location.href)) || (/purchasers/.test(window.location.href))) {

    $(document).on('click', '.cart_instructions', function() {
      $("#cart_instructions_modal").modal();
    });

    $(document).on('click', '.mark_as_complete_instructions', function() {
      $("#mark_as_complete_modal").modal();
    });
  };

});