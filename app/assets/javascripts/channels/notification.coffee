App.notification = App.cable.subscriptions.create "NotificationChannel",
  connected: ->
  disconnected: ->

  received: (data) ->
    notificationBadge = $('.notifications-badge')
    count = data.notification_count

    if notificationBadge.length
      notificationBadge.text("#{count}").removeClass('hidden')
    else
      $('.notification_popover').append("<span class='notifications-badge badge'>#{count}</span>")

    messages = $('.notif_container .messages')
    day = $('.notif_container').find('.notif_day_text:first').text().trim()
    if data.time == day
      messages.prepend("<li data-notifid='#{data.notification_id}'>#{data.notification}</li>")
    else
      messages = $('.notif_container')
      messages.prepend("
        <p class='notif_day_text'>#{data.time}</p>
        <ul class='messages'>
          <li data-notifid='#{data.notification_id}'>#{data.notification}</li>
        </ul>
      ")
      $('.no_notifs').hide()