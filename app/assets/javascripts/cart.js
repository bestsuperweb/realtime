$(document).on('turbolinks:load', function () {

  function getCartItemsCount() {
    return $.ajax({
      type: 'GET',
      url: '/api/v1/carts/items_count',
      dataType: 'json'
    });
  };

  function getCartItems() {
    return $.ajax({
      type: 'GET',
      url: '/api/v1/carts/items',
      dataType: 'json'
    });
  };
    
  $(document)
  .off('ajax:complete', '.add_to_cart')
  .on('ajax:complete', '.add_to_cart', function() {
    
    // DATA LAYER EVENT
    getCartItems().then(function(result){
      dataLayer.push({
        'event': 'addToCart',
        'ecommerce': {
          'currencyCode': 'USD',
          'add': {                                
            'products': result
          }
        }
      });
    })

    getCartItemsCount().then(function(result) {
      if($(window).width() >  480){
          if (result > 1) {
            $('span#cart_item_count').text(result);
          }
          else if(result == 1){
            $('.discover_nav').addClass('move_left');
            setTimeout(function(){
                $('.navbar .cart_icon').fadeIn();
            }, 200);
            $('span#cart_item_count').text(result);
          }
          else {
            $('.navbar .cart_icon').addClass('hide');
            $('span#cart_item_count').text(result);
          }
      }
      else{
          if (result > 1) {
            $('span#cart_item_count').text(result);
          }
          else if(result == 1){
            $('.discover_nav').fadeOut(function(){
                $('.navbar .avatar_icon').css('top', 0);
                $('.navbar .cart_icon').css('display', 'inline-block').hide().fadeIn();
            });
            $('.logged_out_nav .cart_icon').fadeIn();
            $('span#cart_item_count').text(result);
          }
          else {
            $('.navbar .cart_icon').addClass('hide');
            $('span#cart_item_count').text(result);
          }  
      }
      toastr.success('Item Added to Cart');
    });
  });

  $(document)
  .off('ajax:complete', '.add_to_cart_then_close_leave_modal')
  .on('ajax:complete', '.add_to_cart_then_close_leave_modal', function() {
    $('.leave_modal').modal('hide');
    getCartItemsCount().then(function(result) {
      if (result > 0) {
        $('span#cart_item_count').removeClass('hide').text(result);
      } else {
        $('span#cart_item_count').addClass('hide').text(result);
      }
      toastr.success('Item Added to Cart');
    });
  });

  $(document).on('click', '.giftbox-modal-toggle', function(e) {
    e.preventDefault();
    $("#giftbox-modal").load('/ajax_partials/giftboxes/giftboxes_modal', function() {
      $("#giftbox-modal").modal();
    });
  });

});