// existing js code
function initCheckoutPage(){

  this.init = function() {

    $.validate({
      modules : 'security',
      borderColorOnError : '#fff'
    });

    giftbox_id = $('#giftbox_id').data("giftbox-id")
    $('div#component-cart-tab').load('/ajax_partials/checkouts/cart_tab'+'?giftbox_id='+giftbox_id, function() {
      updateSidebar();

      //instantiate touchspin
      $(".cart_item_qty").TouchSpin({
        min: 1
      });

      $('.cart_item_qty').on('touchspin.on.startupspin', function(){
        id = $(this).closest('.bootstrap-touchspin').find('input').attr('id');
        input = $(this)[0];
        addToCart(id).then(function(result) {
          input.value = result.quantity
          updateSidebar();
        });
      });

      $('.cart_item_qty').on('touchspin.on.startdownspin', function(){
        id = $(this).closest('.bootstrap-touchspin').find('input').attr('id');
        input = $(this)[0];
        removeFromCart(id).then(function(result) {
          input.value = result.quantity
          updateSidebar();
        });
      });

      $('.cart_item_qty').on('keyup', function(){
        id = $(this).attr('id');
        quantity = $(this)[0].value;
        input = $(this)[0];

        if (quantity >= 1) {
          updateCartItemQuantity(id, quantity).then(function(result) {
            input.value = result.quantity
            updateSidebar();
          });
        } else {
          input.value = 1;
        };
      });
      
      if ($(window).width() >=  992 ) { //the total items is the same height as the body
        var right_container = $("body").height() + "px";
        $(".cart_right").css({"height": right_container});
      }
      else{
        $(".cart_right").css({"height": "100%"});
        $(".cart_scrolldown").removeClass("cart_scrolldown");
      }
      
      $(window).on('resize', function(){
        if ($(window).width() >=  992 ) {
          var right_container = $("body").height() + "px";
          $(".cart_right").css({"height": right_container});
        }
        else{
          $(".cart_right").css({"height": "100%"});
        }
      });
            
      if ($(window).width() <= 768 ) {
        $(".cart_item_details").each(function() { //place cart_item name and retailer above shipping div
          $(this).closest("li").find(".image_checkout").after(this);
        });      
      };

      $(window).on('resize', function(){
        if ($(window).width() <= 768 ) { //place cart_item name and retailer above shipping div
          $(".cart_item_details").each(function() {
            $(this).closest("li").find(".image_checkout").after(this);
          });
        }
        else{
          $(".cart_item_details").each(function() {
            $(this).closest("li").find(".quantity_container").after(this);
          });
        }
      });

      $(document)
      .off('click', '.cart_left .cart_item_actions .remove_item')
      .on('click', '.cart_left .cart_item_actions .remove_item', function() { //removes item and reduces all prices
        
        id = $(this).attr('data-id');
        merchant_name = $(this).closest('.merchant_cart_container').attr('data-merchant');

        deleteFromCart(id).done(function() {
          removeCartItem(id, merchant_name);
          // DATA LAYER EVENT
          getCartItems().then(function(result){
            dataLayer.push({
              'event': 'removeFromCart',
              'ecommerce': {
                'currencyCode': 'USD',
                'remove': {                                
                  'products': result
                }
              }
            });
          })
          getCartItemsCount().then(function(result) {
              if (result > 0) {
                $('span#cart_item_count').text(result);
              }
              else {
                $('.navbar .cart_icon').addClass('hide');
                $('span#cart_item_count').text(result);
              }
          }); 
        });
      });
    });

    $('#collapse1').on('hidden.bs.collapse', function () {
      $('.checkout_accordion[href="#collapse1"]').find('.checkout_edit').fadeIn(300);
    });

    $('#collapse1').on('shown.bs.collapse', function () {
      $('.checkout_accordion[href="#collapse1"]').find('.checkout_edit').fadeOut(300);
    });

    $('#collapse2').on('hidden.bs.collapse', function () {
      $('.checkout_accordion[href="#collapse2"]').find('.checkout_edit').fadeIn(300);
    });

    $('#collapse2').on('shown.bs.collapse', function () {
      $('.checkout_accordion[href="#collapse2"]').find('.checkout_edit').fadeOut(300);
    });

    $('#collapse3').on('hidden.bs.collapse', function () {
      $('.checkout_accordion[href="#collapse3"]').find('.checkout_edit').fadeIn(300);
    });

    $('#collapse3').on('shown.bs.collapse', function () {
      $('.checkout_accordion[href="#collapse3"]').find('.checkout_edit').fadeOut(300);
    });

    $('#collapse4').on('hidden.bs.collapse', function () {
      $('.checkout_accordion[href="#collapse4"]').find('.checkout_edit').fadeIn(300);
    });

    $('#collapse4').on('shown.bs.collapse', function () {
      $('.checkout_accordion[href="#collapse4"]').find('.checkout_edit').fadeOut(300);
    });

  };

  this.init();

  return this;
};

// TODO ========= integrate NEW JS CODE ==================
function getCartItemsCount() {
    return $.ajax({
      type: 'GET',
      url: '/api/v1/carts/items_count',
      dataType: 'json'
    });
};

function getCartItems() {
  return $.ajax({
    type: 'GET',
    url: '/api/v1/carts/items',
    dataType: 'json'
  });
};

function loadEmptyCart() {
  $div = $('div#checkout');
  if ($div.find('.cart_item').length == 0) {
    $div.load('/ajax_partials/checkouts/empty_cart', function() {
      $('#component-empty-cart').fadeIn(400);
    });
  };
};

function addToCart(id) {
  cart_item = {
    id: id
  };
  return $.ajax({
    type     : 'POST',
    url      : '/api/v1/cart_items',
    dataType: 'json',
    data: {
      cart_item: cart_item
    }
  });
};

function removeFromCart(id){
  cart_item = { 
    id: id 
  };
  return $.ajax({
    type     : 'PUT',
    url      : '/api/v1/cart_items/'+id,
    dataType : 'json',
    data: {
      cart_item: cart_item
    }
  }); 
};

function updateCartItemQuantity(id, quantity){
  cart_item = { 
    id: id ,
    quantity: quantity
  };
  return $.ajax({
    type     : 'PUT',
    url      : '/api/v1/cart_items/'+id,
    dataType : 'json',
    data: {
      cart_item: cart_item
    }
  }); 
};

function deleteFromCart(id){
  cart_item = {
    id: id
  };
  return $.ajax({
    type     : 'DELETE',
    url      : '/api/v1/cart_items/'+id,
    dataType : 'json',
    data: {
      cart_item: cart_item
    }
  }); 
};

function removeCartItem(id, merchant_name) {
  id = '[data-id="'+id+'"]';
  $cart_item = $(id).closest('.cart_item');
  $cart_item.slideUp(function() {
    $cart_item.remove();
    updateSidebar();
    loadEmptyCart();
    toastr.success('Item removed from cart.');
  });
  removeMerchantCart(merchant_name);
};

function updateSidebar() {
  giftbox_id = $('#giftbox_id').data("giftbox-id");
  $('div#component-sidebar').load(
    '/ajax_partials/checkouts/sidebar'+'?giftbox_id='+giftbox_id, function() {
      var pathname = window.location.pathname;
      if (pathname == '/checkout/confirm') {
        $('a.checkout_btn').hide();
      }
    }
  );
};

function removeMerchantCart(merchant_name) {
  selector = '[data-merchant="' + merchant_name + '"]';
  $merchant_container = $(selector);
  cart_items_count = $(selector + ' ' + 'li.cart_item').length;
  if (cart_items_count == 1) {
    $merchant_container.animate({ height: 0, opacity: 0 }, 'normal', function(){
        $(this).remove();
    });
  };
};

function shippingOptionsAccordion() {
  giftbox_id = $('#giftbox_id').data("giftbox-id")

  this.selectShippingOption = function(shipping_option_id, giftbox_id) {
    return $.ajax({
      type     : 'POST',
      url      : '/api/v1/checkouts/select_shipping_option',
      dataType: 'json',
      data: { shipping_option: shipping_option, giftbox_id: giftbox_id}
    });
  };

  this.init = function() {
    $(document)
    .off('click', 'button.select_shipping_option')
    .on('click', 'button.select_shipping_option', function () {
      shipping_option = $('input:radio[name=shipping_option]:checked').val();
      selectShippingOption(shipping_option, giftbox_id).done(function(){
        updateSidebar();
        reviewAccordion().loadShippingOption();
      });
    });
  };

  this.init();

  return this
};


function shippingLocationAccordion() {
  giftbox_id = $('#giftbox_id').data("giftbox-id")

  this.selectShippingAddress = function(address_id, giftbox_id) {
    return $.ajax({
      type     : 'POST',
      url      : '/api/v1/checkouts/select_shipping_address',
      dataType: 'json',
      data: { shipping_address_id: address_id, giftbox_id: giftbox_id }
    });
  };

  this.initSelectShippingAddressForm = function() {
    $(document)
    .off('click', 'button.select_shipping_address')
    .on('click', 'button.select_shipping_address', function () {
      address_id = $('input:radio[name=shipping_address]:checked').val();
      selectShippingAddress(address_id, giftbox_id).done(function() {
        reviewAccordion().loadShippingAddress();
      });
    });
  };

  this.loadSelectShippingAddressForm = function() {
    $('div#shipping_address_form').load('/ajax_partials/shipping_addresses_form'+'?giftbox_id='+giftbox_id, function() {
      initSelectShippingAddressForm();
      $('#choose_shipping_address_form').hide();
      $('#choose_shipping_address_form').fadeIn();
    });
  }

  this.createAddress = function(form_id, address_type, method){
    form_id = typeof form_id !== 'undefined' ? form_id : 'form#ship_to_form';
    address_type = typeof address_type !== 'undefined' ? address_type : 'shipping';
    method = typeof method !== 'undefined' ? method: 'POST';
    save_as_default_checkbox = $(form_id+" input#address_save_as_default")
    save_as_default = save_as_default_checkbox[0].checked ? true : false

    address = {
      name: $(form_id+" input#address_name").val(),
      address_1: $(form_id+" input#address_address_1").val(),
      address_2: $(form_id+" input#address_address_2").val(),
      city: $(form_id+" #address_city").val(),
      state: $(form_id+" #address_state").val(),
      postal_code: $(form_id+" input#address_postal_code").val(),
      phone_number: $(form_id+" input#address_phone_number").val(),
      save_as_default: save_as_default,
      force_check_for_save_as_default: $(form_id+" input#address_force_check_for_save_as_default").val(),
      address_type: address_type
    };

    return $.ajax({
      type     : method,
      url      : '/api/v1/addresses',
      dataType: 'json',
      data: {
        address: address
      }
    });
  };

  this.initCreateShippingAddressForm = function() {
    $(document).off('click', 'button.create_shipping_address').on('click', 'button.create_shipping_address', function () {
      createAddress().done(function(address) {
        selectShippingAddress(address.id, giftbox_id).done(function() {
          loadSelectShippingAddressForm();
          reviewAccordion().loadShippingAddress();
        });
      });
    });
    $('#ship_to_form').slideDown();
  };

  this.loadCreateShippingAddressForm = function() {
    $('div#shipping_address_form').load('/ajax_partials/shipping_addresses_form/new'+'?giftbox_id='+giftbox_id, function() {
      
      initCreateShippingAddressForm();
    });
  }

  this.init = function() {
    initCreateShippingAddressForm();
    initSelectShippingAddressForm();

    $(document).off('click', 'a.create_shipping_address').on('click', 'a.create_shipping_address', function () {
      loadCreateShippingAddressForm();
    });

    $(document).off('click', 'a.select_shipping_address').on('click', 'a.select_shipping_address', function () {
      $("#ship_to_form").css('overflow', 'hidden');
      $("#ship_to_form").animate({height: 88, opacity: 0});
      setTimeout(function() {
          loadSelectShippingAddressForm();
      }, 450);
    });
  };

  this.init();

  return this
};

function paymentMethodAccordion() {
  giftbox_id = $('#giftbox_id').data("giftbox-id")

  this.createBillingAddress = function(form_id, address_type){
    form_id = typeof form_id !== 'undefined' ? form_id : 'form#ship_to_form';
    address_type = typeof address_type !== 'undefined' ? address_type : 'shipping';
      
    address = {
      name: $(form_id+" input[name=name]").val(),
      address_1: $(form_id+" input[name=address_1]").val(),
      address_2: $(form_id+" input[name=address_2]").val(),
      city: $(form_id+" [name=city]").val(),
      state: $(form_id+" [name=state]").val(),
      postal_code: $(form_id+" input[name=postal_code]").val(),
      address_type: address_type
    };
    return $.ajax({
      type     : 'POST',
      url      : '/api/v1/addresses',
      dataType: 'json',
      data: {
        address: address
      }
    });
  };

  this.selectPaymentMethod = function(payment_method_uuid, giftbox_id) {
    return $.ajax({
      type     : 'POST',
      url      : '/api/v1/checkouts/select_payment_method',
      dataType: 'json',
      data: { payment_method_id: payment_method_uuid, giftbox_id: giftbox_id }
    });
  };

  this.initSelectPaymentMethodForm = function() {
    $(document).off('click', 'button.select_payment_method').on('click', 'button.select_payment_method', function () {
      payment_method_uuid = $('input:radio[name=payment_method]:checked').val();
      selectPaymentMethod(payment_method_uuid, giftbox_id).done(function() {
        reviewAccordion().loadPaymentMethod();
      });
    });
  };

  this.loadSelectPaymentMethodForm = function(reload) {
    form = typeof reload !== 'undefined' ? reload : false;

    $("div#select_payment_method_form_container").removeClass( "hide" );
    $("div#create_payment_method_form_container").addClass( "hide" );

    $('div#select_payment_method_form_container').load('/ajax_partials/payment_methods_form'+'?giftbox_id='+giftbox_id, function() {
      initSelectPaymentMethodForm();
    });
  };

  this.createPaymentMethod = function(source, billing_address, giftbox_id) {
    payment_method = {
      name: billing_address.name,
      stripe_id: source.id,
      metadata: source.card,
      payment_type: source.type,
      address_id: billing_address.id
    }
    return $.ajax({
      type     : 'POST',
      url      : '/api/v1/payment_methods',
      dataType: 'json',
      data: {
        select_payment_method: true,
        payment_method: payment_method,
        giftbox_id: giftbox_id
      }
    });
  };

  this.initStripeCardElement = function(stripe, elements) {    
    var style = {
      base: {
        color: '#32325d',
        fontFamily: 'Helvetica Neue, Helvetica, Arial, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '13px',
        '::placeholder': {
          color: '#787878'
        }
      },
      invalid: {
        color: '#b94a48',
        iconColor: '#fa755a'
      }
    };

    var card = elements.create('card', {
      style: style
    });

    card.mount('#card-element');

    card.addEventListener('change', function(event) {
      const displayError = document.getElementById('card-element-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });

    return card
  };

  this.createStripeSource = function(stripe, card) {
    return stripe.createSource(card, {
      usage: 'reusable'
    })
  };

  this.savePaymentMethod = function(stripe, card) {
    var promise = $.when(createStripeSource(stripe, card), createBillingAddress("form#secure_payment_form", "billing"))
                  .done(function(stripe_promise, address_result) {
                    stripe_promise.then(function(stripe_result) {
                      if (stripe_result.error) {
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = stripe_result.error.message;
                      } else {
                        var source = stripe_result.source
                        createPaymentMethod(source, address_result[0], giftbox_id).done(function(){
                          loadSelectPaymentMethodForm();
                          reviewAccordion().loadPaymentMethod();
                        });
                      };
                    });
                  });
    return promise
  };

  this.teardownStripe = function(card) {
    card.unmount();
    card.mount('#card-element');
  };

  this.initCreatePaymentMethodForm = function(stripe, elements, card) {
    $(document).off('click', 'button.create_payment_method').on('click', 'button.create_payment_method', function () {
      savePaymentMethod(stripe, card).done(function() {
        teardownStripe(card);
      });
    });
  };

  this.loadCreatePaymentMethodForm = function(stripe, elements) {
    $("div#select_payment_method_form_container").addClass( "hide" );
    $("div#create_payment_method_form_container").removeClass( "hide" );
  };

  this.init = function() {
    var stripe = Stripe($('meta[name="stripe-publishable-key"]').attr('content'));
    var elements = stripe.elements();
    var card = initStripeCardElement(stripe, elements);

    initCreatePaymentMethodForm(stripe, elements, card);
    initSelectPaymentMethodForm();

    $(document)
    .off('click', 'a.create_payment_method')
    .on('click', 'a.create_payment_method', function () {
      loadCreatePaymentMethodForm(stripe, elements);
    });

    $(document)
    .off('click', 'a.select_payment_method')
    .on('click', 'a.select_payment_method', function () {
      loadSelectPaymentMethodForm();
    });
  };

  this.init();

  return this
};

function reviewAccordion() {
  giftbox_id = $('#giftbox_id').data("giftbox-id")

  this.loadShippingOption = function() {
    $('div#review_shipping_option').load('/ajax_partials/checkouts/review_shipping_option'+'?giftbox_id='+giftbox_id);
  };

  this.loadPaymentMethod = function() {
    $('div#review_payment_method').load('/ajax_partials/checkouts/review_payment_method'+'?giftbox_id='+giftbox_id);
  };

  this.loadShippingAddress = function() {
    $('div#review_shipping_address').load('/ajax_partials/checkouts/review_shipping_address'+'?giftbox_id='+giftbox_id);
  };

  return this
};

// --------- Get current Cart, payment_method, shipping address, and optional cart_items.
function getCartState() {
  return $.ajax({
    type     : 'GET',
    url      : '/api/v1/checkouts/review',
    dataType: 'json',
    data: {
      show_cart_items: true //optional
    }
  });
};  

// TODO ========= integrate checkout page ==================
$(document).on('turbolinks:load', function () {
  
  if (/checkout/.test(window.location.href)) {
    // load previously defined js.
    initCheckoutPage();
  };

  if (/checkout\/confirm/.test(window.location.href)) {
    //view/checkouts/components/__accordion_shipping_options.html.erb
    shippingOptionsAccordion();

    // view/checkouts/components/__accordion_shipping.html.erb
    shippingLocationAccordion();

    // view/checkouts/components/__accordion_payment.html.erb
    paymentMethodAccordion();

    // view/checkouts/components/__accordion_review.html.erb
    reviewAccordion();
  };
});
