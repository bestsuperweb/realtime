// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require turbolinks
//= require jquery
//= require jquery_ujs
//= require jquery.remotipart
//= require bootstrap-sprockets
//= require dropzone
//= require toastr/toastr.min.js
//= require masonry
//= require geocomplete
//= require social-share-button
//= require moment
//= require intro.js/minified/intro.min.js
//= require js-notify/notify.min.js
//= require bootstrap-datetimepicker
//= require wdt-emoji-bundle/emoji.min.js
//= require wdt-emoji-bundle/wdt-emoji-bundle.js
//= require bootstrap-switch
//= require bootstrap_validator
//= require private_pub
//= require Chart.bundle
//= require chartkick
//= require clipboard
//= require_tree .



//// =========================================================* |||||||||||||| ========================================================= ////
//// =======================================================*     LIBRARIES     ========================================================= ////
//// =========================================================* |||||||||||||| ========================================================= ////

// AOS                               Use: for scroll animations               Link: https://michalsnik.github.io/aos/
// EASY AUTOCOMPLETE                 Use: for search autocomplete             Link: http://easyautocomplete.com/guide#sec-templates
// MASONRY                           Use: for masonry grid system             Link: https://masonry.desandro.com/
// TOUCHSPIN                         Use: for quantity input                  Link: http://www.virtuosoft.eu/code/bootstrap-touchspin/
// BOOTSTRAP VALIDATOR               Use: for form validation                 Link: http://1000hz.github.io/bootstrap-validator/#
// ANIMATE CSS                       Use: for css animations                  Link: https://github.com/daneden/animate.css
// SLIM SCROLL                       Use: for                                 Link: http://rocha.la/jQuery-slimScroll
// TOASTR                            Use: for flash notifications             Link: https://github.com/CodeSeven/toastr
// EMOJI PICKER                      Use: for emoji                           Link: https://github.com/needim/wdt-emoji-bundle  || http://ned.im/wdt-emoji-bundle/#apple
// GEMOJI                            Use: for Decoding emoji text             Link: https://github.com/github/gemoji
// JQUERY CIRCLE PROGRESS            Use: for walk through progress           Link: https://github.com/kottenator/jquery-circle-progress
// DATE TIME PICKER                  Use: for DATE TIME PICKER                Link: http://eonasdan.github.io/bootstrap-datetimepicker/#enableddisabled-dates
// IMAGE CROPER                      Use: for CROPING IMAGES                  Link: https://fengyuanchen.github.io/cropperjs/

var ua = window.navigator.userAgent;
var iOS = !!ua.match(/iPad/i) || !!ua.match(/iPhone/i);
var webkit = !!ua.match(/WebKit/i);
var iOSSafari = iOS && webkit && !ua.match(/CriOS/i);
var body_scroll_position ;    

$(document).on('turbolinks:load', function() {

//// ===========================================* |||||||||||||| =========================================== ////
//// =======================================*    SCROLL ANIMATION    ======================================= ////
//// ===========================================* |||||||||||||| =========================================== ////

    AOS.init({
      easing: 'ease-in-out-sine'
    });
    
//// ===========================================* |||||||||||||| =========================================== ////
//// =======================================*       LAZY LOAD    =========================================== ////
//// ===========================================* |||||||||||||| =========================================== ////
    $('.lazy').Lazy({
        effect: "fadeIn",
        effectTime: 800,
        threshold: 0,
        delay: 0
    });
    $(function() {
        $('.edit_banner_btn').click(function() {
            $('.lazy_banner').lazy({
                bind: "event",
                effect: "fadeIn",
                effectTime: 800,
                threshold: 0,
                delay: 0
            });
        });
    });

//// ===========================================* |||||||||||||| =========================================== ////
//// ==========================================*    SLIM SCROLL   ========================================== ////
//// ===========================================* |||||||||||||| =========================================== ////

    $(".upcoming_birthdays_modal .modal-body").slimScroll({
      size: '6px',
      height: '440px',
      wheelStep: 20,
      allowPageScroll: true
    });
    
//// =======================================*    GA BUTTON   ==================================== ////
    setTimeout(function() {
        $('.ga_button').addClass('pulse');
    }, 2000);

    //// ===========================================* |||||||||||||| =========================================== ////
//// =======================================*    Landing Page Friend Search  ==================================== ////
//// ===========================================* |||||||||||||| =========================================== ////

    
    if ($("#friend_q").length) {
        document.getElementById('friend_q').focus();
        
        var options_friend = {
            url: function(phrase) {
                if (phrase !== "") {
                    return "/friend-search?friend_q=" + phrase + "&format=json";
                }
            },
            getValue: "name",
            ajaxSettings: {
                dataType: "json"
            },
            template: {
              type: "custom",
              fields: {
                link: "id"
              },
              method: function(value, item) {
                if (item.avatar) {
                  return "<a href='/"+item.username+"'>  <img src='" + item.avatar + "' class='searchImage' /> " + "<span>" + item.name + "</span></a>";
                }
              }
            },
            categories: [
              {
                listLocation: "customers",
                maxNumberOfElements: 6,
              }
            ],
            list: {
              maxNumberOfElements: 8,
              match: {
                enabled: true
              },
              sort: {
                enabled: true
              }
            },
            requestDelay: 50,
        };

        $("input[name='friend_q']").easyAutocomplete(options_friend);


        $(".search_customers input[name='friend_q']").keypress(function(e) {
            $(".search_container").submit(function(event){
                event.preventDefault();
            });
            if(e.which == 13) {
                $('.search_container li.selected a')[0].click();
            }
        });
    }

//// ===========================================* |||||||||||||| =========================================== ////
//// =======================================*    SEARCH FUNCTIONALITY   ==================================== ////
//// ===========================================* |||||||||||||| =========================================== ////

    var options = {
      url: function(phrase) {
        if (phrase !== "") {
          return "/search?q=" + phrase + "&format=json";
        }else {
          return "/search?q=empty&format=json";
        }
      },
      getValue: "name",
      ajaxSettings: {
        dataType: "json"
     },
     template: {
        type: "custom",
        fields: {
          link: "id"
        },
        method: function(value, item) {
          if (item.avatar) {
            return "<a href='/"+item.username+"'>  <img src='" + item.avatar + "' class='searchImage' /> " + "<span>" + item.name + "</span></a>";
          }else if (item.tag_list) {
            return  "<a href='/search?q=" + item.name +"' > " + "<span>" + item.name + "</span></a>";
          } else if (item.url) {
            return  "<a href='/search?q=" + item.name +"' > " + "<span>" + item.name + "</span></a>";
          }
        }
      },
      categories: [
      // {
      //   listLocation: "products",
      //   maxNumberOfElements: 8
      // },
      {
        listLocation: "customers",
        maxNumberOfElements: 6,
        header: "People"
      }//,
      // {
      //   listLocation: "merchants",
      //   maxNumberOfElements: 6,
      //   header: "Brands"
      // }
      ],
      list: {
        maxNumberOfElements: 8,
        match: {
          enabled: false
        },
        sort: {
          enabled: true
        }
      },
        requestDelay: 50,
    };
      
    $("input[name='q']:visible").easyAutocomplete(options);

    $(document).on('keypress', '.search_customers input[name="q"]', function(){
        if($('.search_container li.selected')[0]){ //form can't "submit" if li is selected but it will go to the link selected
            $(".search_container").submit(function(event){
                event.preventDefault();
            });
            if(e.which == 13) {
                $('.search_container li.selected a')[0].click();
            }
        }
    });
    
    $('.activate_search').click(function(){
        $('.navbar-nav').fadeOut(function(){
            $('.search_container').addClass('active');
            $('.search_container').fadeIn();
            
            $('.search_container.active .searchbar_icon').click(function(){
                $('.search_container').fadeOut(function(){
                    $('.search_container').removeClass('active');
                    $('.navbar-nav').fadeIn();
                });
            });
        });
    });
    $(window).on('resize', function(){
        if ($('.search_container').hasClass('active')) {
            $('.search_container').fadeOut(function(){
                $('.search_container').removeClass('active');
                $('.navbar-nav').fadeIn();
            });
        }
        if($(window).width() >  992){
            $('.search_container').show();
        }
        else if($(window).width() >  480){
            $('.search_container').hide();
        }
        else{
            $('.search_container').show();
        }
    });

//// ===========================================* |||||||||||||| =========================================== ////
//// ===========================================*    FB LOGIN    =========================================== ////
//// ===========================================* |||||||||||||| =========================================== ////

// $('body').on('click', '#sign_in', function () {
//   $.post("/customer/fromfb", { show: true }, function (data) {
//   });
// });
    
//// ===========================================* |||||||||||||| =========================================== ////
//// ===========================================*    POPOVERS    =========================================== ////
//// ===========================================* |||||||||||||| =========================================== ////
    
    
    //POPOVER SETTINGS
    
    function close_popover(popover_name){
        $("#" + popover_name).removeClass("popover_animate");
        $("#" + popover_name).addClass("popover_animate_disable");
        $('.' + popover_name).removeClass('active');
        if($(window).width() <=  480){
            $('body').removeClass('overflow_hidden');
        }
    }

    //// HIDE POPOVERS IF click Outside
    $('html').on('click touchend', function (e) {
        if($(window).width() >  480){
            if (($(e.target).data('toggle') !== 'popover') && ($(e.target).parents('.popover').length === 0)) {
                $(".popover").removeClass("in fade");
                $(".popover").removeClass("popover_animate");
                $(".popover").addClass("popover_animate_disable");
            }
        }
        
        // CLOSING POPOVER WHEN CLICKED OUTSIDE
        
        if(!$(e.target).hasClass('ion-android-notifications') && $('.notification_popover').hasClass('active')){
            close_popover('notification_popover');
        }
        if(!$(e.target).hasClass('ion-android-menu') && $('.profile_popover').hasClass('active')){
            close_popover('profile_popover');
        }
        if(!$(e.target).hasClass('ion-gear-a') && $('.multitool').hasClass('active')){
            close_popover('multitool');
        }
        if(!$(e.target).hasClass('hamburger_menu') && $('.hamburger_menu').hasClass('active')){
            close_popover('hamburger_menu');
        }
        
        // END CLOSE BUTTON
    });
    
    $('body').on('click', '.multitool_popover a', function () {
        $(".popover").removeClass("popover_animate");
        $(".popover").addClass("popover_animate_disable");
    });
    //TODO DELETE .send_to .messages li when multiple messages is working
    $('body').on('click', '.message_popover, .message_next, .send_to .messages li', function () { //close popover
      $(".popover").removeClass("popover_animate");
      $(".popover").addClass("popover_animate_disable");
      $('body').css({'overflow': ''});
      $('.nav [data-toggle=popover]').removeAttr('aria-describedby');
    });

    $('.nav .avatar_icon').click(function(e){
        $('.profile_popover').closest('.popover').css('margin-top', "14px");
    });
    //close popover if link clicked
    $(document).on('click', '.popover .nav li a',function(e) {
      $(e.target).closest('.popover').popover('hide');
    });
    
    
    
    // NAV CLICK
    
    
    function nav_click(popover_name){
        setTimeout(function(){
            $('.' + popover_name).removeClass('active');
            $('#' + popover_name).addClass('hidden');
            $('#' + popover_name).removeClass('popover_animate');
        }, 200);
    }
    
    $('.popover_button').click(function(){
        if($('.profile_popover').hasClass('active')){
            nav_click('profile_popover');
        }
        if($('.notification_popover').hasClass('active')){
            nav_click('notification_popover');
        }
        if($('.multitool').hasClass('active')){
            nav_click('multitool');
        }
        if($('.hamburger_menu').hasClass('active')){
            nav_click('hamburger_menu');
        }
    });
    
    function menu_click(popover_name){
        $('#' + popover_name).removeClass('hidden');
        $('.' + popover_name).toggleClass('active');
        
        if($('.' + popover_name).hasClass('active')){
            $("#" + popover_name).removeClass("popover_animate_disable");
            $("#" + popover_name).addClass("popover_animate");
        }
        else{
            $("#" + popover_name).removeClass("popover_animate");
            $("#" + popover_name).addClass("popover_animate_disable");
            $('.' + popover_name).removeClass('active');
        }
        if($(window).width() <=  480){
            $('body').addClass('overflow_hidden');
        }
    }
    
    // class is button (icon) & ID is popover element
    
    
    $('.notification_popover').click(function(){
        $('.lazy_banner').Lazy({
            effect: "fadeIn",
            effectTime: 800,
            threshold: 0,
            delay: 0
        });
        menu_click('notification_popover');
    });
    
    $('.profile_popover').click(function(){      
        menu_click('profile_popover');
    });
    
    
    $('.multitool').click(function(){
        menu_click('multitool');
    });
    
    $('.hamburger_menu').click(function(){
        menu_click('hamburger_menu');
    });
    
    // END NAV CLICK
    
    
    // POPOVER PREVENT CLOSE ON TOUCH
    
    $('#notification_popover').on('click touchend', function(e) {
        if(!$(e.target).hasClass('close_x')){
            if(!$(e.target).hasClass('see_all')){
                e.stopPropagation();
            }
        }
    });
    
    $('#multitool, #profile_popover, #hamburger_menu').on('click touchend', function(e) {
        if($(e.target).hasClass('popover_animate')){
            e.stopPropagation();
        }
    });
    
    // END POPOVER PREVENT CLOSE ON TOUCH
    
    
    
    // CLOSE POPOVER WHEN LINK IS CLICKED
    
    function popover_link_close(popover_name){
        $("#" + popover_name).removeClass("popover_animate");
        $("#" + popover_name).addClass("popover_animate_disable");
        $('.' + popover_name).removeClass('active');
        if($(window).width() <=  480){
            $('body').removeClass('overflow_hidden');
        }
    }
    
    $('#multitool li a').click(function(e){
        popover_link_close('multitool');
    });
    
    $('#profile_popover li a').click(function(e){
        popover_link_close('profile_popover');
    });
    
    $('#hamburger_menu li a').click(function(e){
        popover_link_close('hamburger_menu');
    });
    
    // END LINKED CLICKED
    
    $('#pymk_mobile .see_all').click(function(){
        $('#pymk_modal').addClass('in');
        $('#pymk_modal').show();
    });

//// ===========================================* |||||||||||||| =========================================== ////
//// ===========================================*    MODALS    =========================================== ////
//// ===========================================* |||||||||||||| =========================================== ////
    
    //// CREATE BASKET ACTIVE ON CLICK
    $('.create_modal .createBasketMask').on('click', '.basket-btn-group button', function(){
      $('.createBasketMask').hide();
      var baskettype = $(this).data('basket');
      $('.createBasketMask.active').removeClass('active')
      $('.create_modal .createBasketMask#'+baskettype).addClass('active');
      $('.createBasketMask.active').show();
    });

    //// CREATE BASKET ACTIVE ON LOAD
    $('.createBasketMask').hide();
    $('.createBasketMask.active').show();
    
    ///// MODAL ON SHOW
    $(document).on('show.bs.modal', '.modal', function (e) {
      //shows nav
      $('.navbar-fixed-top').css({transform: '', transition: 'all 0s'});
    })
    
    
    //IOS and Safari
    if($(window).width() <=  480 && (iOS || iOSSafari)){
        ///// MODAL ON SHOWN
        $(document).on("shown.bs.modal", '.modal', function () { 
            body_scroll_position = $(document).scrollTop();
            setTimeout(function(){
                $('body.modal-open').css({'position': 'fixed', 'overflow': 'hidden', 'top' : body_scroll_position + 'px'});
            },20)
        });
    }

    $(".modal").on("shown.bs.modal", function () {
        $('.modal').promise().done(function(){
            $('#gift_url.form-control').focus();
        });
    });

    ///// MODAL ON HIDE
    $(document).on('hide.bs.modal', '.modal', function () {
      //IOS and Safari
      if($(window).width() <=  480 && (iOS || iOSSafari)){
        $('.navbar-fixed-top').css({transform: '', transition: 'all 0s'});
        $('body.modal-open').removeAttr('style');
        $(document).scrollTop(body_scroll_position);
      }

      $(this).removeClass('hidemodal');
      //close emoji picker
      wdtEmojiBundle.close();
    })

    //// MODAL ON HIDDEN
    $(document).on('hidden.bs.modal', '.modal', function (e) {
      //if a modal is still open add class open modal to body
      if ($('.modal-backdrop').length) {
        $('body').addClass('modal-open');
      }
    });

    if($(window).width() <=  480){
        $('.modal .closeBtn').click(function() {
            $(this).closest('.modal').addClass('hidemodal');
            var this_object = $(this).closest('.modal');
            setTimeout(function(){
                this_object.modal('hide');
            }, 400)
        })        
    }
    
    $('body').on('click', '.modal[data-backdrop="static"]:not(.center_modal)', function (e){
        if($(e.target).is('.modal') || $(e.target).is('.closeBtn') || $(e.target).is('.closeBtn div')){
            $(this).addClass('hidemodal');
            var this_object = $(this);
            setTimeout(function(){
                this_object.modal('hide');
            }, 400);
        }
    });


//// ===========================================* |||||||||||||| =========================================== ////
//// ================================*     MESSAGE POPOVER AND POPUP      ================ ================= ////
//// ===========================================* |||||||||||||| =========================================== ////


    $('body').on('click', '.scroll_message li, .message_next, .vert_btn_message, .connection .message_button, .profile_message', function (e) {
      e.preventDefault();

      var message_popup = $(this).data("user");
      if ($(this).data('cid') !== undefined) {
        var convo_id = $(this).data('cid');
        chatBox.open(convo_id);
      }else{
        var sender_id = $(this).data('sid');
        var recipient_id = $(this).data('rid');
        $.post("/conversations", { sender_id: sender_id, recipient_id: recipient_id }, function (data) {
          chatBox.open(data.conversation_id);
        });
      }

    });

    $('body').on('click', '.popup .closeBtn', function (){
        if($(window).width() <=  480){
            $(this).closest('.popup').addClass('hidemodal');
            setTimeout(function(){
                $('.popup').collapse('hide');
            }, 400)
        }
        else{
            $('.popup').collapse('hide');
        }
    })

    ///// POPUP ON HIDE
    $(".popup").on('hide.bs.collapse', function () {
      $(this).removeClass('hidemodal');
    })

    var message_array = [];

    $('body').on('click', '.send_to li', function () {

      // toggle checkbox on second menu
      var message_user = $(this).data("user");

      if ($(this).find(".message_check").hasClass("appear")){
        message_array.push(" " + message_user);
      }
      else{
        remove(" " + message_user,message_array);
      }

      function remove(item, message_array){
        for (var i=0;i<message_array.length;i++){
          if (message_array[i]==item){
            message_array.splice(i,1); //this delete from the "i" index in the array to the "1" length
            break;
          }
        }
      }
      $('#message_popup .top p').text(message_array);

        // TODO DELETE when multiple message is working
        var sender_id = $(this).data('sid');
        var recipient_id = $(this).data('rid');
        $.post("/conversations", { sender_id: sender_id, recipient_id: recipient_id }, function (data) {
            chatBox.open(data.conversation_id);
        });

        // END DELETE

    });

    $( ".navbar-nav li .ion-ios-chatbubble" ).click(function() {
      //message array with user names is cleared.
      message_array = [];
      $(".message_cancel").text("Cancel").css({'color':'#58a0c3', 'text-decoration':'underline'}).attr("href",".message_start").removeClass("collapsed message_next").attr("data-toggle","tab");
    });
    


//// ===========================================* |||||||||||||| =========================================== ////
//// ===========================================*     OTHER      =========================================== ////
//// ===========================================* |||||||||||||| =========================================== ////

    //// BACK TO TOP
    $('body').on('click', 'a.bktotop', function (e) {
      e.preventDefault();
      $('html, body').animate({scrollTop: 0}, 1000);
    });

    $('.save_and_continue_btn, .checkout_accordion').on('click', function(e){
      e.preventDefault();
      $("body").animate({ scrollTop: 0 }, 500);
    });

    $('body').bind('touchstart', function() {});


    //Share Message Comment Popup on click outside
    $(document).click(function(e) {
      if (!$(e.target).is('.collapse') && $(e.target).parents('.collapse').length === 0  && !$(e.target).parents('.wdt-emoji-popup').is('.wdt-emoji-popup') ) {
        $('.popup').collapse('hide');
        $(".scroll_message_popup").slimScroll({destroy: true});
      }
    });

    //Close emoji picker on click outside
    $(document).click(function(e) {
        if (!$(e.target).is('.wdt-emoji-picker') && $('.wdt-emoji-popup').hasClass('open') && $(e.target).parents('.wdt-emoji-popup').length === 0 ) {
         wdtEmojiBundle.close();
        }
    });

    //date picker
    $('.date-time-picker').datetimepicker({
      format: 'MM/DD/YYYY HH:mm A',
      minDate:new Date()
    });

    $('.date-picker').datetimepicker({
      format: 'MM/DD/YYYY'
    });


//// ===========================================* |||||||||||||| =========================================== ////
//// ========================================*     GIFT QUANTITY     ======================================= ////
//// ===========================================* |||||||||||||| =========================================== ////

    //instantiate touchspin
    $(".gift_qty").TouchSpin({
      min: 1,
      max: 99
    });

    $(".gift_qty").on('change', function(e){
      input = e.target.id;
      gift_id = input.substring(0, input.indexOf("_") + 0);
      val = $('#'+input).val();

      $('.tab_heading_right .info div:first-child p:first-child').html(val);

      //change gift quantity
      $.ajax({
        type     : 'PATCH',
        url      : '/gift/quantity_change/' + gift_id,
        data     : {
                     id: gift_id,
                     quantity: val,
                     quantity_type: 'desired'
                   },
        dataType : 'json',
        encode   : true,
        success  : function(data, response){
          if (data.response == true) {
            toastr.success('Wish quantity changed!');
          }else{
            toastr.error('Somthing went wrong. Please try again!');
          }
        }
      })

    })

//// ===========================================* |||||||||||||| =========================================== ////
//// ========================================*     EMOJI PICKER      ======================================= ////
//// ===========================================* |||||||||||||| =========================================== ////

    //Emoji Picker Image Sheet
    wdtEmojiBundle.defaults.emojiSheets = {
      'apple'    : 'https://cdnjs.cloudflare.com/ajax/libs/wdt-emoji-bundle/0.2.0/sheets/sheet_apple_64_indexed_128.png',
      'google'   : 'https://cdnjs.cloudflare.com/ajax/libs/wdt-emoji-bundle/0.2.0/sheets/sheet_google_64_indexed_128.png',
      'twitter'  : 'https://cdnjs.cloudflare.com/ajax/libs/wdt-emoji-bundle/0.2.0/sheets/sheet_twitter_64_indexed_128.png',
      'emojione' : 'https://cdnjs.cloudflare.com/ajax/libs/wdt-emoji-bundle/0.2.0/sheets/sheet_emojione_64_indexed_128.png',
      'facebook' : 'https://cdnjs.cloudflare.com/ajax/libs/wdt-emoji-bundle/0.2.0/sheets/sheet_facebook_64_indexed_128.png',
      'messenger': 'https://cdnjs.cloudflare.com/ajax/libs/wdt-emoji-bundle/0.2.0/sheets/sheet_messenger_64_indexed_128.png'
    };

    //Initiate Picker with class
    wdtEmojiBundle.init('.wdt-emoji-bundle-enabled');

    // Emoji Picker Tab order
    wdtEmojiBundle.defaults.sectionOrders = {
      'Recent'  : 10,
      'Custom'  : 9,
      'People'  : 8,
      'Nature'  : 7,
      'Foods'   : 6,
      'Activity': 5,
      'Places'  : 4,
      'Objects' : 3,
      'Symbols' : 2,
      'Flags'   : 1
    };

    //Hover Color
    wdtEmojiBundle.defaults.pickerColors = [
    'gray'
    ];

  if($(window).width() <=  480){
    $('.dropzone_file_info').html('We suggest cropping images at 200 x 56 for best display')
  }


}); //end of turbokink



//// ===========================================* |||||||||||||| =========================================== ////
//// ===============================*     FLASH NOTIFICATIONS SETTINGS     ================================= ////
//// ===========================================* |||||||||||||| =========================================== ////

toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "3000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }


//// ===========================================* |||||||||||||| =========================================== ////
//// =======================================*        COMMENT     ====================================== ////
//// ===========================================* |||||||||||||| =========================================== ////


if($(window).width() <=  480 && (iOS || iOSSafari)){
    $(document).on("focus", ".modal .reply_discussion textarea", function(e){
        $('body.modal-open').css('position', 'inherit');
    });
    $(document).on("blur", ".modal .reply_discussion textarea", function(e){
        setTimeout(function(){
            $('body.modal-open').css('position', 'fixed');
            $('.reply_container .reply_discussion').slideUp('fast');
        }, 1000);
    });
}


//// ===========================================* ||| SUCCESS COMMENT ||| =========================================== ////

$(document).on('ajax:success', '.comment_form', function(e, data, status, xhr){
  $(".comment_form input[name='content']").val('').attr('placeholder', 'Start a new discussion');
  $(e.target).find('.submit_comment').css({color: '#ccc'});
  $(e.target).find('.submit_comment').css({color: '#ccc'});
  $(e.target).find('textarea').val(null)
  $('.reply_discussion').slideUp('fast');
  $('.message_upload i').css({color: ''});
  if (status == 'success') {
    toastr.success('Comment Added');
  }else{
    toastr.error('Somthing went wrong. Please try again!');
  }
});

//// ===========================================* ||| KEY UP ||| =========================================== ////

$(document).on("keyup", ".comment_form textarea", function(e){
  if (e.target.value.length > 0) { 
    $(e.target).closest('.comment_form').find('.submit_comment').css({color: '#74a49d'});
  }else{
    $(e.target).closest('.comment_form').find('.submit_comment').css({color: '#ccc'});
  }
})

//// ===========================================* ||| REPLY COMMENT ||| =========================================== ////

$(document).on("click", ".reply", function (e) {
  e.preventDefault();
  comment_id = $(this).data('comment-id')
  $(this).parents('.message_container, .modal_comments_container').find('.reply_discussion').slideToggle('fast');
  $(this).parents('.message_container, .modal_comments_container').find('.reply_discussion textarea').focus();
  $(this).parents('.message_container, .modal_comments_container').find('.reply_discussion #commentable_id').val(comment_id)
  $(this).parents('.message_container, .modal_comments_container').animate({ scrollTop: $('.message_container, .modal_comments_container')[0].scrollHeight },500)
})

//// ===========================================* ||| DELETE COMMENT ||| =========================================== ////

$(document).on('ajax:success', '.delete_comment', function(e, data, status, xhr){
  $(this).parents('.message_details').fadeOut(200);
  $(this).parents('.message_details').prev('.img_discussion').fadeOut(200);
  if (data.response == true) {
    toastr.success('Comment Deleted');
  }else{
    toastr.error('Somthing went wrong. Please try again!');
  }
});

//// ===========================================* ||| EVENT INVITE CUSTOMER ||| =========================================== ////

$(document).on('ajax:success', '.message.invite', function(e, data, status, xhr){
    if (data.response == true) {
        $(this).addClass('invited');
        $(this).removeClass('message');
        $(this).text('Invited');
        $('a[href="#invited"] span:first-of-type').text(data.invite_count);
        toastr.success('Customer invited to event!');
        var clone = $(this).closest('.connection-tile').clone();
        $('.guest_list_container #invited').append(clone)
    }else{
        toastr.error('Somthing went wrong. Please try again!');
    }
});


////TO DO IMAGE UPLOAD COMMENT
// $(document).on("click", ".message_upload", function (e) {
//   e.preventDefault();
//   $(this).next('input').click();
//   $('input[type=file]').change(function () {
//     $('.message_upload i').css({color: '#e8a149'})
//     $('.comment_form input[name=content]').attr('placeholder', 'Enter image description')
//     toastr.success('image slected for upload');
//   });
// })

//// ===========================================* ||| LIKE COMMENT ||| =========================================== ////
  
  $(document).on("click",".like", function(e) {
    e.preventDefault();
      link = $(this)
      var formData = {
        'likable_id'    : $(this).data('id'),
        'likable_type' : $(this).data('likable')
      }
      $.ajax({
          type     : 'PATCH',
          url      : '/likes/toggle',
          data     : formData,
          dataType : 'json',
          encode   : true,
          success  : function(data, response){
                if(link.data('solid-heart') == true){
                    link.next().text(data.like_count);
                    link.find('i').toggleClass('liked');
                    //Tile like
                    if(link.find('i').hasClass('heart_disable')){
                        link.find('i').removeClass('heart_disable');
                        link.find('i').addClass('heart_enable');

                    }
                    else if(link.find('i').hasClass('heart_enable')){
                        link.find('i').removeClass('heart_enable');
                        link.find('i').addClass('heart_disable');
                    }
                    else{
                        link.find('i').addClass('heart_enable');
                    }
                }
                else{
                    link.next().text(data.like_count);
                    link.find('i').toggleClass('ion-android-favorite ion-android-favorite-outline');
                    //Tile like
                    if(link.find('i').hasClass('ion-android-favorite-outline')){
                        link.find('i').removeClass('heart_disable');
                        link.find('i').removeClass('liked');
                        link.find('i').addClass('heart_enable');

                    }
                    else if(link.find('i').hasClass('ion-android-favorite')){
                        link.find('i').removeClass('heart_enable');
                        link.find('i').addClass('liked');
                        link.find('i').addClass('heart_disable');
                        
                    }
                }
                //Discussion like
                if(formData.likable_type == 'Comment'){
                  if (data.like_count == 1) {
                    $('<span class="likes fadeIn">'+ data.like_count + ' <i class="icon ion-android-favorite" aria_hidden="true"></i> </span>').insertAfter(link.parent('.message_stats').find('.reply'));
                  }else if(data.like_count > 1){
                    link.parent('.message_stats').find('span.likes').html(data.like_count + ' <i class="icon ion-android-favorite" aria_hidden="true"></i>');
                  }else{
                    link.parent('.message_stats').find('span.likes').remove();
                  }
                // basket like
                }

          },
          error : function(request,error) {
            console.log("Request: "+JSON.stringify(request));
          }
      });
  });


//// ===========================================* |||||||||||||| =========================================== ////
//// =====================================*     HOW IT WORKS      ================================= ////
//// ===========================================* |||||||||||||| =========================================== ////

  $(document).on('click', '.animate_scroll', function(e) {
    
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 125 //offsets for fixed header
        }, 1000);
        return false;
      }
    }

    //Executed on page load with URL containing an anchor tag.
    if($(location.href.split("#")[1])) {
      setTimeout(function(){
        var target = $('#'+location.hash.substr(1));
        if (target.length) {
          $('html,body').animate({
          scrollTop: target.offset().top - 125 //offset height of header here too.
        }, 1000);
          return false;
        }
      },1000)
    }

  });


//// ===========================================* |||||||||||||| =========================================== ////
//// ====================================*    Connecting Functionality     ================================= ////
//// ===========================================* |||||||||||||| =========================================== ////

$(document).on("click", ".follow-connection", function () {
    var id             = $(this).attr('id').split('-');
    var customer_id    = id[1];

    $.ajax({
        url      : '/profile/follow',
        type     : 'PATCH',
        data     : { id: customer_id },
        dataType : 'json',
        success  : function(data) {
            if(data['response']) {
                window.location.reload();
            }
        },
        error    : function(data) {
            console.log("Request: "+JSON.stringify(request));
        }
    });
});

//// ===============================*     CONFIRM CONNECTION     ================================= ////
$(document).on("click", ".unfollow-connection", function() {
    var id             = $(this).attr('id').split('-');
    var customer_id    = id[1];

    $.ajax({
        url      : '/profile/unfollow',
        type     : 'PATCH',
        data     : { id: customer_id },
        dataType : 'json',
        success  : function(data) {
            if(data['response'] == true){
                window.location.reload();
            }
        },
        error    : function(request,error) {
            console.log("Request: "+JSON.stringify(request));
        }
    });
});

//// ===============================*     FOLLOW CONNECTION     ================================= ////
$(document).on("click", ".follow_connection", function () {
    var id             = $(this).data('id').split('-');
    var customer_id    = id[1];
    var this_button = $(this);
    var name = $(this).closest('.connection').find('.name').text();

    $.ajax({
        url      : '/profile/follow',
        type     : 'PATCH',
        data     : { id: customer_id, notify: true },
        dataType : 'json',
        success  : function(data) {
            if(data.response == true) {
                $(this_button).addClass('unfollow_connection');
                $(this_button).removeClass('follow_connection');
                toastr.success('Successfully followed ' + name);
            }
        },
        error    : function(data) {
            console.log("Request: "+JSON.stringify(request));
        }
    });
});

//// ===============================*     UNFOLLOW CONNECTION     ================================= ////
$(document).on("click", ".unfollow_connection", function() {
    var id             = $(this).data('id').split('-');
    var customer_id    = id[1];
    var this_button = $(this);
    var name = $(this).closest('.connection').find('.name').text();

    $.ajax({
        url      : '/profile/unfollow',
        type     : 'PATCH',
        data     : { id: customer_id, notify: true },
        dataType : 'json',
        success  : function(data) {
            if(data.response == true){
                $(this_button).find('span').text('Follow');
                $(this_button).addClass('follow_connection');
                $(this_button).removeClass('unfollow_connection');
                toastr.success('Successfully unfollowed ' + name);
            }
        },
        error    : function(request,error) {
            console.log("Request: "+JSON.stringify(request));
        }
    });
});

//// ===============================*     BLOCK CONNECTION     ================================= ////
$(document).on("click", ".block-connection", function() {
    var customer_id = $(this).data("id").split('-')[2];
    var main_parent = $(this).closest('.connections_container');
    $('[data-connection-id="'+ customer_id +'"').fadeOut();
    var $this = $(this);
    $.ajax({
        url      : '/profile/block',
        type     : 'PATCH',
        data     : { id: customer_id },
        dataType : 'json',
        success  : function(data) {
            if(data.response == true){
                if ($this.parents('.popover').length){
                    location.reload();
                }
                else{
                    main_parent.find('.connection-tile-'+ customer_id).fadeOut(function(){
                        $('[data-connection-id="'+ customer_id +'"').remove();
                        var update_friends =  main_parent.find('#friends').children().length;
                        var update_following =  main_parent.find('#following').children().length;
                        var update_followers =  main_parent.find('#followers').children().length;
                        $('ul a[href="#friends"] span:first-of-type').text(update_friends);
                        $('ul a[href="#following"] span:first-of-type').text(update_following);
                        $('ul a[href="#followers"] span:first-of-type').text(update_followers);
                    });
                }
                toastr.success('Successfully Blocked This User');
            }
        },
        error   : function(request, error) {
            console.log("Request: "+JSON.stringify(request));
        }
    });
});

//// ===============================*     UNBLOCK CONNECTION     ================================= ////
$(document).on("click", ".unblock-connection", function() {
    var id           = $(this).data('id').split('-');
    var customer_id  = id[2];
    var blockedCount = parseInt($('#blocked-count').html());
    var $this = $(this);
    $.ajax({
        url      : '/profile/unblock',
        type     : 'PATCH',
        data     : { id: customer_id },
        dataType : 'json',
        success  : function(data) {
            if(data['response'] == true){
                if ($this.parents('.popover').length){
                  location.reload();
                }
                else{
                    blockedCount--;
                    $('#blocked-tile-' + customer_id).parent().fadeOut();
                    $('#blocked-count').html('' + blockedCount);
                }
                toastr.success('Successfully Unblocked This User');
            }
        },
        error    : function(request, error) {
            console.log("Request: "+JSON.stringify(request));
        }
    });

});

//// ===============================*     REMOVE CONNECTION     ================================= ////
$(document).on("click", ".remove-connection", function() {
    var id              = $(this).data('id').split('-');
    var tab             = id[1];
    var customer_id     = id[2];
    var connectionCount = parseInt($('#connection-count').html());
    var this_button = $(this);

    $.ajax({
        url      : '/profile/remove',
        type     : 'PATCH',
        data     : { id: customer_id },
        dataType : 'json',
        success  : function(data) {
            if(data.response == true) {
                toastr.success('Connection Successfully Removed');
                connectionCount--;
                $(this_button).closest('.col-xl-3').fadeOut(function(){
                    $(this_button).closest('.col-xl-3').remove();
                });
                $('#connection-count').html('' + connectionCount);
            }
        },
        error    : function(request,error) {
            console.log("Request: "+JSON.stringify(request));
        }
    });
});

$(document).on("click", ".multitool_popover .remove-connection", function() {
    location.reload();
});

//// ===============================*     CONFIRM CONNECTION     ================================= ////
$(document).on("click", ".confirm-connection", function(e) {
    e.preventDefault();
    var id              = $(this).attr('id').split('-');
    var customer_id     = id[2];
    var connectionCount = parseInt($('#connection-count').html());
    var pendingCount    = parseInt($('#pending-count').html());
    var location        = $(this).data("location");

    $.ajax({
        context  : this,
        url      : '/profile/approve',
        type     : 'PATCH',
        data     : { id: customer_id },
        dataType : 'json',
        success  : function(data) {
            if (data['response'] == true) {
                if( location == "profile") {
                    connectionCount++;
                    pendingCount--;
                    $('.requester-tile-' + customer_id).parent().fadeOut(function(){
                        $(this).remove();
                    });
                    $('#connection-count').html('' + connectionCount);
                    $('#pending-count').html('' + pendingCount);
                } else if( location == "notifications" ) {
                    pendingCount--;
                    $('#pending-count').html(pendingCount);
                    $('.requester-tile-' + customer_id).find('.confirm_remove_container').fadeOut(function(){
                        $(this).html('<span class="confirmed">Connection Added</span>')
                        $(this).fadeIn();
                    })
                } else {
                    $(this).fadeOut();
                }
            }
        },
        error    : function(request, error) {
            console.log("Request: " + JSON.stringify(request));
        }
    });
});

//// ===============================*     REQUEST CONNECTION    ================================= ////
$(document).on("click", ".request-connection", function(e) {
    e.preventDefault();
    var id          = $(this).attr('id').split('-');
    var customer_id = id[2];
    var location    = $(this).data("location");

    $.ajax({
        context  : this,
        url      : '/profile/request',
        type     : 'PATCH',
        data     : { id: customer_id },
        dataType : "json",
        success  : function(data) {
            if (data['response'] == true) {
                if( location == "navbar" ) {
                     window.location.reload();
                } else {
                  $(this).fadeOut();
                  
                }
            }
        },
        error : function(request, error) {
            console.log("Request: " + JSON.stringify(request));
        }
    });
});

//// ===============================*     REQUEST CONNECTION     ================================= ////
$(document).on("click", ".request_connection", function(e) {
    e.preventDefault();
    var id          = $(this).data('id').split('-');
    var customer_id = id[2];
    var location    = $(this).data("location");
    var name = $(this).data('name');

    $.ajax({
        context  : this,
        url      : '/profile/request',
        type     : 'PATCH',
        data     : { id: customer_id, notify: true },
        dataType : "json",
        success  : function(data) {
            if (data.response == true) {
                toastr.success('Connection Request Sent to ' + name);
                $(this).fadeOut(function(){
                    $(this).remove();
                });
            }
        },
        error : function(request, error) {
            console.log("Request: " + JSON.stringify(request));
        }
    });
});

//// ===============================*     REJECT CONNECTION     ================================= ////
$(document).on("click", ".reject-connection", function() {
    var id           = $(this).attr('id').split('-');
    var customer_id  = id[2];
    var pendingCount = parseInt($('#pending-count').html());
    var location     = $(this).data("location"); 

    $.ajax({
        context  : this,
        url      : '/profile/reject',
        type     : 'PATCH',
        data     : { id: customer_id },
        dataType : 'json',
        success  : function(data) {
            if (data['response'] == true) {
                if( location == "profile") {
                    pendingCount--;
                    $('.requester-tile-' + customer_id).parent().fadeOut();
                    $('#pending-count').html('' + pendingCount);
                } else if( location == "notifications" ) {
                    pendingCount--;
                    $('#pending-count').html(pendingCount);
                    $('.requester-tile-' + customer_id).find('.confirm_remove_container').fadeOut(function(){
                        $(this).html('<span class="confirmed">Connection Rejected</span>')
                        $(this).fadeIn();
                    })
                } else {
                    $(this).fadeOut();
                }
            }
        },
        error    : function(request, error) {
            console.log("Request: " + JSON.stringify(request));
        }
    });
});

//// ===========================================* |||||||||||||| =========================================== ////
//// =========================================*    GIFT ACTIONS     ====================================== ////
//// ===========================================* |||||||||||||| =========================================== ////


//// ===============================*     RECIEVED GIFT     ================================= ////
$(document).on('ajax:success', '.mark_received', function(e, data, status, xhr){
   if (data.response == true) {
    toastr.success('Gift Marked as Recieved');
    $(this).find('span').fadeOut('normal', function() {
        $(this).closest('.product-item-content').prepend('<img alt="Wish Already Purchased Ribbon" class="gift_bought_ribbon" src="/images/products/bought_ribbon.png" style="display:none">');
        $(this).closest('.product-item-content').find('.gift_bought_ribbon').fadeIn();
        $(this).text('Received');
        $(this).fadeIn('normal', function() {
            $(this).closest('.mark_received').removeClass('mark_received');    
        });
    });
    if ($('.product_item_container.gift').length > 0 ) {
        $('.product_item_container.gift').prepend('<img alt="Wish Already Purchased Ribbon" class="gift_bought_ribbon" src="/images/products/bought_ribbon.png" style="display:none">');
        $('.gift_bought_ribbon').fadeIn();
    }
   }else{
    toastr.error('Something went wrong please try again');
   }
})

//// ===============================*     REMOVE GIFT     ================================= ////
$(document).on('ajax:success', '.remove_gift', function(e, data, status, xhr){
   if (data.response == true) {
    // remove from basket view 
    if (e.target.parentElement.className == 'product-content-upper') {
        $(this).closest('.product-item').fadeOut(200, function() {
            $('.products').masonry();
            if($('.product_list > .product-item').length <= 1){
                $('#gifts').children().fadeOut(function(){
                    $('#gifts').append('<div class="no_gifts" style="display:none"><p>This wish list has no wishes to show. Head to the <a href="/">feed</a> or use our <a rel="nofollow" data-toggle="modal" href="#gift_url">Wish Upload Tool</a> and add some wishes!</p></div>');
                    $('.no_gifts').fadeIn();
                });
            }
        });
    // remove from gift view 
    }
    else{
      window.location = data.basket.url
    }
    toastr.success('Wish successfully deleted!');
  }
  else{
    toastr.error('Somthing went wrong. Please try again!');
  }
});

//// ===============================*     GOOGLE DATA LAYER PRODUCT CLICK     ================================= ////
function productClick( product, merchant ){
    color = ( product.color.length > 0 ? product.color : 'none' )
    dataLayer.push({
        'event': 'productClick',
        'ecommerce': {
            'click': {
                'actionField': {'list': 'Search Results'},      
                'products': [{
                  'name': product.name,                      
                  'id': product.id,
                  'price': product.price/100,
                  'brand': merchant.name,
                  'variant': color,
                  'position': product.position
                 }]
            }
        }
    });
}

//// ===========================================* |||||||||||||| =========================================== ////
//// =========================================*    IMAGE CROPPING     ====================================== ////
//// ===========================================* |||||||||||||| =========================================== ////

function ImageCropper(file, dropzone, current_modal, autoprocess, crop_type){
  // transform cropper dataURI output to a Blob which Dropzone accepts
  function dataURItoBlob(dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: 'image/jpeg' });
  }

  if (file.cropped) {
    return;
  }
  var cachedFilename = file.name;

  var reader = new FileReader();
  reader.onloadend = function () {
    $(current_modal+' .crop-container').removeClass('hidden');
    $(current_modal+' .crop-container .dz-cr-image img').attr('src', reader.result);
    $(current_modal+' .stock_banners').hide();

    //idenifies croper image
    collapse = $(current_modal+' .collapse.in').attr('id');
    if (collapse) {
     var $img = $(current_modal+' #'+collapse+' .crop-container .dz-cr-image img');
    }else{
      var $img = $(current_modal+' .crop-container .dz-cr-image img');
    }
    
    if (crop_type == 'banner') {
      //Banner Cropper
      $img.cropper({
        movable: false,
        aspectRatio: 32/9,
        cropBoxResizable: true,
        responsive: true,
        ready: function (e) {
          //displays initial dimensions
          banner_crop = $img.cropper('getImageData');
          width = banner_crop.naturalWidth.toFixed(0);  
          height = banner_crop.naturalHeight.toFixed(0); 
          $(current_modal+' .crop-container .cropper_width').one().html('Width: '+width+'px');
          $(current_modal+' .crop-container .cropper_height').html('Height: '+height+'px');
        }
      });

    }else{

      //Avatar Cropper
      $img.cropper({
        movable: false,
        aspectRatio: 1/1,
        cropBoxResizable: true,
        responsive: true,
        ready: function (e) {
          //displays initial dimensions
          avatar_crop = $img.cropper('getImageData');
          width = avatar_crop.naturalWidth.toFixed(0);  
          height = avatar_crop.naturalHeight.toFixed(0);  
          $(current_modal+' .crop-container .cropper_width').html('Width: '+width+'px');
          $(current_modal+' .crop-container .cropper_height').html('Height: '+height+'px');
        }
      });

    }
    //croper upload
    $('.crop-upload').on('click', function(e) {
      e.preventDefault()
      //use croped canvas for new file
      var blob = $img.cropper('getCroppedCanvas').toDataURL('image/jpeg');
      var newFile = dataURItoBlob(blob);
      newFile.cropped = true;
      newFile.name = Date.now()+'_'+cachedFilename
      dropzone.addFile(newFile);
      if (autoprocess == true) {
          $(current_modal).modal('hide');
          dropzone.processQueue();
      }else if (autoprocess == false) {
        $('#lifestyle-upload, #event-upload, #charity-upload').collapse('hide');
        toastr.success('Wish list banner added');
        $('input#default_banner').remove();
      }
    });
    //croper rotate right
    $('.rotate-right').on('click', function (e) {
      e.preventDefault();
      $img.cropper('rotate', 90);
    })
    //croper rotate left
    $('.rotate-left').on('click', function (e) {
      e.preventDefault();
      $img.cropper('rotate', -90);
    })
    //croper zoom in
    $('.zoom-in').on('click', function (e) {
      e.preventDefault();
      $img.cropper('zoom', 0.1);
    })
    //croper zoom out
    $('.zoom-out').on('click', function (e) {
      e.preventDefault();
      $img.cropper('zoom', -0.1);
    })
    //croper reset
    $('.reset').on('click', function (e) {
      e.preventDefault();
      $img.cropper('reset');
    })
    //destroys dropzone file and cropper when hidden
    $(current_modal).on('hidden.bs.modal', function(){
      dropzone.removeAllFiles();
      $(current_modal+' .crop-container').addClass('hidden');
      $img.cropper('destroy');
      $(current_modal+' .stock_banners').show();
    })
  };
  reader.readAsDataURL(file);
}

//// ===========================================* |||||||||||||| =========================================== ////
//// =========================================*    CONNECTIONS AUTOCOMPLETE     ====================================== ////
//// ===========================================* |||||||||||||| =========================================== ////


function connections_autocomplete(connection_type, data){
    var search_type = {
        data: data,
        list: {
            maxNumberOfElements: 5,
            match: {
                enabled: true
            },
            sort: {
                enabled: true
            },
            onClickEvent: function() {
                if (connection_type == 'search_wishlists') {

                  val = $("input[name='search_wishlists']").val();
                  var filter = "(?:^|\\s)" + val, itemsFound = 0;
                  $(".group_tile_Details h2 span a").each(function(){
                    if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                      $(this).closest('.group_tile').fadeOut();
                    }else{
                      $(this).closest('.group_tile').show();
                      itemsFound++;
                    }
                  })

                }else if (connection_type == 'search_wishes') {

                  val = $("input[name='search_wishes']").val();
                  var filter = "(?:^|\\s)" + val, itemsFound = 0;
                  $(".product-item .product-content-lower .product_title").each(function(){
                    if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                     $(this).closest('.product-item').removeClass('product-active').fadeOut();
                     // resize masonry grid after click
                     var masonry = new Masonry( '.products', {
                      itemSelector: '.product-active',
                      columnWidth: '.product-sizer',
                      isAnimated: true,
                      percentPosition: true
                    })
                   }else{
                    $(this).closest('.product-item').addClass('product-active').show();
                      // resize masonry grid after click
                      var masonry = new Masonry( '.products', {
                        itemSelector: '.product-active',
                        columnWidth: '.product-sizer',
                        isAnimated: true,
                        percentPosition: true
                      })
                    }
                  })

                }else{
                  
                  val = $("input[name='"+connection_type+"']").val();
                  var filter = val, itemsFound = 0;
                  $(".connection_info .name").each(function(){
                    if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                      $(this).closest('.connection-tile').fadeOut();
                    }else{
                      $(this).closest('.connection-tile').show();
                      itemsFound++;
                    }
                  })

                }
                
            }   
        }
    };

    $("input[name='"+connection_type+"']").easyAutocomplete(search_type);

    $("input[name='"+connection_type+"']").keyup(function(){

      if (connection_type == 'search_wishlists') {
        
        filter = "(?:^|\\s)" + $(this).val(), itemsFound = 0;
        $(".group_tile").each(function(){
          if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).closest('.group_tile').fadeOut();
          } else {
            $(this).closest('.group_tile').show();
            itemsFound++;
          }
        });

      }else if (connection_type == 'search_wishes') {

        filter = "(?:^|\\s)" + $(this).val(), itemsFound = 0;
        $(".product-item .product-content-lower .product_title").each(function(){
          if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).closest('.product-item').removeClass('product-active').fadeOut();
          } else {
            $(this).closest('.product-item').addClass('product-active').show();
            itemsFound++;
            var masonry = new Masonry( '.products', {
              itemSelector: '.product-active',
              columnWidth: '.product-sizer',
              isAnimated: true,
              percentPosition: true
            })
          }
        });

      }else{

        filter = $(this).val(), itemsFound = 0;
        $(".connection_info .name").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).closest('.connection-tile').fadeOut();
            } else {                
                $(this).closest('.connection-tile').show();
                itemsFound++;
            }
        });

      }
        
    });

}

//// ===========================================* |||||||||||||| =========================================== ////
//// =========================================*    SHARE POPUP     ====================================== ////
//// ===========================================* |||||||||||||| =========================================== ////

$.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
  // Prevent default anchor event
  e.preventDefault();
  // Set values for window
  intWidth = intWidth || '500';
  intHeight = intHeight || '400';
  strResize = (blnResize ? 'yes' : 'no');
  // Set title and open popup with focus on it
  var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
      strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
      objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
}

$(document).on("click", ".share_social a[target='_blank']", function(e) {
  $(this).customerPopup(e);
})



//// ===========================================* |||||||||||||| =========================================== ////
//// =========================================*    MESSAGE POPUP     ====================================== ////
//// ===========================================* |||||||||||||| =========================================== ////

//message text input
$(document).on("keyup", ".chatboxtextarea", function(e){
  conversation_id = e.target.parentNode.id.split('_')[2];
  chatBox.checkInputKey(e, conversation_id);
  if (e.target.value.length > 0) { 
    $(e.target).closest('.chatbox').find('.submit_message').css({color: '#74a49d'})
  }else{
    $(e.target).closest('.chatbox').find('.submit_message').css({color: '#ccc'})
  }
})

// message image modal trigger
$(document).on("click", "#message_image_modal", function () {
 var imgsrc = $(this).data('url');
 $('#messageImage img').attr('src',imgsrc);
});

// message upload trigger
$(document).on("click", '.comment_addimage_btn', function(e){
  e.preventDefault();
  id = e.target.parentNode.id;
  $("#"+id+" #message_media").trigger('click');
})

// message preview clear trigger
$(document).on('click', '.clear_commentImage', function(e){
  e.preventDefault();
  chatBox.closePreview(e)
})

// message preview submit trigger
$(document).on('click', '.submit_commentImage', function(e){
  e.preventDefault();
  conversation_id = $(e.target).closest('.chatbox').find('.chatboxinput form').attr('id').split('_')[2];
  chatBox.fileSubmit(e, conversation_id)
})

// message paste trigger
if (!!window.chrome) {
  $(document).on('paste', '#message_body', function(e){
    //check if image
    img = null;
    items = e.originalEvent.clipboardData.items
    for (var i = 0; i < items.length; i++) {
      if (items[i].type.indexOf("image") == 0){
        img = items[i].getAsFile();
      }
    }
    var FileSize = (img.size / 1024 / 1024).toFixed(3); // in MB
    // preview if image
    if (img != null && FileSize <= 1) {
      e.preventDefault();
      conversation_id = e.target.parentNode.id.split('_')[2];
      chatBox.filePreview(e, conversation_id, 'paste', img);
      img = null;
    }else{
      e.preventDefault();
      $(this).val('');
      autosize.update($(this));
      toastr.error("Your file must be under 1MB");
      img = null;
    }

  });
}else{

  $(document).on('paste', '#message_body', function(e){
    str = (e.originalEvent || e).clipboardData.getData('text/plain');
    idx = str.lastIndexOf('.');
    ext = str.slice(idx + 1);
    if (ext == 'jpg' || ext == 'png' || ext == 'jpeg') {
      e.preventDefault();
      $(this).val('');
      autosize.update($(this));
      toastr.error("sorry can't pasted image please use image selector");
    }
  })

}

// message close trigger
$(document).on('click', '.closeChat', function (e) {
    e.preventDefault();
    var id = $(this).data('cid');
    if($(window).width() <=  480){
        $(this).closest('.chatbox').addClass('hidemessage');
        setTimeout(function(){
            chatBox.close(id);
        },400)
    }
    else{
        chatBox.close(id);
    }
});

// message button submit trigger
$(document).on('click', '.submit_message', function(e){
  e.preventDefault();
  conversation_id = e.target.parentNode.id.split('_')[2];
  chatBox.sendMessage(e, conversation_id);
})

$(document).on('click', '.share_icon', function(e){
  patch_url = $(this).data('target');
  //change gift quantity
  $.ajax({
    type     : 'PATCH',
    url      : patch_url,
    dataType : 'script',
    encode   : true
  })
})

//// ===========================================* |||||||||||||| =========================================== ////
//// =========================================*    Messaging Inbox Count    ====================================== ////
//// ===========================================* |||||||||||||| =========================================== ////
/*
$(document).ready(function(){
  setInterval( function(){
    $.ajax({
       url: '/messages/not_read',
       type: 'get',
       success: function(data) {
          //console.log(data.count);
         if(data.count){
           $('#unread_message_count').html(data.count);
           $('#unread_message_count').removeClass("hidden");
         }
          else {
            $('#unread_message_count').addClass("hidden");
          }
       },
       error: function(data) {
           console.log('get msg not read',data.count);
       }
    });
  }, 10000 );
})
*/