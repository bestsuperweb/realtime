/**
 * Chat logic
 *
 * Most of the js functionality is inspired from anatgarg.com
 * jQuery tag Module from the tutorial
 * http://anantgarg.com/2009/05/13/gmail-facebook-style-jquery-chat/
 *
 */
var chatboxFocus = new Array();
var chatBoxes = new Array();
var chatboxBlob = new Array();
var old_val = '';
var FILE = null;

var ready = function () {

    chatBox = {

        //Chatbox open 
        open: function (conversation_id) {
            chatBox.createChatBox(conversation_id);
        },

        // Chatbox close 
        close: function (conversation_id) {

            $("#chatbox_" + conversation_id).hide(50, function(){
              $('#chatbox_' + conversation_id).remove();
              chatBox.restructure();
            });

            chatBox.blobDestroy(conversation_id);
            for (var i=0;i<chatBoxes.length;i++){
                if (chatBoxes[i]==conversation_id){
                  chatBoxes.splice(i,1); //this delete from the "i" index in the array to the "1" length
                  break;
                }
            }  
        },

        // Chatbox notify todo 
        notify: function () {
            setTimeout(function() {
                $(".chatboxcontent ul").scrollTop($(".chatboxcontent ul")[0].scrollHeight);
            }, 100);
        },

        // Chatbox layout restructure
        restructure: function () {
            chatBoxes.forEach(function(id, idx) {
              right_position = idx * 310 + 20;
              $('#chatbox_'+id).css({right: right_position +'px'});
            });
        },

        // Chatbox close preview
        closePreview: function(e){
          conversation_id = $(e.target).closest('.chatbox').find('.chatboxinput form').attr('id').split('_')[2];
          chatbox = '#chatbox_'+conversation_id;
          $('#conversation_form_'+conversation_id+' #message_media').val(null);
          $(chatbox+' .preview-container').fadeOut(500, function(){
            $(chatbox+' .preview-container').remove();
          });
          chatBox.blobDestroy(conversation_id);
        },

        // Chatbox blob exists
        blobExists: function(conversation_id){
          return chatboxBlob.includes(conversation_id);
        },

        // Chatbox blob destroy
        blobDestroy: function(conversation_id){
          for (var i=0;i<chatboxBlob.length;i++){
                if (chatboxBlob[i]==conversation_id){
                  chatboxBlob.splice(i,1); //this delete from the "i" index in the array to the "1" length
                  break;
                }
            }  
        },

        // Chatbox infinite scroll
        infiniteScroll: function(chatbox, id, page){

          //init controller
          var messageController = new ScrollMagic.Controller({container: chatbox+" .chat", globalSceneOptions: {triggerHook: "onCenter"}});

          if(page > 0){
            $(chatbox+' .chat > div').prepend('<div class="message_loader"><i aria_hidden="true" class="icon ion-load-c spinner icon_center active"></i></div>');
          }
          // build scene
          var messageScene = new ScrollMagic.Scene({triggerElement: chatbox+" .chat .message_loader", duration: 400})
          .addTo(messageController)
          .on('enter', function (e){
            if(e.target.controller().info('scrollDirection') == "REVERSE"){
              page--;
              if(page > 0){
                var more_messages_url = '/conversations/' + id + '?page=' + page;
                $.ajax({
                url: more_messages_url, //pagination url
                type: 'GET',
                success: function (data) {
                  var message_list = $(data).find('.chat > div').html();
                  add_new_page(message_list, chatbox);
                  $(chatbox+" .chat .message_loader").remove();
                },
                error: function(){
                  console.log("Error");
                  $(chatbox+" .chat .message_loader i").removeClass("active");
                }
              });
              }
              else if(page <= 1){
                $(chatbox+" .message_loader").remove();
              }
            }
          });

          function add_new_page (message_list, chatbox) {
            var old_height = $(chatbox+' .chat > div').height();  //store document height before modifications
            var old_scroll = $(chatbox+' .chat').scrollTop(); //remember the scroll position
            $(chatbox+' .chat > div').prepend(message_list);
            $(chatbox+' .chat > div').prepend('<div class="message_loader"><i aria_hidden="true" class="icon ion-load-c spinner icon_center active"></i></div>');
            var previous_height = 0;
            $(chatbox+' .chat').scrollTop(old_scroll + $(chatbox+' .chat > div').height() - old_height); //restore "scroll position"
            messageScene.update(); // make sure the scene gets the new start position
          } 

        },


        //Chatbox create
        createChatBox: function (conversation_id, minimizeChatBox) {
            // check if convo exists if not add id to chatboxes 
            convo_exists = chatBoxes.includes(conversation_id);
            if (convo_exists == false) {
              chatBoxes.push(conversation_id);
            } 

            $("body").append('<div id="chatbox_' + conversation_id + '" class="chatbox" style="display: none;"></div>')

            $.get("/conversations/" + conversation_id, function (data) {
                $('#chatbox_' + conversation_id).html(data);
                $("#chatbox_" + conversation_id + " .chatboxcontent").scrollTop($("#chatbox_" + conversation_id + " .chatboxcontent ul")[0].scrollHeight);
                
                // show chatbox after get data 
                $("#chatbox_" + conversation_id).show(50, function(){
                  shownChatbox();
                });
                chatBox.restructure();

            }, "html");

            $("#chatbox_" + conversation_id).css('bottom', '0px');

            $("#chatbox_" + conversation_id).click(function (e) {
                if ($('#chatbox_' + conversation_id + ' .chatboxcontent').css('display') != 'none') {
                    $("#chatbox_" + conversation_id + " .chatboxtextarea").focus();
                }
            });

        },

        //Chatbox send message using button
        sendMessage: function(e, conversation_id){
          message = $("#conversation_form_"+conversation_id+" #message_body").val()
          
          if(message.length > 0 && !chatBox.blobExists(conversation_id) ) {
             //Fade preview 
              $('#chatbox_'+conversation_id+' .preview-container').fadeOut(100, function(){
                $('#chatbox_'+conversation_id+' .preview-container').remove();
                $('#chatbox_'+conversation_id+' .chat').scrollTop($('.chat')[0].scrollHeight);
              });
              //Submit Form 
              $("#conversation_form_"+conversation_id+"").submit().bind("ajax:complete", function(event,xhr,status){
                //remove media and clear blob 
                $(e.target).closest('.chatbox').find('.submit_message').css({color: '#ccc'})
                $("#conversation_form_"+conversation_id+' #message_media').val(null);
                chatBox.blobDestroy(conversation_id);

                //Clear Text input and resize autosize
                $("#conversation_form_"+conversation_id+" #message_body").val('');
                old_val = '';
                autosize.update($("#conversation_form_"+conversation_id+" #message_body"));
              })

          }else if(message.length > 0 && chatBox.blobExists(conversation_id) ){
              //Form data for image paste 
              var fd = new FormData();
              fd.append('media', FILE);
              fd.append('body', message);
              $.ajax({
                type: 'POST',
                url: '/conversations/'+conversation_id+'/messages',
                data: fd,
                processData: false,
                contentType: false,
                success: function(){
                  //remove media and clear blob 
                  $(e.target).closest('.chatbox').find('.submit_message').css({color: '#ccc'})
                  $("#conversation_form_"+conversation_id+' #message_media').val(null);
                  chatBox.blobDestroy(conversation_id);

                  //Clear Text input and resize autosize
                  $("#conversation_form_"+conversation_id+" #message_body").val('');
                  old_val = '';
                  autosize.update($("#conversation_form_"+conversation_id+" #message_body"));
                }
              })
              //Fade preview 
              $('#chatbox_'+conversation_id+' .preview-container').fadeOut(100, function(){
                $('#chatbox_'+conversation_id+' .preview-container').remove();
                $('#chatbox_'+conversation_id+' .chat').scrollTop($('.chat')[0].scrollHeight);
              });
          }

        },

        //Chatbox send message enter key 
        checkInputKey: function (e, conversation_id) {

            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode == 13 && e.target.value.length > 0 && !chatBox.blobExists(conversation_id) ) {
 
              //Fade preview 
              $('#chatbox_'+conversation_id+' .preview-container').fadeOut(100, function(){
                $('#chatbox_'+conversation_id+' .preview-container').remove();
                $('#chatbox_'+conversation_id+' .chat').scrollTop($('.chat')[0].scrollHeight);
              });
              //Submit Form 
              $("#conversation_form_"+conversation_id+"").submit().bind("ajax:complete", function(event,xhr,status){
                $(e.target).closest('.chatbox').find('.submit_message').css({color: '#ccc'})
                $("#conversation_form_"+conversation_id+' #message_media').val(null);
                chatBox.blobDestroy(conversation_id);

                //Clear Text input and resize autosize
                $("#conversation_form_"+conversation_id+" #message_body").val('');
                old_val = '';
                autosize.update($("#conversation_form_"+conversation_id+" #message_body"));
              })

            }else if(keycode == 13 && e.target.value.length > 0 && chatBox.blobExists(conversation_id) ){
 
              //Form data for image paste 
              var fd = new FormData();
              fd.append('media', FILE);
              fd.append('body', e.target.value);
              $.ajax({
                type: 'POST',
                url: '/conversations/'+conversation_id+'/messages',
                data: fd,
                processData: false,
                contentType: false,
                success: function(){
                  $(e.target).closest('.chatbox').find('.submit_message').css({color: '#ccc'})
                  $("#conversation_form_"+conversation_id+' #message_media').val(null);
                  chatBox.blobDestroy(conversation_id);

                  //Clear Text input and resize autosize
                  $("#conversation_form_"+conversation_id+" #message_body").val('')
                  old_val = '';
                  autosize.update($("#conversation_form_"+conversation_id+" #message_body"));
                }
              })
              //Fade preview 
              $('#chatbox_'+conversation_id+' .preview-container').fadeOut(100, function(){
                $('#chatbox_'+conversation_id+' .preview-container').remove();
                $('#chatbox_'+conversation_id+' .chat').scrollTop($('.chat')[0].scrollHeight);
              });

            }

        },

        // Chatbox file preview 
        filePreview: function(e, conversation_id, method, file){
            $('#chatbox_'+conversation_id+' .preview-container').remove();
            FILE = file;

            if( method == 'upload'){
                  
                  var reader = new FileReader();
                  reader.onload = function (e) {

                  // preview html appended to chat
                  $("#chatbox_"+conversation_id+" .chatboxcontent .chat").append("<li class='preview-container'>"+
                    "<img src="+reader.result+" alt='Message Image'>"+
                    "<div class='actions'>"+
                    "<a href='#' class='submit_commentImage'><i class='ion-ios-checkmark'></i></a>"+
                    "<a href='#' class='clear_commentImage'><i class='ion-ios-close'></i></a>"+
                    "</div>"+
                    "<div class='overlay'></div>"+
                    "</li>");

                    $('#chatbox_'+conversation_id+' .preview-container').fadeIn(500);
                    $('#chatbox_'+conversation_id+' .chat').scrollTop($('.chat')[0].scrollHeight);
                  }
                  reader.readAsDataURL(file);

            }else if( method == 'paste'){

                  if (!chatBox.blobExists(conversation_id)) {
                    chatboxBlob.push(conversation_id)
                  }

                    var reader = new FileReader();
                    reader.onload = function (e) {

                    // preview html appended to chat
                    $("#chatbox_"+conversation_id+" .chatboxcontent .chat").append("<li class='preview-container'>"+
                      "<img src="+reader.result+" alt='Message Image'>"+
                      "<div class='actions'>"+
                      "<a href='#' class='submit_commentImage'><i class='ion-ios-checkmark'></i></a>"+
                      "<a href='#' class='clear_commentImage'><i class='ion-ios-checkmark'></i></a>"+
                      "</div>"+
                      "<div class='overlay'></div>"+
                      "</li>");

                    $('#message_body').val('');
                    var autosize_input = $('.chatboxtextarea');
                    autosize.update(autosize_input);
                    $('#chatbox_'+conversation_id+' .preview-container').fadeIn(500);
                    $('#chatbox_'+conversation_id+' .chat').scrollTop($('.chat')[0].scrollHeight);
                  }
                  reader.readAsDataURL(file);

            }

        },

        // Chatbox file submit 
        fileSubmit: function(e, conversation_id){
            if ( chatBox.blobExists(conversation_id) ) {  
              
              //Form data for image paste 
              var fd = new FormData();
              fd.append('media', FILE);
              $.ajax({
                type: 'POST',
                url: '/conversations/'+conversation_id+'/messages',
                data: fd,
                processData: false,
                contentType: false,
                success: function(){
                 $('#chatbox_'+conversation_id+' #message_media').val(null);
                 chatBox.blobDestroy(conversation_id);
                }
              })

              $('#chatbox_'+conversation_id+' .preview-container').fadeOut(100, function(){
                $('#chatbox_'+conversation_id+' .preview-container').remove();
                $('#chatbox_'+conversation_id+' .chat').scrollTop($('.chat')[0].scrollHeight);
              });
               
            }
            else if( !chatBox.blobExists(conversation_id) ){

              old_val = $("#conversation_form_"+conversation_id+" #message_body").val();
              $("#conversation_form_"+conversation_id+" #message_body").val('');
              //Fade preview 
              $('#chatbox_'+conversation_id+' .preview-container').fadeOut(100, function(){
                $('#chatbox_'+conversation_id+' .preview-container').remove();
                $('#chatbox_'+conversation_id+' .chat').scrollTop($('.chat')[0].scrollHeight);
              });
              //Submit Form 
              $("#conversation_form_"+conversation_id+"").submit().bind("ajax:complete", function(event,xhr,status){
                $('#chatbox_'+conversation_id+' #message_media').val(null);
                $("#conversation_form_"+conversation_id+" #message_body").val(old_val);
                chatBox.blobDestroy(conversation_id);
              })
             
            }

        }



    }


    /**
     * Cookie plugin
     *
     * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
     * Dual licensed under the MIT and GPL licenses:
     * http://www.opensource.org/licenses/mit-license.php
     * http://www.gnu.org/licenses/gpl.html
     *
     */

    jQuery.cookie = function (name, value, options) {
        if (typeof value != 'undefined') { // name and value given, set cookie
            options = options || {};
            if (value === null) {
                value = '';
                options.expires = -1;
            }
            var expires = '';
            if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
                var date;
                if (typeof options.expires == 'number') {
                    date = new Date();
                    date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
                } else {
                    date = options.expires;
                }
                expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
            }
            // CAUTION: Needed to parenthesize options.path and options.domain
            // in the following expressions, otherwise they evaluate to undefined
            // in the packed version for some reason...
            var path = options.path ? '; path=' + (options.path) : '';
            var domain = options.domain ? '; domain=' + (options.domain) : '';
            var secure = options.secure ? '; secure' : '';
            document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
        } else { // only name given, get cookie
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }
    };

}
$(document).ready(ready);
$(document).on("page:load", ready);
