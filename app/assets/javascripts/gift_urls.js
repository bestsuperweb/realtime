function initWishByUrl() {
  if ($('div#basket_list').length) {

    var cached_baskets;
    var cached_chosen_baskets = [];
    var cached_product_info;
    var cached_error_report;
    var cached_show_size_and_color = false;
    var cached_wish_created = false;
    //spinner
    var $spinner = $('#loading-spinner');
    var $spinner_text = $('#product_info_container #loading-spinner span');
    //url form
    var $url_form = $('form#gift_url_form');
    var $url_input = $('input#gift_url');
    var $new_site_notification = $('#new_site_notification');
    var $url_form_title = $('#gift_url_form #title');
    //user info
    var $username = $('.nav .avatar_icon > a').data('username');
    // product info
    var $product_image_container = $('.image-container');
    var $product_image = $('img#image');
    var $product_name = $('#name');
    var $product_color = $('#color');
    var $product_size = $('#size');
    var $product_price = $('#price');
    // product notes
    var $add_notes_button = $('a#add_notes_button');
    var $add_notes_container = $('#add_notes_container');
    var $notes_inputs_container = $('#notes_inputs_container');
    var $item_size_input = $('input#item_size');
    var $item_color_input = $('input#item_color'); 
    // baskets
    var $basket_container = $('#add_gift_to_basket');
    var $basket_list_container = $('div#basket_list_container');
    var $basket_list = $('div#basket_list ul');
    var $create_wish_button = $('#create_wish_button');
    var $added_to_basket_notification_container = $('div#added_to_basket_notification');
    var $added_to_basket_notification_list = $('div#added_to_basket_notification #notification ul');
    var $add_another_url_button = $('div#added_to_basket_notification a#add_another_url_button');
    var $new_basket_container = $('div#new_basket_container');
    var $new_basket_input = $('input#new_basket_input');
    var $save_new_basket_button = $('button#save_new_basket');
    // error reporting
    var $error_report_toggle = $('a#error_report_toggle');
    var $cancel_error_report_toggle = $('a#cancel_error_report_toggle');
    var $error_report_container = $('div#error_report_container');
    var $reset_error_button = $('button#reset_error');
    var $error_notification = $('div#error_notification');
    var $error_submitted = $('div#error_submitted');
    var $error_checklist = $('div#error_checklist');
    var $error_checkboxes = $('div#error_checklist input:checkbox');
    var $error_text = $('.error_text');
    var $notify_not_a_product_page = $('#not_product_page');
    var $notify_something_went_wrong = $('#something_went_wrong');

    //spinner
    var Spinner = {
      start: function() {
        $spinner.fadeIn();
      },
      stop: function() {
        $spinner.hide();
      },
      text: function(string) {
        $spinner_text.text(string);
        $spinner_text.fadeIn();
      }
    };

    // url form
    var UrlForm = {
      reset: function() {
        this.show();
      },
      show: function() {
        Baskets.hide();        
        $url_input.val('');
        $url_form.fadeIn();
        $url_input.focus();
      },
      hide: function() {
        $url_form.hide();
        Baskets.show();
      },
      notifyNewSite: function() {
        $url_form_title.hide();
        $new_site_notification.slideDown();
        this.show();
      },
      submit: function(){
        ProductInfo.reset();
        UrlForm.hide();
        Spinner.start();
        this.get().done(function(data) {
          Spinner.stop();
          ProductInfo.cache(data.data);
          ProductInfo.display(true);
          $error_report_toggle.show();
          ProductInfo.showNotes();
          WishModal.displayButton();
        })
        .fail(function(result) {
          if (result.status == 400) {
            toastr.error('Please enter a valid URL');
            UrlForm.show();
          } else if (result.status == 412) {
            UrlForm.notifyNewSite();
          } else {
            Errors.notify(result);
          };
        })
        .always(function() {
          Spinner.stop();
        });
      },
      get: function() {
        url = $url_input.val()
        return $.ajax({
          type     : 'GET',
          url      : '/api/v1/gift_urls',
          dataType: 'json',
          data: {
            url: url
          }
        });
      }
    };

    // product info
    var ProductInfo = {
      cache: function(data) {
        data.short_item_name = this.shortProductName(data.item_name);
        cached_product_info = data;
      },
      display: function() {
        if (cached_product_info != null) {
          this.displayImage(true);
          this.displayName(true);
          this.displayPrice(true);
          this.showSizeColor(true);
        };
      },
      shortProductName: function(name) {
        if (name != null) {
          var name_length = jQuery.trim(name);
          var short_item_name = jQuery.trim(name).substring(0, 49).trim(this);
          if(name_length.length > 50){
            return short_item_name + "...";
          }
          else{
            return short_item_name;
          };
        };
      },
      displayImage: function(show) {
        if (show && cached_product_info.image != null) {
          $product_image_container.fadeIn();
          $product_image.attr('src', cached_product_info.image)
        } else {
          $product_image_container.hide();
          $product_image.attr('src', '');
        };
      },
      displayName: function(show) {
        if (show && cached_product_info.item_name != null) {
          $product_name[0].textContent = cached_product_info.short_item_name;
          $product_name.css('display', 'inline-block');
        } else {
          $product_name.hide();
        };
      },
      displayPrice: function(show) {
        if (show && cached_product_info.price != null) {
          var price = parseFloat(cached_product_info.price).toFixed(2).toString();
          $product_price[0].textContent = '$' + price;
          $product_price.css('display', 'block');
        } else {
          $product_price.hide();
        }
      },
      showSizeColor: function(show) {
        if (show) {
          if (cached_product_info.item_size != null) {
            cached_show_size_and_color = true;
            cached_product_info.item_size = null;
          };
          if (cached_product_info.color != null) {
            cached_show_size_and_color = true;
            cached_product_info.color = null;
          };
        } else {
          cached_show_size_and_color = false;
        };
      },
      showNotes: function() {
        if (cached_show_size_and_color) {
          $add_notes_button.fadeIn(600);
          $item_size_input.fadeIn(600);
          $item_color_input.fadeIn(600);
          $add_notes_container.css('display', 'flex');
        };
      },
      mergeNotes: function() {
        var entered_size = $item_size_input.val().trim();
        var entered_color = $item_color_input.val().trim();
        if (entered_color.length > 0 || entered_size.length > 0){
          $product_size.text(entered_size);
          $product_color.text(entered_color);
          $product_size.css('display', 'inline-block');
          $product_color.css('display', 'inline-block');
        };
        cached_product_info.item_size = (entered_size != null) ? entered_size : cached_product_info.item_size
        cached_product_info.color = (entered_color != null) ? entered_color : cached_product_info.color
        $add_notes_container.hide();
      },
      reset: function() {
        cached_product_info = null;
        this.displayImage(false);
        this.displayName(false);
        this.displayPrice(false);
        this.showSizeColor(false);
        this.resetNotes();
      },
      resetNotes: function() {
        $notes_inputs_container.hide();
        $add_notes_button.hide();
        $item_color_input.val('').hide();
        $item_size_input.val('').hide();
        $product_color.text('').hide();
        $product_size.text('').hide();
      }
    };

    var Baskets = {
      cache: function(result) {
        cached_baskets = result
      },
      isChosen: function(id) {
        return cached_chosen_baskets.filter(function(e) { return e.id === id; }).length > 0;
      },
      isExist: function(name) {
        return cached_baskets.filter(function(e) { return e.name === name; }).length > 0;
      },
      populate: function() {
        if (cached_baskets == null) {
          this.index().done(function(result) {
            Baskets.cache(result);
            Baskets.populateIndex();
          });
        } else {
          Baskets.populateIndex();
        };
      },
      populateIndex: function() {
        $('div#basket_list ul li').empty();
        var $list = $basket_list[0]
        var baskets_without_liked_products_array = cached_baskets.filter(function(e) { return e.name !== 'Liked Products'; });

        $.each(baskets_without_liked_products_array, function(i) {
          var $li = $('<li/>')
                  .appendTo($list)
          var $link =  $('<a/>')
                      .text(baskets_without_liked_products_array[i].name)
                      .addClass('choose_basket')
                      .attr('data-id', baskets_without_liked_products_array[i].id)
                      .attr('data-name', baskets_without_liked_products_array[i].name)
                      .appendTo($li);
          var $button =  $('<button/>')
                        .text('Add')
                        .addClass('choose_basket btn')
                        .appendTo($link);
          var $icon = $('<i/>')
                  .addClass("ion-checkmark-round")
                  .appendTo($button)

          if (Baskets.isChosen(baskets_without_liked_products_array[i].id)) {
            this.choose($link);
          };
        });
        $basket_list_container.show();
      },
      show: function() {
        $basket_container.fadeIn();
        $basket_list_container.show();
      },
      hide: function() {
        $basket_container.hide();
        $basket_list_container.hide();
      },
      // choosing baskets
      cache_chosen: function(obj) {
        cached_chosen_baskets.push(obj);
      },
      uncache_chosen: function(id){
        cached_chosen_baskets = cached_chosen_baskets.filter(function(e){ return e.id !== id});
      },
      choose: function($link) {
        var $btn = $link.children('button');
        var id = $link.data("id");
        var name = $link.data("name");
        var id_chosen = Baskets.isChosen(id);
        
        if (id_chosen) {
          this.uncache_chosen(id)
          $link.removeClass('checked')
          $btn.text('Add')
        } else {
          this.cache_chosen({id: id, name: name});
          $link.addClass('checked')
          $btn.text('').addClass('adding');
          var $icon =  $('<i/>')
                      .addClass("ion-checkmark-round")
                      .appendTo($btn);
        };
      },
      reset: function() {
        cached_chosen_baskets = [];
        Baskets.populate();
        this.hide();
        $added_to_basket_notification_container.hide();
        $added_to_basket_notification_list.empty();
        this.resetForm();
      },
      resetForm: function() {
        $new_basket_input.val('');
        $save_new_basket_button.hide();
      },
      index: function() {
        return $.ajax({
          type     : 'GET',
          url      : '/api/v1/gift_urls/baskets',
          dataType: 'json'
        });
      },
      save: function(name) {
        $.ajax({
          type     : 'POST',
          url      : '/api/v1/baskets',
          dataType: 'json',
          data: {
            basket: {
              name: name
            }
          }
        }).done(function(result){
          // void input and hide button
          Baskets.resetForm();
          // push to list of cached baskets
          cached_baskets.push(result);
          Baskets.populate();
          // manually trigger click on link to add to chosen baskets
          basket_selector = "a.choose_basket[data-id=" + result.id + "]"
          $(basket_selector).click();
        });
      },
      populateSubmittedBaskets: function() {
        var $list = $added_to_basket_notification_list;
        var baskets = cached_chosen_baskets;

        $.each(baskets, function(i) {
          var $li = $('<li/>').appendTo($list);
          var $new_link = baskets[i].name;
          var $new_link = "/" + $username + "/registry/" + $new_link.replace(/\s+/g, '-').toLowerCase();
          var $link = $('<a/>')
                      .text(baskets[i].name)
                      .attr('href', $new_link + "-" + baskets[i].id)
                      .attr('data-id', baskets[i].id)
                      .attr('data-name', baskets[i].name)
                      .appendTo($li);
        });

        $basket_list_container.hide();
        $added_to_basket_notification_container.show();
      }
    };

    var Errors = {
      cache: function(obj) {
        cached_error_report = obj;
      },
      notify: function(response) {
        responseJSON = response.responseJSON
        if (responseJSON != null && responseJSON.error_report != null) {
          this.cache(responseJSON.error_report);
        } else {
          this.cache({})
        };
        if (cached_error_report.error_type != null && cached_error_report.error_type == 'not_product_page') {
          $notify_not_a_product_page.show();
        } else {
          $notify_something_went_wrong.show();
        };
        this.display(true);
      },
      display: function(boolean) {
        if (boolean) {
          Baskets.hide();
          $error_report_container.show();
          $error_notification.fadeIn(); 
        } else {
          $error_report_container.hide();
          $error_notification.hide();
          $error_checklist.hide();
          $error_submitted.hide();
          $error_checkboxes.prop('checked', false);
          $error_text.hide();
          Baskets.show();
        }
      },
      reset: function() {
        this.display(false);
        $error_report_toggle.hide();
        $error_report = null;
      },
      displayNewReport: function(boolean) {
        if (boolean) {
          Baskets.hide();
          $error_report_toggle.hide();
          $error_report_container.show();
          $error_checklist.fadeIn();
        } else {
          $error_checklist.hide();
          $error_report_container.hide();
          $error_report_toggle.show();

          if (!cached_wish_created) {
            Baskets.show();
          } else {
            $basket_container.show()
          };
        };
      },
      submitNewReport: function() {
        var error_type = 'reported_issue';
        var notes_array = [];
        $('div#error_checklist input:checkbox:checked').each(function() {
          notes_array.push($(this).val());
        });
        var notes = notes_array.join(', ')

        $.ajax({
          type     : 'POST',
          url      : '/api/v1/api_reports',
          dataType: 'json',
          data: {
            api_report: {
              error_type: error_type,
              note: notes,
              data: cached_product_info
            }
          }
        }).done(function() {
          Errors.reportSubmitted();
        });
      },
      confirmReport: function() {
        var id = cached_error_report.id
        $.ajax({
          type     : 'POST',
          url      : '/api/v1/api_reports/confirm',
          dataType: 'json',
          data: {
            api_report: {
              id: id,
            }
          }
        }).done(function(){
          Errors.reportSubmitted();
        });
      },
      reportSubmitted: function() {
        $error_notification.hide();
        $error_checklist.hide();
        $error_submitted.show(); 
      }
    };

    var WishModal = {
      displayButton: function() {
        if (cached_chosen_baskets.length != 0 && cached_product_info != null) {
          wishlist_text = cached_chosen_baskets.length > 1 ? "wish lists" : "wish list"
          $create_wish_button
            .text('Add to ' + wishlist_text)
            .fadeIn()
            .css("display","block");
        } else {
          $create_wish_button.hide();
        };
      },
      reset: function() {
        UrlForm.reset();
        Baskets.reset();
        ProductInfo.reset();
        Errors.reset();
        $basket_list_container.hide();
        $create_wish_button.hide();
        cached_wish_created = false;
        Spinner.text('Please select a wish list while we fetch your wish...');
      },
      create: function() {
        $('.gift_url').fadeOut(function(){
            $('.gift_url').fadeIn();
            ProductInfo.mergeNotes();
            $notes_inputs_container.hide();
            Baskets.populateSubmittedBaskets();
            $create_wish_button.hide();
            cached_wish_created = true;
        });
        
        ids = cached_chosen_baskets.map(function(e) {return e.id;});
        product = cached_product_info
        this.save(product, ids)
      },
      save: function(product, ids) {
        return $.ajax({
          type     : 'POST',
          url      : '/api/v1/gift_urls',
          dataType: 'json',
          data: {
            product: product,
            basket_ids: ids
          }
        });
      }
    }

    $url_form.submit(function(e){
      e.preventDefault();
      UrlForm.submit();
    });

    $(document)
    .off('click', 'a.choose_basket')
    .on('click', 'a.choose_basket', function(e) {
      Baskets.choose($(this));
      WishModal.displayButton();
      $spinner_text.fadeOut(function(){
          Spinner.text('Fetching your wish...');
      });
    });

    $(document)
    .off('click', 'a#create_wish_button')
    .on('click', 'a#create_wish_button', function(e) {
      WishModal.create();
    });

    $(document)
    .off('click', 'a#add_another_url_button')
    .on('click', 'a#add_another_url_button', function(e) {
      WishModal.reset();
    });

    $(document)
    .on('keyup', '#input-search', function() {
      // Retrieve the input field text and reset the count to zero
      var filter = "(?:^|\\s)" + $(this).val(), itemsFound = 0;

      // Loop through each basket
      $("#basket_list ul li").each(function(){

        // If the list item does not contain the text phrase fade it out
        if ( ($(this).text().search(new RegExp(filter, "i")) < 0) ) {
          $(this).hide();

        // Show the list item if the phrase matches and increase the count by 1
        } else {
          $(this).show();
          itemsFound++;
        };
      });
    });

    $(document)
    .off('keyup', 'input#new_basket_input')
    .on('keyup', 'input#new_basket_input', function() {
      if ($(this).val()) {
        $save_new_basket_button.fadeIn();
      } else {
        $save_new_basket_button.fadeOut();
      };
    });

    $(document)
    .off('click', 'button#save_new_basket')
    .on('click', 'button#save_new_basket', function(e) {
      name = $new_basket_input.val();
      if (!Baskets.isExist(name)) {
        Baskets.save(name);
      } else {
        Baskets.resetForm();
        toastr.error("A wishlist with this name already exists");
      };
    });

    $(document)
    .off('click', 'a#add_notes_button')
    .on('click', 'a#add_notes_button', function(e) {
      $(this).fadeOut(function(){
          $notes_inputs_container.fadeIn();
      });
    });

    $(document)
    .off('click', 'a#error_report_toggle')
    .on('click', 'a#error_report_toggle', function(e) {
      Errors.displayNewReport(true);
    });

    $(document)
    .off('click', 'a#cancel_error_report_toggle')
    .on('click', 'a#cancel_error_report_toggle', function(e) {
      Errors.displayNewReport(false);
    });

    $(document)
    .off('click', 'button#confirm_report')
    .on('click', 'button#confirm_report', function(e) {
      Errors.confirmReport();
    });

    $(document)
    .off('click', 'button#create_report')
    .on('click', 'button#create_report', function(e) {
      Errors.submitNewReport();    
    });

    $(document)
    .off('click', 'button#reset_error')
    .on('click', 'button#reset_error', function(e) {
      WishModal.reset();
    });

    Baskets.populate();
  };
};

$(document).on('turbolinks:load', function () {
  initWishByUrl();
});