class NotificationRelayJob < ApplicationJob
  queue_as :default

  def perform(notification, notification_count)
    html = ApplicationController.render(
                                 partial: "shared/notifications/notification",
                                 locals: { notification: notification },
                                 formats: [:html])

    ActionCable.server.broadcast("notification_channel_#{notification.customer_id}",
                                  notification_count: notification_count,
                                  notification: html,
                                  notification_id: notification.id,
                                  time: notification.created_at.strftime("%B %d, %Y"))
  end
end