# == Schema Information
#
# Table name: oauth_credentials
#
#  id          :integer          not null, primary key
#  provider    :string
#  uid         :string
#  customer_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class OauthCredential < ApplicationRecord
  belongs_to :customer
end
