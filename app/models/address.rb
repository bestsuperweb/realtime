# == Schema Information
#
# Table name: addresses
#
#  id                        :integer          not null, primary key
#  encrypted_address_1       :string           not null
#  encrypted_address_2       :string
#  city                      :string           not null
#  state                     :string           not null
#  postal_code               :string           not null
#  owner_id                  :integer
#  created_at                :datetime
#  updated_at                :datetime
#  address_type              :integer          default(0), not null
#  instructions              :string
#  default                   :boolean          default(FALSE)
#  encrypted_address_1_iv    :string
#  encrypted_address_2_iv    :string
#  encrypted_name            :string
#  encrypted_name_iv         :string
#  encrypted_phone_number    :string
#  encrypted_phone_number_iv :string
#  owner_type                :string           default("Customer")
#  status                    :integer          default(0)
#

class Address < ApplicationRecord
  belongs_to :owner, polymorphic: true
  has_many :shipping_orders, dependent: :nullify
  has_many :payment_methods, dependent: :nullify
  has_many :giftboxes, dependent: :nullify

  validates :owner, presence: true
#  validates :name, presence: true
  validates :address_1, presence: true
  validates :city, presence: true
  validates :state, presence: true
  validates :postal_code, presence: true
#  validates :phone_number, presence: true, if: :shipping?

  around_create :assign_as_default_shipping_address

  enum address_type: [:shipping, :billing]
  enum status: [:active, :retired]

  attr_encrypted :name, key: ENV['ATTR_ENCRYPTED_KEY']
  attr_encrypted :address_1, key: ENV['ATTR_ENCRYPTED_KEY']
  attr_encrypted :address_2, key: ENV['ATTR_ENCRYPTED_KEY']
  attr_encrypted :phone_number, key: ENV['ATTR_ENCRYPTED_KEY']

  attr_accessor :save_as_default,
                :force_check_for_save_as_default

  def initialize(args = nil)
    super
    @save_as_default = false
    @force_check_for_save_as_default = false
  end

  def assign_as_default_shipping_address
    if assign_as_default?
      current_default_address = self.owner.default_shipping_address
      self.default = true
    end

    yield

    current_default_address.update(default: false) if current_default_address.try(:present?)
  end

  def full_address
    [address_1, address_2, city, state].reject { |line| line.blank? }.join(', ') + ' ' + postal_code
  end

  def full_address_without_name
    [address_1, address_2, city, state].reject { |line| line.blank? }.join(', ') + ' ' + postal_code
  end

  private

  def assign_as_default?
     self.save_as_default || 
      (
        self.shipping? && 
        self.owner.default_shipping_address.blank? && 
        !self.force_check_for_save_as_default
      )
  end
end
