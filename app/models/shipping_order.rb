# == Schema Information
#
# Table name: shipping_orders
#
#  id         :integer          not null, primary key
#  owner_id   :integer
#  invoice_id :integer
#  status     :integer          default(0)
#  address_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  uuid       :string           not null
#  owner_type :string           default("Customer")
#

class ShippingOrder < ApplicationRecord
  has_secure_token :uuid
  
  belongs_to :owner, polymorphic: true
  belongs_to :invoice
  belongs_to :shipping_address, class_name: 'Address', foreign_key: 'address_id'

  has_many :merchant_carts, dependent: :destroy
  has_one :giftbox, through: :invoice

  validates :owner, presence: true
  validates :invoice, presence: true
  validates :address_id, presence: true

  enum status: [:pending, :completed, :failed, :cancelled]

  after_create :assign_shipping_order_to_merchant_carts

  def ready_to_complete?
    self.merchant_carts.completed.count == self.merchant_carts.count
  end

  def assign_shipping_order_to_merchant_carts
    merchant_carts = self.giftbox.merchant_carts
    merchant_carts.update_all(shipping_order_id: self.id)
    merchant_carts.map {|e| e.update_estimates!}
    true
  end

  def complete!
    invoice = self.invoice
    payment = invoice.payments.authorized.first

    self.transaction do
      invoice.update_actual_amounts!
      payment.capture!
      invoice.completed!
      self.giftbox.gifts.map { |e| e.update(quantity_received: e.cart_item.quantity) }
      self.completed!
    end
    true
  end

  def notify_cart_success
    unsuccessful_statuses = MerchantCart.statuses.except('completed').values
    return true if self.merchant_carts.where(status: unsuccessful_statuses).any?

    self.invoice.giftbox.cart_items.each do |ci|
      if ( ci.gift_id.present? ) && ( ci.gift.customer.setting_selected?( "purchase_gift" ) )
        ci.gift.purchase_responses( self.invoice.giftbox.owner )
      end
    end

    self.complete!
  end

  def notify_cart_failure
    self.failed!
  end

end
