# == Schema Information
#
# Table name: charities
#
#  id           :integer          not null, primary key
#  name         :string
#  url          :string
#  cash_charity :boolean
#  basket_id    :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Charity < ApplicationRecord
  belongs_to :basket

end
