class PurchaseFeedback < ApplicationRecord
	belongs_to :gift, required: false
	belongs_to :variant, required: false
	belongs_to :customer, required: false

	def url
		slug = self.actable_type == 'gift' ? Gift.find( self.actable_id ).try(:slug) : Variant.find( self.actable_id ).try(:slug)	
		link = self.actable_type == 'gift' ? "/gift/#{slug}" : "/products/#{slug}"
		return link
	end

end