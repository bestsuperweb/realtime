# == Schema Information
#
# Table name: gift_otds
#
#  id          :integer          not null, primary key
#  variant_id  :integer
#  title       :string           not null
#  description :text             not null
#  occurs_at   :date             not null
#  created_at  :datetime
#  updated_at  :datetime
#

class GiftOtd < ApplicationRecord
  belongs_to :variant

end
