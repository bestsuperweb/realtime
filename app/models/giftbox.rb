# == Schema Information
#
# Table name: giftboxes
#
#  id                         :integer          not null, primary key
#  address_id                 :integer
#  payment_method_id          :integer
#  owner_id                   :integer
#  owner_type                 :string
#  cart_id                    :integer
#  is_a_gift                  :boolean          default(FALSE)
#  giftee_id                  :integer
#  giftee_type                :string
#  giftee_shipping_address_id :integer
#  shipping_option            :integer          default(0)
#  status                     :integer          default(0)
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  cart_items_count           :integer          default(0)
#

class Giftbox < ApplicationRecord
  include CartCalculator


  belongs_to :shipping_address, class_name: 'Address', foreign_key: 'address_id'
  belongs_to :payment_method
  belongs_to :owner, polymorphic: true
  belongs_to :giftee, polymorphic: true
  belongs_to :giftee_shipping_address, class_name: 'Address', foreign_key: 'giftee_shipping_address_id'
  belongs_to :cart
  has_one :invoice, dependent: :destroy

  has_many :merchant_carts, dependent: :destroy
  has_many :cart_items, dependent: :destroy
  has_many :variants, through: :cart_items
  has_many :gifts, through: :cart_items

  enum status: [:pending, :checked_out, :retired]
  enum shipping_option: [:standard, :fast]

  validates :giftee, presence: true
  validates :owner, presence: true

  def items_quantity
    self.cart_items.map(&:quantity).sum
  end

  def ready_to_checkout?
    self.pending? &&
    self.payment_method_id.present? && 
    self.address_id.present? && 
    self.total_amount_above_minimum?
  end

  def checkout!
    CheckoutService.build.call(self)
  end

  # minimum amount for stripe is 50 cents
  def total_amount_above_minimum?
    self.total_amount_estimate*100 > 50
  end

end
