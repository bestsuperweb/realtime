# == Schema Information
#
# Table name: customer_connections
#
#  id            :integer          not null, primary key
#  customer_1_id :integer
#  customer_2_id :integer
#  state         :string           default("pending")
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class CustomerConnection < ApplicationRecord
  belongs_to :customer, foreign_key: 'customer_1_id'
  belongs_to :customer, foreign_key: 'customer_2_id'  

  validates_inclusion_of :state, in: %w( pending accepted )
end

