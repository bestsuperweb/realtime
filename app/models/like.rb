# == Schema Information
#
# Table name: likes
#
#  id           :integer          not null, primary key
#  likable_type :string
#  likable_id   :integer
#  customer_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Like < ApplicationRecord
  belongs_to :likable, polymorphic: true
  has_many :activities, as: :actable, dependent: :destroy
  belongs_to :customer


end
