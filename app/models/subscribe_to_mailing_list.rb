class SubscribeToMailingList < ActiveRecord::Base
	require 'gibbon'

	def self.subscribe(customer)
		gibbon = Gibbon::Request.new(api_key: ENV['MAILCHIMP'] )
		gibbon.timeout = 15

		gibbon.lists(ENV['MAILCHIMP_LIST']).members.create(body:{
			email_address: customer.email,
			status: 'subscribed',
			merge_fields: { FNAME: customer.first_name, LNAME: customer.last_name}
		})
		
	end
end
