# == Schema Information
#
# Table name: baskets
#
#  id          :integer          not null, primary key
#  name        :string           not null
#  customer_id :integer
#  max_gifts   :integer
#  created_at  :datetime
#  updated_at  :datetime
#  basket_type :string           default("lifestyle")
#  description :text
#  banner      :string
#  share_count :integer          default(0)
#  privacy     :string           default("public")
#

class Basket < ApplicationRecord
  
  extend FriendlyId
  friendly_id             :name, use: :slugged

  BASKET_TYPES            = ["lifestyle", "charity", "cash", "event"].freeze
  PRIVACY_LEVELS          = ["public", "friends", "private"].freeze

  belongs_to              :customer, required: false
  has_many                :gifts, dependent: :destroy
  has_many                :comments, as: :commentable, dependent: :destroy
  has_one                 :event, dependent: :destroy
  has_one                 :charity, dependent: :destroy
  has_many                :likes, as: :likable, dependent: :destroy
  has_many                :activities, as: :actable, dependent: :destroy

  validates               :name, presence: true
  validates_uniqueness_of :name, scope: :customer_id
  validates_inclusion_of  :basket_type, in: BASKET_TYPES

  mount_uploader          :banner, BannerUploader

  before_destroy          :confirm_not_liked_products
  before_update           :confirm_not_liked_products
  before_update           :update_basket_slug
  after_create            :create_basket_slug

  def basket_slug_name
    self.try(:slug)
  end

  def create_basket_slug
    self.slug = "#{name}-#{id}".parameterize
    self.save!
  end

  def update_basket_slug
    self.slug = "#{name}-#{id}".parameterize
  end

  def self.basket_types
    BASKET_TYPES
  end

  def recent_gifts
    self.gifts.sort_by(&:updated_at).reverse[0..2]
  end

  def make_wishlist
    if( self.gifts.length > 20 )
      return false
    else
      self.max_gifts = 20
      self.save
    end
  end

  def visible_to?( customer )
    ( customer == self.customer ) || ( self.privacy == "public" ) || ( ( self.privacy == "friends" ) && ( customer.follows? self.customer ) )
  end

  def add_to_basket( variant, quantity = 1 )
    if( self.max_gifts.present? )
      return false if self.max_gifts <= self.gifts.length
    end
    unless( self.contains?(variant) )
      gift = Gift.create(
        basket_id: self.id,
        variant_id: variant.id,
        quantity_desired: quantity,
        quantity_received: 0
      )

      self.gifts << gift

      return true
    else
      return false
    end
  end

  def remove_from_basket(variant)
    if self.contains?(variant)
      gift = Gift.find_by_basket_id_and_variant_id(self.id, variant.id)

      gift.destroy

      return true

    else
      return false
    end
  end

  def nuke
    self.gifts.each do |gift|
      CartItem.where( gift_id: gift.id ).destroy_all
      gift.destroy
    end
    self.event.destroy if self.event.present?
    self.destroy
  end

  def contains?(variant)
    Gift.where( basket_id: self.id, variant_id: variant.id ).present?
  end

  def event?
    self.basket_type == "event"
  end

  def lifestyle?
    self.basket_type == "lifestyle"
  end

  def cash?
    self.basket_type == "cash"
  end

  def charity?
    self.basket_type == "charity"
  end

  def image_path
    if self.gifts.empty?
      '/images/events/createbasket_default.jpg'
    else
      self.gifts[0].variant.product_image.url
    end
  end

protected

  def confirm_not_liked_products
    if ( self.name_was == "Liked Products" ) && ( self.name != "Liked Products" )
      errors.add( :name, :cannot_alter, message: "Cannot alter Liked Products name" )
      p "#{self.class.to_s} Cannot alter Liked Products basket"
      return false
    else
      return true
    end
  end

end
