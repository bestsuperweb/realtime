# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  content          :string
#  customer_id      :integer
#  commentable_id   :integer
#  commentable_type :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Comment < ApplicationRecord
  belongs_to :commentable, polymorphic: true
  belongs_to :customer
  has_many :activities, as: :actable, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :likes, as: :likable, dependent: :destroy

  after_create :generate_activity

  def generate_activity
    a               = Activity.new
    a.activity_type = "comment"
    a.actable       = self
    a.customer      = self.customer

    a.save

    if self.commentable_type == "Gift"
      to_be_notified = self.commentable.basket.customer
    elsif self.commentable_type == "Comment"
      to_be_notified = self.commentable.customer
    elsif self.commentable_type == "Basket"
      to_be_notified = self.commentable.customer
    end 

    if ( to_be_notified != self.customer )
      notify = false
      if self.commentable_type == "Gift"
        notify = true if to_be_notified.setting_selected?( "gift_comment" )
      elsif self.commentable_type == "Basket"
        notify = true if to_be_notified.setting_selected?( "basket_comment" )
      elsif self.commentable_type == "Comment"
        notify = true
      end

      if notify
        n          = Notification.new
        n.activity = a 
        n.customer = to_be_notified

        n.save
      end
    end 
  end

end
