# == Schema Information
#
# Table name: merchants
#
#  id              :integer          not null, primary key
#  name            :string           not null
#  public_url      :string           not null
#  alexa_rank      :integer
#  status          :string           default("private"), not null
#  commission      :float            default(100.0), not null
#  ppc_price       :integer          default(0)
#  cpm_price       :integer          default(0)
#  validation_days :integer          default(0)
#  created_at      :datetime
#  updated_at      :datetime
#  icon            :string
#  pending         :boolean          default(FALSE)
#  screenshot      :string
#

class Merchant < ApplicationRecord
  has_many :variants
  has_many :merchant_carts, dependent: :destroy
  has_many :likes, as: :likable, dependent: :destroy
  has_many :merchant_category, dependent: :destroy

  mount_uploader :icon, IconUploader
  mount_uploader :screenshot, ScreenshotUploader

  validates_presence_of :name
  validates :name, uniqueness: true

  # Ensures that only the following statuses are available:
  #  * Removed - For one reason or another, removed from being publicly or privately seen
  #  * Placeholder - Won't actively pull variants as no API info exists
  #  * Private - Only admin can see
  #  * Public - All can see

  validates_inclusion_of :status, in: %w( removed placeholder private public )

  validates_numericality_of :alexa_rank, only_integer: true, greater_than_or_equal_to: 0, allow_nil: true
  validates_numericality_of :commission, greater_than_or_equal_to: 0, allow_nil: false
  validates_numericality_of :ppc_price, only_integer: true, greater_than_or_equal_to: 0, allow_nil: false
  validates_numericality_of :cpm_price, only_integer: true, greater_than_or_equal_to: 0, allow_nil: false
  validates_numericality_of :validation_days, only_integer: true, greater_than_or_equal_to: 0, allow_nil: false

  scope :order_by_name, -> { order('LOWER(name)') }

  acts_as_taggable
  acts_as_taggable_on :categories
  
  after_create :grab_icon

  searchable do
    #searchable by merchant.name
    text :name
  end

  def removed?
    self.status == "removed"
  end

  def placeholder?
    self.status == "placeholder"
  end

  def private?
    self.status == "private"
  end

  def public?
    self.status == "public"
  end

  def remove
    self.status = "removed"
    self.save
  end

  def make_placeholder
    self.status = "placeholder"
    self.save
  end

  def make_private
    self.status = "private"
    self.save
  end

  def make_public
    self.status = "public"
    self.save
  end

  def grab_icon
    return false if self.public_url.nil?

    self.remote_icon_url = "http://www.google.com/s2/favicons?domain=" + self.public_url
    self.save

    return true
  end

  def categories
    MerchantCategory.where(name: self.category_list)
  end

  def merchant_cart_success_rate_by_week
    success_hash = self.merchant_carts.completed.group_by_week(:purchase_gateway_success_date).count
    failure_hash = self.merchant_carts.failed.group_by_week(:purchase_gateway_failure_date).count
    weeks = success_hash.merge(failure_hash).keys
    success_rate_hash = Hash.new

    weeks.each do |e|
      success_count = success_hash[e].to_i
      failure_count = failure_hash[e].to_i
      total = success_count + failure_count

      success_rate = total.nonzero? ? (success_count/total)*100 : 100
      success_rate_hash[e] = success_rate
    end

    return success_rate_hash
  end

  def url
    u = URI.parse(public_url)
    if(!u.scheme)
      "https://" + public_url
    elsif(%w{http https}.include?(u.scheme))
      public_url
    else
      nil
    end
  end

end
