# == Schema Information
#
# Table name: guests
#
#  id         :integer          not null, primary key
#  email      :string           default(""), not null
#  name       :string           default("Guest"), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  stripe_id  :string
#

class Guest < ApplicationRecord
  has_many :carts, as: :owner, dependent: :nullify
  has_many :owned_giftboxes, class_name: 'Giftbox', as: :owner, dependent: :nullify
  has_many :giftboxes, as: :giftee, dependent: :nullify
  has_many :payment_methods, as: :owner, dependent: :nullify
  has_many :invoices, as: :owner, dependent: :nullify
  has_many :shipping_orders, as: :owner, dependent: :nullify
  has_many :addresses, as: :owner, dependent: :nullify

  # def name
    #"Guest"
    #self.name
  # end

  def default_shipping_address
    self.addresses.where(address_type: 0, default: true).first
  end

  def default_payment_method
    self.payment_methods.where(default: true).first
  end
end
