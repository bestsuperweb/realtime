class MerchantCategory < ApplicationRecord

	extend FriendlyId
	friendly_id             :name, use: :slugged

	mount_uploader :screenshot, Category_Uploader
	before_update           :update_category_slug
  	after_create            :create_category_slug

	def category_merchants
		Merchant.where(id: self.merchants )
	end

	def create_category_slug
		self.slug = name.parameterize
		self.save!
	end

	def update_category_slug
		self.slug = name.parameterize
	end

end
