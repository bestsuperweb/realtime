# frozen_string_literal: true
# == Schema Information
#
# Table name: merchant_carts
#
#  id                              :integer          not null, primary key
#  merchant_id                     :integer
#  status                          :integer          default(0), not null
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  shipping_order_id               :integer
#  amazon_remote_cart_id           :text
#  amazon_remote_cart_hmac         :text
#  amazon_remote_cart_purchase_url :text
#  estimated_shipping_fee          :decimal(, )      default(0.0), not null
#  estimated_taxes                 :decimal(, )      default(0.0), not null
#  estimated_subtotal              :decimal(, )      default(0.0), not null
#  actual_shipping_fee             :decimal(, )      default(0.0), not null
#  actual_taxes                    :decimal(, )      default(0.0), not null
#  actual_subtotal                 :decimal(, )      default(0.0), not null
#  actual_total                    :decimal(, )      default(0.0), not null
#  cart_items_count                :integer
#  manual                          :boolean          default(FALSE), not null
#  purchase_gateway_errors         :text
#  giftbox_id                      :integer
#  purchase_gateway_failure_date   :datetime
#  purchase_gateway_success_date   :datetime
#  logs                            :text
#

class MerchantCart < ApplicationRecord
  include CartCalculator

  belongs_to :giftbox
  belongs_to :merchant
  belongs_to :shipping_order
  belongs_to :assignable, polymorphic: true
  belongs_to :completable, polymorphic: true
  has_many :cart_items, dependent: :nullify
  has_many :variants, through: :cart_items

  validates :giftbox, presence: true
  validates :merchant, presence: true

  enum status: [
    :pending,
    :started,
    :completed,
    :failed
  ]

  store :purchase_gateway_errors, coder: JSON

  def update_estimates!
    self.update!(
      estimated_subtotal: subtotal_estimate, 
      estimated_shipping_fee: shipping_fee_estimate, 
      estimated_taxes: tax_estimate
    )
  end

  def calculate_actual_total
    self.actual_subtotal - self.actual_shipping_fee - self.actual_taxes
  end

  def complete!(result, options={})
    shipping_order = options[:shipping_order] || self.shipping_order
    purchase_gateway_success_date = options[:via_purchase_gateway] ? Time.now.utc : nil

    self.update!(
      actual_subtotal: result.actual_subtotal,
      actual_shipping_fee: result.actual_shipping_fee,
      actual_taxes: result.actual_taxes,
      status: 'completed',
      purchase_gateway_success_date: purchase_gateway_success_date,
      logs: self.logs
    )

    shipping_order.notify_cart_success
  end

  def failed!(options={})
    shipping_order = options[:shipping_order] || self.shipping_order
    errors = options[:errors]
    if errors
      errors.each do |e| 
        self.purchase_gateway_errors["#{Time.now.utc}"] = e
      end
      self.purchase_gateway_failure_date = Time.now.utc
    elsif (self.running? || self.completed?)
      self.purchase_gateway_errors["#{Time.now.utc}"] = "status_error cart is #{self.status}"
    end
    self.status = self.completed? ? self.status : 'failed'
    self.save!
    
    p self.purchase_gateway_errors
    shipping_order.notify_cart_failure
  end

  def prepare
    MerchantCartService.build.call(self, self.cart_items)
  end

  def prepare!
    merchant_cart = self.prepare
    merchant_cart.save!
  end

  def assign_to(user)
    self.status = 'started'
    self.assignable = user
    self.assigned_at = Time.now.utc
  end

  def assignable_to_s
    assignable_type.to_s + ' ' + assignable.try(:email).to_s
  end

  def completed_by(user)
    self.status = 'completed'
    self.completable = user
    self.completed_at = Time.now.utc
    self.actual_total = calculate_actual_total
  end

  def completable_to_s
    completable_type.to_s + ' ' + completable.try(:email).to_s
  end

  # TODO Deprecated
  # def run!
  #   return true if self.completed? || self.running?
  #   self.prepare!
    
  #   if self.failed?
  #     raise 'CartError'
  #   end

  #   self.running!

  #   merchant_cart = MerchantCart.includes(:giftbox, shipping_order: :shipping_address, cart_items: :variant).where(id: self.id).first
  #   shipping_order = merchant_cart.shipping_order
  #   merchant = merchant_cart.merchant
  #   provider = Rails.env.test? ? 'test' : merchant.name
  #   shipping_address = merchant_cart.shipping_order.shipping_address
  #   url = merchant_cart.amazon_remote_cart_purchase_url

  #   result = PurchaseGatewayService.build.call(
  #     provider, 
  #     url: url, 
  #     shipping_address: shipping_address,
  #     shipping_option: giftbox.shipping_option
  #   )
    
  #   self.complete!(result, shipping_order: shipping_order, via_purchase_gateway: true) if result.success?

  # rescue StandardError => e
  #   p e.inspect
  # ensure
  #   self.logs = get_tail_log
  #   self.failed!(shipping_order: shipping_order, errors: result.try(:errors)) unless result.try(:success)
  #   return true
  # end

  # def get_tail_log
  #   last_200_lines = puts `tail -n 200 log/shoryuken.log`
  #   self.log = last_200_lines
  # end
end
