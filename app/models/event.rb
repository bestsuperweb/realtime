# == Schema Information
#
# Table name: events
#
#  id          :integer          not null, primary key
#  customer_id :integer
#  name        :string           not null
#  description :text
#  occurs_at   :datetime         not null
#  state       :string           default("draft"), not null
#  created_at  :datetime
#  updated_at  :datetime
#  location    :string
#  event_image :string
#  basket_id   :integer
#

class Event < ApplicationRecord

  belongs_to :customer
  belongs_to :basket
  has_many :customers_events, dependent: :destroy
  has_many :attendees, through: :customers_events, source: :customer
  has_many :activities, as: :actable, dependent: :destroy

  after_create :invite_host
  after_destroy :destroy_basket

  validates_inclusion_of :state, in: %w( draft private public )

  def clean_date
    self.occurs_at.strftime("%B %d | %I:%M %p")
  end

  def hosted_by?(customer)
    self.customer == customer
  end

  def self.current_month_events
    current_evt = []

    Event.all.each do |event|
      current_evt << event if event.occurs_at.present? && event.occurs_at.month == Time.now.month
    end

    current_evt
  end

  def remove_from_event(variant)
    if self.basket.contains?(variant)
      gift = Gift.find_by_basket_id_and_variant_id(self.basket.id, variant.id)

      gift.destroy

      return true

    else
      return false
    end
  end

  def invite_host
    attendee = CustomersEvent.new
    attendee.customer = self.customer
    attendee.event = self
    attendee.status = "attending"

    attendee.save!
  end

  def destroy_basket
    p 'basket destroy'
    basket = Basket.find(self.basket_id)
    p basket.inspect
    basket.destroy if !basket.nil?
  end

  def invite_customer( customer_id, notify = true )
    customer = Customer.find customer_id

    if customer.present? && !( self.attendees.include? customer )
      attendee          = CustomersEvent.new
      attendee.customer = customer
      attendee.event    = self

      attendee.save!

      if customer != self.customer

        a               = Activity.new
        a.activity_type = "event_invitation"
        a.customer      = self.customer
        a.actable       = self

        a.save!

        if customer.setting_selected?( "event_invite" ) && notify
          n          = Notification.new
          n.activity = a
          n.customer = customer

          n.save!
        end
      end

      return true
    else
      return false
    end
  end

  def confirm_invitation( customer_id )
    attendee = CustomersEvent.where( customer_id: customer_id, event_id: self.id ).first
    valid_flag = true

    unless attendee.present?
      customer = Customer.find customer_id
      if customer.present?
        self.invite_customer( customer, false )

        attendee = CustomersEvent.where( customer_id: customer_id, event_id: self.id ).first
      else
        valid_flag = false
      end
    end

    if valid_flag
      attendee.status = "attending"
      attendee.save!

      if attendee != self.customer

        a                 = Activity.new
        a.activity_type   = "event_response_yes"
        a.customer        = attendee.customer
        a.actable         = self

        a.save!

        if self.customer.setting_selected?( "event_attendee_status_change" )
          n          = Notification.new
          n.activity = a
          n.customer = self.customer

          n.save!
        end

      end

    end
    return valid_flag
  end

  def reject_invitation( customer_id )
    attendee = CustomersEvent.where( customer_id: customer_id, event_id: self.id ).first
    valid_flag = true

    unless attendee.present?
      customer = Customer.find customer_id
      if customer.present?
        self.invite_customer( customer, false )

        attendee = CustomersEvent.where( customer_id: customer_id, event_id: self.id ).first
      else
        valid_flag = false
      end
    end

    if valid_flag
      attendee.status = "not_attending"
      attendee.save!

      if attendee != self.customer
        a               = Activity.new
        a.activity_type = "event_response_no"
        a.customer      = attendee.customer
        a.actable       = self

        a.save!

        if self.customer.setting_selected?( "event_attendee_status_change" )
          n          = Notification.new
          n.activity = a
          n.customer = self.customer

          n.save!
        end
      end

      return true
    end
    return valid_flag
  end

  def maybe_invitation( customer_id )
    attendee = CustomersEvent.where( customer_id: customer_id, event_id: self.id ).first
    valid_flag = true

    unless attendee.present?
      customer = Customer.find customer_id
      if customer.present?
        self.invite_customer( customer, false )

        attendee = CustomersEvent.where( customer_id: customer_id, event_id: self.id ).first
      else
        valid_flag = false
      end
    end

    if valid_flag
      attendee.status = "maybe"
      attendee.save!

      if attendee != self.customer
        a               = Activity.new
        a.activity_type = "event_response_maybe"
        a.customer      = attendee.customer
        a.actable       = self

        a.save!

        if self.customer.setting_selected?( "event_attendee_status_change" )
          n          = Notification.new
          n.activity = a
          n.customer = self.customer

          n.save!
        end
      end

      return true 
    end

    return valid_flag
  end

  def confirmed_count
    CustomersEvent.where( event_id: self.id, status: "attending" ).count
  end

  def maybe_count
    CustomersEvent.where( event_id: self.id, status: "maybe" ).count
  end

  def pending_count
    CustomersEvent.where( event_id: self.id, status: "invited" ).count
  end

  def not_attending_count
    CustomersEvent.where( event_id: self.id, status: "not_attending" ).count
  end

  def confirmed_attendees
    CustomersEvent.where( event_id: self.id, status: "attending" ).collect { |ce| ce.customer }
  end

  def maybe_attendees
    CustomersEvent.where( event_id: self.id, status: "maybe" ).collect { |ce| ce.customer }
  end

  def pending_attendees
    CustomersEvent.where( event_id: self.id, status: "invited" ).collect { |ce| ce.customer }
  end

  def not_attendees
    CustomersEvent.where( event_id: self.id, status: "not_atttending" ).collect { |ce| ce.customer }
  end

end
