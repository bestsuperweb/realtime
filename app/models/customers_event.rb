# == Schema Information
#
# Table name: customers_events
#
#  id          :integer          not null, primary key
#  customer_id :integer
#  event_id    :integer
#  status      :string           default("invited")
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class CustomersEvent < ApplicationRecord
  belongs_to :customer
  belongs_to :event

  validates_inclusion_of :status, in: %w( invited attending maybe not_attending )
end
