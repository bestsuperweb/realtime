# == Schema Information
#
# Table name: gifts
#
#  id                :integer          not null, primary key
#  note              :text
#  variant_id        :integer
#  basket_id         :integer
#  created_at        :datetime
#  updated_at        :datetime
#  quantity_desired  :integer          default(1)
#  quantity_received :integer          default(0)
#  description       :text
#  slug              :string
#  share_count       :integer          default(0)
#  customer_id       :integer
#  rating            :integer          default(0)
#

class Gift < ApplicationRecord
  extend FriendlyId
  friendly_id :gift_slug_name, use: :slugged
  acts_as_taggable

  belongs_to        :basket, required: false
  belongs_to        :variant, required: false
  has_one           :cart_item, dependent: :nullify
  belongs_to        :customer, required: false
  has_many          :comments, as: :commentable, dependent: :destroy
  has_many          :likes, as: :likable, dependent: :destroy
  has_many          :activities, as: :actable, dependent: :destroy

  before_update     :update_gift_slug

  after_create      :inherit_tags
  after_create      :assign_to_customer
  after_create      :create_gift_slug


  def assign_to_customer
    self.customer_id = self.basket.customer.id
    self.save
  end

  def create_gift_slug
    self.slug = "#{self.variant.try(:slug)}-#{id}".parameterize
    self.save!
  end

  def update_gift_slug
    self.slug = "#{self.variant.try(:slug)}-#{id}".parameterize
  end

  def gift_slug_name
    self.slug
  end

  def gift?
    true
  end

  def inherit_tags
    tags = self.try(:variant).try(:product).try(:tag_list).try(:join, ", ")
    self.tag_list.add(tags, parse: true)
  end

  def purchase_responses( purchaser )
    if ( purchaser.is_a? Guest ) || ( purchaser == "Guest" )
      a = Activity.new
      a.activity_type = "purchase_gift_guest"
      a.actable = self

      a.save
    elsif purchaser.is_a? Customer
      a = Activity.new
      a.activity_type = "purchase_gift_customer"
      a.actable = self
      a.customer = purchaser

      a.save
    end 

    n = Notification.new
    n.customer = self.basket.customer
    n.activity = a

    n.save
  end

end
