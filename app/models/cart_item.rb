# == Schema Information
#
# Table name: cart_items
#
#  id                         :integer          not null, primary key
#  cart_id                    :integer
#  merchant_cart_id           :integer
#  gift_id                    :integer
#  variant_id                 :integer
#  quantity                   :integer          default(0)
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  price                      :decimal(, )
#  subtotal                   :decimal(, )
#  giftbox_id                 :integer
#  verified_at                :datetime
#  current_offer_id           :string
#  current_available_quantity :integer          default(0)
#  current_price              :decimal(, )      default(0.0)
#  current_sale_price         :decimal(, )      default(0.0)
#

class CartItem < ApplicationRecord
  belongs_to :cart
  belongs_to :giftbox
  belongs_to :merchant_cart
  belongs_to :gift
  belongs_to :variant
  
  validates :cart, presence: true
  validates :variant, presence: true

  before_create :assign_current_price
  before_save :calculate_subtotal, if: :quantity_changed?
  before_create :assign_to_giftbox
  before_create :assign_to_merchant_cart
  after_destroy :destroy_merchant_cart, if: :last_merchant_cart_gift?
  after_destroy :destroy_giftbox, if: :last_giftbox_gift?

  after_create :recount_cart_items
  after_update :recount_cart_items, if: :giftbox_or_merchant_cart_changed?
  after_destroy :recount_cart_items

  def gift?
    self.gift_id.present?
  end

  def calculate_subtotal
    self.subtotal = self.current_price*quantity
  end

  def displayed_price
    self.current_price
  end

  def adjust_quantity!(new_quantity)
    self.quantity = new_quantity
    self.save
  end

  def add_to_cart!
    new_quantity = quantity + 1
    self.quantity = new_quantity
    self.save
  end

  def remove_from_cart!
    if self.quantity > 1
      self.quantity -= 1
      self.save
    else
      true
    end
  end

  def assign_to_giftbox
    cart = self.cart
    owner = cart.owner

    if self.gift?
      gift = Gift.includes(:customer).where(id: self.gift_id).first

      if gift.customer != owner
        giftbox = initialize_giftee_giftbox(gift, cart, owner)
      else
        giftbox = initialize_owner_giftbox(cart, owner)
      end
    else
      giftbox = initialize_owner_giftbox(cart, owner)
    end

    if giftbox.save
      self.giftbox = giftbox
    else
      raise "invalid giftbox"
    end
  end

  def initialize_giftee_giftbox(gift, cart, owner)
    giftee = gift.customer
    giftee_shipping_address = gift.customer.default_shipping_address

    cart.giftboxes.where(
      owner: owner,
      giftee: giftee,
      is_a_gift: true,
      status: 0
    ).first_or_initialize(
      giftee_shipping_address_id: giftee_shipping_address.try(:id)
    )
  end

  def initialize_owner_giftbox(cart, owner)
    cart.giftboxes.where(
      owner: owner,
      giftee: owner,
      is_a_gift: false,
      status: 0
    ).first_or_initialize
  end

  def assign_to_merchant_cart
    giftbox = self.giftbox
    merchant_id = self.variant.merchant_id
    merchant_cart = giftbox.merchant_carts.where(merchant_id: merchant_id).first_or_initialize

    merchant_cart.save if merchant_cart.new_record?
    self.merchant_cart_id = merchant_cart.id
  end

  private

  def giftbox_or_merchant_cart_changed?
    saved_change_to_attribute?(:giftbox_id) || saved_change_to_attribute?(:merchant_cart_id)
  end

  def last_giftbox_gift?
    self.try(:giftbox).try(:cart_items).try(:count).try(:zero?) || false
  end

  def last_merchant_cart_gift?
    self.try(:merchant_cart).try(:cart_items).try(:count).try(:zero?) || false
  end

  def destroy_merchant_cart
    self.merchant_cart.destroy
  end

  def destroy_giftbox
    self.giftbox.merchant_carts.destroy_all
    self.giftbox.destroy
  end

  def assign_current_price
    self.current_price = variant.pretty_price
  end

  def recount_cart_items
    unless last_giftbox_gift?
      giftbox = Giftbox.includes(:cart_items).where(id: self.giftbox_id).first
      giftbox.update(cart_items_count: giftbox.cart_items.size)
    end

    unless last_merchant_cart_gift?
      merchant_cart = MerchantCart.includes(:cart_items).where(id: self.merchant_cart_id).first
      merchant_cart.update(cart_items_count: merchant_cart.cart_items.size) if merchant_cart.present?
    end
  end

end
