# == Schema Information
#
# Table name: payment_methods
#
#  id                    :integer          not null, primary key
#  uuid                  :string           not null
#  owner_id              :integer
#  address_id            :integer
#  stripe_id             :string           not null
#  default               :boolean          default(FALSE)
#  encrypted_metadata    :text
#  encrypted_metadata_iv :text
#  payment_type          :string
#  encrypted_name        :string
#  encrypted_name_iv     :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  owner_type            :string           default("Customer")
#  status                :integer          default(0)
#

class PaymentMethod < ApplicationRecord
  has_secure_token :uuid

  belongs_to :owner, polymorphic: true
  belongs_to :billing_address, class_name: 'Address', foreign_key: 'address_id'
  has_many :invoices, dependent: :nullify
  has_many :payments, dependent: :nullify
  has_many :giftboxes, dependent: :nullify

  validates :owner, presence: true
  validates :stripe_id, presence: true
  validates :metadata, presence: true
  validates :payment_type, presence: true
  validates :billing_address, presence: true

  before_create :assign_default_payment_method
  after_create :connect_to_stripe_customer

  store :metadata, coder: JSON

  enum status: [:active, :retired]

  attr_encrypted :name, key: ENV['ATTR_ENCRYPTED_KEY']
  attr_encrypted :metadata, key: ENV['ATTR_ENCRYPTED_KEY'], marshal: true

  def assign_default_payment_method
    return true unless self.owner.default_payment_method.blank?
    self.default = true
  end

  def first_payment_method?
    customer.payment_methods.count.zero?
  end

  def connect_to_stripe_customer
    StripeService.build.attach_payment_method_to(owner, self)
  end

end
