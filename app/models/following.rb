# == Schema Information
#
# Table name: followings
#
#  id            :integer          not null, primary key
#  customer_1_id :integer
#  customer_2_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Following < ApplicationRecord
  belongs_to :customer, foreign_key: 'customer_1_id'
  belongs_to :customer, foreign_key: 'customer_2_id'  

end
