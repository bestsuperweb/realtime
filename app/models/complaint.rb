# == Schema Information
#
# Table name: complaints
#
#  id         :integer          not null, primary key
#  name       :string
#  error_type :string
#  body       :text
#  email      :string
#  created_at :datetime
#  updated_at :datetime
#

class Complaint < ApplicationRecord
	belongs_to :customer, required: false
	belongs_to :admin, required: false

	TOPICS = [ "Gift Assistant", "Login / Sign Up", "Basket Help", "Life Event", "Careers", "Advertisement", "Affiliate", "Help", "Legal", "Other" ]

  	validates_inclusion_of :error_type, in: TOPICS

  	def self.error_types
  		TOPICS
  	end

  	def admin
  		Admin.find(self.admin_id)
  	end

  	def customer
  		Customer.find_by_email(self.email)
  	end

end
