# == Schema Information
#
# Table name: payments
#
#  id                :integer          not null, primary key
#  state             :string           default("checkout"), not null
#  created_at        :datetime
#  updated_at        :datetime
#  uuid              :string           not null
#  payment_method_id :integer
#  owner_id          :integer
#  invoice_id        :integer
#  status            :integer          default(0)
#  outcome           :text
#  stripe_id         :string
#  idempotent_key    :string
#  authorized_amount :decimal(, )      default(0.0), not null
#  charged_amount    :decimal(, )      default(0.0), not null
#  owner_type        :string           default("Customer")
#

class Payment < ApplicationRecord
  has_secure_token :uuid
  has_secure_token :idempotent_key

  belongs_to :payment_method
  belongs_to :owner, polymorphic: true
  belongs_to :invoice

  validates :invoice, presence: true
  validates :owner, presence: true
  validates :payment_method, presence: true
  validates :authorized_amount, presence: true

  enum status: [:pending, :authorized, :captured, :authorization_failed, :capture_failed]

  store :outcome, coder: JSON

  def capture!
    amount = self.invoice.total_amount
    StripeService.build.capture(self, amount)
    self.update!(status: 'captured', charged_amount: amount)
  end
end

