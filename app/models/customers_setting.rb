# == Schema Information
#
# Table name: customers_settings
#
#  id              :integer          not null, primary key
#  customer_id     :integer
#  setting_id      :integer
#  selected_option :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class CustomersSetting < ApplicationRecord
  belongs_to :setting
  belongs_to :customer
end
