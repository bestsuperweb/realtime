class Purchaser < ApplicationRecord
  has_many :merchant_carts, as: :assignable
  has_many :merchant_carts, as: :completable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable #:registerable,
        #:rememberable, :trackable, :validatable

  # Hack needed by devise to workaround config/initializers/devise.rb:37
  # config.authentication_keys = [:login]
  #===================
  def login=(login)
    @login = login
  end

  def login
    self.email
  end

  def self.find_for_database_authentication(warden_conditions)
    where(email: warden_conditions[:login].downcase).first
  end
  #==================-

  def soft_delete  
    update_attribute(:deleted_at, Time.current)  
  end  
  
  # ensure user account is active  
  def active_for_authentication?  
    super && !deleted_at  
  end  
  
  # provide a custom message for a deleted account   
  def inactive_message   
    !deleted_at ? super : :deleted_account  
  end  
end
