# == Schema Information
#
# Table name: notifications
#
#  id          :integer          not null, primary key
#  activity_id :integer
#  customer_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  read        :boolean          default(FALSE)
#

class Notification < ApplicationRecord
  belongs_to :customer, required: false
  belongs_to :activity, required: false
  after_create_commit :push_notification

  def content
    activity = self.activity
    #
    # Something is Liked
    #
    if ["like"].include? activity.activity_type
      response = "<a href='/#{activity.customer.username}'>#{activity.customer.name}</a> has liked your "
      if activity.actable_type == "Gift"
        response << "wish: <a href='/gift/#{activity.actable.slug}'>#{activity.actable.variant.trunc_name}</a>"
      elsif activity.actable_type == "Basket"
        basket_type = activity.actable.lifestyle? ? "registry" : activity.actable.basket_type
        response << "wish list: <a href='/#{activity.actable.customer.username}/#{basket_type}/#{activity.actable.slug}'>#{activity.actable.name}</a>"
      elsif activity.actable_type == "Comment"
        if activity.actable.commentable_type == "Gift"
          response << "<a href='/gift/#{activity.actable.commentable.slug}#discussion'>comment</a>"
        elsif activity.actable.commentable_type == "Basket"
          basket_type = activity.actable.commentable.lifestyle? ? "registry" : activity.actable.commentable.basket_type
          response << "<a href='/#{activity.actable.commentable.customer.username}/#{basket_type}/#{activity.actable.commentable.slug}#discussion'>comment</a>"
        elsif activity.actable.commentable_type == "Comment"
          if activity.actable.commentable.commentable_type == "Gift"
            response << "<a href='/gift/#{activity.actable.commentable.commentable.slug}#discussion'>comment</a>"
          elsif activity.actable.commentable.commentable_type == "Basket"
            basket_type = activity.actable.commentable.lifestyle? ? "registry" : activity.actable.commentable.basket_type
            response << "<a href='/#{activity.actable.commentable.customer.username}/#{basket_type}/#{activity.actable.commentable.slug}#discussion'>comment</a>"
          end
        end
      end
    #
    # Something is commented
    #
    elsif ["comment"].include? activity.activity_type
      response = "<a href='/#{activity.customer.username}'>#{activity.customer.name}</a> "
      if activity.actable.commentable_type == "Gift"
        response << "left a comment on your wish: <a href='/gift/#{activity.actable.commentable.slug}#discussion'>#{activity.actable.commentable.variant.trunc_name}</a>"
      elsif activity.actable.commentable_type == "Basket"
        if activity.actable.commentable.basket_type == "event"
          response << "left a comment on your event: "
        else
          response << "left a comment on your wish list: "
        end
          basket_type = activity.actable.commentable.lifestyle? ? "registry" : activity.actable.commentable.basket_type
          response << "<a href='/#{activity.actable.commentable.customer.username}/#{basket_type}/#{activity.actable.commentable.slug}#discussion'>#{activity.actable.commentable.name}</a>"
      elsif activity.actable.commentable_type == "Comment"
        if activity.actable.commentable.commentable_type == "Gift"
          response << "replied to your comment on wish: <a href='/gift/#{activity.actable.commentable.commentable.slug}#discussion'>#{activity.actable.commentable.commentable.variant.trunc_name}</a>"
        elsif activity.actable.commentable.commentable_type == "Basket"
          if activity.actable.commentable.commentable.basket_type == "event"
            response << "replied to your comment on event: "
          else
            response << "replied to your comment on basket: "
          end
            basket_type = activity.actable.commentable.lifestyle? ? "registry" : activity.actable.commentable.basket_type
            response << "<a href='/#{activity.actable.commentable.customer.username}/#{basket_type}/#{activity.actable.commentable.slug}#discussion'>#{activity.actable.commentable.commentable.name}</a>"
        end
      end
    #
    # Someone is followed
    #
    elsif ["followed"].include? activity.activity_type
      response = "<a href='/#{activity.customer.username}'>#{activity.customer.name}</a> has followed you"
    #
    # Someone made a connection request
    #
    elsif ["connection_request"].include? activity.activity_type
      response = "<a href='/#{activity.customer.username}'>#{activity.customer.name}</a> has sent you a connection request."
    #
    # Connection request Accepted
    #
    elsif ["connection_accepted"].include? activity.activity_type
      response = "<a href='/#{activity.customer.username}'>#{activity.customer.name}</a> has accepted your connection request."
    #
    # Something is shared
    #
    elsif ["share"].include? activity.activity_type
      response = "<a href='/#{activity.customer.username}'>#{activity.customer.name}</a> has shared "
      if activity.actable_type == "Customer"
        if activity.customer_id == activity.actable_id
          response << "<a href='/#{activity.actable.username}'>their profile</a>"
        else
          response << "<a href='/#{activity.actable.username}'>#{activity.actable.name}</a>'s profile"
        end
      elsif activity.actable_type == "Basket"
        basket_type = activity.actable.lifestyle? ? "registry" : activity.actable.basket_type
        response << "the <a href='/#{activity.actable.customer.username}/#{basket_type}/#{activity.actable.slug}'>#{activity.actable.name}</a> wish list"
      elsif activity.actable_type == "Gift"
        response << "<a href='/gift/#{activity.actable.slug}'>#{activity.actable.variant.trunc_name}</a>"
      elsif activity.actable_type == "Variant"
        response << "<a href='/products/#{activity.actable.slug}'>#{activity.actable.trunc_name}</a>"
      end
      response << " with you."
    #
    # Something owned is shared
    #
    elsif ["shared_thing"].include? activity.activity_type
      response = "<a href='/#{activity.customer.username}'>#{activity.customer.name}</a> has shared "
      if activity.actable_type == "Customer"
        response << "<a href='/#{activity.actable.username}'>your profile</a>"
      elsif activity.actable_type == "Basket"
        basket_type = activity.actable.lifestyle? ? "registry" : activity.actable.basket_type
        response << "the <a href='/#{activity.actable.customer.username}/#{basket_type}/#{activity.actable.slug}'>#{activity.actable.name}</a> wish list"
      elsif activity.actable_type == "Gift"
        response << "your <a href='/gift/#{activity.actable.slug}'>#{activity.actable.variant.trunc_name}</a> wish"
      end
      response << " with someone."
    #
    # Event invitation
    #
    elsif ["event_invitation"].include? activity.activity_type
      response = "<a href='/#{activity.customer.username}'>#{activity.customer.name}</a> has invited you to attend their <a href='/#{activity.actable.basket.customer.username}/event/#{activity.actable.basket.slug}'>#{activity.actable.name}</a> event."
    #
    # Event invitiation acceptance
    #
    elsif ["event_response_yes"].include? activity.activity_type
      response = "<a href='/#{activity.customer.username}'>#{activity.customer.name}</a> will be attending your <a href='/#{activity.actable.basket.customer.username}/event/#{activity.actable.basket.slug}'>#{activity.actable.name}</a> event."
    #
    # Event invitation rejection
    #
    elsif ["event_response_no"].include? activity.activity_type
      response = "<a href='/#{activity.customer.username}'>#{activity.customer.name}</a> will not be attending your <a href='/#{activity.actable.basket.customer.username}/event/#{activity.actable.basket.slug}'>#{activity.actable.name}</a> event."
    #
    # Event invitation maybe
    #
    elsif ["event_response_maybe"].include? activity.activity_type
      response = "<a href='/#{activity.customer.username}'>#{activity.customer.name}</a> might attend your <a href='/#{activity.actable.basket.customer.username}/event/#{activity.actable.basket.slug}'>#{activity.actable.name}</a> event."
    #
    # Known customer purchased a gift
    #
    elsif ["purchase_gift_customer"].include? activity.activity_type
      response = "<a href='/#{activity.customer.username}'>#{activity.customer.name}</a> has purchased one of your wishes for you: <a href='/gift/#{activity.actable.slug}'>#{activity.actable.variant.trunc_name}</a>"
    #
    # Unknown guest purchased a gift
    #
    elsif ["purchase_gift_guest"].include? activity.activity_type
      response = "Someone has purchased one of your wishes for you: <a href='/gift/#{activity.actable.slug}'>#{activity.actable.variant.trunc_name}</a>"
    end

    response
  end

  def push_notification
    if customer.present?
      NotificationRelayJob.perform_later(self, customer.notification_count)
    end
  end
end