# frozen_string_literal: true
# == Schema Information
#
# Table name: carts
#
#  id                   :integer          not null, primary key
#  owner_id             :integer
#  flagged_as_current   :boolean          default(FALSE)
#  status               :integer          default(0), not null
#  cart_items_count     :integer          default(0)
#  flagged_for_purchase :boolean          default(FALSE)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  address_id           :integer
#  payment_method_id    :integer
#  owner_type           :string           default("Customer")
#

class Cart < ApplicationRecord  
  belongs_to :owner, polymorphic: true
  has_many :cart_items, dependent: :destroy
  has_many :giftboxes, dependent: :destroy
  has_many :variants, through: :cart_items
  has_many :gifts, through: :cart_items
  
  enum status: [:current, :checked_out, :retired]
  enum shipping_option: [:standard, :fast]

  def assign_guest_owner!(guest)
    transaction do
      self.update!(owner: guest)
      self.giftboxes.update_all(
        owner_id: guest.id,
        owner_type: 'Guest', 
        address_id: nil,
        payment_method_id: nil
      )
    end
  rescue
    raise "failed to assign_guest_owner"
  end

  def pending_items_count
    self.giftboxes.pending.map(&:cart_items_count).sum
  end

  def owner_is_a_guest?
    self.owner_id.present? && self.owner.class.name == 'Guest'
  end

end
