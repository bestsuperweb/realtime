module PunditHelper
  extend ActiveSupport::Concern

  def is_admin?
    self.class == Admin
  end

  def is_customer?
    self.class == Customer
  end
  
end