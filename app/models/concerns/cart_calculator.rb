# frozen_string_literal: true

# Formula
# subtotal = $10
# + shipping_fee = $10 (merchant_cart.count * (shipping_flat_rate * shipping_rate_multiplier_percentage)
# + giftibly_processing_fee = $20*giftibly_processing_fee
# + stripe_per_transaction_fee = $20*stripe_fee_percentage + stripe_per_transaction_fee
# ----------
# subtotal_before_taxes
# + taxes
# ----------
# total


module CartCalculator
  extend ActiveSupport::Concern

  def app_setting
    AppSetting.first
  end

  def merchant_carts_count
    return 1 if self.class == MerchantCart
    return giftbox.merchant_carts.count if self.class == Invoice
    
    merchant_carts.count
  end

  def subtotal_estimate
    cart_items.sum(:subtotal)
  end

  def giftbox_obj
    return self if self.class == Giftbox
    self.giftbox
  end

  def shipping_rate
    giftbox_obj.fast? ? app_setting.fast_shipping_flat_rate : app_setting.shipping_flat_rate
  end

  def shipping_fee_estimate
    merchant_carts_count * shipping_rate * app_setting.shipping_rate_multiplier_percentage
  end

  def service_fee_estimate
    (subtotal_estimate + shipping_fee_estimate) * app_setting.invoice_processing_fee_percentage + app_setting.stripe_per_transaction_fee
  end

  def subtotal_before_taxes_estimate
    subtotal_estimate + shipping_fee_estimate + service_fee_estimate
  end

  def tax_estimate
    subtotal_estimate * app_setting.tax_percentage
  end

  def total_amount_estimate
    subtotal_before_taxes_estimate + tax_estimate
  end

  def authorization_estimate
    total_amount_estimate * app_setting.authorization_multiplier_percentage
  end

end
