# frozen_string_literal: true
module CartEstimator
  extend ActiveSupport::Concern

  APP_SETTING = AppSetting.first.freeze
  SERVICE_FEE_RATE = (APP_SETTING.try(:service_fee_rate) || 0).freeze
  TAX_RATE = (APP_SETTING.try(:sales_tax_rate) || 0).freeze
  SHIPPING_FEE = (APP_SETTING.try(:shipping_flat_rate) || 0) * (APP_SETTING.try(:shipping_rate_multiplier) || 0).freeze
  PROCESSING_FEE_RATE = [(APP_SETTING.try(:service_fee_rate) || 0), (APP_SETTING.try(:stripe_processing_rate) || 0)].sum.freeze
  STRIPE_PER_TRANSACTION_FEE = (APP_SETTING.try(:stripe_per_transaction_fee) || 0).freeze
  AUTHORIZATION_MULTIPLIER = (APP_SETTING.try(:authorization_multiplier) || 0).freeze

  def subtotal_estimate
    self.cart_items.sum(:subtotal)
  end

  def shipping_fee_estimate
    SHIPPING_FEE
  end

  def service_fee_estimate
    (subtotal_estimate * (PROCESSING_FEE_RATE/100) + STRIPE_PER_TRANSACTION_FEE)
  end

  def tax_estimate
    (subtotal_estimate * (TAX_RATE/100))
  end

  def total_amount_estimate
    (subtotal_estimate + service_fee_estimate + shipping_fee_estimate + tax_estimate)
  end

  def authorization_estimate
    total_amount_estimate * AUTHORIZATION_MULTIPLIER
  end

end
