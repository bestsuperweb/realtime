# == Schema Information
#
# Table name: variants
#
#  id                  :integer          not null, primary key
#  name                :string           not null
#  price               :integer          default(0), not null
#  product_id          :integer
#  merchant_id         :integer
#  api_id              :string
#  description         :text
#  merchant_url        :string
#  status              :string           default("active"), not null
#  created_at          :datetime
#  updated_at          :datetime
#  product_image       :string
#  pending             :boolean          default(FALSE)
#  color               :string
#  size                :string
#  slug                :string
#  share_count         :integer          default(0)
#  shipping_dimensions :text
#  shipping_category   :text
#  metadata            :text
#

class Variant < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :gifts, dependent: :destroy
  has_many :gift_otds, dependent: :destroy
  belongs_to :product, required: false
  belongs_to :merchant, required: false
  has_many :cart_items, dependent: :destroy
  has_many :carts, through: :cart_items
  has_many :likes, as: :likable, dependent: :destroy

  after_destroy :destroy_activity

  validates :name, presence: true
  validates_numericality_of :price, allow_nil: false, only_integer: true, greater_than_or_equal_to: 0
  validates_inclusion_of :status, in: %w( active inactive )
  validates :product, presence: true

  mount_uploader :product_image, ProductImageUploader

  store :metadata, coder: JSON
  store :shipping_dimensions, coder: JSON

  searchable do
    #search by tag list method
    #we use variants to search for tags
    #to then search for products matching tags
    text :tag_list
    text :description
    text :name
    integer :merchant_id 
  end

  def active?
    self.status == "active"
  end

  def inactive?
    self.status == "inactive"
  end

  def make_active
    self.status = "active"
    self.save
  end

  def make_inactive
    self.status = "inactive"
    self.save

    #Activity.new_inactive_variant( self )
  end

  #Tag list for products
  #solr index's tags for products
  #we search by tags and then return products matching results
  def tag_list
    product = self.product_id.present? ? self.product : nil
    product.try(:tag_list)
  end

  def self.all_active
    where(status: 'active')
  end

  def self.all_inactive
    where(status: 'inactive')
  end

  def gift?
    false
  end

  def pretty_price
    self.price.to_f/100
  end

  def merchant_family( num = 10 )
    Variant.where( 'merchant_id = ? AND id != ?', self.merchant_id, self.id ).shuffle[0..(num-1)]
  end

  def trunc_name( num = 25 )
    if self.name.length > num
      self.name[0..(num-1)] + '...'
    else
      self.name
    end
  end

  def destroy_activity
    activities = Activity.where(actable_id: self.id)
    activities.each do |activity|
      activity.destroy unless activity.nil?
    end
  end

end
