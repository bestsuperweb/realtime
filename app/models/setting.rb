# == Schema Information
#
# Table name: settings
#
#  id            :integer          not null, primary key
#  name          :string
#  description   :text
#  options       :string           default([]), is an Array
#  family        :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  default_value :string
#  group         :string
#

class Setting < ApplicationRecord
  has_many :customers_settings, dependent: :destroy

  validates_inclusion_of :family, in: %w( notification )

  validates_inclusion_of :group, in: [ "Connections", "Sharing", "Likes", "Comments and Messages", "Events", "Gifts for Me" ]
end
