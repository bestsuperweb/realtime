# == Schema Information
#
# Table name: invoices
#
#  id                      :integer          not null, primary key
#  uuid                    :string           not null
#  owner_id                :integer
#  payment_method_id       :integer
#  total_amount            :decimal(, )      default(0.0)
#  status                  :integer          default(0)
#  currency_code           :string
#  currency_symbol         :string
#  idempotent_key          :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  estimated_shipping_fees :decimal(, )      default(0.0), not null
#  shipping_fees           :decimal(, )      default(0.0), not null
#  estimated_taxes         :decimal(, )      default(0.0), not null
#  taxes                   :decimal(, )      default(0.0), not null
#  estimated_subtotal      :decimal(, )      default(0.0), not null
#  subtotal                :decimal(, )      default(0.0), not null
#  service_fee             :decimal(, )      default(0.0)
#  owner_type              :string           default("Customer")
#  giftbox_id              :integer
#

class Invoice < ApplicationRecord
  include ActiveModel::AttributeAssignment
  include CartCalculator

  has_secure_token :uuid
  has_secure_token :idempotent_key

  belongs_to :owner, polymorphic: true
  belongs_to :giftbox
  belongs_to :payment_method
  has_many :payments, dependent: :nullify
  has_one :shipping_order, dependent: :destroy
  has_many :merchant_carts, through: :shipping_order
  has_one :shipping_address, through: :cart

  validates :owner, presence: true
  validates :giftbox, presence: true
  validates :total_amount, presence: true
  validates :payment_method, presence: true

  enum status: [:pending, :authorized, :completed, :failed]

  before_create :assign_currency

  def cart_items
    giftbox.cart_items
  end

  def assign_currency
    self.currency_code = 'USD'
    self.currency_symbol = '$'
  end

  def authorizable_amount_in_cents
    (authorization_estimate*100).to_i
  end

  def build_shipping_order(shipping_address=self.shipping_address)
    ShippingOrder.where(
      invoice_id: self.id,
      owner_id: self.owner_id,
      owner_type: self.owner_type
    ).first_or_initialize(
      address_id: shipping_address.id
    )
  end

  def update_actual_amounts!
    self.assign_attributes(
      shipping_fees: total_actual_shipping_fees,
      taxes: total_actual_taxes,
      subtotal: total_actual_subtotal,
      service_fee: processing_fee
    )

    self.total_amount = total_chargeable_amount
    self.save!
  end

  def total_actual_shipping_fees
    merchant_carts.map(&:actual_shipping_fee).compact.sum
  end

  def total_actual_taxes
    merchant_carts.map(&:actual_taxes).compact.sum
  end

  def total_actual_subtotal
    merchant_carts.map(&:actual_subtotal).compact.sum
  end

  def processing_fee
    (total_actual_subtotal + total_actual_shipping_fees) * app_setting.invoice_processing_fee_percentage + app_setting.stripe_per_transaction_fee
  end

  def actual_subtotal_before_taxes
    total_actual_subtotal + total_actual_shipping_fees + processing_fee
  end

  def total_chargeable_amount
    actual_subtotal_before_taxes + total_actual_taxes
  end

  def estimated_service_fee
    service_fee_estimate
  end

  def estimated_total_amount
    total_amount_estimate
  end

end
