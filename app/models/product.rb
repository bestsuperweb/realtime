# == Schema Information
#
# Table name: products
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime
#  updated_at :datetime
#  pending    :boolean          default(FALSE)
#

class Product < ApplicationRecord
  has_many :variants
  acts_as_taggable

  validates :name, presence: true

  def generate_tags
    ActsAsTaggableOn::Tag.all.each do |tag|
      unless self.tags.include? tag
        if (/\A#{tag.name}\Z/.match(self.name.downcase).present?)
          self.tag_list.add( tag.name )
        else
          self.variants.each do |variant|
            if (variant.description.present?) && (/\A#{tag.name}\Z/.match(variant.description.downcase).present?)
              self.tag_list.add( tag.name )
              break
            end
          end
        end
        self.save
      end
    end
  end

end
