class AppSetting < ApplicationRecord

  def invoice_processing_fee_percentage
    [giftibly_service_fee_percentage, stripe_processing_fee_percentage].sum
  end

  def giftibly_service_fee_percentage
    (service_fee_rate || 0)/100
  end

  def tax_percentage
    (sales_tax_rate || 0)/100
  end

  def shipping_rate_multiplier_percentage 
    (shipping_rate_multiplier || 100)/100
  end

  def stripe_processing_fee_percentage
    (stripe_processing_rate || 0)/100
  end

  def authorization_multiplier_percentage
    (authorization_multiplier || 0)/100
  end

end