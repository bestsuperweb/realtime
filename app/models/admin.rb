# == Schema Information
#
# Table name: admins
#
#  id                         :integer          not null, primary key
#  email                      :string           default(""), not null
#  encrypted_password         :string           default(""), not null
#  reset_password_token       :string
#  reset_password_sent_at     :datetime
#  remember_created_at        :datetime
#  sign_in_count              :integer          default(0), not null
#  current_sign_in_at         :datetime
#  last_sign_in_at            :datetime
#  current_sign_in_ip         :inet
#  last_sign_in_ip            :inet
#  name                       :string
#  token_id                   :string
#  encrypted_mobile_number    :string
#  encrypted_mobile_number_iv :string
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class Admin < ApplicationRecord
  include PunditHelper
  has_secure_token :token_id
  has_many :api_reports
  has_many :merchant_carts, as: :assignable
  has_many :merchant_carts, as: :completable

  devise :database_authenticatable #:timeoutable
  
  attr_encrypted :mobile_number, key: ENV['ATTR_ENCRYPTED_KEY']

  # Hack needed by devise to workaround config/initializers/devise.rb:37
  # config.authentication_keys = [:login]
  #===================
  def login=(login)
    @login = login
  end

  def login
    self.email
  end

  def self.find_for_database_authentication(warden_conditions)
    where(email: warden_conditions[:login].downcase).first
  end
  #==================-

end
