# == Schema Information
#
# Table name: deals
#
#  id                    :integer          not null, primary key
#  name                  :string
#  ad_image              :string
#  start_time            :datetime
#  end_time              :datetime
#  alt_text              :string
#  size                  :string           default("small")
#  google_analytics_code :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  url                   :string
#

class Deal < ApplicationRecord

  mount_uploader :ad_image, AdUploader

  validates_inclusion_of :size, in: %w( small large )

  def self.active_small
    Deal.where( 'start_time <= ? AND end_time > ? AND size = ?', DateTime.now, DateTime.now, "small").order( updated_at: 'desc' )
  end

  def self.active_large
    Deal.where( 'start_time <= ? AND end_time > ? AND size = ?', DateTime.now, DateTime.now, "large").order( updated_at: 'desc' )
  end

end
