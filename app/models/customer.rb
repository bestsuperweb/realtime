# == Schema Information
#
# Table name: customers
#
#  id                     :integer          not null, primary key
#  first_name             :string           default("")
#  middle_initial         :string           default("")
#  last_name              :string           default("")
#  location               :string           default("")
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  role                   :string           default("customer"), not null
#  username               :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  website                :string
#  description            :text
#  gender                 :string
#  private                :boolean          default(FALSE)
#  birthday               :date
#  avatar                 :string
#  banner                 :string
#  employer               :string
#  job_start_date         :date
#  auth_token             :string
#  stripe_id              :string
#

class Customer < ApplicationRecord
  include PunditHelper
  require 'obscenity/active_model'

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, and :timeoutable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  devise :omniauthable, omniauth_providers: [:facebook, :google_oauth2, :linkedin, :twitter]

  has_many :baskets, dependent: :nullify
  has_many :followings, foreign_key: "customer_1_id", dependent: :destroy
  has_many :customer_connections, foreign_key: "customer_1_id", dependent: :destroy
  has_many :blockings, foreign_key: "customer_1_id"
  has_many :hosted_events, class_name: "Event"
  has_many :customers_events
  has_many :events, through: :customers_events
  has_many :activities, as: :actable, dependent: :destroy
  has_many :notifications, dependent: :destroy
  has_many :oauth_credentials, dependent: :destroy
  has_many :customers_settings, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :caused_activities, class_name: "Activity"
  has_many :api_reports, dependent: :destroy
  has_many :gifts, dependent: :destroy

  has_many :owned_giftboxes, class_name: 'Giftbox', as: :owner, dependent: :destroy
  has_many :giftboxes, as: :giftee, dependent: :nullify
  has_many :carts, as: :owner, dependent: :destroy
  has_many :payment_methods, as: :owner, dependent: :destroy
  has_many :payments, as: :owner, dependent: :nullify
  has_many :invoices, as: :owner, dependent: :destroy
  has_many :shipping_orders, as: :owner, dependent: :destroy
  has_many :addresses, as: :owner, dependent: :destroy   

  acts_as_taggable

  mount_uploader :avatar, AvatarUploader
  mount_uploader :banner, BannerUploader

  after_create :notify_admin

  # Ensures that there are only the following roles available:
  #  * admin       - an administrator
  #  * customer    - a basic user
  #  * banned      - a user no longer allowed access to the site
  #  * deactivated - a user that has intentionally shut down their account
  #
  validates_inclusion_of :role, in: %w( admin customer banned deactivated )

  # Excludes any potential affiliates, so we can prevent brand confusion, as well as any routes
  # we'll need
  validates_exclusion_of :username, in: %w( admin superuser register cancel edit update destroy login
                                            logout home basket terms privacy site guest variant user
                                            variants gift gifts baskets customer customers state states
                                            merchant merchants product products etsy amazon ebay ikea
                                            urbanoutfitters nordstrom yoox groupon sephora walgreens
                                            dickssportinggoods saksfifthavenue target saksfifthave
                                            zappos neimanmarcus adidas gymboree overstock scholastic
                                            ralphlauren forever21 nike officedepot handm iherb sony
                                            bodybuildingcom bodybuilding gamestop walmart oldnavy
                                            bananarepublic staples ulta underarmour hottopic sears
                                            homedepot shutterfly jcpenny jcrew potterybarn tigerdirect
                                            newegg barnesnoble barnesandnoble wayfair toysrus
                                            bestbuy best_buy lowes victoriassecret costco macys
                                            ticketmaster bedbathandbeyond bedbathbeyond profile devise
                                            session api message event messages events sessions users
                                            superusers charity charities charitys cash cashs cashes
                                            search searches solr checkout community communities
                                            profiles cart carts activity activities blocking blockings
                                            comment comments following followings medium media guests
                                            payment payments setting settings feed
                                            address addresses feedback errors error report reports
                                            likes notifications faq termsandconditions privacypolicy
                                            whatisgiftibly what_is_giftibly terms_and_conditions
                                            ourpurpose our_purpose terms_and_conditions browsing 
                                            friendsearch friend_search oauth root media_contents 
                                            mediacontents token tokens woot jet bhphotovideo petsmart
                                            1800flowers kohls shoes apple qvc gap hm hsn belk loft
                                            rei williams-sonoma williamssonoma bathandbodyworks wish
                                            neimanmarcus coolstuffinc zulily menswearhouse freepeople 
                                            crateandbarrel roomstogo hottopic riflepaperco aeropostale
                                            bloomingdales disneystore disney footlocker allmodern cb2
                                            anthropologie brokkestone containerstore dwellstudios
                                            food52 kaufmannmercantile pier1 potterybarn potterybarnkids
                                            shopko shopterrain landofnod americaneagle armaniexchange
                                            anntaylor bananarepublic billabong coach chanel express
                                            gucci guess hollister lenscrafters microsoft oakley pink
                                            sunglasshut tiffany underarmour urbanoutfitters vans rue21
                                            biglots verabradley advancedautoparts asus daintyhooligan
                                            dillards dollargeneral girlfriend lulus modcloth tjmaxx
                                            tractorsupply hellomerch burlingtoncoatfactory dell asos
                                            acehardware autozone ethanallen francescas glossier
                                            teeturtle )

  # Username must be between 4 and 20 in length. Can only consist of alphanumeric characters, must be unique
  # and case sensitivity won't count (so hello will prevent heLLO from being a username)
  validates :username, presence: true, length: { minimum: 3, maximum: 20, message: 'must be between 3 and 20 characters in length' }, format: { with: /\A[a-zA-Z0-9]+\z/ },
            uniqueness: { case_sensitive: false, message: 'not unique' }

  validates :username, obscenity: true

  validates :email, presence: true, uniqueness: { case_sensitive: false }

  before_create :generate_token
  after_create :generate_settings

  # solr search block
  # https://github.com/sunspot/sunspot -- documentation
  searchable do
    text :name
  end

  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end

  def facebook
    if self.oauth_credentials.present? && facebook_connected?
      @graph ||= Koala::Facebook::API.new(self.oauth_credentials.last.oauth_token)
    end
  end

  def facebook_friends

    fb_friends = []
    customers = []

    self.facebook.get_connections('me', 'friends').each do |friend|
      fb_friends << friend['id']
    end 

    Customer.find_each do |c|
      if c.oauth_credentials.where( provider: "Facebook", uid: fb_friends ).present? && !self.connected_to?(c) && !self.follows?(c)
        customers << c
      end 
    end 

    return customers.flatten
  end

  def self.to_csv
    attributes = %w{id email first_name last_name}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |customer|
        csv << customer.attributes.values_at(*attributes)
      end
    end

  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { value: login.downcase } ]).first
    elsif conditions.has_key?(:username) || conditions.has_key?(:email)
      conditions[:email].downcase! if conditions[:email]
      where(conditions.to_h).first
    end
  end

  def create_or_update_oauth_credential(provider, access_token)
    stale_credential = self.oauth_credentials.where(provider: provider).first
    if stale_credential.present?
      stale_credential.update!(
        uid: access_token.uid,
        oauth_token: access_token.credentials.token
      )
    else
      self.oauth_credentials.create!(
        provider: provider, 
        uid: access_token.uid,
        oauth_token: access_token.credentials.token
      )
    end
  end

  def self._get_omniauth_email_and_name(provider, access_token)
    raise "Provider #{provider} not handled" unless ['Facebook', 'Twitter', 'LinkedIn', 'Google+'].include? provider
    
    email = access_token.info.email
    full_name = provider == "Twitter" ? "" : access_token.info.first_name.to_s + access_token.info.last_name.to_s
    
    return [email, access_token.info.first_name.to_s, access_token.info.last_name.to_s, full_name]
  end

  def self._get_or_create_customer_from_oauth(provider, access_token)
    email, first_name, last_name, full_name = Customer._get_omniauth_email_and_name(provider, access_token)

    customer =  Customer.find_or_initialize_by(
                  email: email
                )

    if customer.new_record?
      customer.first_name = first_name
      customer.last_name = last_name
      customer.password = Devise.friendly_token[0,20]

      case provider
      when "Facebook"
        customer.remote_avatar_url = access_token.extra.raw_info.picture.data.url if access_token.extra.raw_info.picture.present?
        customer.gender = access_token.extra.raw_info.gender if access_token.extra.raw_info.gender.present?
      when "Twitter"
        customer.remote_avatar_url = access_token.info.image.gsub!( "_normal", "" ) if access_token.info.image.present?
        customer.description = access_token.info.description
      end

      customer.generate_username #( name )
      customer.save
    end
    
    customer
  end

  def self.from_omniauth(provider, access_token, resource)  

    oauth_credential = OauthCredential
                        .includes(:customer)
                        .where(provider: access_token.provider, uid: access_token.uid)
                        .first

    customer =  if resource.present?
                  resource
                elsif oauth_credential.present?
                  oauth_credential.customer
                elsif access_token.info.email.present?
                  Customer._get_or_create_customer_from_oauth(provider, access_token)
                else
                  nil
                end

    #if customer.present? && oauth_credential.blank?
      customer.create_or_update_oauth_credential(provider, access_token)
    #end

    return customer
  end

  def admin?
    self.role == "admin"
  end

  def customer?
    self.role == "customer"
  end

  def banned?
    self.role == "banned"
  end

  def make_admin
    return false if self.banned?
    self.role = "admin"
    self.save!
    return true
  end

  def make_customer
    self.role = "customer"
    self.save!
    return true
  end

  def ban
    self.role = "banned"
    self.save!
    return true
  end

  def deactivated?
    self.role == "deactivated"
  end

  def deactivate
    self.baskets.each do |basket|
      basket.nuke
    end

    Activity.where(customer_id: self.id).destroy_all

    # delete related notifications
    Notification.where(customer_id: self.id).destroy_all

    # delete customer  connection
    CustomerConnection.where(customer_1_id: self.id).destroy_all
    CustomerConnection.where(customer_2_id: self.id).destroy_all

    # delete followings
    Following.where(customer_1_id: self.id).destroy_all
    Following.where(customer_2_id: self.id).destroy_all

    # delete likes
    Like.where(customer_id: self.id).destroy_all

    # delete comments
    Comment.where(customer_id: self.id).destroy_all
    
    # delete giftboxes
    self.giftboxes.each do |giftbox|
      giftbox.destroy
    end
    
    # delete carts
    self.carts.each do |cart|
      cart.destroy
    end

    # delete addresses, payment_methods
    self.addresses.each do |address|
      address.destroy
    end
      
    self.payment_methods.each do |payment_method|
      payment_method.destroy      
    end

    # uninvite from all events
    # purge all related notifications

    self.role = "deactivated"
    self.save!

    return true
  end

  def reactivate

    unless self.username == "giftibly"
      giftibly = Customer.find_by( username: "giftibly" )
      if giftibly.present?
        self.request_connection( giftibly, false )
        giftibly.accept_connection( self, false )
        self.unfollow( giftibly )
      end
    end

    basket          = Basket.new
    basket.name     = "Liked Products"
    basket.customer = self
    basket.privacy  = "private"

    basket.save
    
    self.role = "customer"
    self.save!
    return true
  end

  def name
    if self.first_name.blank? || self.last_name.blank?
      name = self.username
    else
      name = self.first_name + " "
      if self.middle_initial.present?
        name += self.middle_initial + " "
      end
      name += self.last_name
    end

    name
  end

  def upcoming_events
    events = self.customers_events.where.not( status: "not_attending" ).collect { |ce| ce.event if ce.event.occurs_at > Time.now }
    events = events.first.nil? ? nil : events.sort_by{ |e| e[:occurs_at] }

    return events
  end

  def baskets_for_dropdown
    self.baskets.collect{ |b| [b.name, b.id] }
  end

  def create_a_copy_basket( basket_to_copy, new_name = basket_to_copy.name )
    return false if basket_to_copy.customer == self

    new_basket = Basket.create(
      customer: self,
      name: new_name
    )
    basket_to_copy.gifts.each do |gift|
      new_basket.add_to_basket(gift.variant)
    end
    return true
  end

  def basket_name_taken?( basket_name )
    Basket.where( customer_id: self.id, name: basket_name ).present?
  end

  def invited_to?( event )
    ce = CustomersEvent.find_by( customer_id: self.id, event_id: event.id )
    ce.present?
  end

  def viewable_by( other_customer )
    if self.private
      ( self == other_customer ) || ( self.connected_to? other_customer )
    else
      self.not_blocked? other_customer
    end
  end

  def purchase_feedback(feedback)
    purchaseFeedback                      = PurchaseFeedback.new
    purchaseFeedback.actable_id           = feedback['actable_id']
    purchaseFeedback.actable_type         = feedback['actable_type']
    purchaseFeedback.customer_id          = feedback['customer_id']
    purchaseFeedback.purchased            = feedback['purchased']
    purchaseFeedback.quantity             = feedback['quantity'] if feedback['quantity'].present?
    purchaseFeedback.surprise             = feedback['surprise'] if feedback['surprise'].present?
    purchaseFeedback.confirmation_number  = feedback['confirmation_number'] if feedback['confirmation_number'].present?
    purchaseFeedback.feedback             = feedback['feedback'] if feedback['feedback'].present?
    purchaseFeedback.save!
  end

  def friends
    ( self.followers + self.connections ).uniq
  end

  def likes?( likable )
    return Like.find_by( likable_type: likable.class.to_s, likable_id: likable.id, customer_id: self.id ).present?
  end

  def like( likable )
    if self.likes? likable
      return false
    else
      like          = Like.new
      like.likable  = likable
      like.customer = self

      if like.valid?
        like.save!

        if ["Gift", "Variant"].include? like.likable
          
        end

        return true
      else
        return false
      end
    end
  end

  def unlike( likable )
    if self.likes? likable
      like = Like.find_by( likable_type: likable.class.to_s, likable_id: likable.id, customer_id: self.id )

      if ["Gift", "Variant"].include? like.likable

      end

      like.destroy

      return true
    else
      return false
    end
  end

  def follow( new_friend, send_notification = true )
    return false if( ( self == new_friend ) || self.follows?( new_friend ) )
    following = Following.create(
      customer_1_id: self.id,
      customer_2_id: new_friend.id
    )

    a               = Activity.new
    a.activity_type = "followed"
    a.actable       = self
    a.customer      = self
    a.save

    if ( self.setting_selected?( "new_connection_request" ) && send_notification )
      n             = Notification.new
      n.customer_id = new_friend.id
      n.activity    = a
      n.save
    end

    return true
  end

  def unfollow( old_friend )
    return false if( !self.follows?( old_friend ) )
    following = Following.where( customer_1_id: self.id, customer_2_id: old_friend.id).first
    following.destroy
    return true
  end

  def followers
    response = Following.where( customer_2_id: self.id ).collect { |following| Customer.where( 'role != ? and id = ?', "deactivated", following.customer_1_id ) }.flatten!
    response = [] if response.nil?
    response
  end

  def users_followed
    response = self.followings.collect { |friend| Customer.where( 'role != ? and id = ?', "deactivated", friend.customer_2_id ) }.flatten!
    response = [] if response.nil?
    response
  end

  def follows?( friend )
    Following.where( 'customer_1_id = ? AND customer_2_id = ?', self.id, friend.id ).present?
  end

  def is_followed_by?( friend )
    Following.where( 'customer_1_id = ? AND customer_2_id = ?', friend.id, self.id ).present?
  end

  def request_connection( new_friend, send_notification = true )
    return false if( ( self == new_friend ) || ( self.connected_to?( new_friend ) ) || ( self.connection_request_sent?( new_friend ) ) )
    connection = CustomerConnection.create(
      customer_1_id: self.id,
      customer_2_id: new_friend.id
    )

    a               = Activity.new
    a.activity_type = "connection_request"
    a.actable       = self
    a.customer      = self
    a.save

    if ( new_friend.setting_selected?( "new_connection_request" ) ) && ( send_notification ) 
      n = Notification.new
      n.customer_id = new_friend.id
      n.activity_id = a.id
      n.save
    end

    self.follow( new_friend, false ) unless self.follows?( new_friend )

    return true
  end

  def notification_count
    self.notifications.where( read: false ).count
  end

  def all_notifications
    notifications = self.notifications.sort_by{ |n| n.created_at }.reverse

    return_array = {}
    today = Time.now.to_date
    notifications.each do |notification|
      days_ago = today - notification.created_at.to_date
      if return_array[days_ago].nil?
        return_array[days_ago] = [notification]
      else
        return_array[days_ago] << notification
      end 
    end 

    return_array
  end 

  def paginate_notifications(page_number)
    notifications = self.notifications.order(created_at: :desc).paginate(page: page_number, per_page: 12)

    return_array = {}
    today = Time.now.to_date
    notifications.each do |notification|
      days_ago = today - notification.created_at.to_date
      if return_array[days_ago].nil?
        return_array[days_ago] = [notification]
      else
        return_array[days_ago] << notification
      end 
    end 

    return_array
  end 

  def recent_notifications
    notifications = self.notifications.sort_by{ |n| n.created_at }.reverse[0..4]

    return_array = {}
    today = Time.now.to_date
    notifications.each do |notification|
      days_ago = today - notification.created_at.to_date
      if return_array[days_ago].nil?
        return_array[days_ago] = [notification]
      else
        return_array[days_ago] << notification
      end
    end

    return_array
  end

  def accept_connection( new_friend, send_notification = true )
    return false if( ( self == new_friend ) || ( self.connected_to?( new_friend ) ) )

    connection = CustomerConnection.create(
      customer_1_id: self.id,
      customer_2_id: new_friend.id,
      state: "accepted"
    )

    old_connection = CustomerConnection.where( customer_1_id: new_friend.id, customer_2_id: self.id, state: "pending" ).last
    old_connection.state = "accepted"
    old_connection.save

    a               = Activity.new
    a.activity_type = "connection_accepted"
    a.actable       = self
    a.customer      = self
    a.save

    if ( new_friend.setting_selected?( "accept_connection_request" ) ) && send_notification
      n = Notification.new
      n.customer_id = new_friend.id
      n.activity_id = a.id
      n.save
    end

    self.follow( new_friend, false ) unless self.follows?( new_friend )

    return true
  end

  def reject_connection( new_friend )
    connection = CustomerConnection.where( customer_1_id: new_friend.id, customer_2_id: self.id ).destroy_all
    return true
  end

  def cancel_connection( new_friend )
    connection = CustomerConnection.where( customer_1_id: self.id, customer_2_id: new_friend.id ).destroy_all
    return true
  end

  def disconnect( old_friend )
    connection1 = CustomerConnection.where( customer_1_id: self.id, customer_2_id: old_friend.id ).destroy_all
    connection2 = CustomerConnection.where( customer_1_id: old_friend.id, customer_2_id: self.id ).destroy_all

    return true
  end

  def connection_requests
    response = CustomerConnection.where( customer_2_id: self.id, state: "pending" ).collect { |connection| Customer.where( 'role != ? AND id = ?', "deactivated", connection.customer_1_id ) }.flatten!
    response = [] if response.nil?
    response
  end

  def connections
    response = CustomerConnection.where( customer_1_id: self.id, state: "accepted").collect { |connection| Customer.where( 'role != ? AND id = ?', "deactivated", connection.customer_2_id ) }.flatten!
    response = [] if response.nil?
    response
  end

  def connected_to?( friend )
    CustomerConnection.where( customer_1_id: self.id, customer_2_id: friend.id ).present? && CustomerConnection.where( customer_1_id: friend.id, customer_2_id: self.id ).present?
  end

  def connection_request_sent?( friend )
    CustomerConnection.where( customer_1_id: self.id, customer_2_id: friend.id ).present? && CustomerConnection.where( customer_1_id: friend.id, customer_2_id: self.id ).blank?
  end

  def block( friend )
    return false unless self.not_blocked?( friend )

    blocking = Blocking.create( customer_1_id: self.id, customer_2_id: friend.id )

    self.disconnect( friend ) if self.connected_to?( friend )
    self.unfollow( friend )   if self.follows?( friend )
    friend.unfollow( self )   if friend.follows?( self )
    self.reject_connection( friend ) if friend.connection_request_sent?( self )

    return true
  end

  def unfriend( friend )
    self.disconnect( friend )         if self.connected_to?( friend )
    self.unfollow( friend )           if self.follows?( friend )
    self.reject_connection( friend )  if friend.connection_request_sent?( self )
    return true
  end

  def mutual_count( friend )
    ( self.friends & friend.friends ).count
  end

  def unblock( friend )
    blocking = Blocking.where( customer_1_id: self.id, customer_2_id: friend.id ).first
    blocking.destroy

    return true
  end

  def all_blocked
    Blocking.where( customer_1_id: self.id ).collect{ |blocking| Customer.find blocking.customer_2_id }
  end

  def not_blocked?( friend )
    #Blocking.where( 'customer_1_id=? AND customer_2_id=?', friend.id, self.id ).blank? && Blocking.where( 'customer_1_id=? AND customer_2_id=?', self.id, friend.id).blank?
    Blocking.where( 'customer_1_id=? AND customer_2_id=?', self.id, friend.id ).blank? 
  end

  def gifts
    Gift.where( customer_id: self.id ).order( 'created_at desc' )
  end

  def all_feedback
    PurchaseFeedback.where( customer_id: self.id ).order( 'created_at desc' )
  end

  def valid_feedback
    PurchaseFeedback.where( customer_id: self.id, purchased: true ).order( 'created_at desc' )
  end

  def purchased_feedback?( actable_id )
    purchased_item = PurchaseFeedback.where( actable_id: actable_id, purchased: true ).order( 'created_at desc' )
    return purchased_item.present? ? true : false
  end

  def recent_gifts
    self.gifts.sort_by(&:updated_at).reverse[0..2]
  end

  def generate_username #( name = nil )
    if self.username.blank?
      $i = 0
      name = self.first_name + self.last_name #if name.nil?

      name = name.gsub(/[^0-9a-z ]/i, '').gsub(/\s+/, "")

      while ( !self.valid? && ( $i < 100 ) ) do
        self.username = name[0, 16] + $i.to_s
        $i += 1
      end
      
      return self.valid?
    else
      return true
    end
  end

  def shipping_address
    self.addresses.where(address_type: "shipping").first
  end

  def setting_selected_value( setting_id )
    self.customers_settings.where( setting_id: setting_id ).first.selected_option
  end

  def setting_selected?( setting_name )
    CustomersSetting.find_by( customer_id: self.id, setting_id: Setting.find_by( name: setting_name ).id ).selected_option
  end

  def generate_token
    self.auth_token = SecureRandom.hex
  end

  def generate_settings
    Setting.all.each do |setting|
      c = CustomersSetting.create(
        customer_id: self.id,
        setting_id: setting.id,
        selected_option: setting.default_value)
    end

    unless self.username == "giftibly"
      giftibly = Customer.find_by( username: "giftibly" )
      if giftibly.present?
        self.request_connection( giftibly, false )
        giftibly.accept_connection( self, false )
        self.unfollow( giftibly )
      end
    end

    basket          = Basket.new
    basket.name     = "Liked Products"
    basket.customer = self
    basket.privacy  = "private"

    basket.save
  end

  def connected?
    self.facebook_connected? || self.google_connected? || self.twitter_connected? || self.linkedin_connected?
  end

  def facebook_connected?
    self.oauth_credentials.where( provider: "Facebook" ).present?
  end

  def google_connected?
    self.oauth_credentials.where( provider: ["Google_oauth2", "Google+"] ).present?
  end

  def twitter_connected?
    self.oauth_credentials.where( provider: "Twitter" ).present?
  end

  def linkedin_connected?
    self.oauth_credentials.where( provider: "Linkedin" ).present?
  end

  def self.current_month_birthdays
    current_bd = []

    Customer.all.each do |customer|
      current_bd << customer if customer.birthday.present? && customer.birthday.month == Time.now.month
    end

    current_bd
  end

  def current_cart
    self.carts.current.first
  end

  def default_shipping_address
    self.addresses.where(address_type: 0, default: true).first
  end

  def default_payment_method
    self.payment_methods.where(default: true).first
  end

  def notify_admin
    CustomerMailer.new_customer( self ).deliver if Rails.env.production?
    SubscribeToMailingList.subscribe( self ) if Rails.env.production?
  end

  def number_unread_messages
    unread = 0
    Conversation.involving(self).each do |c|
      c.messages.each do |m|
        if ( m.customer_id != self.id ) && ( m.read == false )
          unread += 1
          break
        end
      end
    end

    return unread
  end

end
