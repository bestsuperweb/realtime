# == Schema Information
#
# Table name: activities
#
#  id            :integer          not null, primary key
#  activity_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  actable_id    :integer
#  actable_type  :string
#  customer_id   :integer
#

class Activity < ApplicationRecord
  belongs_to :actable, polymorphic: true
  belongs_to :customer, required: false
  has_many :notifications, dependent: :destroy

  validates_inclusion_of :activity_type, in: %w( inactive_variant connection_request connection_accepted followed comment share event_invitation 
                                                 event_response_yes like event_response_no event_response_maybe purchase_gift_customer 
                                                 purchase_gift_guest shared_thing )

  # TODO: once we have inactive variant searches, work this
  #def self.new_inactive_variant_activity( variant )
  #  a = Activity.new
  #  a.activity_type = "inactive_variant"
  #  a.actable = variant
  #
  #  a.save
  #
  #  variant.gifts.each do |gift|
  #    # TODO: put a conditional around here based off of customer setting
  #    n = Notification.new
  #    n.customer_id = gift.basket.customer.id
  #    n.activity_id = a.id
  #
  #    n.save
  #  end
  #
  #end

end
