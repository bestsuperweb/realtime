# == Schema Information
#
# Table name: messages
#
#  id              :integer          not null, primary key
#  media           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  body            :text
#  conversation_id :integer
#  customer_id     :integer
#  read            :boolean          default(FALSE)
#  prev_message_id :integer
#  next_message_id :integer
#

class Message < ApplicationRecord
  belongs_to :conversation
  belongs_to :customer
  validates_presence_of :conversation_id, :customer_id

  mount_uploader :media, MediaUploader
    
  def previous_message
    return self.prev_message_id.nil? ? nil : Message.find( self.prev_message_id )
  end
    
  def next_message
    return self.next_message_id.nil? ? nil : Message.find( self.next_message_id )
  end

end
