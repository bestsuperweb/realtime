# == Schema Information
#
# Table name: api_reports
#
#  id                 :integer          not null, primary key
#  error_type         :string
#  customer_id        :integer
#  url                :string
#  note               :string
#  created_at         :datetime
#  updated_at         :datetime
#  resolved_at        :datetime
#  admin_id           :integer
#  customer_confirmed :boolean          default(FALSE)
#  data               :text
#

class ApiReport < ApplicationRecord
  belongs_to :customer, required: false
  belongs_to :admin, required: false

  validates_inclusion_of :error_type, in: %w( reported_issue unknown_merchant not_product_page validation )

  store :data, coder: JSON

  def customer_confirmed!
    self.customer_confirmed = true
    self.save!
  end
end
