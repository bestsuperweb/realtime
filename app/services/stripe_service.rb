# frozen_string_literal: true

# This service provides a central API to interact with Stripe.
require "stripe"

class StripeService

  Stripe.api_key = ENV['STRIPE_SECRET_KEY']
  Stripe.max_network_retries = 2
  Stripe.open_timeout = 30
  Stripe.read_timeout = 80

  if Rails.env.production?
    Stripe.log_level = 'info'
  else
    Stripe.log_level = 'debug'
  end

  def self.build
    new
  end

  def create_customer(owner, payment_method)
    stripe_customer =   Stripe::Customer.create({
                          email: owner.email
                        })
    owner.update!(stripe_id: stripe_customer.id)
    stripe_customer
  end

  def attach_payment_method_to(owner, payment_method)
    stripe_customer = find_or_create_stripe_customer(owner, payment_method)
    
    stripe_customer.sources.create({:source => payment_method.stripe_id})
    set_as_default(payment_method, stripe_customer) if payment_method.default?
  end

  def authorize(owner, payment_method, invoice, payment)
    return error_obj("Invoice already paid") if invoice.completed?
    authorization =  Stripe::Charge.create({
                      amount: invoice.authorizable_amount_in_cents,
                      customer: owner.stripe_id,
                      source: payment_method.stripe_id,
                      currency: invoice.currency_code.downcase,
                      description: "Giftibly #{invoice.uuid}",
                      capture: false,
                      metadata: {
                        invoice_uuid: invoice.uuid,
                        payment_uuid: payment.uuid,
                        payment_method_uuid: payment_method.uuid,
                        owner_id: owner.id,
                        owner_type: owner.class.name
                      },
                      receipt_email: owner.email,
                      statement_descriptor: "Giftibly purchase"
                    })
  rescue StandardError => e
    puts e.message
  ensure
    authorization
  end

  def capture(payment, amount)
    charge = Stripe::Charge.retrieve(payment.stripe_id)
    charged_amount = check_authorized_ceiling(payment, amount)

    charge.capture(amount: (charged_amount*100).to_i) #in cents
  rescue StandardError => e 
    puts e.message
  end

  private

  def check_authorized_ceiling(payment, amount)
    charged_amount = amount > payment.authorized_amount ? payment.authorized_amount : amount
    charged_amount
  end


  def error_obj(message)
    p message
    OpenStruct.new(
      type: :paid,
      error: message
    )
  end

  def find_or_create_stripe_customer(owner, payment_method)
    if owner.stripe_id.blank?
      create_customer(owner, payment_method)
    else
      Stripe::Customer.retrieve(owner.stripe_id)
    end
  end

  def set_as_default(payment_method, stripe_customer)
    stripe_customer.default_source = payment_method.stripe_id
    stripe_customer.save
  end

end
