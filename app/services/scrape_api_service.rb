# frozen_string_literal: true

# 1. This service is meant to provide an API to fetch 
#    products from a single URL.

# 2. The service is structured in a modular way, with one central service called as follows:

      # ScrapeApiService.build.call(url)

# 3. Response codes
      # 200 => success
      # 400 => url is invalid
      # 404 => url is valid but site cannot be found
      # 412 => url is valid and site is implemented and found but the response is unexpected (something changed)
      # 501 => url is valid but site is not implemented
      # 500 => catchall error code

# How to manually test pages in irb:
  # url = "www.giftibly.com"
  
  ## to get the data object
  # data = ScrapeApiService.build.call(url)

  ## to get the html
  # provider = ScrapeApiService.build.providers[URI.parse(url).host]
  # page = ScrapeApiService.build.html(url)

    ### if a straight css selector
    # page.at(provider['image_tag']).text

    ### if a ruby command
    # eval(provider['image_tag'])

# How to audit the commands in providers.yml
  # 1. for all providers
  #   results = ScrapeApiService.build.audit
  # 2. for a provider with url
  #   results = ScrapeApiService.build.audit(url: 'http://www...')
  
  ## returns a Hash object with hosts and invalid keys.

# IMPORTANT: After adding each merchant run the following command:
  
  # url = ''
  # ScrapeApiService.build.audit(url: url)  => to verify syntax. No errors returns an empty Hash
  # ScrapeApiService.build.call(url)        => to verify that data is fetched

class ScrapeApiService

  def self.build
    new(ScrapeDataService.build)
  end

  attr_accessor :providers

  def initialize(scrape_data_service)
    @scrape_data_service = scrape_data_service
    @giftybot_service = PurchaseGatewayService::GiftyBot
    @providers = get_providers_from_yml
  end

  def call(url, options={})
    return Struct.new(:data, :status, :error_report).new(nil, 400, nil) unless url.present? || options[:html].present?

    uri = URI.parse(URI.extract(url).first)
    default_provider = get_default
    provider = is_valid?(uri) ? get_provider(uri) : nil
    error_report = nil

    page = provider.present? ? get_page(uri, provider, options[:html]) : nil
    data = page.present? ? get_data_from(page, provider, default_provider, uri) : nil

    status =  get_status(
                is_valid?(uri),
                provider.present?, 
                page.present?,
                is_successful?(data)
              )

    # generate error report unless success or invalid url
    unless status == 200 || status == 400
      error_report = generate_error_report(url, status, data, options[:customer])
    end

    result = Struct.new(:data, :status, :error_report).new(data, status, error_report)

    return result
  end

  def html(url)
    #uri = URI.parse(url)
    uri = URI.parse(URI.extract(url).first)
    provider = is_valid?(uri) ? get_provider(uri) : nil
    page = get_page(url, provider, nil)
    return page
  end

  def audit(options={})
    url = options[:url]
    provider = url.present? ? @providers[URI.parse(url).host] : nil

    page = provider.present? ? get_page(url, provider, nil) : Nokogiri::HTML::Document.new
    hash = Hash.new

    if provider.present?
      host = URI.parse(url).host
      hash[host] = get_provider_results(page, host, provider) 
    else
      @providers.each do |host, values|
        provider_hash = get_provider_results(page, host, values) 
        hash[host] = provider_hash unless provider_hash.empty?
      end
    end

    hash
  end

  private

  def get_iframe_content(giftybot, iframe_name)
    giftybot.within_frame(iframe_name) do 
      return giftybot.html 
    end
  rescue
    nil
  end

  def get_provider_results(page, host, values)
    hash = Hash.new

    values.keys.each do |k|
      unless k == 'merchant_name'
        command = values[k]

        result = test_code(page, command)
        
        if result.class == String
          hash[k] = result
        end
      end
    end
    hash
  end

  def test_code(page, value)
    result = value.include?("page.") ? eval(value) : page.at(value).try(:text)
    result.nil? ? true : false
  rescue StandardError, SyntaxError => e
    e.message.include?("nil:NilClass") ? true : e.message
  end

  def is_successful?(data)
    return false unless data.present?

    required_data_keys.each do |key|
      next if data[key].present?
      return false
    end

    return true
  end

  def required_data_keys
    [
      :merchant_name, :price, :image, :item_name
    ]
  end

  def get_providers_from_yml
    file_path = File.join(Rails.root, 'app', 'services', 'scrape_api_service', 'providers.yml')
    hash = YAML.load_file(file_path)
    return hash
  end

  def is_valid?(uri)
    !uri.host.nil?
  end

  def get_provider(uri)
    @providers[uri.host]
  end

  def get_default
    @providers["default"]
  end

  def get_page(url, provider, html)
    page =  if html.present?
              page = Nokogiri::HTML(html)
            elsif url.present?
              giftybot = @giftybot_service.build
              giftybot.visit url
              sleep(2)
              html_document = provider['iframe'].present? ? get_iframe_content(giftybot, provider['iframe']) : giftybot.html
              page = Nokogiri::HTML(html_document)
            end
    return page
  rescue StandardError => e
    p e.message
    return nil
  ensure
    giftybot.stop if giftybot.present?
  end

  def get_data_from(page, provider, default_provider, uri)
    @scrape_data_service.call(page, provider, default_provider, uri)  
  end

  def get_status(url_valid, implemented, found, success)
    if success
      200
    elsif !url_valid
      400
    elsif url_valid && implemented && !found
      404
    elsif url_valid && !implemented
      412
    else
      501
    end
  end

  def generate_error_report(url, status, data, customer)
    report = ApiReport.new(
              error_type: error_type(status),
              url: url,
              data: data.try(:as_json),
              customer_id: customer.try(:id)
            )
    report.save!
    report
  end

  def error_type(status)
    case status
    when 404
      "unknown_merchant"
    when 412
      "unknown_merchant"
    when 501
      "not_product_page"
    end
  end

end
