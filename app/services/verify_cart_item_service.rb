# frozen_string_literal: true

# use this service to verify cart item's quantity and price before checkout.
# only handles amazon for the time being

class VerifyCartItemService
  include AmazonPaapiHelpers

  def self.build
    new
  end

  def call(giftbox)
    merchant_carts = MerchantCart.includes(cart_items: [:cart, :variant]).where(giftbox_id: giftbox.id)

    merchant_carts.each do |merchant_cart|
      cart_items = merchant_cart.cart_items
      cart_items.each do |cart_item|
        cart_item = verify(cart_item)
        cart_item = recalculate(cart_item)
      end   
      CartItem.transaction do
        cart_items.map(&:save) 
      end    
    end
    return true
  end

  private

  def verify(cart_item)
    request = initialize_amazon_paapi_request
    response = request.item_lookup(
      query: {
        'ItemId' => cart_item.variant.api_id,
        'ResponseGroup' => "Offers"
      }
    ).to_h

    cart_item.verified_at = Time.now.utc
    cart_item.current_offer_id = response.dig("ItemLookupResponse", "Items", "Item", "Offers", "Offer", "OfferListing", "OfferListingId")
    cart_item.current_available_quantity = response.dig("ItemLookupResponse", "Items", "Item", "OfferSummary", "TotalNew").to_i
    cart_item.current_price = (response.dig("ItemLookupResponse", "Items", "Item", "Offers", "Offer", "OfferListing", "Price", "Amount").to_f/100).to_s
    cart_item.current_sale_price = (response.dig("ItemLookupResponse", "Items", "Item", "Offers", "Offer", "OfferListing", "SalePrice", "Amount").to_f/100).to_s

    cart_item
  end

  def recalculate(cart_item)
    cart_item.quantity = cart_item.quantity >= cart_item.current_available_quantity ? cart_item.current_available_quantity : cart_item.quantity
    cart_item.calculate_subtotal
    cart_item
  end

end