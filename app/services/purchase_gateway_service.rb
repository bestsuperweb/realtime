# frozen_string_literal: true
require 'gmail' # For email verification codes
require "selenium-webdriver" # bring in headless chrome

class PurchaseGatewayService

  def self.build
    new(Amazon.build, Test.build)
  end

  def initialize(amazon, test)
    @amazon = amazon
    @test = test
  end

  def call(provider, options={})
    response = 
      case provider.to_s.downcase
      when 'amazon'
        check_amazon_arguments(options[:url], options[:shipping_address], options[:shipping_option])
        @amazon.call(options[:url], options[:shipping_address], options[:shipping_option])
      when 'test'
        @test.call
      else
        raise 'Provider does not exist.'
      end

    wrap(response)
  end

  private

  def check_amazon_arguments(url, shipping_address, shipping_option)
    unless url.present? && url.is_a?(String)
      raise 'Needs Amazon Paapi remotecart purchaseUrl as options[:url]'
    end

    unless shipping_address.present? && shipping_address.is_a?(Address)
      raise 'Needs shipping address as Address in options[:shipping_address]'
    end

    unless shipping_option.present? && shipping_option.is_a?(String)
      raise 'Needs shipping option as String in options[:shipping_option]'
    end
  end

  def wrap(response)
    response.success = true unless response.errors.any?
    response
  end

end