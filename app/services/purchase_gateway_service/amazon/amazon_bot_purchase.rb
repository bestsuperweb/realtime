# frozen_string_literal: true
module PurchaseGatewayService::Amazon::AmazonBotPurchase
  include PurchaseGatewayService::Amazon::AmazonBotSettings

  def clear_previous_items_from_cart(bot)
    if have_items_in_cart?(bot) 
      go_to_cart(bot)
      bot = clear_cart(bot)
    end
    bot
  end

  def go_to_cart(bot)
    bot.find("a#nav-cart").click
    bot
  end

  def have_items_in_cart?(bot)
    count = bot.find("span#nav-cart-count").text.to_i
    !count.zero?
  end

  def clear_cart(bot)
    item_delete_selector = "form#activeCartViewForm span.sc-action-delete input"
    unless bot.has_no_selector? item_delete_selector
      el = bot.first(item_delete_selector)
      el.click if el.present?

      sleep(5)
      clear_cart(bot) if have_items_in_cart?(bot)
    end

    bot
  end

  def merge_cart(bot)
    bot.find("input[value='add']").click
    bot
  end

  def proceed_to_checkout(bot)
    bot.find("input[name='proceedToCheckout']").click
    bot
  end

  def enter_shipping_address(bot, shipping_address)
    bot.find("input#enterAddressFullName").click.set(shipping_address.name)
    bot.find("input#enterAddressAddressLine1").click.set(shipping_address.address_1)
    
    if shipping_address.address_2.present?
      bot.find("input#enterAddressAddressLine2").click.set(shipping_address.address_2)
    end

    bot.find("input#enterAddressCity").click.set(shipping_address.city)
    bot.find("input#enterAddressStateOrRegion").click.set(shipping_address.state)
    bot.find("input#enterAddressPostalCode").click.set(shipping_address.postal_code)
    bot.select 'United States', from: "enterAddressCountryCode"
    bot.find("input#enterAddressPhoneNumber").click.set(shipping_address.phone_number)
    
    #billing address is not the same as shipping address
    billing_selector = "input[name='isBillingAddress'][value='0']"
    if !bot.has_no_selector?(billing_selector)
      bot.find(billing_selector).click
    end

    bot.find("input[name='shipToThisAddress']").click
    bot
  end

  def verify_address(bot)
    bot.find("input#addr_0").click
    bot.find("input[name='useSelectedAddress']").click
    bot
  end

  #leave shipping options at default
  def choose_shipping_options(bot, shipping_option)
    if shipping_option.casecmp("fast").zero?
      options = bot.all(:css, '.shipping_option')
      options.last.click
      p "Selected fastest shipping option" 
      bot.rest
    end

    bot.first("input[type='submit']").click 
    bot
  end

  def choose_payment_method(bot)
    bot.find("input#pm_0").click

    sleep(2)
    #Amazon requires reentry of card number for new shipping addresses    
    if bot.has_text?("Please re-enter your card number.")
      bot.find("input#addCreditCardNumber").click.send_keys(AMZN_CREDIT_CARD_NUMBER)
      bot.find("input#confirm-card").click
    end

    bot.log
    bot.find("input#continue-top").click
    bot
  end

  # Amazon occasionally pops an interstitial for Prime. Just click no thanks.
  def why_pay_for_shipping(bot)
    bot.find("a#prime-pip-updp-decline").click
    bot
  end

  #just leave at default billing address
  def choose_billing_address(bot)
    bot.first("a.checkout-continue-link").click
    bot
  end

  def place_order(bot)
    bot = grab_actual_amounts(bot)
    if ENABLE_PURCHASES
      bot.find("input[name='placeYourOrder1']").click 
    else
      bot.visit(bot.signed_in_url)
    end
    bot
  end

  def grab_actual_amounts(bot)
    bot.see_hidden!

    div = bot.find("div#subtotals-marketplace-table")
    array = div.all(:css, 'td.aok-nowrap').map {|e| e.text.gsub(/[^0-9,\.]/, '').to_f }

    bot.actual_subtotal = array[0]
    bot.actual_shipping_fee = array[1]
    bot.actual_taxes = array[3]

    bot.unsee_hidden!
    
    bot
  end

end