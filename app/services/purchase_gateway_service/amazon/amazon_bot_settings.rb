# frozen_string_literal: true
module PurchaseGatewayService::Amazon::AmazonBotSettings

  VENDOR = Rails.configuration.x.purchase_gateway.amazon_vendor_name
  
  # REQUIRED ENV INFO
  AMZN_CREDIT_CARD_NUMBER = ENV["AMZN_CREDIT_CARD_NUMBER"]
  AMZN_USERNAME = ENV['AMZN_USERNAME']
  AMZN_PASSWORD = ENV['AMZN_PASSWORD']
  GMAIL_USERNAME = ENV['GMAIL_USERNAME']
  GMAIL_PASSWORD = ENV['GMAIL_PASSWORD']
  DEATHBYCAPTCHA_USERNAME = ENV['DEATHBYCAPTCHA_USERNAME']
  DEATHBYCAPTCHA_PASSWORD = ENV['DEATHBYCAPTCHA_PASSWORD']

  # Only sess-at-main, at-main, x-main required for a valid session. Others included for safety's sake.
  SESSION_COOKIES_LIST = ['lc-main', 'sess-at-main', 'at-main', 'x-main', 'a-ogbcbff']
  SESSION_COOKIES_FILEPATH =  Rails.configuration.x.purchase_gateway.amazon_session_cookies_filepath

  # Config settings in config/initializers/giftybot.rb
  CAPYBARA_WAIT_IN_SECONDS = Rails.configuration.x.purchase_gateway.capybara_wait_in_seconds
  ENABLE_PURCHASES = Rails.configuration.x.purchase_gateway.enable_purchases

  SIGN_IN_URL = 'https://www.amazon.com'
  SIGNED_IN_URL = 'https://www.amazon.com'
  SIGNED_IN_TEXT = Rails.configuration.x.purchase_gateway.amazon_signed_in_text

  # Pages in order
  # TODO consider fallback text match search in case page titles change.
  PURCHASE_PAGES = {
    'Amazon.com: Please Confirm Your Action': 'merge_cart',
    'Amazon.com Shopping Cart': 'proceed_to_checkout',
    'Select a shipping address': 'enter_shipping_address',
    'Select Shipping Options - Amazon.com Checkout': 'choose_shipping_options',
    'Select a Payment Method - Amazon.com Checkout': 'choose_payment_method',
    'Place Your Order - Amazon.com Checkout': 'place_order'
  }

  UNCOMMON_PAGES = {
    'Amazon Sign In': 'sign_in',
    'Please confirm your identity': 'confirm_identity',
    'Robot Check': 'robot_check',
    'Verify Address - Amazon.com Checkout': 'verify_address',
    'Select a Billing Address - Amazon.com Checkout': 'choose_billing_address',
    'Get UNLIMITED two-day shipping for a year FREE once you join Amazon Prime': 'why_pay_for_shipping',
    'Amazon Password Assistance': 'password_assistance',
    'Amazon Sign In Options': 'sign_in_options'
  }

  PAGES = PURCHASE_PAGES.merge(UNCOMMON_PAGES)

end