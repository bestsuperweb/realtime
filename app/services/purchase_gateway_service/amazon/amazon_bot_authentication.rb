# frozen_string_literal: true
module PurchaseGatewayService::Amazon::AmazonBotAuthentication
  include PurchaseGatewayService::Amazon::AmazonBotSettings

  def sign_in(bot, options={})
    captcha_selector = "input#auth-captcha-guess"
    email_selector = "input#ap_email"
    password_selector = "input#ap_password"

    if options[:click_sign_in]
      bot.visit SIGN_IN_URL
      bot.find("a#nav-link-accountList").click
    end

    bot.rest

    if need_captcha?(bot, captcha_selector)
      raise "captcha triggered"
      # bot = sign_in_with_captcha(bot)
    elsif separate_sign_in?(bot, password_selector)
      bot = sign_in_separately(bot, email_selector, password_selector)
    else
      bot = sign_in_normally(bot, email_selector, password_selector)
    end

    bot.save_cookies
    bot
  end

  def need_captcha?(bot, captcha_selector)
    !bot.has_no_selector?(captcha_selector)
  end

  def separate_sign_in?(bot, password_selector)
    bot.has_no_selector?(password_selector)
  end

  def sign_in_normally(bot, email_selector, password_selector)
    bot.find(email_selector).click.set AMZN_USERNAME
    sleep(2)
    bot.find(password_selector).click.set AMZN_PASSWORD
    sleep(1)
    remember = bot.find("input[name='rememberMe']")
    remember.click unless remember.checked? 
    bot.log
    bot.find('input#signInSubmit').click
    bot
  end

  def sign_in_separately(bot, email_selector, password_selector)
    bot.find(email_selector).click.set AMZN_USERNAME
    bot.log
    bot.find("input#continue").click

    bot.rest

    bot.find(password_selector).click.set AMZN_PASSWORD
    bot.log
    bot.find('input#signInSubmit').click

    bot
  end

  def sign_in_with_captcha(bot)
    img_url = bot.find("img#auth-captcha-image")['src']
    captcha_text = solve_captcha(img_url)
    bot.find("input#ap_password").click.send_keys AMZN_PASSWORD
    bot.find("input#auth-captcha-guess").click.send_keys captcha_text
    bot.log
    bot.find('input#signInSubmit').click
    bot
  end

  def robot_check(bot)
    raise "robot check triggered"
    # img_url = bot.find("form img")['src']
    # captcha_text = solve_captcha(img_url)
    # bot.find("input#captchacharacters").click.send_keys captcha_text
    # bot.log(message: "Robot check triggered")
    # bot.find("form button[type='submit']").click
    # bot
  end

  # TODO add deathbycaptcha
  def solve_captcha(img_url)
    raise unless DEATHBYCAPTCHA_USERNAME.present? && DEATHBYCATCHA_PASSWORD.present? 
    
    client = DeathByCaptcha.new(DEATHBYCAPTCHA_USERNAME, DEATHBYCATCHA_PASSWORD, :http)
    captcha = client.decode!(url: img_url)
    captcha.text

    # Use this block if planning to save img to file
    # filepath = "#{Rails.root}/public/phantomjs/captcha.jpg"
    # image_string = bot.driver.render_base64(:png, selector: "img#auth-captcha-image")
  end

  def password_assistance(bot)
    raise "password assistance triggered"
    # bot.find("input#ap_email").click.set AMZN_USERNAME
    # bot.log(message: "Password reset form triggered")
    # bot.find("input#continue").click
    # bot
  end

  def confirm_identity(bot)
    raise "confirm identity triggered"
    # if bot.find("img[alt='captcha']").present?
    #   bot = confirm_identity_from_captcha(bot)
    # elsif bot.find("input[name='code']").present?
    #   bot = verify_code_from_email(bot)
    # end
    # bot
  end

  def confirm_identity_from_captcha(bot)
    img_url = bot.find("img[alt='captcha']")['src']
    captcha_text = solve_captcha(img_url)
    bot.find("input[name='cvf_captcha_input']").click.set captcha_text
    bot.log(message: "Confirm identity captcha triggered")
    bot.find("input[type='submit']").click
    bot
  end

  def verify_code_from_email(bot)
    verification_code = get_verification_code_from_email
    bot.find("input[name='code']").click.set verification_code
    bot.log(message: 'verification_code triggered', save_page: true)
    bot.find("input[type='submit']").click
    bot
  end

  # https://github.com/gmailgem/gmail
  # https://github.com/mikel/mail
  def get_verification_code_from_email
    code = nil

    Gmail.connect(GMAIL_USERNAME, GMAIL_PASSWORD) do |gmail|
      if gmail.logged_in?
        email = gmail.inbox.emails(gm: '"your amazon verification code"').last
        message = Nokogiri::HTML(email.body.to_s)
        code = message.css("p.otp").text
      end
    end
    code
  end

  def sign_in_options(bot)
    raise "sign in options triggered"
    # bot.find("input[type='radio'][value='skip']").click
    # bot.log(message: 'sign_in_options triggered', save_page: true)
    # bot.find("input[type='submit']").click
    # bot
  end

end