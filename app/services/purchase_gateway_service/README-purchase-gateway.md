# The Purchase Gateway service 
By Ryan chew: ryachew.dev[at]gmail.com.

The Service is used to trigger purchases progammatically on Giftibly. It's structured as a self-contained service nestled within a monolithic Rails app with component services for each vendor and an automated bot called GiftyBot.

Each vendor may require a different approach to triggering a purchase. Some vendors may have a documented API while some will require the use of GiftyBot. This approach allows for scalability and flexibility as we add more vendors while maintaining clean, modular code that can be easily tested, swapped, or updated without affecting the rest of the app or other components

The Service calls the component service which triggers the purchase on the vendor's site, then collects the data returned and packages it as a standard ruby object for the service caller to consume.

## ENV Variables required

```ruby
AMZN_USERNAME: String
AMZN_PASSWORD: String
AMZN_CREDIT_CARD_NUMBER: String
GMAIL_USERNAME: String
GMAIL_PASSWORD: String
DEATHBYCAPTCHA_USERNAME: String
DEATHBYCAPTCHA_PASSWORD: String
```

## File structure

```ruby
app/services/
  purchase_gateway_service.rb
    /purchase_gateway_service/
    
      # gifty_bot
      gifty_bot.rb
      /gifty_bot/
        gifty_bot_settings.rb
        gifty_bot_driver.rb
        ....
      ################
        
      # vendor component service
      amazon.rb
      /amazon/
        amazon_bot_settings.rb
        amazon_bot_authentication.rb
        ...
      #################
            
```

## Installation

`$ bundle install`
#### Chrome

GiftyBot requires chrome and selenium's chromedriver to be installed.

Resources for deployment to AWS:

[Blog article](http://itsallabtamil.blogspot.my/2013/02/setting-up-chrome-firefox-ec2-selenium-java.html)

[Medium article](https://medium.com/@davidkadlec/installing-google-chrome-on-amazon-linux-ec2-d1cb6aa37f28)

[AWS forum thread](https://forums.aws.amazon.com/thread.jspa?threadID=229187)

#### Settings
```ruby
$ rake notes
```
to show a list of settings to be changed in actual production

#### Automated tests
```ruby
Rails 5
$ bin/rails test test/services/purchase_gateway_service/
$ bin/rails test test/services/purchase_gateway_service_test.rb

Rails 4
$ bundle exec rake TEST=test/services/purchase_gateway_service.rb
$ bundle exec rake TEST=test/services/purchase_gateway_service/**/*.rb
```

## Usage
The Service can be called from anywhere with a one-liner:

```ruby
obj = PurchaseGatewayService.build.call(provider, url: PAAPI_purchaseUrl, shipping_address: Address, shipping_option: shipping_option)
```
The arguments are: 
-  ```provider``` - A string calling the vendor component service that MUST match the filename of the component service. ie. 'amazon'
- ```options``` - optional arguments for specific vendors. Refer to the vendor section for a detailed list of arguments. An ArgumentError will be raised if the vendor specific arguments are not provided.


The service will return an object with the following methods
```
obj.success?
obj.errors == [
  {
    filepath: String of filepath of saved files,
    filename: String of filename of saved files,
    log_message: String of specific log message printed to log,
    page: String of html page title,
    message: String of user friendly messages for Flash notices etc.
  }
]
```

## Vendor component services

### Amazon
Amazon requires GiftyBot.

The Amazon service requires 2 arguments: 

- ```url``` - PAAPI_purchaseUrl obtained from an [Amazon remote cart](http://docs.aws.amazon.com/AWSECommerceService/latest/DG/CHAP_PurchasingtheItemsinaRemoteShoppingCart_PurchaseURL.html).
-  ```shipping_address``` - Address with the following mapping:
```ruby
{
  name: "customer's name",
    address_1: "",
    address_2: "",
    city: "",
    state: "",
    postal_code: "",
    country_code: "", #iso-code ie: "US",
    phone_number: "" 
}
```

When calling the gateway service for amazon, use this line:
```
obj = PurchaseGatewayService.build.call('amazon', url: PAAPI_purchaseUrl, shipping_address: shipping_address)
```

## Production considerations
- Run bot through a proxy
- Use AWS elastic IPs
- Run bot through a background queue



