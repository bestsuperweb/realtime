# frozen_string_literal: true

# copyright: Ryan Cheaw 2017
# email: ryachew.dev[at]gmail.com
# For the exclusive use of Giftibly

class PurchaseGatewayService::GiftyBot < Capybara::Session
  include GiftyBotSettings
  include GiftyBotDriver
  include GiftyBotLogging
  include GiftyBotCookieManager

  attr_accessor :vendor,
                :signed_in_url, 
                :signed_in_text,
                :session_cookies_filepath, 
                :session_cookies_list, 
                :pages, # bot loads a hash of pages it needs to process
                :index, # used for ordering logs, screenshots, and saved html files
                :success, 
                :errors,
                # used for grabbing values off merchant site to charge customer cards.
                :actual_subtotal,
                :actual_shipping_fee,
                :actual_taxes,
                :run_success

  alias :success? :success
  alias :run_success? :run_success
  
  def self.build
    new(:gifty_bot_driver)
  end

  def initialize(driver)
    super
    @success = false
    @errors = []
    @index = 0 #sets index at 0, and subsequent actions will increment the index
  end

  ### LIFECYCLE ###
  def start(
    vendor,
    signed_in_url, 
    signed_in_text, 
    session_cookies_filepath, 
    session_cookies_list, 
    pages
    )

    self.signed_in_url = signed_in_url.to_s
    self.signed_in_text = signed_in_text.to_s

    # Used to save cookies in a json file
    self.session_cookies_filepath = session_cookies_filepath.to_s
    self.session_cookies_list = session_cookies_list.to_s

    # Used to store the page flow of each vendor. Undefined pages will throw an error.
    self.pages = pages
      
    # Used for logging
    self.vendor = vendor.to_s

    log(message: 'GiftyBot initializing...')

    # Spoof an actual browser
    simulate_human_user!

    visit REFERRER_SITE_TO_SET_REQUEST_REFERRER
    
    log(message: 'GiftyBot initialized')

    self
  end

  def setup_for_page
    rest
    log
    self.index += 1
  end

  def rest
    sleep(rand(
      MIN_PAUSE_BETWEEN_PAGES_IN_SECONDS..MIN_PAUSE_BETWEEN_PAGES_IN_SECONDS*2
    ))
  end

  def stop
    driver.quit
  end

  # Helpers
  def simulate_human_user!
    driver.execute_script("
      Object.defineProperty(navigator, 'languages', {
        get: function() {
          return ['en-US', 'en'];
        },
      });

      Object.defineProperty(navigator, 'plugins', {
        get: function() {
          return [1, 2, 3, 4, 5];
        },
      });"
    )
  end

  # use negative test to speed up capybara
  def not_signed_in?(options={})
    return true unless File.exists?(session_cookies_filepath)
    visit signed_in_url
    self.load_cookies
    visit signed_in_url
    
    result =  if options[:selector]
                has_no_selector? signed_in_text
              else
                has_no_text? signed_in_text
              end

    self.log(message: "not_signed_id: #{result}")

    return result
  end

  def see_hidden!
    Capybara.ignore_hidden_elements = false
  end

  def unsee_hidden!
    Capybara.ignore_hidden_elements = true
  end

  def page_undefined?
    !pages.key?(title.to_sym)
  end

end