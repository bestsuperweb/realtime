# frozen_string_literal: true

class PurchaseGatewayService::Amazon
  include AmazonBotSettings
  include AmazonBotAuthentication
  include AmazonBotPurchase

  def self.build
    new(PurchaseGatewayService::GiftyBot.build)
  end

  def initialize(giftybot)
    @bot = giftybot
    @purchase_pages = PURCHASE_PAGES.keys  
  end

  def call(purchase_url, shipping_address, shipping_option)

    #setup
    bot = @bot.start(
            VENDOR,
            SIGNED_IN_URL, 
            SIGNED_IN_TEXT, 
            SESSION_COOKIES_FILEPATH, 
            SESSION_COOKIES_LIST,
            PAGES
          )

    if bot.not_signed_in?
      bot = sign_in(bot, click_sign_in: true)
    end
    
    bot = clear_previous_items_from_cart(bot)
    
    bot.visit purchase_url

    # run
    bot = run_bot(bot, @purchase_pages, shipping_address, shipping_option)

  rescue StandardError => e
    bot.log(error: e)
  ensure
    bot.cleanup_cookies
    bot.stop 
    return bot
  end

  private

  def run_bot(bot, remaining_pages, shipping_address, shipping_option)
    pages_ran = {}

    Capybara.using_wait_time CAPYBARA_WAIT_IN_SECONDS do
      remaining_pages.each_with_index do |key, index|
        bot.setup_for_page
        
        title = bot.title.to_sym
        raise "page_undefined" if bot.page_undefined?
        raise "page_stuck" if pages_ran[title]

        method_name = PAGES[title]

        if method_name == "enter_shipping_address"
          bot = self.send(method_name, bot, shipping_address)
        elsif method_name == "choose_shipping_options"
          bot = self.send(method_name, bot, shipping_option)
        else
          bot = self.send(method_name, bot)
        end

        case method_name
        when "place_order"
          bot.run_success = true
          remaining_pages = []
        else
          remaining_pages = remaining_pages - [title]
        end

        pages_ran[title] = true
      end
    end

    if remaining_pages.any? && !bot.errors.any? && !bot.run_success?
      bot = run_bot(bot, remaining_pages, shipping_address)
    end
  
    bot.log(message: "#{self.class.to_s} run success", save_page: Rails.env.development?)
    return bot
  end

end
