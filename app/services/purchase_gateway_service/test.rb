# frozen_string_literal: true

class PurchaseGatewayService::Test
  include TestBotSettings
  include TestBotAuthentication
  include TestBotPurchase

  def self.build
    new(PurchaseGatewayService::GiftyBot.build)
  end

  def initialize(gifty_bot)
    @bot = gifty_bot
    @pages = PURCHASE_PAGES.keys
  end

  def call
    bot = @bot.start(
        VENDOR,
        SIGNED_IN_URL, 
        SIGNED_IN_TEXT, 
        SESSION_COOKIES_FILEPATH, 
        SESSION_COOKIES_LIST,
        PAGES)
    if bot.not_signed_in?(selector: true)
      bot = sign_in(bot)
      bot.save_cookies
    end
    bot = run_bot(bot, @pages)

    bot = assign_actual_values_to_test_bot(bot)
  rescue StandardError => e
    bot.log(error: e)
  ensure
    bot.cleanup_cookies
    bot.stop 
    return bot
  end

  private

  def run_bot(bot, remaining_pages)
    Capybara.using_wait_time CAPYBARA_WAIT_IN_SECONDS do
      remaining_pages.each_with_index do |key, index|
        bot.setup_for_page

        raise "page_undefined" if bot.page_undefined?

        title = bot.title.to_sym
        method_name = PAGES[title]
        bot = self.send(method_name, bot)

        # remove successful page
        remaining_pages = remaining_pages - [title]
      end
    end
    bot = run_bot(bot, remaining_pages, shipping_address) if remaining_pages.any? && !bot.errors.any?

    bot.log(message: "#{self.class.to_s} run success", save_page: Rails.env.development?)
    return bot
  end

  def assign_actual_values_to_test_bot(bot)
    bot.actual_subtotal = 0.30
    bot.actual_shipping_fee = 0.10
    bot.actual_taxes = 0.10
    bot
  end

end