# frozen_string_literal: true
module PurchaseGatewayService::Test::TestBotPurchase
  include PurchaseGatewayService::Test::TestBotSettings

  def search(bot)
    bot.find("i.fa-search").click
    bot
  end
end