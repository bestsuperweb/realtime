# frozen_string_literal: true
module PurchaseGatewayService::Test::TestBotAuthentication
  include PurchaseGatewayService::Test::TestBotSettings

  #Authentication
  def sign_in(bot)
    bot.visit SIGN_IN_URL
    if bot.find("button.just_browsing")
      bot.first("button.just_browsing").click
      bot.rest
    end
    bot.find("a#landing_login").click
    bot.rest
    bot = sign_in_normally(bot)
    bot
  end

  def sign_in_normally(bot)
    bot.first("form#new_customer input#customer_login").click.set USERNAME
    bot.first("form#new_customer input#customer_password").click.set PASSWORD
    bot.log
    bot.first("form#new_customer input[type='submit']").click
    bot
  end

end