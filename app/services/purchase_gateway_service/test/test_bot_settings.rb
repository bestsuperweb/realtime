# frozen_string_literal: true
module PurchaseGatewayService::Test::TestBotSettings

  VENDOR = Rails.configuration.x.purchase_gateway.test_vendor_name
  CAPYBARA_WAIT_IN_SECONDS = Rails.configuration.x.purchase_gateway.capybara_wait_in_seconds

  # Only sess-at-main, at-main, x-main required for a valid session. Others included for safety's sake.
  SESSION_COOKIES_LIST = ['_giftibly_inc_session']

  SESSION_COOKIES_FILEPATH = Rails.configuration.x.purchase_gateway.test_session_cookies_filepath

  SIGN_IN_URL = 'https://giftibly.com'
  SIGNED_IN_URL = 'https://giftibly.com/home'
  SIGNED_IN_TEXT = Rails.configuration.x.purchase_gateway.test_signed_in_text

  # REQUIRED ENV INFO
  USERNAME = ENV['GIFTYBOT_TEST_USERNAME']
  PASSWORD = ENV['GIFTYBOT_TEST_PASSWORD']
  GMAIL_USERNAME = ENV['GMAIL_USERNAME']
  GMAIL_PASSWORD = ENV['GMAIL_PASSWORD']

  # Pages in order
  # TODO consider fallback text match search in case page titles change.
  PURCHASE_PAGES = {
    "Giftibly": 'search'
  }

  UNCOMMON_PAGES = {
    'Amazon Sign In': 'sign_in'
  }

  PAGES = PURCHASE_PAGES.merge(UNCOMMON_PAGES)

end