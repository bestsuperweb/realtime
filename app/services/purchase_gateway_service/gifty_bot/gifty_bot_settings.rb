# frozen_string_literal: true
module PurchaseGatewayService::GiftyBot::GiftyBotSettings

  REFERRER_SITE_TO_SET_REQUEST_REFERRER = Rails.configuration.x.giftybot.referrer_site
  ENABLE_LOG_FILES = Rails.configuration.x.giftybot.enable_logs
  FILEPATH = Rails.configuration.x.giftybot.log_filepath
  MIN_PAUSE_BETWEEN_PAGES_IN_SECONDS = Rails.configuration.x.giftybot.minimum_pause_between_pages

end
