# frozen_string_literal: true
module PurchaseGatewayService::GiftyBot::GiftyBotDriver

  Capybara.javascript_driver = :headless_chrome

  Capybara.register_driver(:gifty_bot_driver) do |app|

    if Rails.configuration.x.gifty_bot.enable_gui
      options = Selenium::WebDriver::Chrome::Options.new
    else
      options = Selenium::WebDriver::Chrome::Options.new(args: %w[--headless --disable-gpu])
    end
    
    # Due to an unknown bug in chromedriver, user agent must be added separately. Don't mess with this.
    user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
    options.add_argument("--user-agent=#{user_agent}")
    options.add_argument("--window-size=1024,768")
    options.add_argument("--proxy=null")
    options.add_argument("--dns-prefetch-disable")

    # increase timeout to prevent Net::Timeout crashes
    client = Selenium::WebDriver::Remote::Http::Default.new
    client.read_timeout = 600

    Capybara::Selenium::Driver.new(
      app,
      browser: :chrome,
      http_client: client,
      options: options
    )
  end
end