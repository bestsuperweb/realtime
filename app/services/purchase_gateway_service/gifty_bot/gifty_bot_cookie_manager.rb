# frozen_string_literal: true
module PurchaseGatewayService::GiftyBot::GiftyBotCookieManager

  def load_cookies
    file = File.read(session_cookies_filepath)
    cookie_hash = JSON.parse(file)
    cookie_hash.each do |cookie|
      hash = cookie.symbolize_keys
      hash[:expires] = hash[:expires].try(:to_time) || nil
      driver.browser.manage.add_cookie(hash)
    end
  end

  def save_cookies
    cookies = driver.browser.manage.all_cookies
    session_cookies = cookies.select {|h| session_cookies_list.include?(h[:name])}

    File.open(session_cookies_filepath, 'w') do |f|
      f.write(session_cookies.to_json)
    end
  end

  def cleanup_cookies
    driver.browser.manage.delete_all_cookies if Rails.env.test?
  end

end