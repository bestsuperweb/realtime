# frozen_string_literal: true
module PurchaseGatewayService::GiftyBot::GiftyBotLogging
  include PurchaseGatewayService::GiftyBot::GiftyBotSettings

  def log_default_options
    {
      error: nil,
      print: true,
      save_page: false
    }
  end

  # TODO consider uploading screenshots and saved html_logs directly to s3
  def log(options={})
    return if Rails.env.test?

    options.reverse_merge!(log_default_options)
    save_page = options[:save_page]
    error = options[:error]
    print = options[:print]

    hash = set_log_hash(self.title, options)

    p "URL:" + current_url.to_s
    p hash[:log_message]

    FileUtils.mkdir_p(FILEPATH) if !File.directory?(FILEPATH)
    FileUtils.mkdir_p(hash[:filepath]) if !File.directory?(hash[:filepath])

    File.open("#{hash[:filepath]}report.txt", 'a') do |f|
      f.puts "#{Time.now.utc}"
      f.puts hash[:log_message]
      f.puts hash[:error]
      f.puts hash[:backtrace]
    end

    save_screenshot(screenshot_filepath(hash[:filepath], hash[:filename]), full: true) if (print || error)
    save_page(html_filepath(hash[:filepath], hash[:filename])) if (save_page || error)

    if error
      self.errors << error
      p error.inspect
      error.backtrace.each { |line| p line }
    end

    true
  end

  def set_log_hash(title, options={})
    tag = options[:error] ? "[ERROR]" : "[PASS]"
    error = options[:error]
    message = options[:message]
    error_message = error.try(:inspect)
    error_backtrace = error.try(:backtrace)
    log_message = "#{tag}page=#{title}#{', message=' + message.to_s if message}"

    {
      filepath: set_filepath,
      filename: set_filename(tag),
      page: title,
      log_message: log_message,
      error_message: error_message,
      backtrace: error_backtrace
    }
  end

  def set_filepath
    "#{FILEPATH}/#{self.vendor.downcase.underscore}/"
  end

  def set_filename(tag)
    timestamp = Time.now.to_s.parameterize
    "#{timestamp}_#{self.index.to_s}_#{tag}#{self.title.parameterize}"
  end

  def screenshot_filepath(filepath, filename)
    "#{filepath}#{filename}.png"
  end

  def html_filepath(filepath, filename)
    "#{filepath}#{filename}.html"
  end

end