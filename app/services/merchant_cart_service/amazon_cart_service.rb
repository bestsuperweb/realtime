# frozen_string_literal: true

class MerchantCartService::AmazonCartService
  include AmazonPaapiHelpers

  def self.build
    new
  end

  def call(merchant_cart, cart_items)
    return merchant_cart if cart_items.size < 1 # Carts must be created with at least one item
    request = initialize_amazon_paapi_request
    hash = generate_paapi_remote_cart_query_hash(request, cart_items)

    raise "No available items in cart" if hash.blank?

    response = create_remote_cart(request, hash)
    if response.dig("CartCreateResponse", "Cart", "Request", "Errors").present?
      message = response.dig("CartCreateResponse", "Cart", "Request", "Errors", "Error", "Message")
      merchant_cart.errors.add(:purchase_gateway_errors, message: message)
      merchant_cart.purchase_gateway_errors["#{Time.now.utc}"] = message
      raise "Remote Cart failure"
    end

    merchant_cart.amazon_remote_cart_id = response.dig("CartCreateResponse", "Cart", "CartId")
    merchant_cart.amazon_remote_cart_hmac = response.dig("CartCreateResponse", "Cart", "HMAC")
    merchant_cart.amazon_remote_cart_purchase_url = response.dig("CartCreateResponse", "Cart", "PurchaseURL")
  
  rescue StandardError => e
    p e
  ensure
   return merchant_cart 
  end

  private

  def generate_paapi_remote_cart_query_hash(request, cart_items)
    hash = {
      'HMAC' => generate_paapi_hmac_signature(request),
    }
    hash = add_cart_items_to_request(hash, cart_items)
  end

  def add_cart_items_to_request(hash, cart_items)
    cart_items.each_with_index do |e, index|
      if e.quantity > 0 && e.current_offer_id.present?
        hash["Item.#{index+1}.OfferListingId"] = e.current_offer_id
        hash["Item.#{index+1}.Quantity"] = e.quantity
      end
    end
    return hash
  end

  def create_remote_cart(request, hash)
    response = request.cart_create(
                query: hash
                )
    response.to_h
  end

end