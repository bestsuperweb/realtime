class CreateGiftFromUrlService

  def self.build
    new
  end

  def call(product_params, basket_ids, customer)
    baskets = Basket.where(id: basket_ids)

    gifts = Product.transaction do
              array = []
              merchant = get_merchant(product_params)
              
              variant = Variant.find_or_initialize_by(
                          merchant_url: product_params[:url],
                          merchant_id: merchant.id
                        )

              variant.name = product_params[:item_name]
              variant.color = product_params[:color]
              variant.size = product_params[:item_size]
              variant.price = (product_params[:price].to_s.to_f*100).to_i
              variant.description = product_params[:description]
              variant.api_id = product_params[:api_id]
        
              variant.remote_product_image_url = product_params[:image] if product_params[:image].present?
              
              if variant.new_record?
                product = Product.find_or_initialize_by(name: product_params[:item_name])
                product.save! if product.new_record?
                variant.product_id = product.id
              end

              variant.save!
              
              baskets.each do |basket|
                gift = customer.gifts.where(variant: variant, basket: basket).first_or_initialize
                
                gift.quantity_desired += 1 unless gift.new_record?
                gift.save!
                
                array << gift
              end
              return array
            end

    return gifts

  end

  private

  def get_merchant(product_params)
    merchant_name = product_params[:merchant_name]
    merchant_public_url = "#{URI.parse(product_params[:url]).host}"

    merchant =  Merchant.where("lower(name) = ? OR public_url = ?", merchant_name.downcase, merchant_public_url).first
    
    unless merchant.present?
      merchant =  Merchant.create!(
                    name: merchant_name,
                    public_url: merchant_public_url
                  )
    end

    merchant
  end

end

