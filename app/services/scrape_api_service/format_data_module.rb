module ScrapeApiService::FormatDataModule

  private

  def call_format_data_module(string, key, uri)
    return nil unless string.present?

    str = string.to_s
    key = key.to_s

    str = sanitize_currency(str) if key == 'price_tag'
    str = format_image_url(str, uri) if key == 'image_tag'
    str = strip_unicode(str)
    str = strip_starting_and_trailing_whitespaces(str)
    str = cleanup_html(str) if key == 'description_tag'
    str = remove_multiple_spaces_and_newlines(str)

    return str
  end  

  def sanitize_currency(string)
    string1 = remove_dash_and_grab_higher_price(string)
    ensure_only_one_price_after_concatenating(string1)
  end

  # deals with $12 - $94 price formats
  def remove_dash_and_grab_higher_price(string)
    array = string.to_s.partition("-")
    array.delete_at(1)
    array.map { |str| remove_non_currency_characters(str).to_f }.max.to_s
  end

  def remove_non_currency_characters(string)
    string.gsub(/[^0-9\.]/, '').to_f 
  end

  # sometimes, prices get concatenated as 149.99.139.99. Calling .to_f.to_s solves the problem.
  # Ideally, we must ensure that only 149.99 gets fetched. This method handles the edge cases where that's not possible.
  # scenarios 
    # 149.99.139.99
    # 309.99309.99
  def ensure_only_one_price_after_concatenating(text)
    text.to_f.round(2).to_s
  end

  def format_image_url(string, uri)
    string.include?('http') ? string : "#{uri.scheme}://#{uri.host}#{string.strip}"
  end

  def strip_unicode(string)
    string.gsub(/[^\x00-\x7F]/, '')
  end

  def strip_starting_and_trailing_whitespaces(string)
    string.strip
  end

  def cleanup_html(string)
    fragment = get_nokogiri_fragment(string)
    formatted_fragment = cleanup_nodes(fragment)
    fragment_without_comments = remove_comments(formatted_fragment)

    cleaned_text_without_newlines = fragment_without_comments.inner_html.squish
    
    return cleaned_text_without_newlines
  end

  def remove_multiple_spaces_and_newlines(string)
    string.gsub(/\s+/, ' ')
  end

  def get_nokogiri_fragment(string)
    Nokogiri::HTML.fragment(string)
  end

  def cleanup_nodes(fragment)
    fragment.search('*').each do |node|
      tag = node.name
      if remove_node?(tag, node)
        node.remove
      else
        node.name = change_tags_to_p(tag)
        node = remove_attributes(node)
      end
    end
    fragment
  end

  def change_tags_to_p(tag)
    dont_convert_tags.has_key?(tag.to_sym) ? tag : 'p'
  end

  def remove_attributes(node)
    node.keys.map {|key| node.delete(key)}
    node
  end

  def remove_node?(tag, node)
    blacklist_tags.has_key?(tag.to_sym) || (node.text.blank? && node.name != 'br')
  end

  def remove_comments(fragment)
    fragment.xpath('descendant::comment()').remove
    fragment
  end

  # remove all tags in the blacklist
  def blacklist_tags
    array = [
      :style,
      :script, 
      :video,
      :center,
      :input
    ]

    Hash[array.each_slice(1).to_a]
  end

  #convert all tags except the following to <p>
  def dont_convert_tags
    array = [
      :ul,
      :li, 
      :h1,
      :h2,
      :h3,
      :h4,
      :b,
      :i,
      :br,
      :span,
      :strong
    ]

    Hash[array.each_slice(1).to_a]
  end
end