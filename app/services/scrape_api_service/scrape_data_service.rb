# frozen_string_literal: true
# This service scrapes the data off a nokogiri document based on fields and values in providers.yml
class ScrapeApiService::ScrapeDataService
  include ScrapeApiService::FormatDataModule
  def self.build
    new
  end
  def call(page, provider, default_provider, uri)
    data = get_data(page, provider, default_provider, uri)
    return data
  end

  private

  def get_data(page, provider, default_provider, uri)
    data_keys = construct_keys(provider)
    data_values = get_values(page, provider, default_provider, uri)
    success = data_values.any?

    obj = Struct.new(*data_keys, :success, :url, :ga_input).new(*data_values, success, uri.to_s, false)
    response = check_ga_input(obj)
    return response
  end

  def check_ga_input(obj)
    if obj.item_size == 'force' || obj.color == 'force'
      obj.item_size = 'true'
      obj.color = 'true'
      obj.ga_input = true
    end
    obj
  end

  def construct_keys(provider)
    provider.keys.map { |e| e.remove("_tag").to_sym }
  end

  def get_values(page, provider, default_provider, uri)
    array = []
    provider.keys.each do |key|
      string =  case key.to_s
                when "merchant_name"
                  provider[key].present? ? provider[key] : uri.host
                else
                  provider[key].present? ? scrape(page, provider[key], key) : scrape(page, default_provider[key], key)
                end
      str = call_format_data_module(string, key, uri)
      result = color_or_size?(key) ? get_color_or_size_value(str, provider[key]) : str
      array << result  
    end
    array
  end

  def color_or_size?(key)
    key == "color_tag" || key == "item_size_tag"
  end

  def get_color_or_size_value(string, provider_key)
    if provider_key == "force"
      'force'
    elsif string.blank? && provider_key.present?
      'true'
    else
      string
    end
  end

  def scrape(page, node, key)
    if eval?(node)
      text = eval(node)
    else
      id = get_id(node)
      order = get_order(node)
      raw_node =  if id.present?
                    page.at(node)
                  else
                    tag = get_tag(node, order)
                    order.present? ? page.css(tag)[order] : page.at(tag)
                  end
      text = key == 'description_tag' ? raw_node.try(:inner_html) : raw_node.try(:text)
    end
    return text
  rescue
    return nil
  end

  # div#id
  def get_id(node)
    node.partition('#').last
  end

  # div[1]
  def get_order(node)
    regex = /.*\[(.*?)\]/
    order = node.slice(regex, 1)
    (order.present? && !order.to_i.zero?) ? order.to_i : nil
  end

  def eval?(node)
    node.include? "page."
  end
  
  def get_tag(node, order)
    node.remove("[#{order}]")
  end
end