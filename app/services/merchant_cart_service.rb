# frozen_string_literal: true
class MerchantCartService
  include AmazonPaapiHelpers

  def self.build
    new(AmazonCartService.build)
  end

  def initialize(amazon)
    @amazon_cart_service = amazon
  end

  def call(merchant_cart, cart_items)
    merchant_cart = @amazon_cart_service.call(merchant_cart, cart_items)
    merchant_cart.status = merchant_cart.errors.any? ? 'failed' : 'ready'
    merchant_cart
  end

end