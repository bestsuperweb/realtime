# frozen_string_literal: true
class CheckoutService
  
  def self.build
    new(StripeService.build)
  end

  def initialize(stripe)
    @stripe = stripe
  end

  def call(giftbox)
    return not_ready_to_checkout_error unless giftbox.ready_to_checkout?

    owner = giftbox.owner
    payment_method = giftbox.payment_method
    shipping_address = giftbox.shipping_address
    invoice = build_invoice(owner, payment_method, giftbox)

    raise "minimum amount must be more than $0.50" unless invoice.authorizable_amount_in_cents > 50
    return invoice unless invoice.save

    payment = build_payment(invoice, owner, payment_method)
    stripe_authorization = @stripe.authorize(owner, payment_method, invoice, payment)
    
    if stripe_authorization.try(:error)
      invoice = handle_authorization_error(stripe_authorization, payment, invoice)
    else
      invoice = handle_authorization_success(stripe_authorization, payment, invoice, shipping_address, giftbox)
    end
  rescue StandardError => e
    p e.message
    invoice.errors.add(:base, :charge, message: e.message)
  ensure
    return invoice
  end

  private

  def build_invoice(owner, payment_method, giftbox)
    invoice = Invoice.where(
      giftbox: giftbox
    ).first_or_initialize

    invoice.assign_attributes(
      payment_method: payment_method, 
      owner: owner,
      estimated_subtotal: invoice.subtotal_estimate,
      estimated_shipping_fees: invoice.shipping_fee_estimate,
      estimated_taxes: invoice.tax_estimate
    )
    return invoice
  end

  def build_payment(invoice, owner, payment_method)
    invoice.payments.new(
      owner: owner,
      payment_method: payment_method
    )    
  end

  def handle_authorization_error(stripe_authorization, payment, invoice)
    payment.update!(status: 'authorization_failed', outcome: stripe_authorization.error)
    invoice.errors.add(:base, :charge, message: stripe_authorization.error)
    return invoice
  end

  # Giftboxes will have cart items and merchant carts with 0 quantity before checkout to enable users to add quantities at will.
  # after checkout, these records are redundant and clutter up the purchasing interface, and are destroyed.
  def handle_authorization_success(stripe_authorization, payment, invoice, shipping_address, giftbox)
    destroy_0_quantity_cart_items_and_merchant_carts(giftbox)

    payment = merge(payment, stripe_authorization)
    shipping_order = invoice.build_shipping_order(shipping_address)
    cart = giftbox.cart

    payment.transaction do
      payment.save
      invoice.authorized!
      shipping_order.save
      giftbox.checked_out!
      cart.checked_out! if cart.owner_is_a_guest?
    end

    return invoice
  end

  def destroy_0_quantity_cart_items_and_merchant_carts(giftbox)
    giftbox.cart_items.where(quantity: 0).destroy_all
    giftbox.merchant_carts.where(cart_items_count: 0).destroy_all
  end


  def merge(payment, stripe_authorization)
    payment.stripe_id = stripe_authorization.id
    payment.outcome = stripe_authorization.outcome
    payment.status = 'authorized'
    payment.authorized_amount = stripe_authorization.amount/100 #in cents
    payment
  end

  def not_ready_to_checkout_error
    OpenStruct.new(
      success: false,
      errors: OpenStruct.new(full_messages: ["Hello! We\'ll need a shipping option, shipping address and a payment method before you can checkout your cart."])
    )
  end
end