module EmojiHelper
  def emojify(content)
    h(content).to_str.gsub(/:([\w+-]+):/) do |match|
      if emoji = Emoji.find_by_alias($1)
        %(<img alt="#$1" src="#{image_path("emoji/#{emoji.image_filename}", skip_pipeline: true)}" style="vertical-align:middle" width="20" height="20" />)
      else
        match
      end
    end.html_safe if content.present?
  end
end


# ====== CUSTOM EMOJI NOT INGLUDED WITH GEMOJI GEM
emoji = Emoji.create("face_with_rolling_eyes") do |char|
  char.add_alias "face_with_rolling_eyes"
  char.add_unicode_alias "\u{1f644}"
  char.add_tag "face_with_rolling_eyes"
end

emoji = Emoji.create("white_frowning_face") do |char|
  char.add_alias "face_with_rolling_eyes"
  char.add_unicode_alias "\u{1f641}"
  char.add_tag "white_frowning_face"
end

emoji = Emoji.create("hugging_face") do |char|
  char.add_alias "face_with_rolling_eyes"
  char.add_unicode_alias "\u{1f917}"
  char.add_tag "hugging_face"
end

