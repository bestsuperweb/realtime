module SiteHelper

# SVG  
def svg(name) 
	file_path = "#{Rails.public_path}/svg/#{name}.svg" 
	return File.read(file_path).html_safe if File.exists?(file_path) 
	'(not found)' 
end 

def timeDifference(date) 
	now = Time.now
	TimeDifference.between(date, now).in_general
end 

def findVariant(id)
	Variant.find_by_id(id)
end

def date_format(date)
  date.nil? ? "" : date.strftime("%m/%d/%Y")
end

def event_date_format(date)
  date.nil? ? "" : date.strftime("%m/%d/%Y %H:%M %p")
end

end
