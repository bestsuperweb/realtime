module MessagesHelper

  def self_or_other(message)
    message.customer == current_customer ? "self" : "other"
  end

  def message_interlocutor(message)
    message.customer == message.conversation.sender ? message.conversation.sender : message.conversation.recipient
  end

end
