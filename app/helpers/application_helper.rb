module ApplicationHelper

  def current_user
    current_customer
  end

  def logged_in?
    current_customer.blank? == false
  end

  def logged_out?
    current_customer.blank? == true
  end

  def resource_name
    :customer
  end

  def resource
    @resource ||= Customer.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:customer]
  end

end
