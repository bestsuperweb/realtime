module CheckoutHelper

  def is_shipping_address?(giftbox, address)
    if giftbox.address_id.present?
      giftbox.address_id == address.id
    else
      address.default?
    end
  end

  def is_payment_method?(giftbox, payment_method)
    if giftbox.payment_method_id.present?
      giftbox.payment_method_id == payment_method.id
    else
      payment_method.default?
    end
  end

  def is_shipping_option?(giftbox, shipping_option)
    giftbox.shipping_option == shipping_option
  end

  def is_own_giftbox?(giftbox)
    customer_signed_in? && current_customer.id == giftbox.giftee_id
  end

end