json.partial! 'api/v1/models/cart', cart: @cart

json.shipping_address do
  json.partial! 'api/v1/models/address', address: @shipping_address
end

json.payment_method do
  json.partial! 'api/v1/models/payment_method', payment_method: @payment_method
end

if @cart_items.present?
  json.cart_items do
    json.array! @cart_items, partial: 'api/v1/models/cart_item', as: :cart_item
  end
end