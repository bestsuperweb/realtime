json.(variant,
  :id,
  :name,
  :price,
  :description,
  :merchant_url,
  :status,
  :product_image,
  :pending,
  :size,
  :share_count
)
