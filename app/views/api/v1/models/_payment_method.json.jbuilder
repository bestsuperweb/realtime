json.(payment_method,
  :uuid,
  :default,
  :metadata,
  :name,
  :payment_type
)