json.(cart,
  :id,
  :flagged_as_current,
  :status,
  :cart_items_count,
  :subtotal,
  :total_shipping_fees,
  :total_taxes,
  :total_amount,
  :ready_to_checkout
)
  