json.(cart_item,
  :id,
  :quantity,
  :price,
  :subtotal
)

json.variant do
  json.partial! 'api/v1/models/variant', variant: cart_item.variant
end
