json.(invoice,
  :uuid,
  :cart_id,
  :total_amount,
  :status,
  :currency_code,
  :currency_symbol
)

