CarrierWave.configure do |config|
  config.fog_provider = 'fog/aws'

  config.fog_credentials = {
    provider:              "AWS",
    aws_access_key_id:     ENV['AWS_S3_ACCESS_KEY_ID'],
    aws_secret_access_key: ENV['AWS_S3_SECRET_ACCESS_KEY'],
    region:                ENV['AWS_REGION']
  }
  config.fog_directory  = ENV['S3_BUCKET']

  if Rails.env.test? || Rails.env.development?
    config.storage = :file
    #config.enable_processing = false
  elsif Rails.env.staging?
    config.storage = :fog
  elsif Rails.env.production?
    config.storage = :fog
    config.asset_host = "https://d1fouod6wmf48j.cloudfront.net"
  end
  
  #caching
  config.fog_attributes = { cache_control: "public, max-age=#{365.days.to_i}" } # optional, defaults to {}
end