# frozen_string_literal: true
Shoryuken.active_job_queue_name_prefixing = true

Rails.application.configure do
  if Rails.env.test? # || Rails.env.development?
    config.active_job.queue_adapter = :inline
  else
    config.active_job.queue_adapter = :shoryuken
    config.active_job.queue_name_prefix = ENV['AWS_SQS_PREFIX']
    config.active_job.queue_name_delimiter = '-'
  end
end

Shoryuken.configure_server do |_config|
  Rails.logger = Shoryuken::Logging.logger
end
