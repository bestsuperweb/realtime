# Be sure to restart your server when you modify this file.

# Configure sensitive parameters which will be filtered from the log file.
Rails.application.config.filter_parameters += [
  :password, :name, :address_1, :address_2, 'payment.metadata', :phone_number
]