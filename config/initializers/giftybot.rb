# PURCHASE GATEWAY Giftybot CONFIG

# GIFTYBOT
# -----------------------------
# Boolean to enable gui for debugging or demo
Rails.application.config.x.giftybot.enable_gui = Rails.env.development?

# Bot visits this site to set request referrer header before proceeding to target site
Rails.application.config.x.giftybot.referrer_site = 'https://giftibly.com'

# Num. of seconds that giftybot will wait for an element to show. 
# Shorter times run the risk of throwing elementNotFound errors. 
# Longer times slows down the bot in case of unexpected errors.
Rails.application.config.x.purchase_gateway.capybara_wait_in_seconds = 10 

# LOGGING
# Saves screenshots and files  
Rails.application.config.x.giftybot.enable_logs = true
Rails.application.config.x.giftybot.log_filepath = "#{Rails.root}/public/gifty_bot"

# Pause time in seconds. Accepts floats. 
# Allows for scripts to load and mimic user behaviour.
Rails.application.config.x.giftybot.minimum_pause_between_pages = 4


# Merchants
#---------------------------------
# Giftybot will match the Merchant's name and it's merchant subservices.
# The vendor name is used to namespace logs files
Rails.application.config.x.purchase_gateway.test_vendor_name = 'test'
Rails.application.config.x.purchase_gateway.amazon_vendor_name = 'amazon'

# Giftybot looks for this text or selector to determine if it's checked in
Rails.application.config.x.purchase_gateway.test_signed_in_text = 'img.avatar'
Rails.application.config.x.purchase_gateway.amazon_signed_in_text = 'Hello, Ryan' #TODO! change this for your own account

# Giftybot will store it's session cookies in this file and loads them at the merchant site.
Rails.application.config.x.purchase_gateway.test_session_cookies_filepath = 'public/test_session_cookies.json'
Rails.application.config.x.purchase_gateway.amazon_session_cookies_filepath = 'public/amazon_session_cookies.json'


# ENV SPECIFIC
#----------------------------------
if Rails.env.production?
  #TODO Toggle to true to allow purchases to go through. This will charge your credit card!
  Rails.application.config.x.purchase_gateway.enable_purchases = false
else
  Rails.application.config.x.purchase_gateway.enable_purchases = false
end
