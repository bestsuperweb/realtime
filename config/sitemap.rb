require 'carrierwave'

# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://www.giftibly.com"

SitemapGenerator::Sitemap.sitemaps_host = "https://#{ENV['S3_BUCKET']}.s3.amazonaws.com/"
SitemapGenerator::Sitemap.public_path = 'tmp/'
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'
SitemapGenerator::Sitemap.adapter = SitemapGenerator::WaveAdapter.new

SitemapGenerator::Sitemap.create do
  
  add discover_deals_path, 					changefreq: 'daily', priority: 0.8
  add discover_categories_path,				changefreq: 'weekly', priority: 0.8
  add discover_wish_lists_path, 			changefreq: 'daily', priority: 0.8

  add join_our_team_path, 				    changefreq: 'monthly', priority: 0.8
  add unlimited_gifts_path, 				changefreq: 'monthly', priority: 0.8
  add faq_path, 							changefreq: 'monthly', priority: 0.8
  add privacy_policy_path, 					changefreq: 'monthly', priority: 0.7
  add terms_path, 							changefreq: 'monthly', priority: 0.7
  add community_path, 						changefreq: 'monthly', priority: 0.7
  add feedback_path, 						changefreq: 'monthly', priority: 0.7

  add new_customer_registration_path, 		changefreq: 'monthly', priority: 0.6
  add friend_search_path, 					changefreq: 'monthly', priority: 0.6


  #all variants 
  Variant.find_each do |variant|
  	add variant_path(variant.slug, p: variant.id), changefreq: 'monthly' 
  end  

  #all gifts 
  Gift.find_each do |gift|
  	add gift_view_path(gift.gift_slug_name), changefreq: 'daily'   
  end

  #all Categories
  ActsAsTaggableOn::Tagging.where( context: "categories" ).collect { |t| t.tag }.flatten.uniq.each do |tag|
    if tag.permalink.present?
      add discover_search_categories_path(tag.permalink), changefreq: 'daily'   
    end
  end  

  #all public Baskets 
  Basket.where(privacy: 'public').find_each do |basket|
  	if basket.lifestyle?
      add basket_view_path(basket.customer.username, basket.slug), changefreq: 'daily', priority: 0.7
  	elsif basket.event?
  	 	add event_view_path(basket.customer.username, basket.slug), changefreq: 'daily', priority: 0.7
  	else
  	 	add charity_view_path(basket.customer.username, basket.slug), changefreq: 'daily', priority: 0.7
  	end  	 	
  end 

  #all public users 
  Customer.where(private: false).find_each do |customer|
  	add user_path(customer.username), changefreq: 'daily', priority: 0.8
  end   

end

#SitemapGenerator::Sitemap.ping_search_engines # Not needed if you use the rake tasks