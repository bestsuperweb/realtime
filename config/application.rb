require_relative 'boot'

#require 'csv'
require 'rails/all'
require 'dotenv-rails'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Giftibly
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.autoload_paths << Rails.root.join('lib')

    config.exceptions_app = self.routes

    Koala.config.api_version = 'v3.0'

    config.assets.paths << Rails.root.join("app", "assets", "fonts", 'images', 'emojipicker')

    config.assets.paths << Rails.root.join('vendor', 'assets', 'bower_components')

    config.assets.initialize_on_precompile = false
      
    # GA LINKS
    config.x.firefox = 'https://addons.mozilla.org/firefox/downloads/file/960292/giftibly_gift_assistant-1.1.15-an+fx.xpi?src=dp-btn-primary'
    config.x.safari = 'https://safari-extensions.apple.com/details/?id=giftibly-gift-assistant.safari-ENUKFAEH6J'
  end
end
