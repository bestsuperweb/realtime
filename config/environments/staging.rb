Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Ensures that a master key has been made available in either ENV["RAILS_MASTER_KEY"]
  # or in config/master.key. This key is used to decrypt credentials (and other encrypted files).
  # config.require_master_key = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  config.public_file_server.enabled = ENV['RAILS_SERVE_STATIC_FILES'].present?

  # Compress JavaScripts and CSS.
  config.middleware.insert_before(Rack::Sendfile, Rack::Deflater)
  config.assets.compress = true
  config.assets.js_compressor = :uglifier
  config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = false

  # `config.assets.precompile` and `config.assets.version` have moved to config/initializers/assets.rb

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = 'http://assets.example.com'

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX

  # Store uploaded files on the local file system (see config/storage.yml for options)
  config.active_storage.service = :local

  # Mount Action Cable outside main process or domain
  # config.action_cable.mount_path = nil
  # config.action_cable.url = 'wss://example.com/cable'
  # config.action_cable.allowed_request_origins = [ 'http://example.com', /http:\/\/example.*/ ]

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  config.log_level = :debug

  # Prepend all log lines with the following tags.
  config.log_tags = [ :request_id ]

  # Use a different cache store in production.
  # config.cache_store = :mem_cache_store

  # Use a real queuing backend for Active Job (and separate queues per environment)
  # config.active_job.queue_adapter     = :resque
  # config.active_job.queue_name_prefix = "giftibly_#{Rails.env}"

  config.action_mailer.perform_caching = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Use a different logger for distributed setups.
  # require 'syslog/logger'
  # config.logger = ActiveSupport::TaggedLogging.new(Syslog::Logger.new 'app-name')

  if ENV["RAILS_LOG_TO_STDOUT"].present?
    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.logger    = ActiveSupport::TaggedLogging.new(logger)
  end

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false
  config.action_mailer.delivery_method = :mailgun
  config.action_mailer.mailgun_settings = {
    api_key: ENV["MAILGUN_API_KEY"],
    domain: "giftibly.com"
  }

  # Needed for Devise
  config.action_mailer.default_url_options = { host: 'http://ec2-54-196-89-160.compute-1.amazonaws.com' }
  config.action_controller.asset_host = 'http://ec2-54-196-89-160.compute-1.amazonaws.com'
  config.action_mailer.asset_host = config.action_controller.asset_host

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false

  config.facebook_api_id = ENV['FACEBOOK_API_ID']

  config.devise.omniauth :facebook, 
    ENV['FACEBOOK_API_ID'],
    ENV['FACEBOOK_SECRET'], 
    scope: 'email, public_profile, user_friends', 
    info_fields: 'email, first_name, last_name, gender, picture.width(300).height(300)', 
    callback_url: "http://ec2-54-196-89-160.compute-1.amazonaws.com/customers/auth/facebook/callback",
    token_params: { parse: :json }

  config.devise.omniauth :google_oauth2, ENV['GOOGLE_CLIENT_ID'],
    ENV['GOOGLE_SECRET'], {
    scope: "profile, email", 
    prompt: "select_account",
    client_options: { ssl: { verify: false } } 
  }

  config.devise.omniauth :linkedin, ENV['LINKEDIN_CLIENT_ID'], ENV['LINKEDIN_SECRET'], scope: 'r_basicprofile r_emailaddress',
    redirect_uri: "http://ec2-54-196-89-160.compute-1.amazonaws.com/customers/auth/linkedin/callback"

  config.devise.omniauth :twitter, ENV['TWITTER_KEY'], ENV['TWITTER_SECRET']

  config.etsy_secret  = ENV['ETSY_SECRET']
  config.etsy_key     = ENV['ETSY_KEY']
  config.etsy_api_url = "https://openapi.etsy.com/v2/"

  config.amazon_secret = ENV['AWS_SECRET_ACCESS_KEY']
  config.amazon_key    = ENV['AWS_ACCESS_KEY_ID']
  config.amazon_tag    = ENV['AMAZON_TAG']

  config.aws_sqs_prefix           = ENV['AWS_SQS_PREFIX']
  config.aws_sqs_endpoint         = ENV['AWS_SQS_ENDPOINT']
  config.aws_sqs_failure_endpoint = ENV['AWS_SQS_FAILURE_ENDPOINT']

  config.recaptcha_site_key   = ENV['RECAPTCHA_SITE_KEY']
  config.recaptcha_secret_key = ENV['RECAPTCHA_SECRET_KEY']

  config.active_record.belongs_to_required_by_default = false


end
