Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false
  
  # Allow concurrency in development
  config.allow_concurrency = true

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Store uploaded files on the local file system (see config/storage.yml for options)
  config.active_storage.service = :local

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true

  config.action_mailer.perform_caching = false
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = { :address => "localhost", :port => 1025 }

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  # firefox link
  config.firefox = 'https://addons.mozilla.org/firefox/downloads/file/960292/giftibly_gift_assistant-1.1.15-an+fx.xpi?src=dp-btn-primary'
    
  #safari link
  config.safari = 'https://safari-extensions.apple.com/details/?id=giftibly-gift-assistant.safari-ENUKFAEH6J'

    
  # firefox link
  config.firefox = ENV['FIREFOX']
    
  #safari link
  config.safari = ENV['SAFARI']
    
  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  BetterErrors.editor = :sublime

  # Needed for Devise
  config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }
  config.action_controller.asset_host = 'http://localhost:3000'
  config.action_mailer.asset_host = config.action_controller.asset_host

  config.facebook_api_id = "278795019239015"

  config.devise.omniauth :facebook, 
    "278795019239015", 
    "1a93cef7b67626293519b7a6c8502d9d", 
    provider_ignores_state: true, 
    scope: 'email, public_profile, user_friends', 
    info_fields: 'email, first_name, last_name, gender, picture.width(300).height(300)', 
    callback_url: "http://localhost:3000/customers/auth/facebook/callback",
    client_options: {
      site: 'https://graph.facebook.com/v3.0',
      authorize_url: "https://www.facebook.com/v3.0/dialog/oauth"
    },
    token_params: { parse: :json }

  config.devise.omniauth :google_oauth2, "285950694245-1k00uv8cjijof6t4nddc5ehgim8oqfu4.apps.googleusercontent.com",
    "6zeaWlgHHeofLfCKxEv5Uzhs", { 
    scope: "profile, email", 
    prompt: "select_account",
    client_options: { ssl: { verify: false } }
  }

  config.devise.omniauth :linkedin, "78060ymo88tdwp", "5WLeml9xYLLCDqBX", scope: 'r_basicprofile r_emailaddress', 
    redirect_uri: "http://localhost:3000/customers/auth/linkedin/callback", provider_ignores_state: true

  config.devise.omniauth :twitter, "jEtquzrXnHIldFgV7B1tMAyDR", "q0hOqanQtvlei85AWXicbqyTNIbuxwMxax4ZXrdXGuZsFyfg5y"

  config.etsy_secret  = "6zq3wrri1a"
  config.etsy_key     = "gaye6acw0qpi2l07k6o7eu9j"
  config.etsy_api_url = "https://openapi.etsy.com/v2/"

  config.amazon_secret = "7hr1vGc9RT08u+LuRPzKtWRde/tJltUB/y2/IuiT"
  config.amazon_key    = "AKIAJUA6T5GPEUF6S4DQ"
  config.amazon_tag    = "ifitibly-20"

  config.aws_sqs_prefix           = "giftibly-dev"
  config.aws_sqs_endpoint         = "https://sqs.us-east-1.amazonaws.com/310133157715/giftibly-dev"
  config.aws_sqs_failure_endpoint = "https://sqs.us-east-1.amazonaws.com/310133157715/giftibly-dev-failed-jobs"

  config.recaptcha_site_key   = "6LeRvTgUAAAAABPETbp6aqw0C8s6nVjkWQ7Fas6e"
  config.recaptcha_secret_key = "6LeRvTgUAAAAAGePWDRmSbwh7YknmKaPuFjf6w7J"

  config.active_record.belongs_to_required_by_default = false
end
