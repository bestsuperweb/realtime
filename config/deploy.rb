# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

#generic config
set :bundle_flags, '--deployment --full-index'
set :pty,           false # DO NOT set to true, or you will waste hours of your life figuring out why your deployment scripts don't work.
set :keep_releases, 5
set :rvm_type,      :user
set :use_sudo,      :false
set :format,        :pretty

set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')
set :linked_files, %w{.env.staging .env.production}

# app config
set :application,     'giftibly'
set :repo_url,        'git@github.com:Giftibly/giftibly.git'
set :stages,          %w(production staging)
set :default_stage,   "staging"
set :deploy_to,       '/home/ubuntu/www/giftibly'
set :rvm_ruby_version,  "2.5.1"
set :sitemap_roles, :web 

set :puma_threads,    [4, 16]
set :puma_workers,    0
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true

# deployment hooks
# before 'deploy', 'rvm1:install:rvm'
# before 'deploy', 'rvm1:install:ruby'
before 'deploy', 'util:backup_solr_configsets_to_config_directory'
before 'deploy', 'util:confirm_deployment'
before 'deploy', 'util:setup_env_variables'
before 'deploy', 'puma:make_dirs'
after  'deploy:restart',  'sitemap:generate_sitemap'
after  'deploy:finished', 'nginx:symlink_conf'
after  'deploy:finished', 'nginx:restart'

# tasks
namespace :util do
  desc "back solr config"
  task :backup_solr_configsets_to_config_directory do
    on roles(:app) do
      run_locally do
        execute "cp -R solr/configsets config/solr"
      end
    end
  end

  desc "confirm deployment"
  task :confirm_deployment do
    stage_name = fetch(:stage).to_s #staging or production
    if stage_name == 'production'
      puts " \n Are you REALLY sure you want to deploy to #{stage_name}?"
      puts " \n Enter the string '#{stage_name}!' to continue\n "
      length = stage_name.length
      input_text = STDIN.gets[0..length] rescue nil
      puts stage_name + '!'
      if input_text != stage_name + '!'
        puts "\n == Please enter '#{stage_name}!' without the quotes =="
        puts "\n exiting deploy"
        exit
      end
    end
  end

  desc "ENV config"
  task :setup_env_variables do 
    on roles :all do |host|
      %w[.env.staging .env.production].each do |f|
        execute "mkdir #{shared_path} -p"
        upload! "#{f}", "#{shared_path}/#{f}"
      end
    end
  end
end

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end
end

namespace :rvm do
  desc "RVM key"
  task :update_gpg_key do
    on roles(:all) do
      execute :gpg, "--keyserver hkp://keys.gnupg.net --recv-keys D39DC0E3"
    end
  end
end

namespace :nginx do
  desc "symlink /config/nginx.conf to /etc/nginx/sites-enabled/giftibly.conf"
  task :symlink_conf do
    on roles(:all) do
      target = "/etc/nginx/sites-enabled/giftibly.conf"
      source = "/home/ubuntu/www/giftibly/current/config/nginx.conf" 
      execute :sudo, 'ln -nfs', source, target
    end
  end

  desc "restart nginx"
  task :restart do
    on roles(:all) do
      execute :sudo, 'service nginx restart'
    end
  end
end

namespace :deploy do
  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end

  after  :finishing,    :compile_assets
  after  :finishing,    :cleanup
  after  :finishing,    :restart
end

namespace :sitemap do
  desc 'Generate sitemap.xml.gz'
  task :generate_sitemap do
    on roles(:app) do
      invoke 'sitemap:create' 
    end
  end
end

