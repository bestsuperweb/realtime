Rails.application.routes.draw do
  devise_for :customers, controllers: { omniauth_callbacks: "customers/omniauth_callbacks" }, skip: [ :sessions, :registration, :password ]
  mount ActionCable.server => '/cable'

  scope '/api' do
    scope '/v1' do
      scope '/baskets' do
        get   '/'        => 'api_baskets#index'
        get   '/create'  => 'api_baskets#create'
      end
      scope '/merchants' do
        get  '/'         => 'api_merchants#check'
      end
      scope '/products' do
        get  '/'         => 'api_products#show'
      end
      scope  '/gifts' do
        post '/add-tags' => 'api_gifts#add_tags'
      end
      scope  '/errors' do
        post '/'         => 'api_errors#submit'
      end

    end
  end

  #======== Cart feature ==========
  namespace :api do
    namespace :v1 do
      resources :carts, only: [:none] do
        collection do
          get 'items'
          get 'items_count'
        end
      end
      resources :api_reports, only: [:create] do
        collection do
          post 'confirm'
        end
      end
      resources :cart_items, only: [:index, :create, :update, :destroy]
      resources :payment_methods, only: [:new, :create]
      resources :checkouts, only: [:create] do
        collection do
          post 'select_shipping_option'
          post 'select_shipping_address'
          post 'select_payment_method'
          get  'review'
        end
      end
      resources :addresses
      resource :gift_urls, only: [:create] do
        get :show, on: :member
        get :baskets, on: :member
        post :html, on: :member
      end
      resources :baskets, only: [:create]
    end
  end

  namespace :ajax_partials do
    resources :shipping_addresses_form, only: [:index, :new]
    resources :payment_methods_form, only: [:index, :new]
    resources :checkouts, only: [] do
      collection do
        get 'sidebar'
        get 'empty_cart'
        get 'cart_tab'
        get 'review_shipping_option'
        get 'review_payment_method'
        get 'review_shipping_address'
      end
    end
    resource :giftboxes, only: [:none] do
      collection do
        get 'giftboxes_modal'
      end
    end
  end

  #admins
  devise_for :admins, controllers: {
    sessions: "admins/sessions"
  }
  authenticated :admin do
    namespace :admin do
      resources :app_settings, only: [:index, :edit, :update]
      resources :purchasers do
        member do
          post 'reset_password'
        end
      end
      resources :shipping_orders, only: [:index, :show, :update] do
        member do
          get 'mark_as_complete'
        end
      end
      resources :merchant_carts, only: [:show, :update] do
        member do
          get 'start'
        end
      end
      resource :ga_reporting, only: [:none] do
        collection do
          get 'home'
        end
      end
      resource :brand_manager, only: [:none] do
        collection do
          get 'home'
        end
      end
      resource :deals, only: [:none] do
        collection do
          get 'home'
        end
      end
      resource :merchant_categories, only: [:none] do
        collection do
          get 'home'
          get 'categories'
          get 'category'
          get 'new'
          post 'add'
          post 'edit'
          delete 'delete'
        end
      end
      resource :feedback, only: [:none] do
        collection do
          get 'home'
          get 'complaints'
          get 'purchases'
          get 'mailinglist'
        end
      end
      root 'dashboards#home', as: :root
    end
  end

  #error status route
  match    ':status',                       to: 'errors#show',                             constraints: {status: /\d{3}/}, via: :all

  get      'admin/brands/view/:id',         to: 'admin/brand_managers#view',               as: :view_admin_brand_manager
  patch    'admin/brands/edit/:id',         to: 'admin/brand_managers#edit',               as: :edit_admin_brand_manager

  get      'admin/deals/view/:id',          to: 'admin/deals#view',                        as: :view_admin_deals
  patch    'admin/deals/edit/:id',          to: 'admin/deals#edit',                        as: :edit_admin_deals
  get      'admin/deals/new',               to: 'admin/deals#new',                         as: :new_admin_deals
  post     'admin/deals/create',            to: 'admin/deals#create',                      as: :create_admin_deals

  resources :guests, only: [:update]

  #=====================

  patch '/admin/ga_reporting/resolve',      to: 'admin#ga_reports_resolve',                as: :resolve_ga_reports
  patch '/admin/complaints/resolve',        to: 'admin#complaints_resolve',                as: :complaints_reports

  #admins
  devise_for :purchaser, controllers: {
    sessions: "purchasers/sessions"
  }
  authenticated :purchaser do
    namespace :purchasers do
      resources :shipping_orders, only: [:index, :show, :update] do
        member do
          get 'mark_as_complete'
        end
      end
      resources :merchant_carts, only: [:show, :update] do
        member do
          get 'start'
        end
      end
      root 'shipping_orders#index', as: :root
    end
  end


  get :token, controller: 'application'

  as :customer do
    # Custom Devise routes for Registration
    get    'register',                      to: 'customers/registrations#new',             as: :new_customer_registration
    post   'register',                      to: 'customers/registrations#create',          as: :customer_registration
    get    'cancel',                        to: 'devise/registrations#cancel',             as: :cancel_customer_registration
    get    'edit',                          to: 'devise/registrations#edit',               as: :edit_customer_registration
    patch  'update',                        to: 'devise/registration#update'
    put    'update',                        to: 'devise/registrations#update'
    delete 'destroy',                       to: 'devise/registrations#destroy'

    # Custom Devise routes for Session
    get    'login',                         to: 'custom_sessions#new',                     as: :new_customer_session
    post   'login',                         to: 'custom_sessions#create',                  as: :customer_session
    delete 'logout',                        to: 'custom_sessions#destroy',                 as: :destroy_customer_session

    # Custom Devise routes for Password
    get    'password/',                     to: 'customers/passwords#new',                    as: :new_customer_password
    post   'password/',                     to: 'customers/passwords#create',                 as: :create_customer_password
    get    'password/edit',                 to: 'customers/passwords#edit',                   as: :edit_customer_password
    post   'password/update',               to: 'customers/passwords#update',                 as: :update_customer_password
    # patch  'password',                      to: 'devise/passwords#update'
    # put    'password',                      to: 'devise/passwords#update'
  end

  resources :media_contents, only: [:create]

  resources :conversations do
    resources :messages do
      collection do
        post :read
      end
    end
  end
  get '/messages/not_read'

  # Baskets
  get    '/:username/registry/:id',         to: 'basket#view',                             as: :basket_view
  post   '/basket/add/:id',                 to: 'basket#add',                              as: :basket_add
  delete '/basket/remove/:id',              to: 'basket#remove',                           as: :basket_remove
  post   '/basket/create',                  to: 'basket#create',                           as: :basket_create
  delete '/basket/destroy/:id',             to: 'basket#destroy',                          as: :basket_destroy

  post   '/basket/update/:id',              to: 'basket#update',                           as: :basket_update
  patch  '/basket/share/:id',               to: 'basket#share',                            as: :basket_share
  patch  '/basket/clone/:id',               to: 'basket#clone',                            as: :basket_clone
  post   '/basket/banner/:id',              to: 'basket#banner_upload',                    as: :basket_banner
  post   '/basket/default_banner/:id',      to: 'basket#banner_default',                   as: :lifestyle_default_banner

  # Event
  get    '/:username/event/:id',            to: 'events#view',                             as: :event_view
  post   '/event/remove/:id',               to: 'events#remove',                           as: :event_remove
  post   '/event/update/:id',               to: 'events#update',                           as: :event_update
  post   '/event/banner/:id',               to: 'events#banner_upload',                    as: :event_banner
  post   '/event/default_banner/:id',       to: 'events#banner_default',                   as: :event_default_banner
  patch  '/event/confirm',                  to: 'events#confirm_invitation',               as: :event_confirm_invitation
  patch  '/event/maybe',                    to: 'events#maybe_invitation',                 as: :event_maybe_invitation
  patch  '/event/reject',                   to: 'events#reject_invitation',                as: :event_reject_invitation
  patch  '/event/invite_customer/:id',      to: 'events#invite_customer',                  as: :event_invite_customer
  # Charity
  get    '/:username/charity/:id',           to: 'charity#view',                           as: :charity_view
  post   '/charity/update/:id',             to: 'charity#update',                          as: :charity_update
  post   '/charity/banner/:id',             to: 'charity#banner_upload',                   as: :charity_banner
  post   '/charity/default_banner/:id',     to: 'charity#banner_default',                  as: :charity_default_banner

  # Cash
  get    '/cash',                           to: 'cash#view',                               as: :cash_view

  delete '/oauth/disconnect',               to: 'customers/omniauth#destroy',              as: :oauth_disconnect

  #welcome page
  get    '/welcome',                        to: 'site#welcome',                            as: :welcome

  #sitemap page
  get    '/sitemaps',                       to: 'site#sitemaps',                           as: :sitemaps

  # Feed
  get    '/feed/following',                 to: 'feed#following',                          as: :following_feed
  get    '/feed/products',                  to: 'feed#products',                           as: :products_feed
  get    '/feed/most-active',               to: 'feed#most_active',                        as: :most_active_feed

  #search
  get    '/search',                         to: 'feed#search',                             as: :search
  get    '/friend-search',                  to: 'feed#friend_search',                      as: :friend_search
  get    '/search-for-friends',             to: 'site#customers_search',                   as: :search_customers

  post   '/browsing',                       to: 'site#browsing'

  #Discover
  get    '/discover/brands',                to: 'discover#discover_brands',                as: :discover_brands
  get    '/discover/deals',                 to: 'discover#discover_deals',                 as: :discover_deals
  get    '/discover/categories',            to: 'discover#discover_categories',            as: :discover_categories
  get    '/discover/wish-lists',            to: 'discover#discover_wish_lists',            as: :discover_wish_lists
  get    '/discover/categories/:category',  to: 'discover#discover_search_categories',     as: :discover_search_categories
  get    '/sort/discover',                  to: 'discover#sort',                           as: :discover_sort
  get    '/paginate/discover',              to: 'discover#paginate',                       as: :discover_paginate

  # Modal Builders
  get    '/products/modal_builder/:id',     to: 'products#modal_builder',                  as: :products_modal_builder
  get    '/products/modal_leave/:id',       to: 'products#modal_leave',                    as: :products_modal_leave
  get    '/basket/modal_builder/:id',       to: 'basket#modal_builder',                    as: :basket_modal_builder
  patch  '/basket/modal_destroy/:id',       to: 'basket#modal_destroy',                    as: :basket_modal_destroy
  patch  '/profile/modal_share/:id',        to: 'customers/profile#modal_share',           as: :customer_modal_share

  # Popup Builders
  patch  '/basket/popup_share/:id',         to: 'basket#popup_share',                      as: :basket_popup_share

  # Product
  get    '/products',                       to: 'products#index',                          as: :products_index
  get    '/products/:id',                   to: 'products#show',                           as: :variant
  patch  '/products/add-tags',              to: 'products#add_tags',                       as: :products_add_tag

  #BASKET INFINITE SCROLL PAGINATION
  get    '/paginate/basket',                to: 'basket#paginate_basket',                  as: :basket_paginate

  resource :checkout, only: [:create, :show] do
    collection do
      get  'guest_email'
      get  'confirm'
      get  'amazon_prime'
      get  'order_placed'
    end
  end

  # Gift
  get    '/gift/:id',                       to: 'gift#view',                               as: :gift_view
  patch  '/gift/rate',                      to: 'gift#rate',                               as: :gift_rate
  delete '/gift/destroy',                   to: 'gift#destroy',                            as: :gift_destroy
  patch  '/gift/share/:id',                 to: 'gift#share',                              as: :gift_share
  patch  '/gift/quantity_change/:id',       to: 'gift#quantity_change',                    as: :gift_quantity_change
  patch  '/gift/add-tags',                  to: 'gift#add_tags',                           as: :gift_add_tag
  patch  '/gift/confirm',                   to: 'gift#confirm',                            as: :gift_confirm
  patch  '/gift/recieved/:id',              to: 'gift#recieved',                           as: :gift_recieved
  post   '/gift/caption/:id',               to: 'gift#caption',                            as: :gift_add_caption

  get    '/join-our-team',                  to: 'site#join_our_team',                      as: :join_our_team
  get    '/faq',                            to: 'site#faq',                                as: :faq
  get    '/privacy-policy',                 to: 'site#privacy_policy',                     as: :privacy_policy
  get    '/terms-and-conditions',           to: 'site#terms',                              as: :terms
  get    '/community',                      to: 'site#community',                          as: :community
  get    '/unlimited-gifts',                to: 'site#unlimited_gifts',                    as: :unlimited_gifts
  get    '/feedback',                       to: 'site#feedback',                           as: :feedback
  post   '/complaint',                      to: 'site#complaint',                          as: :new_complaint

  patch  '/notifications/mark-as-read',     to: 'notifications#mark_as_read',              as: :notifications_mark_as_read

  #NOTIFICATIONS INFINITE SCROLL PAGINATION
  get    '/paginate/notifications',         to: 'customers/profile#paginate_notifications',as: :notifications_paginate

  patch  '/comments/create',                to: 'comments#create',                         as: :comments_create
  patch  '/comments/delete/:id',            to: 'comments#delete',                         as: :comments_delete

  patch  '/activities/share',               to: 'activities#share',                        as: :activities_share

  patch  '/likes/toggle',                   to: 'likes#toggle',                            as: :likes_toggle

  delete '/customer/deactivate',            to: 'customers/views#deactivate',              as: :customer_deactivate
  patch  '/customer/share/:id',             to: 'customers/views#share',                   as: :customer_share
  post   '/customer/category',              to: 'customers/views#category',                as: :customer_category

  #FACEBOOK POPUP CONNECTION
  post   '/customer/fb-popup-session',      to: 'customers/views#fb_popup_session',        as: :customer_fbpopup_session
  post   '/customer/fb-popup-db',           to: 'customers/views#fb_popup_db',             as: :customer_fbpopup_db
  post   '/customer/fromfb',                to: 'customers/views#fromfb',                  as: :customer_fromfb

  #CUSTOMER INFINITE SCROLL PAGINATION
  get    '/paginate/wishlist',              to: 'customers/views#wishlist',                as: :paginate_wishlist
  get    '/paginate/connection',            to: 'customers/views#connection',              as: :paginate_connection
  get    '/paginate/wishes',                to: 'customers/views#wishes',                  as: :paginate_wishes

  post   '/profile/avatar',                 to: 'customers/profile#avatar_upload',         as: :customer_avatar
  post   '/profile/banner',                 to: 'customers/profile#banner_upload',         as: :customer_banner
  post   '/profile/default_banner',         to: 'customers/profile#banner_default',        as: :customer_default_banner
  post   '/profile/default_avatar',         to: 'customers/profile#avatar_default',        as: :customer_default_avatar

  patch  '/profile/update',                 to: 'customers/profile#profile_update',        as: :profile_update
  patch  '/profile/basic-update',           to: 'customers/profile#basic_update',          as: :basic_update
  patch  '/profile/shipping-update',        to: 'customers/profile#shipping_update',       as: :shipping_update
  patch  '/profile/notification-update',    to: 'customers/profile#notification_update',   as: :notification_update
  patch  '/profile/approve',                to: 'customers/profile#approve_connection',    as: :approve_connection
  patch  '/profile/request',                to: 'customers/profile#request_connection',    as: :request_connection
  patch  '/profile/reject',                 to: 'customers/profile#reject_connection',     as: :reject_connection
  patch  '/profile/follow',                 to: 'customers/profile#follow_customer',       as: :follow_customer
  patch  '/profile/unfollow',               to: 'customers/profile#unfollow_customer',     as: :unfollow_customer
  patch  '/profile/block',                  to: 'customers/profile#block_customer',        as: :block_customer
  patch  '/profile/unblock',                to: 'customers/profile#unblock_customer',      as: :unblock_customer
  patch  '/profile/remove',                 to: 'customers/profile#remove_customer',       as: :remove_customer
  patch  '/profile/update_email',           to: 'customers/profile#profile_update_email',  as: :profile_update_email
  get    '/notifications',                  to: 'customers/profile#notifications',         as: :notifications_view
  patch  '/profile/verify',                 to: 'customers/profile#profile_verify',        as: :profile_verify
  get    '/profile',                        to: 'customers/profile#view',                  as: :profile_view

  get    '/customer/pause',                 to: 'customers/walkthrough#pause',             as: :walkthrough_pause

  ####### DON'T PUT ANYTHING BELOW HERE ########

  get    '/:username',                      to: 'customers/views#show',                    as: :user

  root   'site#landing'

end
