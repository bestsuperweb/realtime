module AmazonPaapiHelpers

  ENDPOINT = "webservices.amazon.com"
  REQUEST_URI = "onca/xml"
  AMZNPA_ACCESS_KEY = ENV['AMZNPA_ACCESS_KEY']
  AMZNPA_SECRET_KEY = ENV['AMZNPA_SECRET_KEY']
  AMZNPA_ASSOCIATE_TAG = ENV['AMZNPA_ASSOCIATE_TAG']

  def generate_paapi_request_url(vacuum_request)
    hash = filter(vacuum_request)
    generate_paapi_url(generate_paapi_query_string(hash), generate_paapi_hmac_signature(hash))
  end

  def generate_paapi_hmac_signature(vacuum_request)
    hash = filter(vacuum_request)
    Base64.encode64(
      OpenSSL::HMAC.digest(
        OpenSSL::Digest.new('sha256'), 
        AMZNPA_SECRET_KEY,
        generate_hmac_string(generate_paapi_query_string(hash))
      )
    )
  end

  def initialize_amazon_paapi_request
    request = Vacuum.new
    request.configure(
      aws_access_key_id: AMZNPA_ACCESS_KEY,
      aws_secret_access_key: AMZNPA_SECRET_KEY,
      associate_tag: AMZNPA_ASSOCIATE_TAG
    )
    request
  end

  private

  # format and filter out Vacuum request params
  # add timestamp
  def filter(params)
    hash = params.as_json.to_h.except("aws_secret_access_key", "aws_endpoint")
    hash["Timestamp"] = Time.now.gmtime.iso8601 if !hash.key?("Timestamp")
    hash.transform_keys{ |key| key.to_s.camelize.gsub(/Aws/, 'AWS') }
  end

  def generate_paapi_url(query_string, signature)
    sign = "#{URI.escape(signature, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))}"
    "http://#{ENDPOINT}#{REQUEST_URI}?#{query_string}&Signature=#{sign}"
  end

  def generate_paapi_query_string(params)
    params.sort.collect do |key, value|
      [
        URI.escape((key.to_s), Regexp.new("[^#{URI::PATTERN::UNRESERVED}]")),
        URI.escape((value.to_s), Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))
      ].join('=')
    end.join('&')
  end

  def generate_hmac_string(query_string)
    "GET\n#{ENDPOINT}\n#{REQUEST_URI}\n#{query_string}"
  end
  
end