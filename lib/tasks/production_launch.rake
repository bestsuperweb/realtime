require 'httparty'
require 'json'

namespace :production_launch do

  desc "Setup database for a production launch"
  task setup: :environment do
    # Create a merchant file
    merchant_file = File.new( "merchant.txt", "w" )
    Merchant.find_each do |merchant|
      shell = { name: merchant.name,
                url: merchant.public_url
              }
      merchant_file.puts( shell.to_json )
      puts "Added #{merchant.name} merchant to file"
    end
    merchant_file.close

    # Create a product file
    product_file = File.new( "product.txt", "w" )
    # each row has a string of a product name
    Product.find_each do |product|
      shell = { name: product.name,
                tag_list: product.tag_list.to_s
              }
      product_file.puts( shell.to_json )
      puts "Added #{product.name} product to file"
    end
    product_file.close
    
    # Create a variant file
    variant_file = File.new( "variant.txt", "w" )
    # encode object into json
    Variant.find_each do |variant|
      shell = { name: variant.name,
                description: variant.description,
                price: variant.price,
                merchant: variant.merchant.public_url,
                color: variant.color,
                size: variant.size,
                merchant_url: variant.merchant_url,
                api_id: variant.api_id,
                image: variant.product_image.url
              }
      variant_file.puts( shell.to_json )
      puts "Added #{variant.name} variant to file"
    end
    variant_file.close
  end

  desc "Build database for production launch"
  task build: :environment do
    # Grab merchant file
    merchant_text = File.open("merchant.txt").read
    merchant_text.gsub!(/\r\n?/, "\n")
    merchant_text.each_line do |line|
      m = JSON.parse(line)
      merchant = Merchant.find_by( name: m["name"] )

      if merchant.nil?
        merchant = Merchant.new
        merchant.name = m["name"]
        merchant.public_url = m["url"]

        merchant.save!

        p "Added #{merchant.name} merchant to our system"
      else
        p "Already had #{merchant.name} in our system"
      end
    end

    # Grab product file
    product_text = File.open("product.txt").read
    product_text.gsub!(/\r\n?/, "\n")
    # create a product based off of name of each row
    product_text.each_line do |line|
      p = JSON.parse(line)
      product = Product.new
      product.name = p["name"]

      product.save!

      product.tag_list.add( p["tag_list"].downcase, parse: true )

      product.save!

      p "Added #{product.name} product to system"
    end

    # Grab variant file
    variant_text = File.open("variant.txt").read
    variant_text.gsub!(/\r\n?/, "\n")
    # create a variant based off of each json object.
    variant_text.each_line do |line|
      v = JSON.parse( line )
      product = Product.find_by( name: v["name"] )
      merchant = Merchant.find_by( public_url: v["merchant"] )

      variant = Variant.new
      variant.name = v["name"]
      variant.description = v["description"]
      variant.price = v["price"]
      variant.color = v["color"]
      variant.size = v["size"]
      variant.merchant_url = v["merchant_url"]
      variant.api_id = v["api_id"]
      variant.product = product
      variant.merchant = merchant

      variant.remote_product_image_url = v["image"] if v["image"].present?

      variant.save!

      p "Added #{variant.name} variant to system"
    end
    # connect to existing product based off of name
    # create image based off of staging copy
  end

end
