namespace :db do
  desc "populate app settings"
  task app_settings: :environment do    
    
    AppSetting.destroy_all
    settings = AppSetting.first_or_initialize
    settings.save!

  end
end