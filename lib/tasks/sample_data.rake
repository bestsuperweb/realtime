require 'httparty'
require 'json'

namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do    
    # make_variants
    make_friends
    make_baskets
    make_gifts
    make_carts
    make_giftboxes
    make_merchant_carts
    make_failed_merchant_cart
    make_payment_method
    make_payments_and_invoices
    make_shipping_orders
  end
end

# def make_variants
#   request = Vacuum.new

#   request.configure(
#     aws_access_key_id:     Rails.application.config.amazon_key,
#     aws_secret_access_key: Rails.application.config.amazon_secret,
#     associate_tag:         Rails.application.config.amazon_tag
#   )

#   prod_ray = [
#     { keyword: "fitness",              search_index: "SportingGoods" },
#   ]

#   merchant = Merchant.find_by_name("Amazon")

#   prod_ray.shuffle.each do |product|

#     p "Attempting #{product[:keyword]} - #{product[:search_index]}"

#     response = request.item_search( 
#       query: {
#         'Keywords'=> product[:keyword],
#         'SearchIndex'=> product[:search_index],
#         'ResponseGroup' => "ItemAttributes,Images,Offers"
#       }
#     )

#     json_hash = response.to_h

#     if json_hash['ItemSearchResponse']['Items']['Item'].try(:any?)
#       json_hash['ItemSearchResponse']['Items']['Item'].each do |data|
#         title = CGI::unescapeHTML( data['ItemAttributes']['Title'] )
#         price = data['ItemAttributes']['ListPrice']

#         if title.present? && price.present?

#           product = Product.find_or_create_by( name: title )

#           variant = Variant.new

#           variant.name = title
#           variant.price = price['Amount']
#           variant.merchant_url = data['DetailPageURL']

#           #important!
#           variant.api_id = data.dig("Offers", "Offer", "OfferListing","OfferListingId")
#           variant.shipping_dimensions = data.dig("ItemAttributes", "PackageDimensions")
#           variant.shipping_category = data.dig("ItemAttributes", "ProductGroup")
#           variant.metadata = data
#           ####

#           variant.merchant = merchant
#           variant.product = product

#           if data['LargeImage'].present? && data['LargeImage']['URL'].present?
#             variant.remote_product_image_url = data['LargeImage']['URL']

#             if variant.valid?
#               product.save!
#               variant.save!

#               p "Created variant # #{variant.id} - #{variant.name}"
#             else
#               p "Error occurred and #{title} could not be saved"
#             end
#           else
#             p "Error occurred - product had no image"
#           end
#         end
#       end
#     end
#   end
# end

def make_friends
  friend = Customer.create(
    first_name: "GiftiblyFriend",
    last_name: "Inc.",
    location: "Orlando, FL, United States",
    email: "info1@giftibly.com",
    password: "passw0rd",
    username: "friend",
    description: "Giftibly is the the world's Social Gift Collection. We take the stress & worry out of giving by making sure you find the perfect gift, every time, guaranteed.",
    birthday: Date.new(1982, 9, 24),
    website: "www.giftibly.com",
    role: "admin"
  )

  # friend.avatar = avatar
  # friend.banner = banner
  friend.save

  friend2 = Customer.create(
    first_name: "GiftiblyFriend2",
    last_name: "Inc.",
    location: "Orlando, FL, United States",
    email: "info2@giftibly.com",
    password: "passw0rd",
    username: "friend2",
    description: "Giftibly is the the world's Social Gift Collection. We take the stress & worry out of giving by making sure you find the perfect gift, every time, guaranteed.",
    birthday: Date.new(1982, 9, 24),
    website: "www.giftibly.com",
    role: "admin"
  )

  # friend2.avatar = avatar
  # friend2.banner = banner
  friend2.save
end

def make_baskets
  friends = Customer.all

  friends.each do |e|
    e.baskets.where(name: "Liked Products").first_or_create
  end

end

def make_gifts
  baskets = Basket.all
  variant_ids = Variant.all.map(&:id)

  baskets.each do |e|
    e.gifts.where(variant_id: rand(variant_ids[0]..variant_ids[2]), customer_id: e.customer_id).first_or_create(quantity_desired: 1)
  end
end

def make_carts
  owner = Customer.first
  owner.carts.create
end

def make_giftboxes
  owner = Customer.first
  giftee = Customer.second
  50.times do |e|
    Giftbox.where(owner: owner, giftee: giftee, status: 1).create
  end
end

def make_merchant_carts
  merchant = Merchant.first
  giftboxes = Giftbox.all

  giftboxes.each do |e|
    merchant_cart = e.merchant_carts.where(
      merchant: merchant, 
      status: rand(0..3)
    ).create
    merchant_cart.cart_items.where(variant_id: 1, cart_id: 1).create

    time = rand(6.months).seconds.ago
    merchant_cart.update!(purchase_gateway_success_date: time, created_at: time)
  end
end

def make_failed_merchant_cart
  m = MerchantCart.where(status: 3).last
  m.update!(
    purchase_gateway_failure_date: m.purchase_gateway_success_date, 
    purchase_gateway_success_date: nil, 
    status: 4
  )
end

def make_payment_method
  o = Customer.first
  a = Address.first
  PaymentMethod.where(owner: o).first_or_create(stripe_id: '123123', metadata: {"2":"2"}, payment_type:'0', address_id: a.id)
end

def make_payments_and_invoices
  m = MerchantCart.where(status: [2,3]).all
  o = Customer.first
  p = PaymentMethod.first
  m.each do |e|
    i = Invoice.where(giftbox_id: e.giftbox_id).first_or_create(owner: o, total_amount: 12, payment_method: p)
    Payment.where(invoice: i, owner: o, payment_method: p, authorized_amount: 12).first_or_create
  end
end

def make_shipping_orders
  m = MerchantCart.where(status: [1,2]).all
  o = Customer.first
  a = Address.first
  m.each do |e|
    status = if e.completed?
              'completed'
             elsif e.failed?
              'failed'
             end
    i = Invoice.where(giftbox_id: e.giftbox_id).first
    s = ShippingOrder.where(owner: o, invoice: i.id, address_id: a.id).first_or_create(status: status)
    e.update!(shipping_order: s)
  end
end

