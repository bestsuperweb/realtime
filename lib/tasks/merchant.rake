namespace :merchant do
	desc "Clean Merchant Categories"
	task clean_categories: :environment do
		Merchant.find_each do |merchant|
			merchant.category_list.clear  
			merchant.save
		end
		p 'Merchant Categories Cleaned'
	end

	desc "Recreate Merchant Screenshot Versions"
	task recreate_versions: :environment do
		Merchant.find_each do |merchant|
			if merchant.screenshot.present?
				merchant.screenshot.recreate_versions! 
				merchant.save 
			end
		end  
		p 'Merchant Versions Recreated'
	end
end