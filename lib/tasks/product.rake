require 'httparty'
require 'json'

namespace :product do

  desc "Generates tags for products without tags"
  task generate_tags: :environment do
    Product.where( 'created_at >= ?', 1.day.ago ).each do |product|
      product.generate_tags
      p "Generated tags for #{product.name}"
    end
  end

  desc "Generates tags and runs a report"
  task tag_report: :environment do
    report_ray = []
    Product.find_each do |product|
      if product.tag_list.empty?
        product.generate_tags
        if product.tag_list.empty?
          line = "#{product.id} - #{product.name} - still has no tags"
          report_ray << line
        else
          p "Generated tags for #{product.name}"
        end
      else
        p "#{product.name} already has tags"
      end
    end
    p "###################################"

    if report_ray.empty?
      p "All products have tags. Hooray!"
    else
      report_file = File.open( "#{Rails.root}/reports/#{Time.now.year}-#{Time.now.month}-#{Time.now.day}-tag-report.txt", "w" )
      report_ray.each do |line|
        report_file.puts line
        p line
      end
      report_file.close
      p "#{report_ray.count} products are without tags."
    end

  end

  desc "Cascades tags from gifts into products"
  task cascade_tags: :environment do
    Gift.find_each do |gift|
      gift.variant.product.tag_list += gift.tag_list
      gift.variant.product.save
    end
  end

end
