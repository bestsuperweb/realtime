require 'httparty'
require 'json'

namespace :etsy do

  desc "Updates all Etsy products"
  task :update => :environment do
    merchant = Merchant.find_by_name( "Etsy" )

    # Collect all products connected to the merchant
    variants = Variant.where( merchant_id: merchant.id )

    # Iterate over each product and update each one
    variants.each do |variant|
      url = Rails.application.config.etsy_api_url + "listings/#{variant.api_id}?api_key=" + Rails.application.config.etsy_key

      response = HTTParty.get( url, format: :plain )
      json_hash = JSON.parse( response, symbolizes_names: true )

      if( json_hash['count'] < 1 || json_hash['results'].blank? )

      else
        data = json_hash['results'][0]
        # If the product is no longer active, remove it from
        if( data['state'] != 'active' )
          # Remove all gifts that have this variant
          gifts = Gift.where( variant_id: variant.id )

          gifts.each do |gift|
            gift.destroy
            # TODO: Inform Customer the gift is no longer available
          end
          
          variant.make_inactive

          p "DEACTIVATED variant # #{variant.id} - #{variant.name}"
        else
          variant.name = CGI::unescapeHTML(data['title'])
          variant.price = (data['price'].to_f * 100).to_i
          variant.description = CGI::unescapeHTML( data['description'] )
          variant.merchant_url = data['url']

          variant.save

          variant.product.tag_list = data['tags'].join(", ")

          p "Updated variant # #{variant.id} - #{variant.name}"
        end
      end
    end
  end

  desc "Imports 100 test products from etsy"
  task :import => :environment do
    url = Rails.application.config.etsy_api_url + "listings/active?api_key=" + Rails.application.config.etsy_key + "&sort_on=score&limit=101"

    merchant = Merchant.find_by_name( "Etsy" )

    # Touch Etsy API
    response = HTTParty.get(url, format: :plain)

    # Format Response to enumerable
    json_hash = JSON.parse( response, smbolize_names: true )

    # Report confirmation or close import
    if( json_hash['count'].blank? || json_hash['results'].blank? )
      p "Error importing"
      exit
    end

    count = ( json_hash['count'] < 101 ) ? json_hash['count'] : 101
    error_count = 0

    # Iterate over response
    for i in 1..count
      data = json_hash['results'][i]
      if( data.blank? )
        #  * If an error, store it, but keep going
        error_count += 1
      elsif( data['state'] != "active" )
        # Catalog error for inactive product
      elsif data['title'].blank?
        p "Error occurred and data has no title"
        next
      else
        title = CGI::unescapeHTML( data['title'] )
        #  * Populate Product
        product = Product.find_or_create_by( name: title )

        variant = Variant.find_or_create_by( api_id: data['listing_id'] )
        variant.name = title
        variant.price = (data['price'].to_f * 100).to_i
        variant.description = CGI::unescapeHTML( data['description'] )
        variant.merchant_url = data['url']

        variant.merchant = merchant
        variant.product = product

        product.tag_list = data['tags'].join(", ").downcase if data['tags'].present?

        image_url = Rails.application.config.etsy_api_url + "listings/" + data['listing_id'].to_s + "/images?api_key=" + Rails.application.config.etsy_key

        image_response = HTTParty.get( image_url, format: :plain )
        image_json_hash = JSON.parse( image_response, smbolize_names: true )
        image = image_json_hash.blank? ? nil : image_json_hash['results'][0]

        if image.present?
          variant.remote_product_image_url = image['url_fullxfull']
        else
          p "OH NO! No image for #{variant.id}"
        end

        if variant.valid?
          variant.save!
          product.save!

          #  * Report Product populated
          p "Created variant # #{variant.id} - #{variant.name}"
        else
          p "Error occurred and #{data['title']} could not be saved"
        end
      end 

    end

    # Report any errors or if all 25 products were populate
    p "There were #{error_count} incidents of errors" if error_count > 0

  end

end
