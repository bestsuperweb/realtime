namespace :images do

  desc "Reupload originals"
  task reupload: :environment do
    variants = Variant.all.order(id: :asc)
    variants.each_with_index do |v, i|
      puts "[#{i}]: reuploading images for #{v.id}"

      # Will work on production
      original_url = v.product_image.url
      v.remote_product_image_url = original_url

      # To rescue staging images
      unless v.save
        jpeg_url = v.product_image.large.url.gsub(/large_/, 'basket_image_').rpartition('.').first << '.jpeg'
        v.remote_product_image_url = jpeg_url
        unless v.save
          jpg_url = jpeg_url.rpartition('.').first << '.jpg'
          v.remote_product_image_url = jpg_url
          v.save
        end
      end

      puts "[#{i}]: #{v.id} saved"
    end

    puts "Variant image versions created"
  end

  desc "Recreate versions"
  task recreate: :environment do
    variants = Variant.all.order(id: :asc)

    variants.each_with_index do |v, i|
      puts "[#{i}]: recreating images for #{v.id}"
      v.product_image.recreate_versions!(:large, :tile, :medium, :small)
      v.save!
    end

    puts "Variant image versions created"
  end

  desc "Rebuild originals on local"
  task rebuild_local: :environment do
    variants = Variant.all.order(id: :asc)
    variants.each_with_index do |v, i|
      puts "[#{i}]: rebuilding images for #{v.id}"

      filepath = Rails.root.join("public" + v.product_image_url)
      if File.exist?(filepath)
        image = filepath.open
      else
        path = v.product_image.url.rpartition('/').first
        basket_image_filepath = Rails.root.join("public" + path + '/basket_image_' + v.product_image.url.rpartition('/').last)
        if File.exist?(basket_image_filepath)
          image = basket_image_filepath.open
        else
          dir = Rails.root.join("public" + path)
          first_file = Dir.entries(dir).detect { |e| e.include?('.jpg') || e.include?('.jpeg') || e.include?('.png')}
          filepath = dir + first_file
          image = filepath.open
        end
      end
      v.product_image = image
      v.save
    end

    puts "Variant image versions created"
  end

  desc "Rescrape images"
  task rescrape: :environment do
    variants = Variant.all.order(id: :asc)
    variants.each_with_index do |v, i|
      puts "[#{i}]: rescraping image for #{v.id}"

      url = v.merchant_url
      data = url.present? ? ScrapeApiService.build.call(url).try(:data) : nil

      if data.present? && data.image.present?
        puts "saving image"
        v.remote_product_image_url = data.image
        v.save!
      end
    end
  end
end
