namespace :basket do

  desc "Default all existing Liked Products baskets to private"
  task privatize: :environment do
    Basket.where( name: "Liked Products" ).each do |b|
      b.privacy = "private"
      b.save!
    end
  end

  desc "Change default to all baskets to public"
  task publicize: :environment do
    Basket.where.not( name: "Liked Products" ).each do |b|
      b.privacy = "public"
      b.save!
    end
  end

  desc "Remove all whitespaces ant end of basket names"
  task clean_names: :environment do
    Basket.where.not( name: "Liked Products" ).each do |b|
      b.name = b.name.strip
      b.save!
    end  
  end

end
