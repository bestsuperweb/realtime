# ensure you have .env.staging and .env.production with database env variables in your app root
# works with RAILS_ENV=prouction && RAILS_ENV=staging

namespace :db do

  # $ RAILS_ENV=production bundle exec rake db:backup
  desc "Backup database to local"
  task backup: :environment do
    sh "pg_dump -h #{ENV['DATABASE_HOST']} -U #{ENV['DATABASE_USER']} -d #{ENV['DATABASE_NAME']} -F c -b -v -f #{ENV['RAKE_ENV']}.dump"
    #----------> enter RDS password
  end

  # $ RAILS_ENV=production bundle exec rake db:restore
  desc "backup development and restore production or staging database to development db"
  task restore: :environment do
    sh "pg_restore -h localhost --clean --no-acl --no-owner -d giftibly_development -v #{ENV['RAKE_ENV']}.dump"
    # rm #{ENV['RAKE_ENV']}.dump
  end

end