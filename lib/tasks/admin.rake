namespace :db do
  desc "add admin"
  task add_admin: :environment do    
    make_admin
  end

  desc "populate admin"
  task populate_admin: :environment do
    populate
  end
end


def make_admin
  admin = Admin.where(email: ENV["ADMIN_EMAIL"]).first_or_initialize
  admin.password = ENV["ADMIN_PASSWORD"]
  admin.mobile_number = ENV["ADMIN_MOBILE_NUMBER"]
  admin.save
end

def populate
  hex = Admin.where( email: ENV["HEX_EMAIL"] ).first_or_initialize
  hex.password = ENV["HEX_PASSWORD"]
  hex.name = "John Carter"
  hex.save

  alfred = Admin.where( email: ENV["ALFRED_EMAIL"] ).first_or_initialize
  alfred.password = ENV["ALFRED_PASSWORD"]
  alfred.name = "Alfred Endeio"
  alfred.save

  daniel = Admin.where( email: ENV["DANIEL_EMAIL"] ).first_or_initialize
  daniel.password = ENV["DANIEL_PASSWORD"]
  daniel.name = "Daniel Travelstead"
  daniel.save

  hilson = Admin.where( email: ENV["HILSON_EMAIL"] ).first_or_initialize
  hilson.password = ENV["HILSON_PASSWORD"]
  hilson.name = "Hilson Francis"
  hilson.save
end
