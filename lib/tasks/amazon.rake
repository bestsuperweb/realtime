require 'httparty'
require 'json'

namespace :amazon do

  desc "Imports 100 test products from amazon"
  task import: :environment do
    request = Vacuum.new

    request.configure(
      aws_access_key_id:     Rails.application.config.amazon_key,
      aws_secret_access_key: Rails.application.config.amazon_secret,
      associate_tag:         Rails.application.config.amazon_tag
    )

    prod_ray = [
      { keyword: "motors",               search_index: "Automotive"    },
      { keyword: "automotive",           search_index: "Automotive"    },
      { keyword: "car",                  search_index: "Automotive"    },
      { keyword: "watercraft",           search_index: "Automotive"    },
      { keyword: "vintage",              search_index: "Automotive"    },
      { keyword: "truck",                search_index: "Automotive"    },
      { keyword: "scooter",              search_index: "Automotive"    },
      { keyword: "motorcycle",           search_index: "Automotive"    },
      { keyword: "rv",                   search_index: "Automotive"    },
      { keyword: "boat",                 search_index: "Automotive"    },
      { keyword: "trailer",              search_index: "Automotive"    },
      { keyword: "aviation",             search_index: "Automotive"    },
      { keyword: "camper",               search_index: "Automotive"    },
      { keyword: "vehicle",              search_index: "Automotive"    },
      { keyword: "atv",                  search_index: "Automotive"    },
      { keyword: "new",                  search_index: "Automotive"    },
      { keyword: "makeup",               search_index: "Beauty"        },
      { keyword: "beauty",               search_index: "Beauty"        },
      { keyword: "skin",                 search_index: "Beauty"        },
      { keyword: "hair",                 search_index: "Beauty"        },
      { keyword: "organic",              search_index: "Beauty"        },
      { keyword: "health",               search_index: "Beauty"        },
      { keyword: "tattoos",              search_index: "Beauty"        },
      { keyword: "spa",                  search_index: "Beauty"        },
      { keyword: "new",                  search_index: "Beauty"        },
      { keyword: "accessories",          search_index: "FashionMen"    },
      { keyword: "accessories",          search_index: "FashionWomen"  },
      { keyword: "accessories",          search_index: "FashionGirls"  },
      { keyword: "accessories",          search_index: "FashionBoys"   },
      { keyword: "clothing",             search_index: "FashionMen"    },
      { keyword: "clothing",             search_index: "FashionWomen"  },
      { keyword: "clothing",             search_index: "FashionGirls"  },
      { keyword: "clothing",             search_index: "FashionBoys"   },
      { keyword: "suit",                 search_index: "FashionMen"    },
      { keyword: "suit",                 search_index: "FashionWomen"  },
      { keyword: "suit",                 search_index: "FashionGirls"  },
      { keyword: "suit",                 search_index: "FashionBoys"   },
      { keyword: "pants",                search_index: "FashionMen"    },
      { keyword: "pants",                search_index: "FashionWomen"  },
      { keyword: "pants",                search_index: "FashionGirls"  },
      { keyword: "pants",                search_index: "FashionBoys"   },
      { keyword: "apparel",              search_index: "FashionMen"    },
      { keyword: "apparel",              search_index: "FashionWomen"  },
      { keyword: "apparel",              search_index: "FashionGirls"  },
      { keyword: "apparel",              search_index: "FashionBoys"   },
      { keyword: "t-shirt",              search_index: "FashionMen"    },
      { keyword: "t-shirt",              search_index: "FashionWomen"  },
      { keyword: "t-shirt",              search_index: "FashionGirls"  },
      { keyword: "t-shirt",              search_index: "FashionBoys"   },
      { keyword: "top",                  search_index: "FashionMen"    },
      { keyword: "top",                  search_index: "FashionWomen"  },
      { keyword: "top",                  search_index: "FashionGirls"  },
      { keyword: "top",                  search_index: "FashionBoys"   },
      { keyword: "jacket",               search_index: "FashionMen"    },
      { keyword: "jacket",               search_index: "FashionWomen"  },
      { keyword: "jacket",               search_index: "FashionGirls"  },
      { keyword: "jacket",               search_index: "FashionBoys"   },
      { keyword: "hoodie",               search_index: "FashionMen"    },
      { keyword: "hoodie",               search_index: "FashionWomen"  },
      { keyword: "hoodie",               search_index: "FashionGirls"  },
      { keyword: "hoodie",               search_index: "FashionBoys"   },
      { keyword: "shorts",               search_index: "FashionMen"    },
      { keyword: "shorts",               search_index: "FashionWomen"  },
      { keyword: "shorts",               search_index: "FashionGirls"  },
      { keyword: "shorts",               search_index: "FashionBoys"   },
      { keyword: "vest",                 search_index: "FashionMen"    },
      { keyword: "vest",                 search_index: "FashionWomen"  },
      { keyword: "vest",                 search_index: "FashionGirls"  },
      { keyword: "vest",                 search_index: "FashionBoys"   },
      { keyword: "sweater",              search_index: "FashionMen"    },
      { keyword: "sweater",              search_index: "FashionWomen"  },
      { keyword: "sweater",              search_index: "FashionGirls"  },
      { keyword: "sweater",              search_index: "FashionBoys"   },
      { keyword: "jeans",                search_index: "FashionMen"    },
      { keyword: "jeans",                search_index: "FashionWomen"  },
      { keyword: "jeans",                search_index: "FashionGirls"  },
      { keyword: "jeans",                search_index: "FashionBoys"   },
      { keyword: "shoes",                search_index: "FashionMen"    },
      { keyword: "shoes",                search_index: "FashionWomen"  },
      { keyword: "shoes",                search_index: "FashionGirls"  },
      { keyword: "shoes",                search_index: "FashionBoys"   },
      { keyword: "watches",              search_index: "FashionMen"    },
      { keyword: "watches",              search_index: "FashionWomen"  },
      { keyword: "watches",              search_index: "FashionGirls"  },
      { keyword: "watches",              search_index: "FashionBoys"   },
      { keyword: "denim",                search_index: "FashionMen"    },
      { keyword: "denim",                search_index: "FashionWomen"  },
      { keyword: "denim",                search_index: "FashionGirls"  },
      { keyword: "denim",                search_index: "FashionBoys"   },
      { keyword: "coat",                 search_index: "FashionMen"    },
      { keyword: "coat",                 search_index: "FashionWomen"  },
      { keyword: "coat",                 search_index: "FashionGirls"  },
      { keyword: "coat",                 search_index: "FashionBoys"   },
      { keyword: "underwear",            search_index: "FashionMen"    },
      { keyword: "underwear",            search_index: "FashionWomen"  },
      { keyword: "underwear",            search_index: "FashionGirls"  },
      { keyword: "underwear",            search_index: "FashionBoys"   },
      { keyword: "swimwear",             search_index: "FashionMen"    },
      { keyword: "swimwear",             search_index: "FashionWomen"  },
      { keyword: "swimwear",             search_index: "FashionGirls"  },
      { keyword: "swimwear",             search_index: "FashionBoys"   },
      { keyword: "electronics",          search_index: "Electronics"   },
      { keyword: "audio",                search_index: "Electronics"   },
      { keyword: "cable",                search_index: "Electronics"   },
      { keyword: "adaptor",              search_index: "Electronics"   },
      { keyword: "video",                search_index: "Electronics"   },
      { keyword: "charger",              search_index: "Electronics"   },
      { keyword: "accessories",          search_index: "Electronics"   },
      { keyword: "head set",             search_index: "Electronics"   },
      { keyword: "batteries",            search_index: "Electronics"   },
      { keyword: "computer",             search_index: "Electronics"   },
      { keyword: "remote",               search_index: "Electronics"   },
      { keyword: "components",           search_index: "Electronics"   },
      { keyword: "speakers",             search_index: "Electronics"   },
      { keyword: "parts",                search_index: "Electronics"   },
      { keyword: "surveillance",         search_index: "Electromics"   },
      { keyword: "software",             search_index: "Electronics"   },
      { keyword: "radar detector",       search_index: "Electronics"   },
      { keyword: "media",                search_index: "Electronics"   },
      { keyword: "decorative",           search_index: "ArtsAndCrafts" },
      { keyword: "decorative",           search_index: "Collectibles"  },
      { keyword: "collectible",          search_index: "ArtsAndCrafts" },
      { keyword: "collectible",          search_index: "Collectibles"  },
      { keyword: "sci-fi",               search_index: "ArtsAndCrafts" },
      { keyword: "sci-fi",               search_index: "Collectibles"  },
      { keyword: "antique",              search_index: "ArtsAndCrafts" },
      { keyword: "antique",              search_index: "Collectibles"  },
      { keyword: "science fiction",      search_index: "ArtsAndCrafts" },
      { keyword: "science fiction",      search_index: "Collectibles"  },
      { keyword: "vintage",              search_index: "ArtsAndCrafts" },
      { keyword: "vintage",              search_index: "Collectibles"  },
      { keyword: "super hero",           search_index: "Collectibles"  },
      { keyword: "super hero",           search_index: "ArtsAndCrafts" },
      { keyword: "asian",                search_index: "ArtsAndCrafts" },
      { keyword: "asian",                search_index: "Collectibles"  },
      { keyword: "fantasy",              search_index: "ArtsAndCrafts" },
      { keyword: "fantasy",              search_index: "Collectibles"  },
      { keyword: "medieval",             search_index: "ArtsAndCrafts" },
      { keyword: "medieval",             search_index: "Collectibles"  },
      { keyword: "golden age",           search_index: "ArtsAndCrafts" },
      { keyword: "golden age",           search_index: "Collectibles"  },
      { keyword: "primative",            search_index: "ArtsAndCrafts" },
      { keyword: "primative",            search_index: "Collectibles"  },
      { keyword: "comic",                search_index: "ArtsAndCrafts" },
      { keyword: "comic",                search_index: "Collectibles"  },
      { keyword: "silver age",           search_index: "ArtsAndCrafts" },
      { keyword: "silver age",           search_index: "Collectibles"  },
      { keyword: "pop",                  search_index: "ArtsAndCrafts" },
      { keyword: "pop",                  search_index: "Collectibles"  },
      { keyword: "mint condition",       search_index: "ArtsAndCrafts" },
      { keyword: "mint condition",       search_index: "Collectibles"  },
      { keyword: "custom",               search_index: "ArtsAndCrafts" },
      { keyword: "custom",               search_index: "Collectibles"  },
      { keyword: "consignment",          search_index: "ArtsAndCrafts" },
      { keyword: "consignment",          search_index: "Collectibles"  },
      { keyword: "baby",                 search_index: "HomeGarden"    },
      { keyword: "bath",                 search_index: "HomeGarden"    },
      { keyword: "bedding",              search_index: "HomeGarden"    },
      { keyword: "kitchen",              search_index: "HomeGarden"    },
      { keyword: "dining",               search_index: "HomeGarden"    },
      { keyword: "crafts",               search_index: "HomeGarden"    },
      { keyword: "furniture",            search_index: "HomeGarden"    },
      { keyword: "home decor",           search_index: "HomeGarden"    },
      { keyword: "housekeeping",         search_index: "HomeGarden"    },
      { keyword: "major appliance",      search_index: "HomeGarden"    },
      { keyword: "yard",                 search_index: "HomeGarden"    },
      { keyword: "garden",               search_index: "HomeGarden"    },
      { keyword: "tools",                search_index: "HomeGarden"    },
      { keyword: "boxing",               search_index: "SportingGoods" },
      { keyword: "cardio",               search_index: "SportingGoods" },
      { keyword: "hunting",              search_index: "SportingGoods" },
      { keyword: "swimming",             search_index: "SportingGoods" },
      { keyword: "mma",                  search_index: "SportingGoods" },
      { keyword: "fitness",              search_index: "SportingGoods" },
      { keyword: "fishing",              search_index: "SportingGoods" },
      { keyword: "martial arts",         search_index: "SportingGoods" },
      { keyword: "running",              search_index: "SportingGoods" },
      { keyword: "hockey",               search_index: "SportingGoods" },
      { keyword: "combat",               search_index: "SportingGoods" },
      { keyword: "vintage",              search_index: "SportingGoods" },
      { keyword: "cricket",              search_index: "SportingGoods" },
      { keyword: "protective",           search_index: "SportingGoods" },
      { keyword: "gym",                  search_index: "SportingGoods" },
      { keyword: "rugby",                search_index: "SportingGoods" },
      { keyword: "training",             search_index: "SportingGoods" },
      { keyword: "tennis",               search_index: "SportingGoods" },
      { keyword: "fencing",              search_index: "SportingGoods" },
      { keyword: "cycling",              search_index: "SportingGoods" },
      { keyword: "outdoor",              search_index: "SportingGoods" },
      { keyword: "indoor",               search_index: "SportingGoods" },
      { keyword: "novelty",              search_index: "SportingGoods" },
      { keyword: "bicycle",              search_index: "SportingGoods" },
      { keyword: "hiking",               search_index: "SportingGoods" },
      { keyword: "bowling",              search_index: "SportingGoods" },
      { keyword: "baseball",             search_index: "SportingGoods" },
      { keyword: "ping pong",            search_index: "SportingGoods" },
      { keyword: "tactical",             search_index: "SportingGoods" },
      { keyword: "gymnastic",            search_index: "SportingGoods" },
      { keyword: "lacrosse",             search_index: "SportingGoods" },
      { keyword: "football",             search_index: "SportingGoods" },
      { keyword: "archery",              search_index: "SportingGoods" },
      { keyword: "kayaking",             search_index: "SportingGoods" },
      { keyword: "basketball",           search_index: "SportingGoods" },
      { keyword: "yoga",                 search_index: "SportingGoods" },
      { keyword: "golf",                 search_index: "SportingGoods" },
      { keyword: "leggings",             search_index: "FashionMen"    },
      { keyword: "leggings",             search_index: "FashionWomen"  },
      { keyword: "leggings",             search_index: "FashionBoys"   },
      { keyword: "leggings",             search_index: "FashionGirls"  },
      { keyword: "sunglasses",           search_index: "FashionMen"    },
      { keyword: "sunglasses",           search_index: "FashionWomen"  },
      { keyword: "sunglasses",           search_index: "FashionBoys"   },
      { keyword: "sunglasses",           search_index: "FashionGirls"  },
      { keyword: "phone cases",          search_index: "Wireless"      },
      { keyword: "gluten free",          search_index: "Grocery"       },
      { keyword: "led",                  search_index: "Electronics"   },
      { keyword: "coconut oil",          search_index: "Grocery"       },
      { keyword: "coconut oil",          search_index: "Health"        },
      { keyword: "3d printer",           search_index: "Electronics"   },
      { keyword: "phone power bank",     search_index: "Electronics"   },
      { keyword: "drone",                search_index: "Electronics"   },
      { keyword: "electronic cigarette", search_index: "Electronics"   },
      { keyword: "coffee",               search_index: "Grocery"       },
      { keyword: "virtual reality",      search_index: "Electronics"   },
      { keyword: "bluetooth",            search_index: "Electroncis"   },
      { keyword: "charcoal",             search_index: "Health"        },
      { keyword: "fitness",              search_index: "SportingGoods" },
      { keyword: "matcha",               search_index: "Health"        },
      { keyword: "fidget spinner",       search_index: "Toys"          }
    ]

    merchant = Merchant.find_by_name("Amazon")

    merchant.variants.destroy_all

    error_count = 0

    prod_ray.shuffle.each do |product|

      p "Attempting #{product[:keyword]} - #{product[:search_index]}"

      response = request.item_search( 
        query: {
          'Keywords'=> product[:keyword],
          'SearchIndex'=> product[:search_index],
          'ResponseGroup' => "ItemAttributes,Images"
        }
      )

      json_hash = response.to_h

      if json_hash['ItemSearchResponse']['Items']['Item'].try(:any?)
        json_hash['ItemSearchResponse']['Items']['Item'].each do |data|
          title = CGI::unescapeHTML( data['ItemAttributes']['Title'] )
          price = data['ItemAttributes']['ListPrice']

          if title.present? && price.present?

            product = Product.find_or_create_by( name: title )

            variant = Variant.new

            variant.name = title
            variant.price = price['Amount']
            variant.merchant_url = data['DetailPageURL']

            #important!
            # variant.api_id = data.dig("Offers", "Offer", "OfferListing","OfferListingId")
            variant.api_id = data.dig("ASIN")
            variant.shipping_dimensions = data.dig("ItemAttributes", "PackageDimensions")
            variant.shipping_category = data.dig("ItemAttributes", "ProductGroup")
            variant.metadata = data
            ####

            variant.merchant = merchant
            variant.product = product

            if data['LargeImage'].present? && data['LargeImage']['URL'].present?
              variant.remote_product_image_url = data['LargeImage']['URL']

              if variant.valid?
                product.save!
                variant.save!

                p "Created variant # #{variant.id} - #{variant.name}"
              else
                p "Error occurred and #{title} could not be saved"
              end
            else
              p "Error occurred - product had no image"
            end

          else
            error_count += 1
            p "Error occurred - product without a title or price"
          end
        end
      end
    end

    p "There were #{error_count} incidents of errors" if error_count > 0

  end

  desc "Prints all variants in Amazon that are missing api_id as well as any customers that may have gifts of those variants"
  task log_errors: :environment do
    m = Merchant.find_by_name( "Amazon")

    puts "Problem variants:"
    variants = m.variants.where( api_id: nil )
    customers = []
    variants.each do |v|
      v.gifts.each do |g|
        customers << g.customer
      end
      puts "www.giftibly.com/products/#{v.slug}"
    end

    puts "======================================"
    puts "Problem Customers"
    customers.uniq.each do |c|
      puts "www.giftibly.com/#{c.username}"
    end
  end

end
