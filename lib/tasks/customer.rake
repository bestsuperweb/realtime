namespace :customer do

	desc "Clean Single Customer Requests"
  task :clean_customer_request, [:user] => [:environment] do |task, args|
		customer = Customer.find(args[:user].to_i)
		customer.connection_requests.each do |connection|  
			if customer.connected_to?(connection)
				customer.disconnect(connection)
				customer.reject_connection(connection)
				connection.request_connection(customer, true)
				p 'clean customer friend request'
 			end  
		end
	end  

  desc "Clean Customers Requests"
  task clean_all_customers_requests: :environment do
  	Customer.find_each do |customer|
  		customer.connection_requests.each do |connection|  
  			if customer.connected_to?(connection)
  				customer.disconnect(connection)
  				customer.reject_connection(connection)
  				connection.request_connection(customer, true)
  				p 'clean customer friend request'
   			end  
  		end  
  	end  
  end

  desc "Clean Customers Facebook Credentials"
  task clean_credentials: :environment do
    Customer.find_each do |customer|
      if customer.oauth_credentials.present?
        customer.oauth_credentials.where(provider: 'facebook').first.update!(provider: 'Facebook') if customer.oauth_credentials.where(provider: 'facebook' ).present?
        customer.oauth_credentials.where(provider: ['google_oauth2', 'google+']).first.update!(provider: 'Google+') if customer.oauth_credentials.where( provider: ['google_oauth2', 'google+'] ).present?
        customer.oauth_credentials.where(provider: 'twitter').first.update!(provider: 'Twitter') if customer.oauth_credentials.where( provider: 'twitter' ).present?
        customer.oauth_credentials.where(provider: 'linkedin').first.update!(provider: 'Linkedin') if customer.oauth_credentials.where( provider: 'linkedin' ).present?
      end
    end
  end

  desc "Destroy Facebook Credentials"
  task destroy_fb_credentials: :environment do
    Customer.find_each do |customer|
      if customer.oauth_credentials.present?
        customer.oauth_credentials.where(provider: 'Facebook').first.destroy if customer.oauth_credentials.where(provider: 'Facebook').first.present?
      end
    end
  end

end