require 'httparty'
require 'rest_client'
require 'json'

namespace :ebay do

  #desc "Updates all Etsy products"
  #task :update => :environment do
  #  merchant = Merchant.find_by_name( "Etsy" )

  #  # Collect all products connected to the merchant
  #  variants = Variant.where( merchant_id: merchant.id )

  #  # Iterate over each product and update each one
  #  variants.each do |variant|
  #    url = Rails.application.config.etsy_api_url + "listings/#{variant.api_id}?api_key=" + Rails.application.config.etsy_key

  #    response = HTTParty.get( url, format: :plain )
  #    json_hash = JSON.parse( response, symbolizes_names: true )

  #    if( json_hash['count'] < 1 || json_hash['results'].blank? )

  #    else
  #      data = json_hash['results'][0]
  #      # If the product is no longer active, remove it from
  #      if( data['state'] != 'active' )
  #        # Remove all gifts that have this variant
  #        gifts = Gift.where( variant_id: variant.id )

  #        gifts.each do |gift|
  #          gift.destroy
  #          # TODO: Inform Customer the gift is no longer available
  #        end
          
  #        variant.make_inactive

  #        p "Made variant # #{variant.id} - #{variant.name} Inactive"
  #      else
  #        variant.name = data['title']
  #        variant.price = (data['price'].to_f * 100).to_i
  #        variant.description = data['description']
  #        variant.merchant_url = data['url']

  #        variant.save

  #        p "Updated variant # #{variant.id} - #{variant.name}"
  #      end
  #    end
  #  end
  #end

  desc "Imports 25 test products from etsy"
  task :import => :environment do
    url = Rails.application.config.ebay_api_url

    merchant = Merchant.find_by_name( "Ebay" )

    # Populate an array of preselected test eBay items
    ebay_items_ray = %w( 221569463259 )

    # Iterate over response
    ebay_items_ray.each do |item_id|
      xml = 'X-EBAY-API-COMPATIBILITY-LEVEL: 613
            X-EBAY-API-DEV-NAME:' + Rails.application.config.ebay_dev_id + '
            X-EBAY-API-APP-NAMEY' + Rails.application.config.ebay_app_id + '
            X-EBAY-API-CERT-NAME:' + Rails.application.config.ebay_cert_id + '
            X-EBAY-API-CALL-NAME:GetItem
            X-EBAY-API-SITEID:0
            Content-Type:text/xml
            Request Payload:
            <?xml version="1.0" encoding="utf-8"?>
            <GetItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
              <IncludeItemSpecifics> true </IncludeItemSpecifics>
              <ItemID> ' + item_id + ' </ItemID>
              <ErrorLanguage> en_US </ErrorLanguage>
            </GetItemRequest>'

      response = RestClient.post( url, xml, content_type: "text/xml" )

      p response.body

    #  data = json_hash['results'][i]
    #  if( data.blank? )
    #    #  * If an error, store it, but keep going
    #    error_count+= 1
    #  elsif( data['state'] != "active" )
    #    # Catalog error for inactive product
    #  else
    #    #  * Populate Product
    #    product = Product.find_or_create_by( name: data['title'] )

    #    variant = Variant.find_or_create_by( api_id: data['listing_id'] )
    #    variant.name = data['title']
    #    variant.price = (data['price'].to_f * 100).to_i
    #    variant.description = data['description']
    #    variant.merchant_url = data['url']

    #    variant.merchant = merchant
    #    variant.product = product
        
    #    variant.save!
    #    product.save!

        #  * Report Product populated
    #    p "Created variant # #{variant.id} - #{variant.name}"
    #  end 

    end

    # Report any errors or if all 25 products were populate
    #p "There were #{error_count} incidents of errors" if error_count > 0

  end

end
