namespace :notification do

  desc "Generate present notification settings"
  task generate_presents: :environment do

    setting = Setting.create(
        name:          "purchase_gift",
        description:   "Someone buys a gift for you",
        options:       [ "true", "false" ],
        family:        "notification",
        default_value: "true"
    )

    Customer.find_each do |c|
      CustomersSetting.create(
        customer_id: c.id,
        setting_id: setting.id,
        selected_option: setting.default_value
      )
    end

  end

  desc "Generate a lot of notification settings"
  task post_alpha: :environment do
    s             = Setting.find_by( name: "share_profile_basket" )
    s.name        = "share_profile"
    s.description = "Someone shares your profile"
    s.group       = "Sharing"
    s.save!

    notification_ray = [ 
                       [ "basket_comment", "Someone comments on a basket of yours", "Comments and Messages" ],
                       [ "like_gift", "Someone likes a gift of yours", "Likes" ],
                       [ "like_basket", "Someone likes a basket of yours", "Likes" ],
                       [ "like_comment", "Someone likes a comment of yours", "Likes" ],
                       [ "accept_connection_request", "Someone accepts your connection request", "Connections" ],
                       [ "event_invite", "Someone invites you to an event", "Events" ],
                       [ "event_attendee_status_change", "Someone changes their status of attending your events", "Events" ],
                       [ "share_gift", "Someone shares one of your gifts", "Sharing" ],
                       [ "share_basket", "Someone shares one of your baskets", "Sharing" ]
                     ]

    new_settings = []

    notification_ray.each do |notification|
      new_settings << Setting.create(
        name:          notification[0],
        description:   notification[1],
        options:       [ "true", "false" ],
        family:        "notification",
        group:         notification[2],
        default_value: "true"
      )
    end

    Customer.find_each do |c|
      new_settings.each do |s|
        CustomersSetting.create(
          customer_id: c.id,
          setting_id: s.id,
          selected_option: s.default_value
        )
      end
    end
  end

  desc "Clean up"
  task clean_up: :environment do
    Notification.find_each do |notification|
      activity = notification.activity

      activity.destroy if activity.actable.nil?
    end
  end

  desc "Assign groups"
  task assign_groups: :environment do
    s1 = Setting.find_by( name: "new_connection_request" )
    s1.group = "Connections"
    s1.save

    s2 = Setting.find_by( name: "share_profile" )
    s2.group = "Sharing"
    s2.save
 
    s3 = Setting.find_by( name: "receive_message" )
    s3.group = "Comments and Messages"
    s3.save

    s4 = Setting.find_by( name: "gift_comment" )
    s4.group = "Comments and Messages"
    s4.save

    s5 = Setting.find_by( name: "basket_comment" )
    s5.group = "Comments and Messages"
    s5.save

    s6 = Setting.find_by( name: "public_events" )
    s6.group = "Events"
    s6.save

    s7 = Setting.find_by( name: "purchase_gift" )
    s7.group = "Gifts for Me"
    s7.save

    s8 = Setting.find_by( name: "like_gift" )
    s8.group = "Likes"
    s8.save

    s9 = Setting.find_by( name: "like_basket" )
    s9.group = "Likes"
    s9.save

    s10 = Setting.find_by( name: "like_comment" )
    s10.group = "Likes"
    s10.save

    s11 = Setting.find_by( name: "accept_connection_request" )
    s11.group = "Connections"
    s11.save

    s12 = Setting.find_by( name: "event_invite" )
    s12.group = "Events"
    s12.save

    s13 = Setting.find_by( name: "event_attendee_status_change" )
    s13.group = "Events"
    s13.save

    s14 = Setting.find_by( name: "share_gift" )
    s14.group = "Sharing"
    s14.save

    s15 = Setting.find_by( name: "share_basket" )
    s15.group = "Sharing"
    s15.save
  end

end
