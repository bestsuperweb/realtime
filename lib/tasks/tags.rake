
namespace :tags do

  desc "Generate permalinks for tags"
  task generate_permalinks: :environment do
    ActsAsTaggableOn::Tag.all.each do |t|
      t.permalink = t.name.parameterize
      t.save
    end
  end

end
