namespace :test_data do

  desc "Creates test data"
  task generate: :environment do
    clean_data
    p "Data cleaned"

    # Why are we seeding the test db????
    unless Rails.env.test?
      if Variant.all.empty?
        p "Run rake db:seed first"
      else
        generate_baskets
        generate_ga_reports
        p "Test data generated!" 
      end
    end
  end

  task generate_tokens: :environment do
    Customer.all.each do |c|
      c.auth_token = SecureRandom.hex
      c.save!
    end
  end

  task generate_reports: :environment do
    generate_ga_reports
  end

  def clean_data
    Event.destroy_all
    Gift.destroy_all
    Basket.destroy_all
  end

  def generate_baskets 

    janedoe = Customer.create(
      first_name: "Jane",
      last_name: "Doe",
      location: "Orlando, FL, United States",
      email: "jane@doe.com",
      password: "passw0rd",
      username: "janedoe",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do elusmod tempor incididunt ut labore et dolore magna aliqua",
      birthday: Date.new(1985, 07, 04) 
    )

    f = File.open("public/images/test_data/jane_avatar.png")

    janedoe.avatar = f
    janedoe.save

    f = File.open("public/images/test_data/jane_banner.jpg")

    janedoe.banner = f
    janedoe.save

    johndoe = Customer.create(
      first_name: "John",
      last_name: "Doe",
      location: "San Francisco, CA, United States",
      email: "john@doe.com",
      password: "passw0rd",
      username: "johndoe",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do elusmod tempor incididunt ut labore et dolore magna aliqua",
      birthday: Date.new(1986, 06, 28) 
    )

    f = File.open("public/images/test_data/john_avatar.png")

    johndoe.avatar = f
    johndoe.save

    f = File.open("public/images/test_data/john_banner.jpg")

    johndoe.banner = f
    johndoe.save

    tomsmith = Customer.create(
      first_name: "Tom",
      last_name: "Haverford",
      location: "Pawnee, IN, United States",
      email: "tom@haverford.com",
      password: "passw0rd",
      username: "tomhaverford",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do elusmod tempor incididunt ut labore et dolore magna aliqua",
      birthday: Date.new(1986, 07, 15)
    )

    f = File.open("public/images/test_data/tom_avatar.png")

    tomsmith.avatar = f
    tomsmith.save

    f = File.open("public/images/test_data/tom_banner.jpeg")

    tomsmith.banner = f
    tomsmith.save

    ricksmith = Customer.create(
      first_name: "Rick",
      last_name: "Grimes",
      location: "Atlanta, GA, United States",
      email: "rick@grimes.com",
      password: "passw0rd",
      username: "rickgrimes",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do elusmod tempor incididunt ut labore et dolore magna aliqua",
      birthday: Date.new(1991, 06, 14)
    )

    f = File.open("public/images/test_data/rick_avatar.jpg")

    ricksmith.avatar = f
    ricksmith.save

    f = File.open("public/images/test_data/rick_banner.png")

    ricksmith.banner = f
    ricksmith.save

    harrysmith = Customer.create(
      first_name: "Harry",
      last_name: "Potter",
      location: "Hogwarts, FL, United States",
      email: "harry@potter.com",
      password: "passw0rd",
      username: "harrypotter",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do elusmod tempor incididunt ut labore et dolore magna aliqua",
      birthday: Date.new(1991, 05, 20)
    )

    f = File.open("public/images/test_data/harry_avatar.jpg")

    harrysmith.avatar = f
    harrysmith.save

    f = File.open("public/images/test_data/harry_banner.jpg")

    harrysmith.banner = f
    harrysmith.save

    p "Generated test users!"

    janedoe.request_connection( tomsmith )
    tomsmith.accept_connection( janedoe )

    janedoe.request_connection( johndoe)
    johndoe.accept_connection( janedoe )

    janedoe.follow( ricksmith )
    johndoe.follow( harrysmith )
    harrysmith.follow( tomsmith )
    harrysmith.follow( johndoe )
    tomsmith.follow( harrysmith )
    ricksmith.follow( janedoe )

    p "Generated test connections!"

    basket1 = Basket.create(
      customer: janedoe,
      name: "Shoes!",
      description: "OMG do I love me some shoes!"
    )

    basket2 = Basket.create(
      customer: janedoe,
      name: "Birthday Basket",
      basket_type: "event",
      description: "Stuff for my birthday"
    )

    basket3 = Basket.create(
      customer: janedoe,
      name: "Shoe Party Basket",
      basket_type: "event",
      description: "Y'all know what to do"
    )

    basket4 = Basket.create(
      customer: johndoe,
      name: "Random junk",
      description: "Stuff I want! Get it for me!"
    )

    p "Generate test baskets!"

    random_variants = Variant.all.sample(10)

    gift1 = Gift.create(
      variant: random_variants[0],
      basket: basket1,
      quantity_desired: 2,
      note: "Definitely need this"
    )

    gift2 = Gift.create(
      variant: random_variants[1],
      basket: basket1,
      quantity_desired: 3 
    )

    gift3 = Gift.create(
      variant: random_variants[2],
      basket: basket1,
      quantity_desired: 5
    )

    gift4 = Gift.create(
      variant: random_variants[3],
      basket: basket1,
      quantity_desired: 10
    )

    gift5 = Gift.create(
      variant: random_variants[4],
      basket: basket2,
      quantity_desired: 7
    )

    gift6 = Gift.create(
      variant: random_variants[5],
      basket: basket3,
      quantity_desired: 4 
    )

    gift7 = Gift.create(
      variant: random_variants[6],
      basket: basket3,
      quantity_desired: 6
    )

    gift8 = Gift.create(
      variant: random_variants[7],
      basket: basket4,
      quantity_desired: 8
    )

    gift9 = Gift.create(
      variant: random_variants[8],
      basket: basket4,
      quantity_desired: 1
    )

    gift10 = Gift.create(
      variant: random_variants[9],
      basket: basket4,
      quantity_desired: 9
    )

    p "Populate test baskets!"

    event1 = Event.create(
      name: "Fourth of July Par-tay!",
      customer: janedoe,
      basket: basket2,
      description: "I share a birthday with America so let's blow something up!",
      occurs_at: Date.new(2017, 07, 04),
      state: "public"
    )

    event2 = Event.create(
      name: "Shoe Party",
      customer: janedoe,
      basket: basket3,
      description: "I Need shoes before my big party! Get me some!",
      occurs_at: Date.new(2017, 06, 20),
      state: "public"
    )

    p "Populate events!"

  end

  def generate_ga_reports
    a1 = ApiReport.create(
      error_type: "unknown_merchant",
      customer_id: 2,
      url: "https://coolstuffgames.com"
    )

    a2 = ApiReport.create(
      error_type: "not_product_page",
      customer_id: 2,
      url: "https://www.amazon.com/Mr-Coffee-Espresso-System-Frother/dp/B000U6BSI2/ref=sr_1_1_sspa?ie=UTF8&qid=1511022667&sr=8-1-spons&keywords=mr+coffee+espresso+machine&psc=1"
    )

    a3 = ApiReport.create(
      error_type: "validation",
      customer_id: 2,
      url: "https://www.girlfriend.com/products/dusty-rose-girlfriend-mid-rise-legging"
    )

    a4 = ApiReport.create(
      error_type: "reported_issue",
      customer_id: 2,
      url: "https://www.gamestop.com/ps4/games/crash-bandicoot-n-sane-trilogy/139553",
      note: "Price error"
    )

    p "Populate API Reports!"
  end

end
